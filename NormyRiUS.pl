#!/usr/bin/perl - w
#
# Normalizes the texts from the input folder (_plain.txt - case insensitive)
#
# Usage:
# text_normalization.pl INPUT_TEXTS_DIR
#
# Find 'roll' (����) and uncomment if necessary (like rok-n-roll)
#
# arguments:
# 1. INPUT_TEXTS_DIR - relative or absolute
#
# outputs:
#
# 1. orig_name_normalized.text - normalized text
# 2. orig_name_message.log - log of what's been done
# 3. orig_name_warning.log - log of warnings
#
# Tested on: perl v5.14.2 built for x86_64-linux-thread-multi
#
# Last modified: 09.08.2013 by Sergey Zablotskiy

use strict;				# requirements to variables should be strickt: to avoid the mistakes
use encoding 'cp1251';
use File::Find;
use File::Path qw(mkpath);
use File::Copy;
use Cwd 'abs_path','getcwd';
use feature "switch";
use Time::HiRes 'gettimeofday';

if ($#ARGV+1 ne 1) { print "The number of arguments should be 1!\n"; exit 1;}

my $work_dir=getcwd; #get working directory
my $input_dir = abs_path($ARGV[0]);
# my $output_dir = abs_path($ARGV[1]);

if (! -d $input_dir) {die "Error!!! Input directory does not exist: $input_dir\n"; }
if(-f $input_dir.'/deleted_files.log') { system('rm', '-f', $input_dir.'/deleted_files.log'); }
if(-f $input_dir.'/info.log') { system('rm', '-f', $input_dir.'/info.log'); }
	
# mkdir "$output_dir" unless -d "$output_dir";	

# my $start_time=time(); //In seconds
my $start_time=gettimeofday();

# --- Imporatant settings for a function get_morph_props

my $mystem_original = '/home/zablotskiy/sr/mystem_yandex/mystem';	# Linux 64bit
#my $mystem_original = '/home/kseniya/sr/mystem_yandex/mystem_32';	# Ubuntu 32bit
# my $mystem_path_new_base = '/scratch.local/sergey/mystem_temp_files';
my $mystem_path_new_base = '/home/zablotskiy/todelete/mystem_temp_files';

# *** Important settings for a function get_morph_props

# --- Creating temporarily folder and files

my $flag=0;
my $mystem_path_new;
while($flag==0)
	{
	$mystem_path_new = $mystem_path_new_base.'_'.gettimeofday();
	if(! -d $mystem_path_new) { $flag = 1;}
	}

mkdir $mystem_path_new or die "Error!!! Can not create a folder: ".$mystem_path_new."\n";

# File to save the word for the mystem function (one word)
my $word_temp_file=$mystem_path_new.'/word_for_mystem_function.txt';
# File to read the word from for the mystem function (one word)
my $morph_temp_file=$mystem_path_new.'/morph_for_mystem_function.txt';
# File to read the word from for the mystem function (full file)
my $full_morph_temp_file=$mystem_path_new.'/all_morph_for_mystem_function.txt';
###
# File for saving the words with hyphens
my $full_morph_hyphen_input_file=$mystem_path_new.'/all_morph_hyphen_input.txt';
# File for saving the results of mystem for the words with hyphens
my $full_morph_hyphen_output_file=$mystem_path_new.'/all_morph_hyphen_output.txt';

# Path to mystem
my $mystem_path=$mystem_path_new.'/mystem';	# Linux 64bit
#my $mystem_path=$mystem_path_new.'/mystem_32';	# Ubuntu 32bit
system('cp', $mystem_original, $mystem_path_new);

# *** Creating temporarily folder and files

### Make arrays for numbers
my %digits; my %ten_19; my %tens; my %hundreds; my %thousands; my %millions; my %billions; my %trillions;
$digits{kol}{im}{m}=["����","����","���","���","������","����","�����","����","������","������"];
$digits{kol}{im}{f}=["����","����","���","���","������","����","�����","����","������","������"];
$digits{kol}{rod}{m}=["����","������","����","���","������","����","�����","����","������","������"];
$digits{kol}{rod}{f}=["����","�����","����","���","������","����","�����","����","������","������"];
$digits{kol}{dat}{m}=["����","������","����","���","������","����","�����","����","������","������"];
$digits{kol}{dat}{f}=["����","�����","����","���","������","����","�����","����","������","������"];
$digits{kol}{vin}{m}=["����","����","���","���","������","����","�����","����","������","������"]; # we do not count odushevlennie
$digits{kol}{vin}{f}=["����","����","���","���","������","����","�����","����","������","������"];
$digits{kol}{tv}{m}=["����","�����","�����","�����","��������","�����","������","�����","�������","�������"];
$digits{kol}{tv}{f}=["����","�����","�����","�����","��������","�����","������","�����","�������","�������"];
$digits{kol}{pr}{m}=["����","�����","����","���","������","����","�����","����","������","������"];
$digits{kol}{pr}{f}=["����","�����","����","���","������","����","�����","����","������","������"];

$ten_19{kol}{im}{m}=["������","�����������","����������","����������","������������","����������","�����������","����������","������������","������������"];
$ten_19{kol}{im}{f}=$ten_19{kol}{im}{m}; # Warning! The link is created: if the left part element is changed, the right-hand part is also changed
$ten_19{kol}{rod}{m}=["������","�����������","����������","����������","������������","����������","�����������","����������","������������","������������"];
$ten_19{kol}{rod}{f}=$ten_19{kol}{rod}{m};
$ten_19{kol}{dat}{m}=["������","�����������","����������","����������","������������","����������","�����������","����������","������������","������������"];
$ten_19{kol}{dat}{f}=$ten_19{kol}{dat}{m};
$ten_19{kol}{vin}{m}=$ten_19{kol}{im}{m};
$ten_19{kol}{vin}{f}=$ten_19{kol}{im}{m};
$ten_19{kol}{tv}{m}=["�������","������������","�����������","�����������","�������������","�����������","������������","�����������","�������������","�������������"];
$ten_19{kol}{tv}{f}=$ten_19{kol}{tv}{m};
$ten_19{kol}{pr}{m}=["������","�����������","����������","����������","������������","����������","�����������","����������","������������","������������"];
$ten_19{kol}{pr}{f}=$ten_19{kol}{pr}{m};

$tens{kol}{im}{m}=["","������","��������","��������","�����","���������","����������","���������","�����������","���������"];
$tens{kol}{im}{f}=$tens{kol}{im}{m};
$tens{kol}{rod}{m}=["","������","��������","��������","������","����������","�����������","����������","������������","���������"];
$tens{kol}{rod}{f}=$tens{kol}{rod}{m};
$tens{kol}{dat}{m}=["","������","��������","��������","������","����������","�����������","����������","������������","���������"];
$tens{kol}{dat}{f}=$tens{kol}{dat}{m};
$tens{kol}{vin}{m}=$tens{kol}{im}{m};
$tens{kol}{vin}{f}=$tens{kol}{im}{m};
$tens{kol}{tv}{m}=["","�������","���������","���������","������","������������","�������������","������������","��������������","���������"];
$tens{kol}{tv}{f}=$tens{kol}{tv}{m};
$tens{kol}{pr}{m}=["","������","��������","��������","������","����������","�����������","����������","������������","���������"];
$tens{kol}{pr}{f}=$tens{kol}{pr}{m};

$hundreds{kol}{im}{m}=["","���","������","������","���������","�������","��������","�������","���������","���������"];
$hundreds{kol}{im}{f}=$hundreds{kol}{im}{m};
$hundreds{kol}{rod}{m}=["","���","�������","������","���������","�������","��������","�������","���������","���������"];
$hundreds{kol}{rod}{f}=$hundreds{kol}{rod}{m};
$hundreds{kol}{dat}{m}=["","���","��������","�������","����������","��������","���������","��������","����������","����������"];
$hundreds{kol}{dat}{f}=$hundreds{kol}{dat}{m};
$hundreds{kol}{vin}{m}=$hundreds{kol}{im}{m};
$hundreds{kol}{vin}{f}=$hundreds{kol}{im}{m};
$hundreds{kol}{tv}{m}=["","���","����������","����������","�������������","����������","�����������","����������","������������","������������"];
$hundreds{kol}{tv}{f}=$hundreds{kol}{tv}{m};
$hundreds{kol}{pr}{m}=["","���","��������","�������","����������","��������","���������","��������","����������","����������"];
$hundreds{kol}{pr}{f}=$hundreds{kol}{pr}{m};

# ----- auto filling of the simular data

my @cases_base=("im","rod","dat","vin","tv","pr");
my @genders_base=("m","f","n","pl");
my @kol_por_base=("kol",'por');

for(my $i=0; $i<11; $i++)
	{
	for(my $j=0; $j<6; $j++) # cases_base
		{
		for(my $k=2; $k<4; $k++) # genders_base
			{
			$digits{kol}{$cases_base[$j]}{$genders_base[$k]}[$i]=$digits{kol}{$cases_base[$j]}{m}[$i];
			$digits{kol}{$cases_base[$j]}{$genders_base[$k]}[$i]=$digits{kol}{$cases_base[$j]}{m}[$i];
			$ten_19{kol}{$cases_base[$j]}{$genders_base[$k]}[$i]=$ten_19{kol}{$cases_base[$j]}{m}[$i];
			$tens{kol}{$cases_base[$j]}{$genders_base[$k]}[$i]=$tens{kol}{$cases_base[$j]}{m}[$i];
			$hundreds{kol}{$cases_base[$j]}{$genders_base[$k]}[$i]=$hundreds{kol}{$cases_base[$j]}{m}[$i];
			}
		}
	}

# processing exceptions

$digits{kol}{im}{n}[1]='����'; $digits{kol}{vin}{n}[1]='����';
$digits{kol}{im}{pl}[1]='����'; $digits{kol}{rod}{pl}[1]='�����'; $digits{kol}{dat}{pl}[1]='�����';
$digits{kol}{vin}{pl}[1]='����'; $digits{kol}{tv}{pl}[1]='������'; $digits{kol}{pr}{pl}[1]='�����';

# # # # pattern for odni
# # # my $odni_pattern='';

# # # for(my $j=0; $j<6; $j++) # cases_base
	# # # {
	# # # for(my $k=0; $k<4; $k++) # genders_base
		# # # {
		# # # $odni_pattern .= $digits{kol}{$cases_base[$j]}{$genders_base[$k]}[1].'|';
		# # # }
	# # # }
# # # $odni_pattern =~ s/\|$//;
# # # $odni_pattern =~ qr/$odni_pattern/; # is treated then as a regexp // maybe unnecessary

# ----- Processing adjectives

my %endings_adj_yj=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"},
							"dat" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, # we do not count odushevlennie
							"tv" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "���"}, 	"pr" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"});

my %endings_adj_kij=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"},
							"dat" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, # we do not count odushevlennie
							"tv" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "���"}, 	"pr" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"});
														
my %endings_adj_tij=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "����", "f" => "���", "n" => "����", "pl" => "���"},
							"dat" => { "m" => "����", "f" => "���", "n" => "����", "pl" => "���"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, # we do not count odushevlennie
							"tv" => { "m" => "���", "f" => "���", "n" => "���", "pl" => "����"}, 	"pr" => { "m" => "���", "f" => "���", "n" => "���", "pl" => "���"});

my %endings_adj_nij=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"},
							"dat" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, # we do not count odushevlennie
							"tv" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "���"}, 	"pr" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"});
							
my %endings_adj_shij=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"},
							"dat" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"},
							"tv" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "���"}, 	"pr" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"});
							
my %endings_adj_oj=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"},
							"dat" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"},
							"tv" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "���"}, "pr" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"});
							
my %endings_adj_goj=("im" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"}, "rod" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"},
							"dat" => { "m" => "���", "f" => "��", "n" => "���", "pl" => "��"}, "vin" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"},
							"tv" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "���"}, "pr" => { "m" => "��", "f" => "��", "n" => "��", "pl" => "��"});
							
							
my @digits_base=("�����", "����", "����","����","������", "���", "����", "�����", "�����", "�����");
my @ten_19_base=("�����","����������","���������","���������","�����������","���������","����������","���������","�����������","�����������");
my @tens_base=("","�����","�������","�������","�������","���������","����������","���������","�����������","��������");
my @hundreds_base=("","���","�������","������","���������","�������","��������","�������","���������","���������");
my @thousands_base=("","����������","����������","���������","������������","����������","�����������","����������","������������","������������");

my $celyh_adj_base="���"; my $kv_adj_base="��������"; my $kub_adj_base="��������"; my $novyj_adj_base="���"; my $informacionnyj_adj_base="������������";
my $starshij_adj_base="�����"; my $svjatoj_adj_base="����"; my $severnyj_adj_base="������"; my $juzhnyj_adj_base="���"; my $zapadnyj_adj_base="������"; my $vostochnyj_adj_base="�������"; my $avtonomnyj_adj_base="��������"; my $nazyvaemyj_adj_base="��������"; my $hozjajstvennyj_adj_base="�����������"; my $astronomicheskij_adj_base="�������������"; my $loshadinyj_adj_base="�������"; my $srednij_adj_base="�����"; my $zheleznodorozhnyj_adj_base="�������������"; my $zhidkokristallicheskij_adj_base="������������������"; my $procentnyj_adj_base="��������"; my $dollarovyj_adj_base="��������"; my $kratnyj_adj_base="�����"; my $djujmovyj_adj_base="������"; my $drugoj_adj_base="����";

my $one_thousand_adj_base="������"; my $one_million_adj_base="��������"; my $one_billion_adj_base="���������"; my $one_trillion_adj_base="���������";

# Adjectives with 10 elements						
for(my $i=1; $i<10; $i++)
	{
	for(my $j=0; $j<6; $j++)
		{
		for(my $k=0; $k<4; $k++)
			{
			$digits{por}{$cases_base[$j]}{$genders_base[$k]}[$i]=$digits_base[$i].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
			$ten_19{por}{$cases_base[$j]}{$genders_base[$k]}[$i]=$ten_19_base[$i].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
			$tens{por}{$cases_base[$j]}{$genders_base[$k]}[$i]=$tens_base[$i].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
			$hundreds{por}{$cases_base[$j]}{$genders_base[$k]}[$i]=$hundreds_base[$i].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
			$thousands{por}{$cases_base[$j]}{$genders_base[$k]}[$i]=$thousands_base[$i].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
			}
		}
	}

# Adjectives
my %celyh_adj; my %kv_adj; my %kub_adj; my %novyj_adj; my %starshij_adj; my %svjatoj_adj; my %severnyj_adj; my %juzhnyj_adj; my %zapadnyj_adj; my %vostochnyj_adj; my %avtonomnyj_adj; my %nazyvaemyj_adj; my %hozjajstvennyj_adj; my %informacionnyj_adj;
my %one_thousand_adj; my %one_million_adj; my %one_billion_adj; my %one_trillion_adj; my %astronomicheskij_adj; my %loshadinyj_adj; my %srednij_adj; my %zheleznodorozhnyj_adj; my %zhidkokristallicheskij_adj; my %procentnyj_adj;  my %dollarovyj_adj; my %kratnyj_adj; my %djujmovyj_adj; my %drugoj_adj;

for(my $j=0; $j<6; $j++)
	{
	for(my $k=0; $k<4; $k++)
		{
		$digits{por}{$cases_base[$j]}{$genders_base[$k]}[0]=$digits_base[0].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$digits{por}{$cases_base[$j]}{$genders_base[$k]}[3]=$digits_base[3].$endings_adj_tij{$cases_base[$j]}{$genders_base[$k]};
		$ten_19{por}{$cases_base[$j]}{$genders_base[$k]}[0]=$ten_19_base[0].$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		
		$celyh_adj{$cases_base[$j]}{$genders_base[$k]}=$celyh_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$kv_adj{$cases_base[$j]}{$genders_base[$k]}=$kv_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$kub_adj{$cases_base[$j]}{$genders_base[$k]}=$kub_adj_base.$endings_adj_kij{$cases_base[$j]}{$genders_base[$k]};
		$informacionnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$informacionnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$novyj_adj{$cases_base[$j]}{$genders_base[$k]}=$novyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$starshij_adj{$cases_base[$j]}{$genders_base[$k]}=$starshij_adj_base.$endings_adj_shij{$cases_base[$j]}{$genders_base[$k]};
		$svjatoj_adj{$cases_base[$j]}{$genders_base[$k]}=$svjatoj_adj_base.$endings_adj_oj{$cases_base[$j]}{$genders_base[$k]};
		$severnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$severnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$juzhnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$juzhnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$vostochnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$vostochnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$zapadnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$zapadnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$avtonomnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$avtonomnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$nazyvaemyj_adj{$cases_base[$j]}{$genders_base[$k]}=$nazyvaemyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$hozjajstvennyj_adj{$cases_base[$j]}{$genders_base[$k]}=$hozjajstvennyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$astronomicheskij_adj{$cases_base[$j]}{$genders_base[$k]}=$astronomicheskij_adj_base.$endings_adj_kij{$cases_base[$j]}{$genders_base[$k]};
		$loshadinyj_adj{$cases_base[$j]}{$genders_base[$k]}=$loshadinyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$srednij_adj{$cases_base[$j]}{$genders_base[$k]}=$srednij_adj_base.$endings_adj_nij{$cases_base[$j]}{$genders_base[$k]};
		$zheleznodorozhnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$zheleznodorozhnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$zhidkokristallicheskij_adj{$cases_base[$j]}{$genders_base[$k]}=$zhidkokristallicheskij_adj_base.$endings_adj_kij{$cases_base[$j]}{$genders_base[$k]};
		$procentnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$procentnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$dollarovyj_adj{$cases_base[$j]}{$genders_base[$k]}=$dollarovyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$kratnyj_adj{$cases_base[$j]}{$genders_base[$k]}=$kratnyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$djujmovyj_adj{$cases_base[$j]}{$genders_base[$k]}=$djujmovyj_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$drugoj_adj{$cases_base[$j]}{$genders_base[$k]}=$drugoj_adj_base.$endings_adj_goj{$cases_base[$j]}{$genders_base[$k]};
		
		
		$one_thousand_adj{$cases_base[$j]}{$genders_base[$k]}=$one_thousand_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$one_million_adj{$cases_base[$j]}{$genders_base[$k]}=$one_million_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$one_billion_adj{$cases_base[$j]}{$genders_base[$k]}=$one_billion_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		$one_trillion_adj{$cases_base[$j]}{$genders_base[$k]}=$one_trillion_adj_base.$endings_adj_yj{$cases_base[$j]}{$genders_base[$k]};
		}
	}
	
# print '*** '.$one_thousand_adj{dat}{m}."\n";
# print '*** '.$hundreds{por}{vin}{m}[8]."\n";
	
# ----- processing exceptions

$tens{por}{im}{m}[4]="���������"; $tens{por}{vin}{m}[4]="���������";
$digits{por}{im}{m}[0]="�������"; $digits{por}{vin}{m}[0]="�������";
$digits{por}{im}{m}[2]="������"; $digits{por}{vin}{m}[2]="������";
$digits{por}{im}{m}[3]="������"; $digits{por}{vin}{m}[0]="������";
$digits{por}{im}{m}[6]="������"; $digits{por}{vin}{m}[6]="������";
$digits{por}{im}{m}[7]="�������"; $digits{por}{vin}{m}[7]="�������";
$digits{por}{im}{m}[8]="�������"; $digits{por}{vin}{m}[8]="�������";

# Odushevlennye

my %starshij_adj_od;
my %svjatoj_adj_od;
my %novyj_adj_od;
my %informacionnyj_adj_od;
my %drugoj_adj_od;
my %nazyvaemyj_adj_od;
my %hozjajstvennyj_adj_od;
my %srednij_adj_od;
my %zheleznodorozhnyj_adj_od;

for(my $i=0; $i<6; $i++)
	{
	for(my $j=0; $j<4; $j++)
		{
		$starshij_adj_od{$cases_base[$i]}{$genders_base[$j]}=$starshij_adj{$cases_base[$i]}{$genders_base[$j]};
		$svjatoj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$svjatoj_adj{$cases_base[$i]}{$genders_base[$j]};
		$novyj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$novyj_adj{$cases_base[$i]}{$genders_base[$j]};
		$informacionnyj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$informacionnyj_adj{$cases_base[$i]}{$genders_base[$j]};
		$drugoj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$drugoj_adj{$cases_base[$i]}{$genders_base[$j]};
		$nazyvaemyj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$nazyvaemyj_adj{$cases_base[$i]}{$genders_base[$j]};
		$hozjajstvennyj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$hozjajstvennyj_adj{$cases_base[$i]}{$genders_base[$j]};
		$srednij_adj_od{$cases_base[$i]}{$genders_base[$j]}=$srednij_adj{$cases_base[$i]}{$genders_base[$j]};
		$zheleznodorozhnyj_adj_od{$cases_base[$i]}{$genders_base[$j]}=$zheleznodorozhnyj_adj{$cases_base[$i]}{$genders_base[$j]};
		}
	}
	
$starshij_adj_od{vin}{m} = "��������";
$svjatoj_adj_od{vin}{m} = "�������";
$novyj_adj_od{vin}{m} = "������";
$informacionnyj_adj_od{vin}{m} = "���������������";
$drugoj_adj_od{vin}{m} = "�������";
$nazyvaemyj_adj_od{vin}{m} = "�����������";
$hozjajstvennyj_adj_od{vin}{m} = "��������������";
$srednij_adj_od{vin}{m} = "��������";
$zheleznodorozhnyj_adj_od{vin}{m} = "����������������";

# ***** processing exceptions

# ***** Processing adjectives

my %thousand=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %million=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});
my %billion=("sin" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"},
						 "pl" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"});
my %trillion=("sin" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"},
						 "pl" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"});
						 
my %chasy=("sin" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %minuty=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %sekundy=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %gradusy=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %gektary=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %metry=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %mili=("sin" => { "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %djujmy=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %krony=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});						 
my %dollary=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});					 
my %evro=("sin" => { "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "����", "pr" => "����"});
my %funty=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});						 
my %procenty=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});					 
my %chelovek=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "�������", "rod" => "�������", "dat" => "���������", "vin" => "�������", "tv" => "����������", "pr" => "���������"});					 
my %rubli=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "�����", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});						 
my %kopejki=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %centy=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %tonny=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %centnery=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});
my %grammy=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %vatty=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %volty=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %ampery=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %omy=("sin" => { "im" => "��", "rod" => "���", "dat" => "���", "vin" => "��", "tv" => "����", "pr" => "���"},
						 "pl" => { "im" => "���", "rod" => "��", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"});
my %paskali=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %jekzempljary=("sin" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"},
						 "pl" => { "im" => "����������", "rod" => "�����������", "dat" => "�����������", "vin" => "����������", "tv" => "������������", "pr" => "�����������"});
my %dni=("sin" => { "im" => "����", "rod" => "���", "dat" => "���", "vin" => "����", "tv" => "����", "pr" => "���"},
						 "pl" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"});
my %nedeli=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %mesjacy=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %shtuki=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %gercy=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %ljumeny=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"}); # schetnaya forma pl, rod
my %dzhoulej=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"}); # schetnaya forma pl, rod
my %oboroty=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %edinicy=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});						 
my %bajty=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"}); # schetnaya forma pl, rod
my %bity=("sin" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "���", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"}); # schetnaya forma pl, rod
my %sily=("sin" => { "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "���", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});


					 
my %litry=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %veka=("sin" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %goda=("sin" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %punkty=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %glavy=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %stranicy=("sin" => { "im" => "��������", "rod" => "��������", "dat" => "��������", "vin" => "��������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "�������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});
my %paragrafy=("sin" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"},
						 "pl" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"});
my %statji=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %nomera=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %toma=("sin" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %kategorii=("sin" => { "im" => "���������", "rod" => "���������", "dat" => "���������", "vin" => "���������", "tv" => "����������", "pr" => "���������"},
						 "pl" => { "im" => "���������", "rod" => "���������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"});
my %gruppy=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %illjustracii=("sin" => { "im" => "�����������", "rod" => "�����������", "dat" => "�����������", "vin" => "�����������", "tv" => "������������", "pr" => "�����������"},
						 "pl" => { "im" => "�����������", "rod" => "�����������", "dat" => "������������", "vin" => "�����������", "tv" => "�������������", "pr" => "������������"});
my %risunki=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %shemy=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %tablicy=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %chasti=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %poselki=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %stancii=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "��������"},
						 "pl" => { "im" => "�������", "rod" => "�������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %ulicy=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %oblasti=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %ozera=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "����", "rod" => "���", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %reki=("sin" => { "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "���", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %grazhdane=("sin" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "����������", "tv" => "�����������", "pr" => "����������"},
						 "pl" => { "im" => "��������", "rod" => "�������", "dat" => "���������", "vin" => "�������", "tv" => "����������", "pr" => "���������"});
my %grazhdanki=("sin" => { "im" => "���������", "rod" => "���������", "dat" => "���������", "vin" => "���������", "tv" => "����������", "pr" => "���������"},
						 "pl" => { "im" => "���������", "rod" => "���������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"});
my %akademiki=("sin" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "���������", "tv" => "����������", "pr" => "���������"},
						 "pl" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "����������", "tv" => "�����������", "pr" => "����������"});
my %docenty=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "��������", "tv" => "���������", "pr" => "��������"});
my %professora=("sin" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "����������", "tv" => "�����������", "pr" => "����������"},
						 "pl" => { "im" => "����������", "rod" => "�����������", "dat" => "�����������", "vin" => "�����������", "tv" => "������������", "pr" => "�����������"});
my %tovarishhi=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "��������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "���������", "tv" => "����������", "pr" => "���������"});
my %derevni=("sin" => { "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %goroda=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %ostrova=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %kraja=("sin" => { "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %okruga=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %regiony=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});
my %respubliki=("sin" => { "im" => "����������", "rod" => "����������", "dat" => "����������", "vin" => "����������", "tv" => "�����������", "pr" => "����������"},
						 "pl" => { "im" => "����������", "rod" => "���������", "dat" => "�����������", "vin" => "����������", "tv" => "������������", "pr" => "�����������"});
my %sever=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %jug=("sin" => { "im" => "��", "rod" => "���", "dat" => "���", "vin" => "��", "tv" => "����", "pr" => "���"},
						 "pl" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"});
my %zapad=("sin" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %vostok=("sin" => { "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
						 "pl" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"});						 
my %telefony=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});
my %kabinety=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});	 
my %knigi=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "����", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %koncy=("sin" => { "im" => "�����", "rod" => "�����", "dat" => "�����", "vin" => "�����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});						 
my %doma=("sin" => { "im" => "���", "rod" => "����", "dat" => "����", "vin" => "���", "tv" => "�����", "pr" => "����"},
						 "pl" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"});
my %kvartiry=("sin" => { "im" => "��������", "rod" => "��������", "dat" => "��������", "vin" => "��������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "�������", "dat" => "���������", "vin" => "��������", "tv" => "����������", "pr" => "���������"});
my %jetazhi=("sin" => { "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
						 "pl" => { "im" => "�����", "rod" => "������", "dat" => "������", "vin" => "�����", "tv" => "�������", "pr" => "������"});
my %kinoteatry=("sin" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"},
						 "pl" => { "im" => "����������", "rod" => "�����������", "dat" => "�����������", "vin" => "����������", "tv" => "������������", "pr" => "�����������"});
my %kinofilmy=("sin" => { "im" => "���������", "rod" => "����������", "dat" => "����������", "vin" => "���������", "tv" => "�����������", "pr" => "����������"},
						 "pl" => { "im" => "����������", "rod" => "�����������", "dat" => "�����������", "vin" => "����������", "tv" => "������������", "pr" => "�����������"});
my %dorogi=("sin" => { "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
						 "pl" => { "im" => "������", "rod" => "�����", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"});
my %apostoly=("sin" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "��������", "tv" => "���������", "pr" => "��������"},
						 "pl" => { "im" => "��������", "rod" => "���������", "dat" => "���������", "vin" => "���������", "tv" => "����������", "pr" => "���������"});					 


my @months=(
			{ "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "������", "pr" => "������"},
			{ "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "�������", "pr" => "�������"},
			{ "im" => "����", "rod" => "�����", "dat" => "�����", "vin" => "����", "tv" => "������", "pr" => "�����"},
			{ "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "�������", "pr" => "������"},
			{ "im" => "���", "rod" => "���", "dat" => "���", "vin" => "���", "tv" => "����", "pr" => "���"},
			{ "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "�����", "pr" => "����"},
			{ "im" => "����", "rod" => "����", "dat" => "����", "vin" => "����", "tv" => "�����", "pr" => "����"},
			{ "im" => "������", "rod" => "�������", "dat" => "�������", "vin" => "������", "tv" => "��������", "pr" => "�������"},
			{ "im" => "��������", "rod" => "��������", "dat" => "��������", "vin" => "��������", "tv" => "��������", "pr" => "��������"},
			{ "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "�������", "pr" => "�������"},
			{ "im" => "������", "rod" => "������", "dat" => "������", "vin" => "������", "tv" => "������", "pr" => "������"},
			{ "im" => "�������", "rod" => "�������", "dat" => "�������", "vin" => "�������", "tv" => "�������", "pr" => "�������"}
			);
			
my %shortcuts_rod=(
			"����"=>"�������", "���"=>"����������", "���"=>"������", "���"=>"��������", "���"=>"��������", "����"=>"���������", "����"=>"���������", "�"=>"����", "���"=>"������", "���"=>"�������", "��"=>"������������", "����"=>"�������", "��"=>"���������", "��"=>"�������", "��"=>"����������", "��"=>"���������", "��"=>"����������", "�"=>"�����", "���"=>"����", "�"=>"�����", "�"=>"��������", "��"=>"����������", "�"=>"������", "��"=>"������", "��"=>"�����������", "�"=>"�����", "��"=>"����������", "���"=>"�����", "���"=>"�������", "����"=>"�����", "�"=>"�����", "�"=>"����", "���"=>"��������", "��"=>"�����", "���"=>"���������", "���"=>"���������", "���"=>"���������", "hz"=>"�����", "khz"=>"���������", "mhz"=>"���������", "ghz"=>"���������", "��"=>"������", "lm"=>"������", "��"=>"�������", "��"=>"�������", "��"=>"����������", "����"=>"���������", "�����"=>"����������", "tb"=>"����������", "tbit"=>"���������", "tbyte"=>"����������", "��"=>"���������", "����"=>"��������", "�����"=>"���������", "gb"=>"���������", "gbit"=>"��������", "gbyte"=>"���������", "��"=>"���������", "����"=>"��������", "�����"=>"���������", "mb"=>"���������", "mbit"=>"��������", "mbyte"=>"���������", "��"=>"���������", "����"=>"��������", "�����"=>"���������", "kb"=>"���������", "kbit"=>"��������", "kbyte"=>"���������", "��"=>"���������", "��"=>"����������", "��"=>"������", "���"=>"����������", "���"=>"����������", "��"=>"���", "���"=>"������", "���"=>"������", "��"=>"�����", "���"=>"���������", "���"=>"���������", "���"=>"���������", "w"=>"�����", "kw"=>"���������", "Watt"=>"�����", "�"=>"������", "��"=>"�����������", "��"=>"����������", "��"=>"����������", "A"=>"������", "mA"=>"�����������", "ka"=>"����������", "MA"=>"����������", "��"=>"����������", "��"=>"����������", "��"=>"���", "���"=>"��������", "����"=>"��������", "���"=>"�������", "���"=>"�������", "��"=>"�������", "���"=>"�����������", "���"=>"�����������", "pa"=>"�������", "kpa"=>"�����������", "mpa"=>"�����������", "��"=>"�������", "���"=>"�����������", "���"=>"�����������", "�"=>"������", "��"=>"����������", "v"=>"������", "volt"=>"������", "kv"=>"����������", "���"=>"����������", "��"=>"�����", "���"=>"��������", "��"=>"�����", "��"=>"��������", "��"=>"�����"
			);
my $shortcuts_pattern=join('|', keys %shortcuts_rod);
$shortcuts_pattern =~ qr/$shortcuts_pattern/; # is treated then as a regexp // maybe unnecessary

my %one_and_half=("m" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "��������", "pr" => "��������"},
						 "f" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "��������", "pr" => "��������"},
						 "n" => { "im" => "�������", "rod" => "��������", "dat" => "��������", "vin" => "�������", "tv" => "��������", "pr" => "��������"});


my @months_rod=("������","�������","�����","������","���","����","����","�������","��������","�������","������","�������");
my @months_rod_short=("���\\b\\.?","����?\\b\\.?","����?\\b\\.?","���\\b\\.?","���\\b\\.?","���\\b\\.?","���\\b\\.?","���\\b\\.?","����?\\b\\.?","���\\b\\.?","�����?\\b\\.?","���\\b\\.?");
my @months_base=("�����","������","����","�����","��","���","���","������","�������","������","�����","������");

my $months_rod_pattern=join('|', @months_rod);
$months_rod_pattern =~ qr/$months_rod_pattern/; # is treated then as a regexp // maybe unnecessary

my $months_rod_short_pattern=join('|', @months_rod_short);
$months_rod_short_pattern =~ qr/$months_rod_short_pattern/; # is treated then as a regexp // maybe unnecessary

my $months_base_pattern=join('|', @months_base);
$months_base_pattern =~ qr/$months_base_pattern/; # is treated then as a regexp // maybe unnecessary

# --- Needed for isAbbreviation function below

my @abbreviations_letters = ('����', '���', '���', '���', '����', '��', '���', '����', '��', '���', '����', '���', '��', '��', '���', '��', '��', '����', '����', '��', '���', '��', '��','��', '����', '����', '���', '��', '��', '��', '��', '���', '���', '���', '���', '��', '���', '��', '����', '������', '���', '����', '���', '���', '���', '���', '��', '��', '���', '���', '��', '��', '���', '���', '������', '��', '���', '���', '��', '���', '���', '����', '��', '���', '���', '���', '���', '���', '��', '���', '����', '��', '��', '����', '���', '���', '��', '���', '����', '�����', '��', '��', '���', '��', '��', '���', '���', '���', '��', '�����', '����', '���', '����', '����', '����', '����', '����', '���', '���', '����', '���', '���', '���', '��', '���', '���', '���', '����', '��', '����', '����', '���', '���', '��', '����', '��', '������', '���', '��', '�\/�', '���', '����', '���', '���', '����', '���', '��', '��', '�����', '���', '���', '����', '��', '����', '����', '���', '���', '���', '��', '����', '���', '��', '��', '���', 'no',

'��','���', '����', '��', '��', '��', '���', '��', '����', '��(��)?', '���', '���', '���', '����?', '����', '��', '���', '����?', '[1-4]?���', '���', '[1-2]?���', '��(��)?', '[1-2]���', '[1-3]?���', '����', '���', '���', '����(��)?', '���', '����', '����', '��(��?', '��)?', '���(��)?', '��(��)?', '���', '���', '��(��)?', '����', '��(��)?', '���', '����?', '���', '����', '��', '���', '��', '���', '���', '[1-3]?����?', '���', '������', '�������', '����', '����', '���(��)?', '���(��)?', '����', '���', '[1-2]?���(��?)?', '[1-3]?��', '[1-3]?�����?', '���', '����?', '[1-2]?���(���)?', '���(��)?', '��(��)?', '���', '���', '�����', '���(��)?', '[1-2]?���(���)?', '[1-2]?���(��)?', '[1-2]?���', '���', '���', '�����', '���', '����', '���'
);	# For '.'

my @abbreviations_sinle_letters = ('�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',  '�', '�', '�', '�', '�', 'a', 'm', 'p');

my @abbreviations_signs = ('\$', '\�', '\#', '\�', '\%', '\�');

# *** Needed for isAbbreviation function below

my $abbr_letters_merged=join('|', @abbreviations_letters);
$abbr_letters_merged =~ qr/$abbr_letters_merged/; # is treated then as a regexp // maybe unnecessary

my $abbr_signs_merged=join('|', @abbreviations_signs);

# my @months_rod_short=("���\.?","����?\.?","����?\.?","���\.?","���\.?","���\.?","���\.?","���\.?","����?\.?","���\.?","�����?\.?","���\.?");

# my %digits=(
		# kol => {"im" => {"m","f","n"}, "rod" => {"m","f","n"}, "dat" => {"m","f","n"}, "vin" => {"m","f","n"}, "tv" => {"m","f","n"}, "prd" => {"m","f","n"}},
		# por => {"im" => {"m","f","n"}, "rod" => {"m","f","n"}, "dat" => {"m","f","n"}, "vin" => {"m","f","n"}, "tv" => {"m","f","n"}, "prd" => {"m","f","n"}}
# );
# $digits{kol}{im}{m} = [@digits_kol_im_m];
# print $digits{kol}{im}{f}[0]."\n";

#print $thousand{pl}{im}."\n";

# my $string = 'MCMLXXIX';
# print "����� ������� �����: ".get_arab_num($string)."\n";
# exit;

# my $string='����������';
# my @test_property=get_morph_props($string, ' -i -w');
# print $test_property[0]{pos}.' '.$test_property[0]{case}.' '.$test_property[1]{case}.' '.$test_property[1]{gender}."\n";

# my $string = '�� 5 �., ���';
# if($string =~ m/(��\b\.?|��\b\.?|�\b\.?|�\b\.?)/) { print "Yes! ".$&."\n";} else {print "No!\n";};
# exit;

# my $roman_numeral1="III";
# my $arab_num=get_arab_num($roman_numeral1);
# my $number_s=sprintf("%d",$arab_num);
# print $arab_num." ".$number_s."\n";
# exit;

# my $str="323232323";
# while ($str =~ m/(?=232)/g) { print "Matches\n";} #http://linuxshellaccount.blogspot.com/2008/09/finding-overlapping-matches-using-perls.html
# exit;

# my $str="323232323";
# my $new_str=$str;
# $new_str="5";
# print $str.'-'.$new_str."-\n";
# exit;

# my $str="323232323";
# $str =~ m/(323)/;
# my $new_str=$1;
# print $str.'-'.$new_str."-\n";
# my $new_str_2=$new_str;
# $new_str_2="5";
# print $str.'-'.$new_str.'-'.$new_str_2."-\n";
# exit;

# my $str='�1';
# if ($str =~ m/\�/) { print "Hi!\n";}
# exit;

# my $str='�  - 82 ���� 56 ��� �������� ������';
# if($str =~ m/([�-��\,\.]*)([^�-��a-z\-\�\�]*)([\-\�\�]\s?)?(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}))?((((\s?)(\-|\�|\�)\12)||(\s*(���|�)\s*))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}))?\s+(�[�-��]*\.?\s?)?(���|�|���|���|��|�����������|����)([�-��]*\.?)/ig)
	# {
	# print "Yes! ".$1.'~'.$2.'~'.$3.'~'.$4."\n";
	# }

# my $str='�� 5 ��.';
# if ($str =~ m/([�-��a-z]*)(\W*[\s\(]|^)(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}))?((((\s?)(\-|\�|\�)\11)||(\s*(���|�)\s*))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}))?\s+(?=([��][�-��]*\.?\s?)?(��\.?[�-��]*\s|���\.?[�-��]*\s)?(��|��|��|��|��|�|��������|������|����|��������|���������|���������)([�-��]*\.?))/ig)
	# {
	# print "Yes!\n";
	# }
# exit; 

# my $test_str='kol';
# if($test_str !~ s/kol/por/) { $test_str =~ s/por/kol/ };
# print $test_str."\~\n";
	
# my $test_string="833596123000";
# print "������ ".num2word($test_string,'por','vin','m')." �������� ��������\n";

# my $test_string='';
# my $string='m';
# if($string =~ m/\Q$test_string\E/i)
	# { print "Matches!\n";}
# exit;

# print "\n\n\n";
# $test_string='��������������������������������';
# if($test_string =~ m/^[�-��]+$/) {print "��!";} else {print "���!";}
# print "\n\n\n";
# exit;

# $test_string='125001000';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='121000,015';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='111000,011000';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='101,001';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='225001,021';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='2981000000,1000000001';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='2981000000,1000000001';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

# $test_string='2,001';
# print '!!! -'.realnum2word_po($test_string,'kol','vin','f').'- '."\n";

#my $test_string;
#$test_string='2,5';
#print '!!! -'.realnum2word($test_string,'kol','vin','pl').'- '."\n";
#print '!!! -'.realnum2word($test_string,'kol','vin','m').'- '."\n";
#exit;

# my $string = 'HiHiHi!';
# my $counter=0;
# while($string =~ m/Hi/g)
	# {
	# $string =~ s/Hi/H/;
	# $counter++;
	# }
# print $counter.'~'.$string."\~\n";
# exit;

# my $string = 'HiHiHi!';
# my $counter=0;

# for(my $i=0; $i<3; $i++)
	# {
	# # $string =~ m/Hi/g;
	# $string =~ s/Hi/1/;
	# print pos($string)."\n";
	
	# }

# while($string =~ m/Hi/g)
	# {
	# print pos($string)."\n";
	# print @-[0]."\n";
	# print @+[0]."\n";
	# }
# print $counter.'~'.$string."\~\n";
# exit;

# my $string = 'Hi, my dearling!';
# if($string =~ m/(?-i:Hi), (my dearling)/i) { print "Ja\~$1\n"; } # case sensitive
# if($string =~ m/(?i:hi), (my dearling)/) { print "Ja\~$1\n"; } # case insensitive
# exit;

# # # my $string = '25 n 125 m 278 d 135 m';

# # # # # # # pos($string) = 10;

# while($string =~ m/\d+/g)
	# {
	# my $pos=pos($string);
	# print $-[0]."\n";
	# print $+[0]."\n";
	# print $pos."\n";
	# # $string =~ s/25//;
	# # pos($string)=$pos;
	# # pos($string) = pos($string)+10;
	# }

# exit;

# my $string = '25 285 296 589 965';
# my ($var) = $string =~ m/(\d+)/;
# print $var;
# print $+[0]."\n";

# $string =~ m/.*?5/;
# my $pos = $+[0];

# $string =~ s/(.{$pos})5/${1}7/;
# print $string."\n";
# print $+[0]."\n";


# exit;

# while($string =~ m/\d+/g)
	# {
	# my $pos=pos($string);
	# print $-[0]."\n";
	# print $+[0]."\n";
	# print $pos."\n";
	# # $string =~ s/25//;
	# # pos($string)=$pos;
	# # pos($string) = pos($string)+10;
	# }

# exit;

# my $test_string='Jellyfish';
# my @aux_var=split(/\s+/, $test_string);
# print $#aux_var."\n";
# exit;

# my $test_string1='(35';
# my $test_string2='(';
# my $test_string = '(35789)';
# $test_string =~ s/\Q$test_string1\E/$test_string2/;
# print $test_string."\n";

# my $b=0;
# for(my $i=0; $i<10; $i++)
	# {
	# print $i."\n";
	# if(($i == 9)&&($b == 0))
		# {
		# $i=0;
		# $b=1;
		# }
	# }
# exit;

# my $string = "\n";
# print length($string)."\~\n";
# exit;

### Tests

# my $string = "Hello\n\, Sergey!";
# $string =~ m/S/;
# my $pos_begin=$-[0];
# print $pos_begin."\~\n";
# print length($string)."\~\n";
# exit;

# my $string = "Hello\n\, �New!";
# if($string =~ m/[\x134\x141\x90]/) { print "Yes ".$&."\~\n"; };
# exit;


my $global_counter=0;
my $global_counter_skipped=0;
my @all_morph_lines; # the file, consisting of morphemic information about all words in text

my $file_list = `find $input_dir -name '*plain.txt'`;
my @file_list_array = split("\n", $file_list);
my $number_of_files = $#file_list_array+1;
print "Total number of files = ".$number_of_files."\n";

find(\&process_file,$input_dir); 	#find recursively all files in the directory $input_dir

my $end_time=gettimeofday();
print "The processing time: ".(int(($end_time-$start_time)*1000)/1000)." ���.\n";

# Delete the temporary files from a memory
system('rm', '-Rf', $mystem_path_new);

# Save time information into file info.log
open(write_file, '>encoding(CP1251)', $input_dir.'/info.log') or die "Error!!! Can not open file ".$input_dir.'/info.log';
print write_file "Number of processed files: ".$global_counter.' from '.$number_of_files.' (including '.$global_counter_skipped." of skipped files)\n";
print write_file "The processing time: \n".(int(($end_time-$start_time)*1000)/1000)." ���.\n";
close(write_file);

sub process_file
{
-f 				or return; # if not a file than do nothing
# /\.txt$/i		or return;
/_plain\.txt$/i		or return;

$global_counter++;
print "\nFile N.".$global_counter.": ".$File::Find::dir.'/'.$_."\n\n";

my $txt_file = $_;

open(read_file, '<:encoding(CP1251)', $txt_file) or die "Error!!! Can not open file ".$File::Find::dir.'/'.$txt_file;
my @lines = <read_file>;
close(read_file);

my @warnings;
my @messages;

# ----- Determine the texts in other languages and delete them

my $eng_count=0;
my $cyr_count=0;
my $common_count=0;

my $line_interval;
my $addition;
if($#lines+1<100)
	{
	$line_interval=1;
	$addition=$#lines-50;
	}
else
	{
	$addition=50;
	$line_interval=int($#lines+1-50)/50;
	}

for(my $i=0; $i<50; $i++)
	{
	# my $rand_val = int(rand($#lines-1));
	my $rand_val=$addition+$i*$line_interval;
	
	# if($lines[$rand_val] =~ m/^[\W^\d\_]*$/)
	if($lines[$rand_val] !~ m/\w/)	# The string is empty (without alphanumeric symbols and "\-" sign)
		{
		}
	else
		{
		$common_count++;
		# if($lines[$rand_val] =~ m/[\x80\x81\x83\x8A\x8C-\x90\x9A\x9C-\x9F\xA1-\xA3\xA5\xAA\xAF\xB2-\xB4\xBA\xBC-\xBF]/) with J - cyr
		if($lines[$rand_val] =~ m/[\x80\x81\x83\x8A\x8C\x8D\x8E\x8F\x90\x9A\x9C\x9D\x9E\x9F\xA1\xA2\xA5\xAA\xAF\xB2\xB3\xB4\xBC\xBD\xBE\xBF]/) # without J - cyr (\xA3), does not work with letters and intervals
			{
			$cyr_count++;
			}
		elsif($lines[$rand_val] =~ m/(i[�-��]|[�-��]i|\b(?-i:i)\b)/i)
			{
			$cyr_count++;
			}
			
		if($lines[$rand_val] =~ m/[a-z]/)
			{
			$eng_count++;
			}
		}
	}

if($common_count > 10)
	{
	if($cyr_count/$common_count >= 0.3)
		{
		print "The language is very likely to be Ukrainian or similar one. Skipped.\n";
		open(write_file, '>>encoding(CP1251)', $input_dir.'/deleted_files.log') or die "Error!!! Can not open file ".$input_dir.'/deleted_files.log';
		print write_file "\nThe language is very likely to be Ukrainian or similar one. Skipped.\n".$File::Find::dir.'/'.$txt_file."\t\t\t\t\t\t\t\t\t(File N. ".$global_counter."\) was deleted \.\n";
		close(write_file);
		$global_counter_skipped++;
		return;
		}
	elsif($eng_count/$common_count >= 0.7)
		{
		print "The language is very likely to be foreign. Skipped.\n";
		open(write_file, '>>encoding(CP1251)', $input_dir.'/deleted_files.log') or die "Error!!! Can not open file ".$input_dir.'/deleted_files.log';
		print write_file "\nThe language is very likely to be foreign. Skipped.\n".$File::Find::dir.'/'.$txt_file."\t\t\t\t\t\t\t\t\t(File N. ".$global_counter."\) was deleted \.\n";
		close(write_file);
		$global_counter_skipped++;
		return;
		}
	# else
		# {
		# print $cyr_count.'~'.$eng_count.'~'.$common_count."\~\n";
		# }
	}
	
# ***** Determine the texts in other languages and delete them

# ----- Resolve the words with hyphens and � sign instead of �

# Open the hyphen input file for writing
open(write_file, '>encoding(CP1251)', $full_morph_hyphen_input_file) or die "Error!!! Can not open file ".$full_morph_hyphen_input_file;

my $prev_line;

for(my $i=0; $i<$#lines+1; $i++)
	{
	
		# - Messaging
		$prev_line=$lines[$i];
		# * Messaging
		
	if($lines[$i] =~ m/\&/)
		{
		$lines[$i] =~ s/[\*\/\-\�\�]*(\b[a-z])?\&(gt|lt|quot|amp|oelig|scaron|yuml|circ|tilde|ensp|thinsp|zwnj|zwj|lrm|rlm|ndash|nbsp|mdash|lsquo|rsquo|sbquo|ldquo|rdquo|bdquo|dagger|permil|lsaquo|rsaquo|euro|euml|sth|copy)\;?/ /i;
		}
		
		# - Messaging
		if($lines[$i] ne $prev_line) { $messages[$#messages+1]="\nHTML Special Entities:\nOld: ".$prev_line."New: ".$lines[$i]."\n"; }
		# * Messaging
	
	# strange signs
	$lines[$i] =~ s/[\x00-\x09\x0B-\x0C\x0E-\x1E\x7F]+/ /g; # replace strange page formatting signs like FF (form feed) by spaces
	$lines[$i] =~ s/[\x1F]+//g; # Delete unit separator, like by hyphenation
	$lines[$i] =~ s/[\�]+/\.\.\./g;
	$lines[$i] =~ s/[\-\�\�\�]/\-/g; # including the soft hyphen (not captured in this file before) \xAD
	
	while($lines[$i] =~ m/\b[�-���-ߨ]*\xA3[�-��]*\b/g) # check the words like �J����
		{
		my $word =$&;
		if($word !~ m/[�-��]/i) { next; }
		$word =~ s/\xA3/�/g;
		print write_file $word."\n";
		}
	
	# while($this_line =~ m/
	# \b(
	# (([�-���-ߨ\xA3]{1,})[\-\�\�]\s{0,3}
	# ){2,}									#
	
	
	
	# (\b([�-��]+)\s+)?										# previous word $2
	# ([\%\$])												# sign ($3)
	# \s*[\-\�\�]\s*											#
	# ([�-��]{1,3}\b)											# word part $4
	# (?=\s*([�-��]+)\b)?										# next word $5 
	# /xig)
	
	while($lines[$i] =~ m/\b(([�-���-ߨ\xA3]{1,}[\-\�\�]\s{0,3}){2,})(?=([�-��\xA3]{1,}|[�-ߨ\xA3]{1,})\b)/g) # Multiple dashes
		{
		my $left_word_part = $1;
		my $right_word_part = $3;
		my $pure_word = $left_word_part.$right_word_part;
		
		$pure_word =~ s/\xA3/�/g;	# �������� ������ ������� �� J �� �
		$pure_word =~ s/[\-\�\�\s]//g;
		
		print write_file $pure_word."\n";
		}
	
	while($lines[$i] =~ m/\b([�-���-ߨ\xA3]{2,})[\-\�\�]\s{0,3}(?=([�-��\xA3]{2,}|[�-ߨ\xA3]{2,})\b)/g) # check the words like folian- ty and save them to file without dash and spaces
		{
		my $left_word_part = $1;
		my $right_word_part = $2;
		$left_word_part =~ s/\xA3/�/g;
		$right_word_part =~ s/\xA3/�/g;
		
		if($right_word_part =~ m/^(��)$/) { print write_file $left_word_part."\n"; } # Very important to be first
		if((length($left_word_part)>3)&&(length($right_word_part)>3))
			{
			print write_file $left_word_part."\n".$right_word_part."\n"; # ��������� ��� ����� �����; ���� ��� ��������������, �� �� ��������� ��
			}
		print write_file $left_word_part.$right_word_part."\n";
		}
		
	if(($lines[$i] !~ m/\b���[\-\�\�]\s*$/i)&&($lines[$i] =~ m/\b([�-��\xA3]{2,})[\-\�\�]\s*$/i))
		{
		my $left_word_part = $1;
		$left_word_part =~ s/\xA3/�/g;
		
		for(my $j=1; $j<=10; $j++) # 10 - is the assigned number of lines after hyphen (when the text is incerted between pages)
			{
			if($i+$j>$#lines) { $j =11; next; }
			if($lines[$i+$j] =~ m/^\s*([�-��\xA3]{2,})\b/)
				{
				my $right_word_part = $1;
				$right_word_part =~ s/\xA3/�/g;
				print write_file $left_word_part."\n".$right_word_part."\n".$left_word_part.$right_word_part."\n";	# ��������� ��� ����� �����, ���� ��� ��������������, �� �� ��������� ��
				}
			}
		}
	}
	
close(write_file);

# mystem for the whole auxiliary file
system($mystem_path.' -i -w -n -e cp1251 '.$full_morph_hyphen_input_file.' '.$full_morph_hyphen_output_file);	# Reads data from input file, saves into output file

open(read_file, '<:encoding(CP1251)', $full_morph_hyphen_output_file) or die "Error!!! Can not open file ".$full_morph_hyphen_output_file;
my @morph_hyphen_list = <read_file>; # the list of word from mystem with or without hyphens
close(read_file);

my @del_index; # indexes of array elements to delete

# for(my $i=0; $i<$#lines+1; $i++)
	# {
	
	# print $lines[$i]."\~\n";

	# };
	
	# exit;

for(my $i=0; $i<$#lines+1; $i++)
	{
		
		# - Messaging
		$prev_line=$lines[$i];
		# * Messaging
	
	while($lines[$i] =~ m/\b[�-���-ߨ]*\xA3[�-��]*\b/g) # handle the words like �J���� (only if this word exists in mystem)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $phrase='';
		my $word=$matching;
		$word =~ s/\xA3/�/g;
		
		for(my $k=0; $k<$#morph_hyphen_list+1; $k++)
			{
			if(($morph_hyphen_list[$k] =~ m/^$word\{/i)&&($morph_hyphen_list[$k] !~ m/\?/))
				{
				$phrase = $word;
				$k=$#morph_hyphen_list+1;
				$lines[$i] =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
				pos($lines[$i])=$pos_begin+length($phrase);
				}
			}
		}
		
		# - Messaging
		if($lines[$i] ne $prev_line) { $messages[$#messages+1]="\nJO-word is replaced:\nOld: ".$prev_line."New: ".$lines[$i]."\n"; }
		# * Messaging
	
		# - Messaging
		$prev_line=$lines[$i];
		# * Messaging
	
	while($lines[$i] =~ m/\b(([�-���-ߨ\xA3]{1,}[\-\�\�]\s{0,3}){2,})(?=([�-��\xA3]{1,}|[�-ߨ\xA3]{1,})\b)/g) # ������� �������� ������������� ������
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $phrase='';
		my $left_word_part = $1;
		my $right_word_part = $3;
		my $right_word_part_orig=$right_word_part;
		my $pure_word = $left_word_part.$right_word_part;
		$pure_word =~ s/\xA3/�/g;	# �������� ������ ������� �� J �� �
		$pure_word =~ s/[\-\�\�\s]//g;
		
		for(my $k=0; $k<$#morph_hyphen_list+1; $k++)
			{
			if(($morph_hyphen_list[$k] =~ m/^$pure_word\{/i)&&($morph_hyphen_list[$k] !~ m/\?/))
				{
				$phrase = $pure_word;
				$k=$#morph_hyphen_list+1;
				$lines[$i] =~ s/^(.{$pos_begin})\Q$matching\E$right_word_part_orig/${1}$phrase/;
				pos($lines[$i])=$pos_begin+length($phrase);
				}
			}
		}
		
		# - Messaging
		if($lines[$i] ne $prev_line) { $messages[$#messages+1]="\nMultiple hyphen(s) inside the line deleted:\nOld: ".$prev_line."New: ".$lines[$i]."\n"; }
		# * Messaging
	
		# - Messaging
		$prev_line=$lines[$i];
		# * Messaging
	
	while($lines[$i] =~ m/\b([�-���-ߨ\xA3]{2,})[\-\�\�]\s{0,3}(?=([�-��\xA3]{2,}|[�-ߨ\xA3]{2,})\b)/g) # handle hyphens inside the line
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $phrase='';
		my $left_word_part=$1;
		my $right_word_part=$2;
		my $right_word_part_orig=$right_word_part;
		$left_word_part =~ s/\xA3/�/g;
		$right_word_part =~ s/\xA3/�/g;
		if($left_word_part =~ m/^$right_word_part$/i) { next; }	# ����-����
		
		for(my $k=0; $k<$#morph_hyphen_list+1; $k++)
			{
			if(($right_word_part =~ m/^(��)$/i)&&($morph_hyphen_list[$k] =~ m/^$left_word_part\{/i)&&($morph_hyphen_list[$k] !~ m/\?/))	# ���� ��������� ����� � "-��"
				{
				$k=$#morph_hyphen_list+1;	# Stop the cycle
				}
			#word-word exceptions!
			elsif(
				($left_word_part =~ m/^(�)$/i)&&($right_word_part =~ m/^(�|�)$/i) ||
				($left_word_part =~ m/^(�)$/i)&&($right_word_part =~ m/^(�)[�-�]{0,3}$/i) ||
				($left_word_part =~ m/^(�)$/i)&&($right_word_part =~ m/^(�|�)[�-�]{0,3}$/i) ||
				($left_word_part =~ m/^(��)$/i)&&($right_word_part =~ m/^(�|�)[�-�]{0,3}$/i) ||
				($left_word_part =~ m/^(�)$/i)&&($right_word_part =~ m/^(�)[�-�]{0,3}$/i) ||
				($left_word_part =~ m/^(�)$/i)&&($right_word_part =~ m/^(��)$/i) ||
				($left_word_part =~ m/^(�)$/i)&&($right_word_part =~ m/^(��)$/i) ||
				($left_word_part =~ m/^(���)$/i)&&($right_word_part =~ m/^(�)[�-�]{0,3}$/i) )
				{
				$k=$#morph_hyphen_list+1;
				}
			elsif(($morph_hyphen_list[$k] =~ m/^$left_word_part\{/i)&&($morph_hyphen_list[$k] !~ m/\?/)&&(length($left_word_part)>3)) # ���� ����� ����� ����� - ��������������� �����
				{
				for(my $t=0; $t<$#morph_hyphen_list+1; $t++)
					{
					if(($morph_hyphen_list[$t] =~ m/^$right_word_part\{/i)&&($morph_hyphen_list[$t] !~ m/\?/)&&(length($right_word_part)>3))	# � ������ ����� ����� - ��������������� �����
						{
						$t=$#morph_hyphen_list+1;	# �� ����� � ������� �� ����������
						$k=$#morph_hyphen_list+1;
						}
					}
				}	
			elsif(($morph_hyphen_list[$k] =~ m/^$left_word_part$right_word_part\{/i)&&($morph_hyphen_list[$k] !~ m/\?/))
				{
				$phrase = $left_word_part.$right_word_part;
				$k=$#morph_hyphen_list+1;
				$lines[$i] =~ s/^(.{$pos_begin})\Q$matching\E$right_word_part_orig/${1}$phrase/;
				pos($lines[$i])=$pos_begin+length($phrase);
				}
			}
		}
		
		# - Messaging
		if($lines[$i] ne $prev_line) { $messages[$#messages+1]="\nHyphen(s) inside the line deleted:\nOld: ".$prev_line."New: ".$lines[$i]."\n"; }
		# * Messaging
		
	if(($lines[$i] !~ m/\b���[\-\�\�]\s*$/i)&&($lines[$i] =~ m/\b([�-��\xA3]{2,})[\-\�\�]\s*$/i)) # handle hyphens at the end of the line
		{
		my $left_word_part = $1;
		my $left_word_part_orig=$left_word_part;
		$left_word_part =~ s/\xA3/�/g;
		
		for(my $j=1; $j<=10; $j++) # 10 - is the assigned number of lines after hyphen
			{
			if($i+$j>$#lines) { $j =11; next; }
			
			if($lines[$i+$j] =~ m/^\s*([�-��\xA3]{2,})\b/)
				{
				my $right_word_part = $1;
				my $right_word_part_orig=$right_word_part;
				$right_word_part =~ s/\xA3/�/g;
				
				for(my $k=0; $k<$#morph_hyphen_list+1; $k++)
					{
					if(($morph_hyphen_list[$k] =~ m/^$left_word_part$right_word_part\{/i)&&($morph_hyphen_list[$k] !~ m/\?/))
						{
						# - Messaging
						$messages[$#messages+1]="\nHyphen at the end of the line is deleted:\nOld: ".$lines[$i];
						# * Messaging
						
						$lines[$i] =~ s/[\-\�\�]\s*$//; # including \n and/or \r
						$lines[$i+$j] =~ s/^\s*//;
						$lines[$i] .= $lines[$i+$j];
						$lines[$i] =~ s/$left_word_part_orig$right_word_part_orig/$left_word_part$right_word_part/ig;
						
						for(my $m=0; $m < $j; $m++)
							{
							$del_index[$#del_index+1]=$i+$m;
							# - Messaging
							$messages[$#messages] .= $lines[$i+$m+1];
							# * Messaging
							}
						
						$lines[$i+$j] = $lines[$i]; # Very important! We fliped the lines. Current line is the last, empty lines before. And repeat the same procedure from the last line
						
						# - Messaging
						$messages[$#messages] .= "New: ".$lines[$i]."\n";
						# * Messaging
						
						$i = $i + $j - 1; # repeat the same procedures for the whole combined line
						$j = 11;
						$k = $#morph_hyphen_list+1;
						
						}
					}
				}			
			}
		}

	}

for(my $i=0; $i<$#del_index+1; $i++)
	{
	splice(@lines, $del_index[$i]-$i, 1);
	}
	
# ***** Resolve the words with hyphens or � sign instead of �

# ----- New! Passing the whole file to mystem

# Execute the system function Mystem and return its output
system($mystem_path.' -i -n -e cp1251 '.$File::Find::dir.'/'.$txt_file.' '.$full_morph_temp_file);

# Read morphological analysis from a file
open(read_file, '<:encoding(CP1251)', $full_morph_temp_file) or die "Error!!! Can not open file for reading: ".$full_morph_temp_file;
@all_morph_lines = <read_file>;
close(read_file);

# ***** New! Passing the whole file to mystem

my @new_lines;
my $counter=0;

# remove lines without letters (adding the point sign to the end of previous sentence instead of available punctuation)
for (my $count = 0; $count<$#lines+1; $count++)
	{
	
	### 1. The lines without russian letters are deleted and the previous "non-empty line" is ended with "."
	if ((@lines[$count] !~ m/([�-��]|^\d+\:\d.*\))/i)||(@lines[$count] =~ m/([�-��a-z])\1{9,}/i)) # or if the line consists of 10 same letters
		{
		if ($counter != 0)
			{
			### All the previous punctuation and signs at the end of the line is deleted (including WS, CR, LF etc.)
			$new_lines[$counter-1] =~ s/(\s+|\,+|\.+|\?+|\!+|\:+|\*+|\\+|\/+)+$//; #\s means every Whitespace character CR, LF, White space and so on
			$new_lines[$counter-1] = $new_lines[$counter-1].'..';
			}
		#delete $lines[$count]; # the indexes are not deleted
		}
	else
		{
		### 2. CR and/or LF at the end of a line are replaced by the white space character				
		$lines[$count] =~ s/\r\n/ /;
		$lines[$count] =~ s/(\r|\n)$/ /;
		$new_lines[$counter]=$lines[$count];
		$counter++;
		
		if($#lines-$count<=100)
			{
			if($lines[$count] =~ m/^[^�-��]*(����������|����������|����������|����������|������\s+����������)[^�-��]*$/i) # leave the word itself in the new file
				{
				$warnings[$#warnings+1] = "\nLast ".($#lines-$count)." lines are deleted, starting from line: $lines[$count+1]\n";
				for(my $aux_count=$count+1; $aux_count<$#lines+1; $aux_count++) {$lines[$aux_count]="";};
				$count=$#lines+1;
				# $new_lines[$counter-1]="";
				}
			elsif($lines[$count] =~ m/\b����\s+�����\s+������\s+harry(fan|\s*fantasyst)\b/i)
				{
				$warnings[$#warnings+1] = "\nLast ".($#lines-$count+1)." lines are deleted, starting from line: $lines[$count]\n";
				for(my $aux_count=$count; $aux_count<$#lines+1; $aux_count++) {$lines[$aux_count]="";};
				$count=$#lines+1;
				$new_lines[$counter-1]="";
				}
			}
		}
	
	if(($counter != 1)&&($count != $#lines))
		{
		if(($new_lines[$counter-1] =~ m/\d\s*$/)&&($lines[$count+1] =~ m/[�-��]/i))
			{
			$new_lines[$counter-1] .= ' '.$lines[$count+1];
			$new_lines[$counter-1] =~ s/\r\n/ /;
			$new_lines[$counter-1] =~ s/(\r|\n)$/ /;
			$count++;
			}
			
		if(($new_lines[$counter-1] =~ m/^\s*\d/)&&($new_lines[$counter-2] =~ m/[�-��]\s*$/i))
			{
			$new_lines[$counter-2] .= ' '.$new_lines[$counter-1];
			$new_lines[$counter-1] =~ '';
			$counter--;
			}
		}
	}
	
# ----- Find all signs, like \., \!, \?, \; and rearrange the sentences (excluding abbreviations)

for(my $i=0; $i<$#new_lines+1; $i++)
	{
	# New! Specially for num2book
	$new_lines[$i] =~ s/(\d)\;\ +(\d)/$1\_semicolon\_ $2/g; # separate the words with ; inside
	
	$new_lines[$i] =~ s/(\w);(\w)/$1\; $2/g; # separate the words with ; inside
	
	# $new_lines[$i] =~ s/([\?\!\;]+)(?!\s*[�-��a-z])/$1\n/g; # check it # Warning!!! Do not use \n as substitution if you plan to find #pos_begin in while (strange behaviour)
	$new_lines[$i] =~ s/([\?\!\;]+)(?!\s*[�-��a-z])/$1\~\*NEWLINE\*\~/g; # check it
	
	my $new_lines_num=0;
	my $pos_begin_adapted;
	while($new_lines[$i] =~ m/([\.]+)(?!\s*[�-��a-z])/g)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching = $1;
		
		my $coming_before_s=substr($new_lines[$i], 0, $pos_begin); # from and how much
		my $coming_after_s=substr($new_lines[$i], $pos_end); # from till the end of the string
		
		if(($new_lines[$i] =~ m/^.{$pos_end}\s*$/)&&($matching =~ m/\.\.$/))
			{
			}
		elsif(($new_lines[$i] =~ m/^.{$pos_end}\s*$/)&&($i != $#new_lines)&&($new_lines[$i+1] =~ m/^\s*[�-ߨA-Z\d]/)&&($new_lines[$i+1] !~ m/^\d+\:\d.*\)/))	# Exceptions, like 15:25)
			{
			if($new_lines[$i] =~ m/\b���\.\s*$/) { next; } # For example, izd. \n Glavsevmorputi
			if(($new_lines[$i] =~ m/\b��\.\s*$/)&&($new_lines[$i+1] =~ m/^\s*\d/)) { next; } # For example, izd. \n Glavsevmorputi
			}
		else
			{		
			if($coming_before_s =~ m/(\b|\d)[�-��a-z]$/i) { next; } # if there is only one letter, like "t."
			elsif((isAbbreviation($coming_before_s, 'end_of_line') == 1)&&($matching =~ m/^\.{1}$/)) { next; } # if this is an abbreviation, like kg.., for example
			elsif(($coming_before_s =~ m/\d\s*$/)&&($coming_after_s =~ m/^\s*\d/)) { next; } # if this is a delimiter in a number
			elsif(($coming_before_s =~ m/[a-z]$/i)&&($coming_after_s =~ m/^[a-z]/i)) { next; } # if the point is in e-mail address, like mail_server.com
			elsif($coming_before_s =~ m/\b[����]+$/) { next; } # like SZ. veter.
			}
		
		$new_lines[$i] =~ s/(.{$pos_begin})\Q$matching\E/${1}$matching\~\*NEWLINE\*\~/; # Warning!!! Do not use \n as substitution if you plan to find #pos_begin in while (strange behaviour)
		pos($new_lines[$i])=$pos_begin+length($matching)+length("\~\*NEWLINE\*\~"); # 1 is for the "\n" sign
		}
	}

my $new_lines_number_before=$#new_lines;

my $aux_string=join('', @new_lines);
@new_lines=split('\~\*NEWLINE\*\~',$aux_string); # Warning!!! Do not use \n as substitution if you plan to find #pos_begin in while (strange behavior)

# If the number of lines was decreased dramatically
if($#new_lines < $new_lines_number_before/100)
	{
	print "The File was significantly reduced after rearrangement. Probably, because of the lack of full-stops at all.\n";
	open(write_file, '>>encoding(CP1251)', $input_dir.'/deleted_files.log') or die "Error!!! Can not open file ".$input_dir.'/deleted_files.log';
	print write_file "\nFile was significantly reduced after rearrangement\n".$File::Find::dir.'/'.$txt_file."\t\t\t\t\t\t\t\t\t(File N. ".$global_counter."\) was deleted \.\n";;
	close(write_file);
	$global_counter_skipped++;
	return;
	}
	
my $index=0;

foreach my $line (@new_lines)
	{
	
	# If the number of characters in the line is too large
	if(length($line)>3000)
		{
		$warnings[$#warnings+1]="\nThe Line has more than 3000 characters:\nOld: ".$line."\n";
		$line = "";
		# print "The File has a line with more than 3000 characters. Probably, because of the lack of full-stops at all.\n";
		# open(write_file, '>>encoding(CP1251)', $input_dir.'/deleted_files.log') or die "Error!!! Can not open file ".$input_dir.'/deleted_files.log';
		# print write_file "\nFile was significantly reduced after rearrangement\n".$File::Find::dir.'/'.$txt_file."\t\t\t\t\t\t\t\t\t(File N. ".$global_counter."\) was deleted \.\n";;
		# close(write_file);
		# $global_counter_skipped++;
		# return;
		}
	}

foreach my $line (@new_lines)
	{

		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	# Delete wiki-like comments
	
	$line =~ s/\[\s*(image|edit|�������)\s*\]/ /gi;
	$line =~ s/\[\s*(����\.)\s*(\d+)\s*\]/ /gi;
	$line =~ s/\[\s*��������\s+��\s+������\s+\d+\s+�(���|��|���)\s*\]/ /gi;
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nWiki-like comments deleted:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging

		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	# Delete Internet addresses
	$line =~ s/\b(https?\:|www|ftps?\:)[a-z\.\_\-\/\d\?\=\&\#\,\%\:]+[a-z\/\d\?\&\#\,\%\_]/ /ig;
	
	while($line =~ m/\b[a-z]+(\.[a-z][a-z\-\/\d\?\=\&\#\,\%\:\@]){2,}/ig)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $phrase='';
		my $matching=$&;
		
		if($matching !~ m/\@/)
			{
			$line =~ s/^(.{$pos_begin})\Q$matching\E/${1}/;
			pos($line)=$pos_begin;
			}
		}
		
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nInternet addresses deleted:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
		
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	$line =~ s/($|\W)\~(?=\d)/${1}�������� /g;
	
	# In the power of the number
	if($line =~ m/\d\^\d/)
		{
		while($line =~ m/(\d)\^(\d+)(?![\.\,]\d)(\s*(($shortcuts_pattern)\b|\%|\$|\�)(\.)?)?/ig)
			{
			my $pos_begin=$-[0];
			my $pos_end=$+[0];
			my $phrase='';
			my $matching=$&;
			my $number_s=$1;
			my $power_s=$2;
			my $abbr=$4;
			my $point=$5;
			
			$phrase = $number_s.' � '.num2word($2, 'por', 'pr', 'f').' �������';
			
			if($abbr =~ m/^(($shortcuts_pattern)\b|\%|\$|\�)$/i)
				{
				if($abbr =~ m/^\$$/) { $abbr = '�������'; }
				elsif($abbr =~ m/^\%$/) { $abbr = '��������'; }
				elsif($abbr =~ m/^\�$/) { $abbr = '����'; }
				else
					{ 
						if($shortcuts_rod{$abbr} !~ m/\w/) { $abbr = lc($abbr); }
						$abbr =~ s/^($shortcuts_pattern)$/$shortcuts_rod{$1}/i;
					}
					
				$phrase .= ' '.$abbr;
				
				# Treat the point (.) by the shortcuts properly
				if(($point =~ m/\.$/)&&($line =~ m/^(.{$pos_end})s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }
				}
			$line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
			pos($line)=$pos_begin+length($phrase);
			}
		}
	
	### Find and remove all with #, ~, ^, `,*
	$line =~ s/[\~\^\`\*]/ /g;
	$line =~ s/\={3,}/ /g;
	
	### Special words will be added by '~', and returned back at the end
	$line =~ s/\b���\-�\-����/���~�~����/i;
	$line =~ s/\b���\-�\-���/���~�~���/i;
	$line =~ s/\b���\-�\-�����/���~�~�����/i;
	
	### Replace english's by english~apostrophe
	$line =~ s/\b([a-z]+)[\'\�]s([^\'\w]|$)/$1\~apostrophe$2/ig;
	$line =~ s/([^\']|^)\b([a-z]+s)\'([^\'\w]|$)/$1$2\~apostrophe$3/ig;
	
	$line =~ s/(^|\W)\"([^\d]+?)\"($|\W)/$1 $2 $3/g; # replace "text" by text, but not "1"
	$line =~ s/(^|\W)\'([^\d]+?)\'($|\W)/$1 $2 $3/g;
	# $line =~ s/(?<![\d\'])\s*(\"|\')/ /g; # lookahead does not work properly
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nApostrophes and other signs:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	# --- E-Mails processing
	$line =~ s/\(at\)/\@/g;
	
	while ($line =~ m/\b([a-z\.\-\_]+)\b\@\b([a-z\.\-\_]+)\b\.([a-z]+)\b/ig)
		#each matching (only - not substitution) is treated separately and not only the last one in a string while using while
		{
		my $email=$&;
		my $email_name=$1;
		my $server=$2;
		my $domain=$3;
		
		$email_name =~ s/[\.\-\_]/\~/g;
		$server =~ s/[\.\-\_]+/\~/g;
		
		$line =~ s/\Q$email\E/$email_name\~unknown ������ $server ����� $domain/g;
		};
		
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nE-Mail:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
	
	# *** E-Mails processing
	
	# --- Replace English letters with Russian where necessary
	
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	# replace some stand-alone English letters in words with English-Russian letters
	$line =~ s/
		\b[^\d\W]*					# some letters of one word
		([�-��][a-z]|[a-z][�-��])	# russian-english letters together 
		[^\d\W]*\b					# some letters of this word
		/
		eng_in_rus_fun($&)
		/iexg;
	
	sub eng_in_rus_fun
		{
		my $matching=$_[0];
		my %replace = ( a=>'�', A=>'�', B => '�', c => '�', C => '�', e => '�', E => '�', H => '�', K => '�', M => '�', o => '�', O => '�', p => '�', P => '�', T => '�', x => '�', X => '�');
		my $pattern = join("|", keys %replace);
		$pattern = qr/$pattern/; # is treated then as a regexp
		
		my $phrase=$matching;
		$phrase =~ s/($pattern)/$replace{$1}/g;
		
		# if($phrase !~ m/[^\d\W]*([�-��][a-z]|[a-z][�-��])[^\d\W]*/i) { return $phrase; } else { return $matching; }
		if($phrase !~ m/[a-z]/i) { return $phrase; } else { return $matching; } 
		}
	
	# replace some stand-alone english letters (that could be prepositions or shortcuts)
	while($line =~ m/\b[aABcCKoO]\b/g) # new! Or the other way round, to replace all the letters, since almost all of them could be shortcuts, except x or X.
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		if(($matching =~ m/c/i)&&($line =~ m/^.{$pos_end}\s+([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})+\b/))	# ���� ���������� "�" ���� ����� ������� ������
			{
			}
		else
			{
			if($line =~ m/^.{$pos_begin}[ap]\.?\s?m/i) { next; }
			if($line =~ m/^.{$pos_end}\s+[a-z]{2,}/i) { next; }		# ������ ����� ���������� ����� ��������� � ���������� ����� (� �� ��������)
			if($line =~ m/^.{$pos_begin-3}\s?[a-z]/i) { next; }		# ������ ����� ���������� ����� ��������� � ���������� ����� (� �� ��������), ����� �� ���������� �� �������, �� ��������� �����
			}
		
		my $phrase = $matching;
		$phrase =~ tr/aABcCKoOpP/����������/;
		$line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($line)=$pos_begin+length($phrase);
		}
		
		# - Messaging
		if($line ne $prev_line) { $warnings[$#warnings+1]="\nEnglish Letters changed to Russian:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging	
	
	$line =~ s/(�|\s|\d)�\.�\.�\./$1�\. �\.�\./g;
	$line =~ s/(�\.?|����|�����|\d)\ +(��)?\ *�\.�\./$1 $2 �\.�\./g;
	$line =~ s/(\d{2,})\-��\ +(����|�����)/\1 \2/g;
	
	# *** Replace english letters with russian where necessary
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	### Number. Find and replace special abbreviations
	# if($line =~ m/(\b(�|��|����|�|����|�(��)?|�(���)?|���������|���|���|�|���|����|���(�)?|���|����|����|���|��(��)?|��(�)?|���|���(�)?|��|����|���|��|����|���|��|��|��|��)\b)|([\-\�\�]|\+|\�)/i) # speeds the process up
	if($line =~ m/\b(�|��|����|�|����|���|�(��)?|�(���)?|���������|���|���)\b/i) # speeds the process up
		{
		$line =~ s/(\W|^)[��]\.\s*�\.?(\W|$)/\1����� �������\2/g;
		$line =~ s/(\W|^)�\s+�\.\s*�\b\.?\,\s+�\s+�\.\s*�\.(\W|$)/$1� ��� ����� � ���� ��������$2/g;
		$line =~ s/(\W|^)�\s+�\.\s*�\b\.?(\s+[�-��a-z])/\1� ��� �����\2/g;
		$line =~ s/(\W|^)�\s+�\.\s*�\.?(\W|$)/\1� ��� �����\.\2/g;
		$line =~ s/(\W|^)�\s+�\.\s*�\b\.?(\s+[�-��a-z])/\1� ���� ��������\2/g;
		$line =~ s/(\W|^)�\s+�\.\s*�\.?(\W|$)/\1� ���� ��������\.\2/g;
#		$line =~ s/(\W|^)�\s+��\.(\s+[�-��a-z])/\1� ������\2/g;
#		$line =~ s/(\W|^)�\s+��\.(\W|$)/\1� ������\.\2/g;
		$line =~ s/\b�\s+��\.(?!(\s+[�-��a-z]))/� ������./g;
		$line =~ s/(\W|^)[��]\.\s*�\.?(\W)/\1�� ����\2/g;
		$line =~ s/(\W|^)[��]\.\s*�\.?(\W)/\1��� ���\2/g;
		$line =~ s/(\W|^)[��]\.\s*�\.?(\W|$)/\1��� �������\2/g;
		$line =~ s/(\W|^)[��]\.\s*�\.?(\W|$)/\1��� ���\2/g;
		# $line =~ s/(\W|^)[��]\.\ ?�\.?(\W|$)/\1��� ����������\2/g;
		$line =~ s/(\W|^)[��]���\.(\W|$)/\1��������\2/g;
		$line =~ s/(\W|^)�\.\s*�\.(\s+[�-��a-z])/\1����� ���\2/g;
		$line =~ s/(\W|^)�\.\s*�\.(\W|$)/\1����� ���\.\2/g;						# with a full-stop at the end
		$line =~ s/(\W|^)[��]���\.\s*[��]��(��(������)?)?(\.|\,|\))(\W|$)/\1���������� �����������\4\5/g;
		$line =~ s/(\W|^)[��]���\.\s*[��]��(������)?(\.|\,|\))(\W|$)/\1���������� ���������\3\4/g;
		$line =~ s/(\W|^)[��]���\.\s*[��]��(���)?(\.|\,|\))(\W|$)/\1���������� ������\3\4/g;
		$line =~ s/(\W|^)���(\.\s*|\s+)���\.(\W|$)/\1��� ���������\3/ig;
		# $line =~ s/\b[��]���\.//g;
		# $line =~ s/\b[��]����\.//g;
		# $line =~ s/\b[��]��\.//g;
		$line =~ s/\b�\.\s*�\.(\W|$)/�������� ������\1/g;
		$line =~ s/\b�\.\s*�\.(\W|$)/����� ������\1/g;
		$line =~ s/\b�\.\s*�\.(\W|$)/�������� �������\1/g;
		$line =~ s/\b�\.\s*�\.(\W|$)/��������� �������\1/g;
		$line =~ s/\b���\.\s*���\.(\W|$)/�������� ������\1/g;
		$line =~ s/\b��\.\s*���\.(\W|$)/����� ������\1/g;
		$line =~ s/\b���\.\s*����\.(\W|$)/�������� �������\1/g;
		$line =~ s/\b����?\.\s*����\.(\W|$)/��������� �������\1/g;
		$line =~ s/\b���\.(\W|$)/���\1/ig;
		$line =~ s/\b���\.\s*���\.(\W|$)/��� ���������\1/ig;
		$line =~ s/\b���\.(\W|$)/���\1/ig;
		}
	
	$line =~ s/(\d+)\s+�\s+���������/\1\,5/g;
	$line =~ s/\-\s?\/\s?\+/ ����� ���� /g;
	$line =~ s/(\+\s?\/\s?\-)|(\�)/ ���� ����� /g; # the \� could be replaced by \xb1 sign in heximal form
	$line =~ s/\=+/ ����� /g;
	$line =~ s/\+/ ���� /g;
	$line =~ s/\b�[\/\\\-\�\�]?�\b\.?[\-\�\�]?/�� /ig; # do not forget to add an exception (look for #word-word), otherwise the word could be joined without a dash
	$line =~ s/\b�[\/\\\-\�\�]?�\b\.?[\-\�\�]?/�� /ig;
	$line =~ s/\be[\-\�\�]mail\b/email/ig;
	$line =~ s/\b�[\-\�\�]�([�-�]{0,3})\b/������$1/ig;
	$line =~ s/\b�[\-\�\�]�([�-�]{0,3})\b/��������$1/ig;
	$line =~ s/\b�[\-\�\�]�([�-�]{1,3})\b/������$1/ig;
	$line =~ s/\b��[\-\�\�]�([�-�]{1,3})\b/���������$1/ig; 	#new
	$line =~ s/\b��[\-\�\�]�([�-�]{1,3})\b/��������$1/ig; 	#new
	$line =~ s/\b�[\-\�\�]�([�-�]{0,3})\b/�����$1/ig;
	$line =~ s/\b�[\-\�\�]�?�([�-�]{0,3})\b/��������$1/g;
	$line =~ s/\b�[\-\�\�]�?�([�-�]{0,3})\b/�����������$1/ig;
	$line =~ s/\b�[\-\�\�]��\b/�����������/ig;
	$line =~ s/\b�[\-\�\�]��\b/�����/ig;
	$line =~ s/\b�[\-\�\�]�+\b/��/ig;
	$line =~ s/\b���[\-\�\�]�([�-�]{0,3})\b/�����������$1/ig; # do not forget to add an exception (look for #word-word)
	$line =~ s/(\b|\d)USD(\b|\d)/$1\$$2/ig;
	$line =~ s/(\b|\d)EURO?(\b|\d)/$1\�$2/ig;
	$line =~ s/\bP\.S\./������������/g;
	$line =~ s/\((c|�)\)/(copy right)/ig;
	$line =~ s/(\b|(?-i:[MDCLXVI��])\s*|\d\s*)(��?\.?|��?\.?|(���|���)(|�|�|��|�|��|�|���|��|��))\s+(��|�����|��)\s+(?-i:�\.�\.)/$1$2 $5 ��������� ��������/ig;
	$line =~ s/\b�\s+�\s+�\s+�\s+�\b/�����/g;
	$line =~ s/[\w\.\-\�\�]+\.([a-z]){2,5}/ /g; # remove file_names with extensions
	
	if($line =~ m/\b(���|����|���(�)?|���|����|����|���|��(��)?|����|������|�����|����|��|��(�)?|���|���(�)?|��|����|��|��)\b/i) # speeds the process up
		{
		$line =~ s/\[���\.?\]|\(���\.?\)|\{���\.?\}|\����\.\�/ ��������� /ig;
		$line =~ s/\[����\.?\]|\(����\.?\)|\{����\.?\}|\�����\.\�/ ���������� /ig;
		$line =~ s/\[���(�)?\.?\]|\(���(�)?\.?\)|\{���(�)?\.?\}|\����(�)?\.\�/ ���������� /ig;
		$line =~ s/\[����\.?\]|\(����\.?\)|\{����\.?\}|\�����\.\�/ ��������� /ig;
		$line =~ s/\[��\.[\-\�\�]����\.?\]|\(��\.[\-\�\�]����\.?\)|\{��\.[\-\�\�]����\.?\}|\���\.[\-\�\�]����\.\�/��������������� /ig;
		$line =~ s/\[���\.?\]|\(���\.?\)|\{���\.?\}|\����\.\�/ ��������� /ig;
		$line =~ s/\[����\.?\]|\(����\.?\)|\{����\.?\}|\�����\.\�/ ����������� /ig;
		$line =~ s/\[���\.?\]|\(���\.?\)|\{���\.?\}|\����\.\�/ �������� /ig;
		$line =~ s/\[��(��)?\.?\]|\(��(��)?\.?\)|\{��(��)?\.?\}|\���(��)?\.\�/ ����������� /ig;
		$line =~ s/\[����\.?\]|\(����\.?\)|\{����\.?\}|\�����\.\�/ ����������� /ig;
		$line =~ s/\[������\.?\]|\(������\.?\)|\{������\.?\}|\�������\.\�/ ��������� /ig;
		$line =~ s/\[�����\.?\]|\(�����\.?\)|\{�����\.?\}|\������\.\�/ ���������� /ig;
		$line =~ s/\[����\.?\]|\(����\.?\)|\{����\.?\}|\�����\.\�/ �������� /ig;
		
		$line =~ s/([\[\(\{])\s*���\b\.?/${1}��������� /ig;
		$line =~ s/([\[\(\{])\s*����\b\.?/${1}���������� /ig;
		$line =~ s/([\[\(\{])\s*���(�)?\b\.?/${1}���������� /ig;
		$line =~ s/([\[\(\{])\s*����\b\.?/${1}��������� /ig;
		$line =~ s/([\[\(\{])\s*��\.[\-\�\�]����\b\.?/${1}��������������� /ig;
		$line =~ s/([\[\(\{])\s*���\b\.?/${1}��������� /ig;
		$line =~ s/([\[\(\{])\s*����\b\.?/${1}����������� /ig;
		$line =~ s/([\[\(\{])\s*���\b\.?/${1}�������� /ig;
		$line =~ s/([\[\(\{])\s*��(��)?\b\.?/${1}����������� /ig;
		$line =~ s/([\[\(\{])\s*����\b\.?/${1}����������� /ig;
		$line =~ s/([\[\(\{])\s*������\b\.?/${1}��������� /ig;
		$line =~ s/([\[\(\{])\s*�����\b\.?/${1}���������� /ig;
		$line =~ s/([\[\(\{])\s*����\b\.?/${1}�������� /ig;
		$line =~ s/([\[\(\{])\s*��\b\.?(?!\s+(������|�������|�����))/${1}�������� /ig;
		
		$line =~ s/([\[\(\{])\s*��(�)?\b\.?\s*(?!\d)/${1}����������� /ig;
		$line =~ s/([\[\(\{])\s*���\b\.?\s*(?!\d)/${1}������� /ig;
		$line =~ s/([\[\(\{])\s*���(�)?\b\.?\s*(?!\d)/${1}������� /ig;
		$line =~ s/([\[\(\{])\s*[��]�\b\.?\s*(?!\d)/${1}����� /g;
		$line =~ s/([\[\(\{])\s*��\s+���\b\.?/${1}�� ���������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+����\b\.?/${1}�� ����������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+���(�)?\b\.?/${1}�� ����������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+����\b\.?/${1}�� ���������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+��\.[\-\�\�]����\b\.?/${1}�� ���������������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+���\b\.?/${1}�� ���������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+����\b\.?/${1}�� ������������ /ig;
		$line =~ s/([\[\(\{])\s*��\s+���\b\.?/${1}�� ��������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+��(��)?\b\.?/${1}�� ������������ /ig;
		$line =~ s/([\[\(\{])\s*��\s+����\b\.?/${1}�� ������������ /ig;
		$line =~ s/([\[\(\{])\s*��\s+������\b\.?/${1}�� ���������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+�����\b\.?/${1}�� ����������� /ig;
		$line =~ s/([\[\(\{])\s*��\s+����\b\.?/${1}�� ��������� /ig;
		
		$line =~ s/([\[\(\{])\s*����\b\.?/${1}��������� /ig;
		$line =~ s/([\[\(\{])\s*��\b\.?/${1}����� /ig;
		
		$line =~ s/\b(���)\.\s*��\./ ������� ����� /ig;
		$line =~ s/\b(��)\.\s*��\./ ���� ����� /ig;
		$line =~ s/\b(���)\.?\s*��\./ ������� ����� /ig;
		$line =~ s/([\[\(\{])\s*��\b\.?/${1}����� /ig;
		$line =~ s/\b��\.\s*(?=(\d|[XIV�]+\b))/ ����� /ig;
		}
	
	$line =~ s/\_semicolon_/\;/g;
	
	my $pos_end;
	my $coming_after_s;
	
	if($line =~ m/\b(��|���|����|��|��|��|���|��|�����?|��(��)?|���|���|���|����?|����|��|���|����?|[1-4]?���|���|[1-2]?���|��(��)?|[1-2]���|[1-3]?���|����|���|���|����(��)?|���|����|����|��(��?|��)?|���(��)?|��(��)?|���|���|��(��)?|����|��(��)?|���|����?|���|����|��|���|��|���|���|[1-3]?����?|���|������|�������|����|����|���(��)?|���(��)?|����|���|[1-2]?���(��?)?|[1-3]?��|[1-3]?����|���|����?|[1-2]?���(���)?|���(��)?|��(��)?|���|���|�����|���(��)?|[1-2]?���(���)?|[1-2]?���(��)?|[1-2]?���|���|���|�����|���|����|���)\b/) # speeds the process up
		{
		# Special Session
		$pos_end=$+[0];
		$coming_after_s=substr($line, $pos_end); # from till the end of the string
		# if(($line =~ m/\d\:\d/)&&($line =~ m/\b(���|���|���|���|����?|����|����|���|���(��)?|���|����?)\b/))
		if(($coming_after_s =~ m/^\.?\,?\s*\d+\:\d/)&&($line =~ m/\b(���|���|���|���|����?|����|����|���|���(��)?|���|����?)\b/))
			{
			# $line =~ s/([\[\(\{])\s*(���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ��������� �� ������ '.num2book(${3}).' '/eg;
			# $line =~ s/([\[\(\{])\s*(���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ��������� �� ���� '.num2book(${3}).' '/eg;
			# $line =~ s/([\[\(\{])\s*���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.'����� ����� '.num2book(${2}).' '/eg;
			# $line =~ s/([\[\(\{])\s*���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� '.num2book(${2}).' '/eg;
			# $line =~ s/([\[\(\{])\s*����?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� '.num2book(${2}).' '/eg;
			# $line =~ s/([\[\(\{])\s*����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ������ �������� '.num2book(${2}).' '/eg;
			# $line =~ s/([\[\(\{])\s*����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ���� ������� '.num2book(${2}).' '/eg;
			# $line =~ s/([\[\(\{])\s*(���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ������� ������� '.num2book(${3}).' '/eg;
			# $line =~ s/([\[\(\{])\s*���(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' �������� � ���������� '.num2book(${3}).' '/eg;
			# $line =~ s/([\[\(\{][\dI]?)\s*���\.?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� ���������� '.num2book(${2}).' '/eg;
			# $line =~ s/([\[\(\{])\s*����?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' �������� � �������� '.num2book(${2}).' '/eg;
			
			$line =~ s/\b(���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ��������� �� ������ '.num2book(${2}).' '/eg;
			$line =~ s/\b(���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ��������� �� ���� '.num2book(${2}).' '/eg;
			$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/'����� ����� '.num2book(${1}).' '/eg;
			$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ����� '.num2book(${1}).' '/eg;
			$line =~ s/([\[\(\{])\s*����?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� '.num2book(${2}).' '/eg;
			$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������ �������� '.num2book(${1}).' '/eg;
			$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ���� ������� '.num2book(${1}).' '/eg;
			$line =~ s/\b(���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ������� '.num2book(${2}).' '/eg;
			$line =~ s/\b���(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � ���������� '.num2book(${2}).' '/eg;
			$line =~ s/\b([\dI]?)\s*���\.?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� ���������� '.num2book(${2}).' '/eg;
			$line =~ s/\b����?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � �������� '.num2book(${1}).' '/eg;
			}
		
		$line =~ s/([\[\(\{])\s*(���)\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ��������� �� ������ '.num2book(${3}).' '/eg;
		$line =~ s/\b(��|����)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ��������� �� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(��|��)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ��������� �� ����� '.num2book(${2}).' '/eg;
		$line =~ s/([\[\(\{])\s*(���)\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ��������� �� ���� '.num2book(${3}).' '/eg;
		$line =~ s/\b(��)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ��������� �� ���� '.num2book(${2}).' '/eg;
		$line =~ s/\b(��|����)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ��������� �� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(��(��)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� '.num2book(${3}).' '/eg;

		$line =~ s/([\[\(\{])\s*���\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.'����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ����� '.num2book(${1}).' '/eg;
		$line =~ s/([\[\(\{])\s*���\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/([\[\(\{])\s*����?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������������ '.num2book(${1}).' '/eg;
		$line =~ s/\b(��)?���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������ ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b����?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ���� '.num2book(${1}).' '/eg;
		$line =~ s/\b(1\s*���|1\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(2\s*���|2\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(3\s*���|3\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(4\s*���|4\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� ����� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(I\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b(II\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b(I\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(II\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� �������'.num2book(${2}).' '/eg;
		
		$line =~ s/\b(1\s*���|1\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(2\s*���|2\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(I\ *��(��)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������ '.num2book(${3}).' '/eg;
		$line =~ s/\b(II\ *��(��)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������ '.num2book(${3}).' '/eg;
		$line =~ s/\b(1���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(2���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(1\s*���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ����� '.num2book(${2}).' '/eg;
		
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������ '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������ '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b\b/' ����� ���� '.num2book(${1}).' '/eg;
		$line =~ s/\b����(��)?\.(���\.)?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����������� '.num2book(${3}).' '/eg;
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ����������� '.num2book(${1}).' '/eg;
		$line =~ s/([\[\(\{])\s*����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ������ �������� '.num2book(${2}).' '/eg;
		$line =~ s/\b��(��?|��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${2}).' '/eg;
		
		$line =~ s/([\[\(\{])\s*����\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ���� ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b��(��)?\.\s*(���(��)?)?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ���� ������� '.num2book(${4}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ��������� '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ������� '.num2book(${1}).' '/eg;
		$line =~ s/\b��(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)[^\d�]/' ����� ������� ���� '.num2book(${2}).' '/eg;
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${1}).' '/eg;
		$line =~ s/\b��(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${1}).' '/eg;
		$line =~ s/\b����?\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ���� '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${1}).' '/eg;
		$line =~ s/\b����\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${1}).' '/eg;
		$line =~ s/\b��\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� �������� '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ������� '.num2book(${1}).' '/eg;
		$line =~ s/\b��\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ����� '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ������� '.num2book(${1}).' '/eg;
		$line =~ s/([\[\(\{])\s*(���)\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ������� ������� '.num2book(${3}).' '/eg;
		
		$line =~ s/\b(1\s*����?|1\-�\ *����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(2\s*����?|2\-�\ *����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(3\s*����?|3\-�\ *����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ������������ '.num2book(${2}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ������ '.num2book(${1}).' '/eg;
		$line =~ s/\b(2\s*���|2\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b(3\s*���|3\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ����� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ����� '.num2book(${1}).' '/eg;
		$line =~ s/\b������\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������ '.num2book(${1}).' '/eg;
		$line =~ s/\b(�������|����\.\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b���(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������� ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ����������� �������� '.num2book(${1}).' '/eg;
		$line =~ s/\b���(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ����������� ������, ���� �������� '.num2book(${2}).' '/eg;
		$line =~ s/\b���(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ����� ������ '.num2book(${2}).' '/eg;
		
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ ������ ��������� '.num2book(${1}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� ������ '.num2book(${1}).' '/eg;
		$line =~ s/\b(1\s*����?|1\s*�����|1\-�\ *����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b(2\s*����?|2\s*�����|2\-�\ *����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ����� '.num2book(${2}).' '/eg;
		$line =~ s/\b(1\s*��|1\s*�����?|1\-�\ *�����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(2\s*��|2\s*�����?|2\-�\ *�����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b(3\s*��|3\s*�����?|3\-�\ *�����?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� ���� '.num2book(${1}).' '/eg;
		
		$line =~ s/([\[\(\{])\s*����?\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' �������� � �������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(1\s*���|1\-�\ *���(���)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� � ���������� '.num2book(${3}).' '/eg;
		$line =~ s/\b(2\s*���|2\-�\ *���(���)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� � ���������� '.num2book(${2}).' '/eg;
		$line =~ s/\b���(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b��(��)?\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � �������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(���|���|�����)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � ����������� '.num2book(${2}).' '/eg;
		$line =~ s/([\[\(\{])\s*���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' �������� � ���������� '.num2book(${2}).' '/eg;
		$line =~ s/([\[\(\{])\s*�����\.\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' �������� � ���������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(1\s*���(���)?|1\-�\ *���(���)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� � ��������������� '.num2book(${4}).' '/eg;
		$line =~ s/\b(2\s*���(���)?|2\-�\ *���(���)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� � ��������������� '.num2book(${4}).' '/eg;
		$line =~ s/\b(1\s*���(��)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ��������� '.num2book(${3}).' '/eg;
		$line =~ s/\b(2\s*���(��)?)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� ��������� '.num2book(${3}).' '/eg;
		$line =~ s/\b(1\s*���|1\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� � ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b(2\s*���|2\-�\ *���)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ������ �������� � ������� '.num2book(${2}).' '/eg;
		$line =~ s/\b���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � ���� '.num2book(${1}).' '/eg;
		$line =~ s/\b(���|�����)\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' �������� � �������� '.num2book(${2}).' '/eg;
		$line =~ s/([\[\(\{][\dI]?)\s*���\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' �������� � ������ '.num2book(${2}).' '/eg;
		$line =~ s/\b����\.?\,?\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/' ���������� ������ ��������� '.num2book(${1}).' '/eg;
		$line =~ s/([\[\(\{][\dI]?)\s*���\.\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ����� ����� ���������� '.num2book(${2}).' '/eg;
		}
	
	if($line =~ m/\d\:\d/)
		{
		$line =~ s/([\[\(\{])\s*��\.\s*(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' ������ '.num2book(${2}).' '/eg;
		$line =~ s/([\[\(\{]\d?)\s+(�������������|������|�����|������|����������|������������|��������|��������\ �������|�������|�����)\s+(\d[\d\,\.\-\:\;\ ]+\d)\b/${1}.' '.${2}.' '.num2book(${3}).' '/eg;
		}
	
	# $line =~ s/\;/\n/g;
	
	if($line =~ m/\b�\.\s*�\./i) { $line =~ s/\b($months_rod_pattern|$months_rod_short_pattern|($months_base_pattern)[�-��]{0,3})(\s*\b(?-i:�\.\s*�((?=(\.\s*[�-ߨA-Z]|$))|(\.))))/$1 ���� ����/ig; }
	
	$line =~ s/(\d|\b)(��|��|��|��|��|�|���|��|���)2\b/$1 ��\. $2/ig; # m2
	$line =~ s/(\d|\b)(��|��|��|��|��|�|���|��|���)3\b/$1 ���\. $2/ig;	# m3
	$line =~ s/(\d|\b)(��|��|��|��|��|�|���|��|���)\b(\.?)\s*(��|����)\b(\.?)/$1 $4$5 $2$3/ig;
	$line =~ s/(\d|\b)(��|��|��|��|��|�|���|��|���)\b(\.?)\s*(���|�����)\b(\.?)/$1 $4$5 $2$3/ig;
	
	$line =~ s/(\d|\b)��\s*\/\s*(���|�|���|���|�)\b/$1 ��\.\/$2/ig;
	$line =~ s/(\d|\b)��\s*\/\s*(�)\b/$1 ��\. � ������/ig;
	
	$line =~ s/\/\s*(���|�|hour|h)\b/ � ���/ig;
	$line =~ s/\/\s*(���|min)\b/ � ������/ig;
	$line =~ s/\/\s*(���|�|sec|s)\b/ � �������/ig;
	$line =~ s/(\d|\b)(lm|��)\/\s*(����|��|Watt)\b/$1$2 �� ����/ig;
	$line =~ s/\/\s*(����|��|Watt)\b/ �� ����/ig;
	
	if(($line =~ m/\d/)&&($line =~ m/(���|���|�)/i))
		{
		$line =~ s/(\d.+)\b�\s+(���)\./$1� ������� /i;
		$line =~ s/(\d.+)\b�\s+(���)\./$1� ������ /i;
		$line =~ s/(\d.+)\b�\s+(�)\./$1� ��� /i;
		}
	
	# !!! Do not forget to add new abbreviations to abbreviation variables for a function 'isAbbreviation' (if the point does not mean the end of the sentence)
	
	$line =~ s/(\W|^)\b��\.(\s+)([�-�]\.)/\1�����\2\3/g;						# im. S. Lorsena.
	
	while($line =~ m/\b��\.(\s+)([�-ߨ][�-��]+)\b/g)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $phrase="";
		my $aux_string='';
	
		my $matching=$&;
		my $non_word=$1;
		my $personal_name=$2;
		
		my @morph_properties;
		@morph_properties=get_morph_props($personal_name, '-i -w');
		
		my $flag=0; # Shows that a noun is found
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if($morph_properties[$i]{misc} eq '���') {$flag=1; $i=$#morph_properties+1; }
			}
		
		if($flag != 1) {next;}
		
		$phrase = '����� '.$personal_name;
		
		# $line =~ s/\Q$matching\E/$phrase/;
		$line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($line)=$pos_begin+length($phrase);
		}
	
	# # process '=' sign
	# while($line =~ m/\b(\d+([\.\,]\d+)?)(\s*)([�-��a-z])\.?\s*\=\s*/ig)
		# {
		# my $pos_begin=$-[0];
		# my $pos_end=$+[0];
		# my $phrase="";
		
		# my $matching=$&;
		# my $number_s=$1;
		# my $non_word=$3;
		# my $word=$4;
		
		# my @number=split(/[\.\,]/, $number_s);
				
		# $phrase = $number_s.$non_word.$word.' ';
		# if($number[0] =~ m/(^|[^1])1$/)
			# {
			# my $gender='';
			# ($gender,) = get_morph_properties($word);
			# if($gender ne '')
				# {
				# if($gender eq 'm') { �����}
				# }
			# else
				# {
				
				# }
			
			# }
		# else
			# {
			# $phrase .= '����� '; }
			# }
		
		# # $line =~ s/\Q$matching\E/$phrase/;
		# $line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		# pos($line)=$pos_begin+length($phrase);
		
		
		# }
		
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nAbbr:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
		
		# - Messaging
		$prev_line=$line;
		# * Messaging
		
	### Find and split words with &
	
	if($line =~ m/(\W)\&\w+\b|\b\w+\&(\W)/) { $warnings[$#warnings+1]="\nWarning!!! There were words combined with &-sign (deleted):\n   ".$line."\n"; $line = ''; next; }
	# $line =~ s/(\W)\&\w+\b/$1/g; # delete sth like &sth or that&.
	# $line =~ s/\b\w+\&(\W)/$1/g;
	
	$line =~ s/\b([a-z0-9]+)\s*\&\s*([a-z0-9]+)\b/\1 and \2/i;
	$line =~ s/\b(\w+)\s*\&\s*(\w+)\b/\1 � \2/i;
	
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nAnd sign:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	if((length($line)>15)&&(length($line)<250))
		{
		### Detecting and deleting the references
		$line = ref_detect($line);
		}
		
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nReference:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging

		
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	
	### Find and remove citations with [number]
	$line =~ s/\[\s*\d+\s*\]/ /g;
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nCitation:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging	
	
	### Find and remove \[,\],\{,\} .
	$line =~ s/[\[\]\{\}]/ /g;
	### Delete multiple "-" sign
	$line =~ s/(\-|\�|\�){2,}/\1/g;
	
		# - Messaging
		$prev_line=$line;
		# * Messaging	
	
	
	# Some exceptions before number processing
	
	# if($line =~ m/\b(���\.|�\/�\b|�\\�\b|���\b|�������[�-��]{0,3})(\.)?([\s\(\)\-\�\�\d]*\d)/i)
		# {
		# $line = abbr_process($line);
		# }
		
	$line =~ s/(\b([�-��]+)\s+)?\b(���\.|�\/�\b|�\\�\b|���\b|�������[�-��]{0,3})(\.)?([\s\(\)\-\�\�\d]*\d)/abbr_process($&)/eig;

	### Process the numbers in text
		
	if ($line =~ m/\d/)
		{
		$line = number_process($line);
		}
		
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nArabic numerals:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
	
	# --- Processing abbriviations like roman numerals
	
		# - Messaging
		$prev_line=$line;
		# * Messaging	
	
	$line =~ s/\b[��]\.\s*/ /g; # if capital letters separately - the name shortcuts, the S being separated - would not be treated as a roman numeral
	# $line =~ s/\b[��]\.(?=\s*([�-ߨ]\.|[�-��a-z0-9]|[\,\;\:\-\�\�]))//g;
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nAbbreviations like Roman Numerals (deleted):\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging	
	
	# --- Processing abbriviations like roman numerals	
		
		# - Messaging
		$prev_line=$line;
		# * Messaging
		
	# Number. Find Roman numerals
	if ($line =~ m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})(��?|��?)?\b/)
		{
		# # # print "Warning! There are Roman numeral - like letters in the sentence: $&\n"; # Important! Here we use the result of previous matching
		# $warnings[$#warnings+1]="There are Roman numeral - like letters in the sentence: $&\n";
		$line = roman_numeral_process($line);
		# $line =~ s/\b[MDCLXVIclvi����]{2,}\b//;
		# Delete the lines without Russian letters again
		#if($line !~ m/([�-ߨ]|[�-��])/) {$warnings[$#warnings+1]="This line was deleted: ".$line."\n"; $line = '';}
		}

	# If there are still Roman numbers, delete them
	if($line =~ m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)
		{
		my $matching=$&;
		if(($matching =~ m/[a-z]/i)&&($matching !~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
			{
			$warnings[$#warnings+1]="\nWarning!!! There were unresolved roman numerals still (deleted):\n   ".$line."\n";
			$line =~ s/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b\.?/ /g;
			}
		};
	
	# # # $line =~ s/\b[MDCLXVImdcxvi����]{2,}\b//g; # with Russian C
	# # # $line =~ s/\b[��]\b($|[^\w\.])/\1/g; # with Russian C
	
		
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nRoman numerals:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging

	$line =~ s/\#/ /g;
	
	# Substitute S.-Z. by S-Z, S.-SZ. by S-SZ, U.-UZ. by U-UZ, etc
	$line =~ s/\b([����]+)\.\s*(\-|\�|\�)\s*([����]+)\./$1\-$3/g;
	# Substitute S-Z. by S-Z . etc
	$line =~ s/\b([����]+)\s*(\-|\�|\�)\s*([����]+)\./$1\-$3 \./g;
	
		# - Messaging
		$prev_line=$line;
		# * Messaging

	### remove sth like S. or S.G. in Russian
	
	$line =~ s/\b([�-ߨ]\.\s*(?![�-��]\.))+/ /g; # T.n. posetiteli
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nCapital letters (deleted):\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging	
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
		
	$line = abbr_process($line);
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nAbbreviation process:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
	
	# Delete the letters like a)
	$line =~ s/\b[a-z�-��]\)/ /ig;
	
	### 3. The characters "." ";" "?" "!" are replaced by "LF" character (new line in Linux).
	$line =~ s/(\.|\?|\!|\;)+/\n/g;
	### 4. The characters "," ":" "(" ")" are removed from a file
	$line =~ s/(\,|\:|\(|\)|\<|\>|\#|\&|\"|\')+/ /g;
	### Number. The characters "\" "/" "-" "�" "�" and their combinations are replaced by " "
	#$line =~ s/(\\+|\/+|(\b\-+\b)|(\b\-+\b)|(\b\�+\b)|(\b\-+\b))+/ /g;
		
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	$line =~ s/\b([�-��]{1,3}\s?(\-|\�|\�)+\s?){2,}[�-��]{1,3}\b/ /g;	# delete ho-ro-sho
	$line =~ s/\b([a-z]+)(\-|\�|\�)[�-��]{1,3}\b/\1/gi;					# delete russian part from good-ij, e.g.
	$line =~ s/\b\w*([�-��a-z])\1{4,}\w*\b/ /i; 						# delete a word with more than 3 same consequent letters
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nDeleted strange words in text:\nOld: ".$prev_line."\nNew: ".$line."\n"; }
		# * Messaging
	
	$line =~ s/(\\|\/|\-|\�|\�)+/ /g;
	$index++;
	}

my $aux_string=join('', @new_lines);
@new_lines=split("\n",$aux_string);

foreach my $line (@new_lines)
	{
	$line = $line."\n";
	
	### Delete the lines without Russian letters again
	
	if($line =~ m/^([^�-���-ߨ]*)$/)
		{
		$warnings[$#warnings+1]="\nThis line was deleted:\n   ".$line;
		$line = '';
		}
	
	### 7. Find and remove the sentences with english-russian letters left in one word
		
	if ($line =~ m/([�-��][a-z]|[a-z][�-��])/i)
		{
		$warnings[$#warnings+1]="\nThere were english-russian letters still in one word (Sentence deleted):\n   ".$line;
		$line = '';
		};
		
	### Remove the sentences having cyrillic letters not used in Russian, and few more signs.
	
	if($line =~ m/[\x80\x81\x83\x8A\x8C\x8D\x8E\x8F\x90\x9A\x9C\x9D\x9E\x9F\xA1\xA2\xA3\xA5\xAA\xAF\xB2\xB3\xB4\xBC\xBD\xBE\xBF]/) # All Cyrillic letters not used in Russian
		{
		$warnings[$#warnings+1]="\nThere were cyrillic non-russian letters still (Sentence deleted):\n   ".$line;
		$line = '';
		}
	elsif($line =~ m/[�-���-ߨa-zA-Z][\�\�\�\�\�\|\%\�\�\�\�\�\@\$]|[\�\�\�\�\�\|\%\�\�\�\�\�\@\$][�-���-ߨa-zA-Z]/)
		{
		$warnings[$#warnings+1]="\nThere were still special signs connected to words (Sentence deleted):\n   ".$line;
		$line = '';
		}
	
	# Ukranian letters: \xAF, \xBF together sometimes with numbers only , �text, �, text%text, ����., reference). ]. ����� �������� (��), �����
	# Delete also | the whole sentence
	# if one letter is repeated more than ����������� �������������
	# &lagio
	
		# - Messaging
		$prev_line=$line;
		# * Messaging

	
	### Number. Delete _text_digits_..._text, like _k_r_u_z_o_
	$line =~ s/\w*(_\w+){2,}/ /g;
	
	### Number. Replace "_" sign and its combinations by " "
	$line =~ s/\_+/ /g;
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nUnderlined sign:\nOld: ".$prev_line."New: ".$line; }
		# * Messaging	
		
	# Delete separate letters, except prepositions
	if ($line =~ m/[^\~]\b[��������������������]\b[^\~]/i)	{ $warnings[$#warnings+1]="\nStand-alone Russian letters left (deleted):\n   ".$line; $line =~ s/([^\~])\b[��������������������]\b([^\~])/\1 \2/ig; }
	if ($line =~ m/\b(�|\w\s+�)\b/)	{ $warnings[$#warnings+1]="\nStand-alone Russian letters left (deleted):\n   ".$line; $line =~ s/\b(�|(\w)\s+�)\b/\2 /ig; }
	
	if ($line =~ m/\b[a-z]\b(?!(\~|\s+[a-z]{2,}))/i)	{ $warnings[$#warnings+1]="\nStand-alone English letters left (deleted):\n   ".$line; $line =~ s/\b[a-z]\b(?!(\~|\s+[a-z]{2,}))/ /ig; }	# �� ��������� ��������� ���������� �����, ���� �� ����� � ����� ������ ��� �� ����� ��������� ������
	
	### Delete numbers at the beginning of a sentence
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	$line =~ s/^\W*\d+([\.\,]\d+)?\s+([�-ߨA-Z])/\2/;
	
		# - Messaging
		if($line ne $prev_line) { $messages[$#messages+1]="\nNumbers at the beginning deleted:\nOld: ".$prev_line."New: ".$line; }
		# * Messaging	
	
	### Delete the numbers inside words

		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	# $line =~ s/.*(\d+[�-��a-z]+|[�-��a-z]+\d+).*//i;
	$line =~ s/\b\w*(\d+[�-��a-z]+|[�-��a-z]+\d+)\w*\b/ /ig;

		# - Messaging
		if($line ne $prev_line) { $warnings[$#warnings+1]="\nSentence deleted (number\&word):\nOld: ".$prev_line; }
		# * Messaging
	
	### delete the number or/and number left
	
		# - Messaging
		$prev_line=$line;
		# * Messaging
	
	$line =~ s/\b(�|���|����)?(\s*\d+)/ /ig;
	
		# - Messaging
		if($line ne $prev_line) { $warnings[$#warnings+1]="\nNumbers deleted!!!:\nOld: ".$prev_line."New: ".$line; }
		# * Messaging
	
	# if ($line =~ m/[a-zA-Z]/) 	{print write_file "There are English letters here     :".$line; }
	# if ($line =~ m/[�-ߨ]\./)	{print write_file "There are Capital letters with dot :".$line; }
	
	if ($line =~ m/[a-z]/i) 	{ $warnings[$#warnings+1]="\nEnglish letters left:\n   ".$line; }
	# if ($line =~ m/[�-ߨ]\./)	{ $warnings[$#warnings+1]="\nCapital letters left:\n   ".$line."\n"; }
	if ($line =~ m/\d/) 	  	{ $warnings[$#warnings+1]="\nNumbers left:\n   ".$line;}
	if ($line =~ m/[^�-��a-z\~\ \r\n]/i) { $warnings[$#warnings+1]="\nSigns left (replaced by spaces):\n   sign \"$&\" in\: ".$line; print $warnings[$#warnings]."\n"; $line =~ s/[^�-��a-z\~\ \r\n]/ /ig; }
	
	### Special words. Replacement back. The words were changed previously
	# $line =~ s/\b���~�~����/���-�-����/i;
	# $line =~ s/\b���~�~���/���-�-���/i;
	# $line =~ s/\b���~�~�����/���-�-�����/i;
	$line =~ s/\b���~�~����/��� � ����/i;
	$line =~ s/\b���~�~���/��� � ���/i;
	$line =~ s/\b���~�~�����/��� � �����/i;
	$line =~ s/\~dju\~//g;
	
	### 9. Delete multiple spaces and spaces at the beginning or ending of a sentence
	$line =~ s/ {2,}/ /g; #it is a mistake to use $line =~ s/\s{2,}/ /g; - because \s means not only a white space " ", but also \n and \r ?
	$line =~ s/^\s+//;
	$line =~ s/ +$//; # $line =~ s/\s+$//; # does not work, since all the new line characters are also deleted
	
	### Delete the lines without Russian letters again (or having just one letter in the sentence)
	
	if($line =~ m/^([^�-��]*|\s*([^\W������]|����|�������|���|���|��|���|��|���|��|����)\s*)$/i)
		{
		$warnings[$#warnings+1]="\nThis line was deleted (empty, too short, or short abbr\.):\n   ".$line;
		$line = '';
		}
	}
	
my $txt_output_file = $_;
#$txt_output_file =~ s/.txt$//i;
$txt_output_file =~ s/_plain.txt$//i;
my $txt_warning_file=$txt_output_file;
my $txt_message_file=$txt_output_file;
#$txt_output_file = $output_dir.'/'.$txt_output_file.'_normalized.text';
$txt_output_file = $File::Find::dir.'/'.$txt_output_file.'_normalized.text';
$txt_warning_file = $File::Find::dir.'/'.$txt_warning_file.'_warning.log';
$txt_message_file = $File::Find::dir.'/'.$txt_message_file.'_message.log';

#writing warnings to file
open(write_file, '>encoding(CP1251)', $txt_warning_file) or die "Error!!! Can not open file ".$txt_warning_file; 
print write_file @warnings;
close(write_file);

# writing messages to file
open(write_file, '>encoding(CP1251)', $txt_message_file) or die "Error!!! Can not open file ".$txt_message_file; 
print write_file @messages;
close(write_file);

#open(write_file, '>>encoding(CP1251)', $txt_output_file) or die "Error!!! Can not open file ".$txt_output_file; 
#important to change the writing of the file from the beginning

# writing processed file number
open(write_file, '>encoding(CP1251)', $input_dir.'/info.log') or die "Error!!! Can not open file ".$input_dir.'/info.log';
print write_file "Number of processed files: ".$global_counter.' from '.$number_of_files.' (including '.$global_counter_skipped." of skipped files)\n";
close(write_file);

# writing output file
open(write_file, '>encoding(CP1251)', $txt_output_file) or die "Error!!! Can not open file ".$txt_output_file; 
print write_file @new_lines;
close(write_file);
}

#detecting the reference to a literature
sub ref_detect
{
my $ref_feat_counter=0; #counter of features typical for reference
my $this_line=$_[0];
my $output;
my @word_no;	# number of words in the line
@word_no = split(/\s+/, $this_line);

$ref_feat_counter++ while ($this_line =~ m/\.+\ +/g);
$ref_feat_counter++ while ($this_line =~ m/(\d+[\.\ \-\,\:]+)+/g);
# $ref_feat_counter++ while ($this_line =~ m/(\d+[\.\-\,\:]*)+ /g); #new
#$ref_feat_counter++ while ($this_line =~ m/(\d+[\.\,]?)+/g);
#$ref_feat_counter++ while ($this_line =~ /\b[MDCLXVIclvi����]{2,}\b/);
$ref_feat_counter=$ref_feat_counter+2 while ($this_line =~ m/\.\,/g);
$ref_feat_counter=$ref_feat_counter+2 while ($this_line =~ m/\.\:/g);
#if ($this_line =~ m/\d+\.{2}$/) { print "***\n\n\n This line :".$this_line."\n\n\n";}
$ref_feat_counter=$ref_feat_counter+2 while ($this_line =~ m/ �\./g);
$ref_feat_counter=$ref_feat_counter+2 while ($this_line =~ m/\d+(\.{2})$/g);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b���\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b����\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b����\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b���\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b����\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b���\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b���\./ig);
$ref_feat_counter=$ref_feat_counter+3 while ($this_line =~ m/\b������������\b/ig);
$ref_feat_counter=$ref_feat_counter+5 while ($this_line =~ m/\b�\.\s*\d/ig);
$ref_feat_counter=$ref_feat_counter+5 while ($this_line =~ m/\b�\.\s*\d+(\s*[\-\�\�]+\s*\d+)?([\)\]]*\.|\.*[\)\]])\s*$/ig);
$ref_feat_counter=$ref_feat_counter+7 while ($this_line =~ m/�����\s*[\.\,]/ig);
$ref_feat_counter=$ref_feat_counter+10 while ($this_line =~ m/[\-\�\�]\s*\d+\s*�\.\s*[\-\�\�]/ig);

# if($ref_feat_counter>=9) {print $ref_feat_counter.": ".$this_line."\n";  $output="";}
# if(($ref_feat_counter/length($this_line)>0.10)&&($ref_feat_counter>=9)) {print $ref_feat_counter.": ".$this_line."\n";  $output="";}
if(($ref_feat_counter/($#word_no+1)>1)&&($ref_feat_counter>=10)) {print $ref_feat_counter.": ".$this_line."\n";  $output="";}
else {$output=$this_line;}

# print $this_line.'~'.$#word_no.'~'.$ref_feat_counter.'~'.$output."\~\n";
# exit;

return $output;
}

#processing the numbers in text
sub number_process
{
my $this_line=$_[0];
my $output;

$this_line =~ s/\b(\d+)\.\.\.(\d+)\b/�� $1 �� $2/g;

# delete the numbers like 111.222.3333.4. ...
$this_line =~ s/(\d+\ *\.+\ *){4,}/ /g;

#while ($this_line =~ m/\W(\d{1,2})\ ?\.\ ?(\d{2})\ ?\.\ ?(\d{4})\W/g)
#while ($this_line =~ m/(\w*)(\W+)(\d{1,2})\ ?\.\ ?(\d{2})\ ?\.\ ?(\d{4}|\d{2})(\W+|�{1,2})(\w*)/g)

# delete the numbers like, 1:15-28
$this_line =~ s/[\,\.\(]\s*\d{1,5}\s*\:\s*\d+\s*(\-|\�|\�|\:)?\s*\d*\s*([\.\,\)])/$2/g;
$this_line =~ s/\b\d{1,3}\:(\d{1,3}\,\ ?)*\d{1,3}(\-|\�|\�)\d+\b(?!\:)/ /g;

# Detect the numbers with words "word-35"
$this_line =~ s/\b([�-��a-z]+)(\-|\�|\�)(\d+)\b/"$1 ".num2word($3, 'kol', 'im', 'm')/eig;

my $substituted=0;

# replace 01.01.0001-31.01.0001 by "c 01.01.0001 �� 31.01.0001"
if($this_line =~ s/(\W)(\d{1,2}\ ?\.\ ?\d{2}\ ?\.\ ?(\d{4}|\d{2})\ ?)(\-{1,3})(\ ?\d{1,2}\ ?\.\ ?\d{2}\ ?\.\ ?(\d{4}|\d{2}))(\W)/\1 � \2 �� \5\7/g) { $substituted=1;};

# replace 01 month 0001-31 month 0001 by "c 01 month 0001 �� 31 month 0001"
if($this_line =~ s/(\W|^)(\d{1,2}\s[�-��]{3,8}\.?\s(\d{4}|\d{2}))\s?([\-\�\�]{1,3})\s?(\d{1,2}\s[�-��]{3,8}\.?\s(\d{4}|\d{2}))(\W|$)/\1 � \2 �� \5\7/ig) { $substituted=1;};

if($substituted == 1) { $this_line =~ s/(\W|^)�\s+�\s/$1� /i; };

# Find and separate numbers from certain letters and signs, e.g. 3r :
$this_line =~ s/(\d+)(�|���|���|�|�|�|�|�|��|��|\%|\$|\�|��|���|���|���|�|�|��|��|���|��|���|���|���|����|��|���|���|W|kW|Watt|V|kV|Volt|��|���|���)(\W)/\1 \2\3/g; #new why not a g. ?

# Replace 8:00 msk
$this_line =~ s/([\:\.]\s*\d{2}|(���|���|���)[�-�]*\b)\s*���\b/$1 �� ����������� �������/ig;

# Replace S 1 Maya! etc. If the exclamation mark was at the end of a line, it was replaced by double-dot.
$this_line =~ s/\b�\s+(\d{1,2})\s+($months_rod_pattern)\s*(\!|\.+$)/'� '.num2word($1, 'por', 'tv', 'm').' '.$2.$3/eig;
$this_line =~ s/(�������[�-��]*(\s+(����|���|����|���|���|�|��|��|���|���|����|���|���|��|��))?)\s+�\s+(\d{1,2})\s+($months_rod_pattern)\b/$1.' � '.num2word($4, 'por', 'tv', 'm').' '.$5/eig;

# Replace 802.11
$this_line =~s/\b802\.11(?!\d)/��������� ��� ����� ����������� /g;

# Detect and replace Gradusy

$this_line =~ s/(\d)\s*\�/$1o/g;
$this_line =~ s/(\d+)[o�]\s*([C�]|�)(\W)/$1.' ���� ������� '.$3/egi;
$this_line =~ s/(\d+)\s*[o�]([C�]|�)(\W)/$1.' ���� ������� '.$3/egi;
$this_line =~ s/(\d+)[o�]\s*(F|�)(\W)/$1.' ���� �� ���������� '.$3/egi;
$this_line =~ s/(\d+)\s*[o�](F|�)(\W)/$1.' ���� �� ���������� '.$3/egi;

$this_line =~ s/(\d+)[o�](\W|\d)/$1.' ���� '.$2/egi;
$this_line =~ s/(\d+)\'([^\w\']|\d)/$1.' ��� '.$2/egi; # not a second

# ----- Detect and replace (v yyyy, yyyy, ...) without gg (may be).

while($this_line =~ m/\s*[\(\,]?\s*([�-��]*)(\s*)\b([1-9]?\d{3}\b)((\s*[\,\;]\s*([1-9]?\d{3}\b))+)\s*(��\.)?\s*((\b��\s+)?\b�����\s+���)?\s*\.?\s*\)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;
	my $multiple_number_s=$4;
	my $goda_word=$7;
	my $next_word=$8;
	my $phrase;
	my $case;

	$phrase=' '.$prev_word.$non_word1;
	
	my $flag=0;
	for($prev_word)
		{
		when (m/^(�|��)$/i) 				{ $case='dat'; }
		when (m/^(��|��|��)$/i)			{ $case='vin'; }
		when (m/^(���������(�|�|�|))$/i)	{ $case='tv'; }
		when (m/^(�|��|�)$/i)				{ $case='pr'; }
		when (m/^(�|��|��|�����|�����|�������)$/i)		{ $case='rod'; }
		when (m/^($months_base_pattern)[�-��]{0,3}$/i)	{ $case='rod'; }
		when (m/^$/)						{ $case='im'; }
		default { $flag=1; }
		}
	if($flag == 1) {next; }
		
	$phrase .= ' '.num2word($number_s,'por',$case,'m');
	
	$multiple_number_s =~ s/^\s*[,\;]\s*//;
	my @number=split(/\s*[,\;]\s*/, $multiple_number_s);
	
	for(my $i=0; $i<$#number+1; $i++)
		{
		$phrase .= ' '.num2word($number[$i],'por',$case,'m');
		}
	
	if($goda_word =~ m/�/i) { $phrase .= ' '.$goda{pl}{$case}; }
	
	$phrase .= ' '.$next_word.' ';
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	}
	
# ***** Detect and replace (v yyyy, yyyy, ...) without gg (may be).

# ----- Detect and replace (yyyy - yyyy) without g.

while($this_line =~ m/\s*[\(\,]\s*([�-��]*)(\s*)\b([12]?\d{3}\b)\s*([\-\�\�]\s*([12]?\d{3}\b))?\s*((��\s+)?�����\s+���)?\s*\.?\s*\)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;
	my $number2_s=$5;
	my $next_word=$6;
	my $phrase;
	my $case;
	
	$phrase=' '.$prev_word.$non_word1;

	my $flag=0;
	for($prev_word)
		{
		when (m/^(�|��)$/i) 				{ $case='dat'; }
		when (m/^(��|��|��)$/i)				{ $case='vin'; }
		when (m/^(���������(�|�|�|))$/i)	{ $case='tv'; }
		when (m/^(�|��|�)$/i)				{ $case='pr'; }
		when (m/^(�|��|��|�����|�����|�������)$/i)		{ $case='rod'; }
		when (m/^($months_base_pattern)[�-��]{0,3}$/i)	{ $case='rod'; }
		when (m/^$/)						{ $case='im'; }
		default { $flag=1; }
		}
	if($flag == 1) {next; }
		
	$phrase .= ' '.num2word($number_s,'por',$case,'m');
	if($number2_s =~ m/\d/)
		{
		$phrase .= ' '.num2word($number2_s,'por',$case,'m');
		}
	$phrase .= ' '.$next_word.' ';
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	}
	
# ***** Detect and replace (yyyy - yyyy) without g.

# ----- Rrocess 15" (djujmy)

if($this_line !~ m/[^\d](\"|\'\')/)
	{
	
	# Process 15'' monitor, e.g., by the abbr_process function
	
	$this_line =~ s/(\b([�-��]+)\s+)?\b(\d+([\.\,]\d+)?(\'\'|\"))\s+((�������|�����|��������|����|������)(|�|�|��|�|�|��|��|���|��|��)\b)/abbr_process($&)/eig; # Important! The number should be integer, but this will be resolved by the abbr_process function
	
	# --- Process na 15.3" monitor as (=) na monitor 15.3 djujmov
	
	while($this_line =~ m/\b(\d+([\.\,]\d+)?)\"\s+(�[�]���(��|���|���|��|��|��|��|���)\s+����(|�|�|��|�|�|��|��|���|��)\b|(�������|�����|��������|����|������|�������|���������?)(|�|�|��|�|�|��|��|���|��|��|�|�|��|���?|��)\b)/ig)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $number_s=$1;
		my $words=$3;
		my $phrase;
		
		$phrase = $words.' '.realnum2word($number_s, 'kol', 'im', 'm');
		
		my @number=split(/[\.\,]/, $number_s);
		if($#number+1 > 2) { next;}
		
		if($number_s =~ m/[\.\,](?!5?0*$)/)
			{
			$phrase .= ' �����';
			}
		elsif($number[0] =~ m/(^|[^1])1$/)
			{
			$phrase .= ' ����';
			}
		elsif($number[0] =~ m/(^|[^1])[2-4]$/)
			{
			$phrase .= ' �����';
			}
		else
			{
			$phrase .= ' ������';
			}
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	# $this_line =~ s/\b(\d+[\.\,]\d+)\"\s+(�[�]���(��|���|���|��|��|��|��|���)\s+����(|�|�|��|�|�|��|��|���|��)\b|(�������|�����|��������|����|�������)(|�|�|��|�|�|��|��|���|��)\b)/$2.' '.realnum2word($1, 'kol', 'im', 'm').' �����'/eig;
	
	# *** Process na 15.3" monitor as (=) na monitor 15.3 djujmov
	
	# --- Process na monitor 15.3" as (=) na monitor 15.3 djujmov
	
	while($this_line =~ m/\b(�[�]���(��|���|���|��|��|��|��|���)\s+����(|�|�|��|�|�|��|��|���|��)\b|(�������|�����|��������|����|������|�������|���������?)(|�|�|��|�|�|��|��|���|��|��|�|�|��|���?|��)\b)\s+(\d+([\.\,]\d+)?)\"(?!\w)/ig)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $words=$1;
		my $number_s=$6;
		my $phrase;
		
		$phrase = $words.' '.realnum2word($number_s, 'kol', 'im', 'm');
		
		my @number=split(/[\.\,]/, $number_s);
		if($#number+1 > 2) { next;}
		
		if($number_s =~ m/[\.\,](?!5?0*$)/)
			{
			$phrase .= ' �����';
			}
		elsif($number[0] =~ m/(^|[^1])1$/)
			{
			$phrase .= ' ����';
			}
		elsif($number[0] =~ m/(^|[^1])[2-4]$/)
			{
			$phrase .= ' �����';
			}
		else
			{
			$phrase .= ' ������';
			}
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	
	# *** Process na monitor 15.3" as (=) na monitor 15.3 djujmov
	
	# --- Process monitor na 15.3" as (=) monitor na 15.3 djujmov
	
	while($this_line =~ m/\b(�[�]���(��|���|���|��|��|��|��|���)\s+����(|�|�|��|�|�|��|��|���|��)\b|(�������|�����|��������|����|������|�������|���������?)(|�|�|��|�|�|��|��|���|��|��|�|�|��|���?|��)\b)\s+(��|��|��|�����|�����|��|�������|������|����|���)\s+(\d+([\.\,]\d+)?)\"(?!\w)/ig)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $words=$1;
		my $prev_word=$6;
		my $number_s=$7;
		my $phrase;
		
		my @number=split(/[\.\,]/, $number_s);
		if($#number+1 > 2) { next;}
		
		if($prev_word =~ m/^(��|��|��)$/i)
			{
			$phrase = $words.' '.$prev_word.' '.realnum2word($number_s, 'kol', 'im', 'm');

			if($number_s =~ m/[\.\,](?!5?0*$)/)
				{
				$phrase .= ' �����';
				}
			elsif($number[0] =~ m/(^|[^1])1$/)
				{
				$phrase .= ' ����';
				}
			elsif($number[0] =~ m/(^|[^1])[2-4]$/)
				{
				$phrase .= ' �����';
				}
			else
				{
				$phrase .= ' ������';
				}
			}
		elsif($prev_word =~ m/^(�����|�����|��|�������|������|����|���)$/i)
			{
			$phrase = $words.' '.$prev_word.' '.realnum2word($number_s, 'kol', 'rod', 'm');

			if($number_s =~ m/[\.\,](?!5?0*$)/)
				{
				$phrase .= ' �����';
				}
			elsif($number[0] =~ m/(^|[^1])1$/)
				{
				$phrase .= ' �����';
				}
			elsif($number[0] =~ m/(^|[^1])[2-4]$/)
				{
				$phrase .= ' ������';
				}
			else
				{
				$phrase .= ' ������';
				}
			}
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	
	# *** Process monitor na 15.3" as (=) monitor na 15.3 djujmov
	
	if(($this_line =~ m/\d\"/)&&($this_line !~ m/\b(����|���)\b/i))
		{
		$this_line =~ s/(\d)\"(?!\w)/$1 \~dju\~/g;
		}
	}
	
if(($this_line =~ m/\d(\"|\'\')\W/)&&($this_line !~ m/[^\d](\"|\'\')/))	# seconds should not be recognized as minutes
	{
	$this_line =~ s/(\d)(\"|\'\')(\W)/$1.' ��� '.$3/egi;
	}
	
# ***** Rrocess 15" (djujmy)

# ----- Process let
$this_line =~ s/\b(����������|����������[��]|�������|�����|�����|�����|������|������|�����|����������|����|���|��|��|�\s+��������)\s+(\d+([\.\,]\d+)?)\s+���\b/"$1 ".realnum2word($2,'kol','rod','m')." ���"/ieg;

while($this_line =~ m/\b((��|�)\s+\d+([\.\,]d+)?\s+)?\b(��)\s+(\d+([\.\,]\d+)?)\s+���\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $matching=$&;
	my $optional=$1;
	my $prev_word=$4;
	my $number_s=$5;
	my $phrase;
	
	if($optional ne '') { next; } # optional should be empty (this case is solved later)
	
	$phrase = $prev_word.' '.realnum2word($number_s,'kol','rod','m').' ���';
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	}

$this_line =~ s/\b��\s{2,}(\d)/�� $1/ig; # do 5 let
$this_line =~ s/(?<!\b��\s)\b(\d+([\.\,]\d+)?)\s+���\b/realnum2word($1,'kol','im','m')." ���"/ieg; # 5 let
$this_line =~ s/\b���\s+(\d+([\.\,]\d+)?)(\s*[\-\�\�]\s*(\d+([\.\,]\d+)?))?\b/"��� ".realnum2word($1,'kol','im','m').' '.realnum2word($4,'kol','im','m')/ieg; # let 5-7,5

# ***** Process let

# ----- Process v vozraste

if($this_line =~ m/\b�\s+��������\s/i)
	{
	# print $this_line."\~\n";
	
	$this_line =~ s/\b�\s+��������\s+(\d+([\.\,]\d+)?)(\s*�\.|\s*����\b)/'� �������� '.realnum2word($1,'kol','rod','m').' ����'/eig;
	
	while($this_line =~ m/\b�\s+��������\s+(\d+([\.\,]\d+)?)(\s*��?\.)/i)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $matching=$&;
		my $number_s=$1;
		my $veka=$1;
		my $phrase;
		$phrase = '� �������� '.realnum2word($number_s,'kol','rod','m');
		
		if((($number_s !~ m/[\.\,]/)||($number_s =~ m/[\.\,]50*/))&&($number_s !~ m/(^|[^1])1$/))
			{
			$phrase .= ' ����� ';
			}
		elsif($veka =~ m/��/i)
			{
			$phrase .= ' ����� ';
			}	
		else
			{
			$phrase .= ' ���� ';
			}
			
		# Treat the point (.) by the shortcuts properly
		if($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/) { $phrase .= '.'; }
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	
	$this_line =~ s/\b�\s+��������\s+(\d+([\.\,]\d+)?)\b/'� �������� '.realnum2word($1,'kol','rod','m')/eig;
	}
	
# ***** Process v vozraste

# Replace v d. 3, str. 5 by stroenie 5.
$this_line =~ s/(\b�\.\s*(\d+(\s*\/\s*\d+)?\s*)[\,\.]?)\s*���\.\s*(\d+)\b/$1.' �������� '.num2word($4,'kol','im','n')/eig;
	
# ----- Find and replace the scores like number x number or number na number

$this_line =~ s/\b(\d+([\.\,]\d+)?)\s*[x|�]\s*(\d+)\b/realnum2word($1, 'kol', 'im', 'm')." �� $3"/ieg;
$this_line =~ s/\b(\d+([\.\,]\d+)?)\s+��\s+(\d+)\b/realnum2word($1, 'kol', 'im', 'm').' �� '.$3/ge;

# ***** Find and replace the scores like number x number or number na number

if($this_line !~ m/\d/) { return $this_line; }

# Process 100% rezultat, e.g. by the abbr_process function
$this_line =~ s/(\b([�-��]+)\s+)?\b(\d+\s*\%)\s+(�������(�|�|�|��|�|��|���|��)\b|(���������|�������)(|�|�|��|�|�|��|��|���|��)\b)/abbr_process($&)/eig;

# Process 12x uvelichenie
$this_line =~ s/(\b([�-��]+)\s+)?\b(\d+\s*[x�]\b)\s+(��������|(���������|���������|����������|(�|��)������)(�|�|�|��|�|�|��|���|��)\b)/abbr_process($&)/eig;
	
# ----- Process 100%-yj, 15$-yj, 15%-yj, etc

while ($this_line =~ m/\b(\d{1,4})\s*([\%\$])\s*[\-\�\�]\s*([�-��]{1,3}\b)(?=\s*([�-��]*)\b)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];	# my $pos_end=pos($this_line); - the same
	my $phrase;
	my $matching=$&;
	my $number_s=$1;
	my $sign=$2;
	my $word_part=$3;
	my $next_word=$4;
	
	my $flag=0;
	
	if($word_part =~ m/^[��]$/i)
		{
		# Read the properties of the word from mystem program;
		my @morph_properties=get_morph_props($next_word, '-i -w');
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} =~ m/^(S|A|APRO)$/)&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{adj_form} ne '��')&&($morph_properties[$i]{number} eq 'sin')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/))
				{
				if(($sign =~ m/\%/)&&($procentnyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}} =~ m/$word_part$/i))
					{
					$phrase = $procentnyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}}; $flag=1; $i=$#morph_properties;
					}
				elsif(($sign =~ m/\$/)&&($dollarovyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}} =~ m/$word_part$/i))
					{
					$phrase = $dollarovyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}}; $flag=1; $i=$#morph_properties;
					}
				
				}
			}
		};

	if($flag == 0)
		{
		for(my $j=0; $j<6; $j++)
			{
			for(my $k=0; $k<4; $k++)
				{
				if(($sign =~ m/\%/)&&($procentnyj_adj{$cases_base[$j]}{$genders_base[$k]} =~ m/$word_part$/))
					{
					$phrase = $procentnyj_adj{$cases_base[$j]}{$genders_base[$k]}; $flag=1;
					}
				elsif(($sign =~ m/\$/)&&($dollarovyj_adj{$cases_base[$j]}{$genders_base[$k]} =~ m/$word_part$/))
					{
					$phrase = $dollarovyj_adj{$cases_base[$j]}{$genders_base[$k]}; $flag=1;
					}
				}
			}
		}
		
	if($flag == 1)
		{
		my $aux_phrase;
		if(int($number_s) == 100) { $aux_phrase = '���'; }
		else { $aux_phrase = num2word($number_s,'kol','rod','m'); };
		$aux_phrase =~ s/������?$/������/i;
		$aux_phrase =~ s/\s//g; #delete the spaces
		$aux_phrase =~ s/��$//; #delete the go from odnogo and so on to get odnotysjachnyj, for example
				
		$phrase = $aux_phrase.$phrase;
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	}

# ***** Process 100%-yj, 15$-yj, 15%-yj, etc

# ----- (S) 3 po/do 15 ijunja ...

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/\b(��?|��)?\s*(\d{1,2})\s+(��|��)\s+(\d{1,2})\s{0,2}(($months_rod_pattern)\b|$months_rod_short_pattern)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase=" ";
	my $matching=$&;
	my $prev_word=$1;
	my $number_s=$2;
	my $preposition=$3;
	my $number2_s=$4;
	my $month=$5;
	
	my $temp_word;
	
	if(($number_s =~ m/^(2)$/)&&($prev_word)&&($prev_word =~ m/^�$/i)) {$prev_word='��'}
	
	if($prev_word)
		{
			$phrase .= $prev_word.' '.num2word($number_s, 'por', 'rod', 'm').' '.$preposition.' ';
		}
	else
		{
			$phrase .= num2word($number_s, 'por', 'vin', 'n').' '.$preposition.' ';
		}
	
	if($preposition =~ m/^��$/i)
		{
		$phrase .= num2word($number2_s, 'por', 'rod', 'n');
		}
	elsif($preposition =~ m/^��$/i)
		{
		$phrase .= num2word($number2_s, 'por', 'vin', 'n');
		}
	
	# Handle months
	
	for(my $i=0; $i<12; $i++)
		{
		if($month =~ m/^($months_rod[$i]\b|$months_rod_short[$i])$/i)
			{
			$phrase .= ' '.$months_rod[$i];
			}
		}
	
	# Treat the point (.) by the shortcuts properly
	if(($matching=~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }
	
	$phrase .= ' ';
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"if s 19 po 20\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}
	
# ***** (S) 3 po/do 15 ijunja ...

### Find and replace the dates

# # # # - Timing
# # # my $start=gettimeofday();
# # # my $end;
# # # my $temp_counter=0; # number of entries into while loop
# # # # * Timing

# ----- Detect the date like: (preposition) (0)1.(1)2.(20)00(�.|��.) and write it in letters
while ($this_line =~ m/(\w*)(\s*)\b(\d{1,2})\s?\.\s?(\d{1,2})\s?\.\s?(\d{4}|\d{2})(?!\d)(\s*(����\b|����\b|�\b\.?))?/ig) # lookahead assertion
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];	# my $pos_end=pos($this_line); - the same
	my $phrase;
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $day_s=$3;						#as strings
	my $month_s=$4;				#as strings
	my $year_s=$5;					#as strings
	my @day=split(//,$day_s);
	my @month=split(//,$month_s);
	my @year=split(//,$year_s);
	
	my $case;
	
	# --- Processing exceptions
	
	if($prev_word =~ m/^(�)$/i) { next; }
	if((int($day_s)>31) || (int($day_s)<1) || (int($month_s)>12) || (int($month_s)<1)) { next; }
	
	# *** Processing exceptions
	
	$phrase=$prev_word.$non_word1;
	
	for($prev_word)
			{
			when (m/^(�|��)$/i) { $case='dat'; if((int($day_s) == 2) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; }; } # change ko vtoromu also
			when (m/^(��|��|��)$/i)				{ $case='vin'; 	}
			when (m/^(���������(�|�|�|))$/i)	{ $case='tv'; 	}
			when (m/^(�|��)$/i)					{ $case='pr';  if((int($day_s) == 11) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; };}
			default								{ $case='rod'; if((int($day_s) == 2) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; }; } # elsif($prev_word =~ m/^(�|��|��|��)$/i)	#processing days (Rod)
			}
		
	$phrase .= num2word($day_s, 'por', $case, 'n').' ';
		
	#processing the month
	$phrase=$phrase.$months_rod[int($month_s)-1]." ";
			
	#processing the year
	
	$phrase .= num2word($year_s, 'por', 'rod', 'm').' ����';
	
	# Treat the point (.) by the shortcuts properly
	if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }

	# $this_line =~ s/\Q$matching/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # if((int(($end-$start)*1000000)/1000000)>0.1) {print "\n\>\>Time spent by \"Detect the date like-1\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";}
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line; }
	
# ***** Detect the date like: (preposition) (0)1.(1)2.(20)00(�.|��.) and write it in letters	

# ----- Detect the date like: (preposition) (0)1 (���\.?|������)2000(�.|��.) and write it in letters

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

for(my $i=0; $i<12; $i++)
	{
	
	while ($this_line =~ m/(\w*)(\s*)\b((\d{1,2}([��\,\s\-\�\�])*)+)\s+($months_rod[$i]\b|$months_rod_short[$i])(\s+(\d{4}|\d{3}|\d{2})(?!\:?\d)(\s*(����\b|����\b|�\b\.?))?)?/ig)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $phrase="";
		my $matching=$&;
		my $prev_word=$1;
		my $non_word1=$2;
		my $days_s=$3;						#as strings
		my $month_s=$6;				#as strings
		my $year_s=$8;					#as strings
		my @month=split(//,$month_s);
		my @year=split(//,$year_s);
		
		my @days=split(/[��\,\s\-\�\�]+/,$days_s);
		my @days_with_and_or=split(/[\,\s\-\�\�]+/,$days_s);
		my @and_or;
		my @and_or_position;
		my $case;
		
		# --- Processing exceptions
	
		if($prev_word =~ m/^(�)$/i) { next; }
		
		# *** Processing exceptions
		
		for(my $j=0; $j<$#days_with_and_or+1; $j++)
			{
			# print $days_with_and[$j].'-'."\n";
			if($days_with_and_or[$j] =~ m/^�$/i) { push(@and_or_position,$j-$#and_or_position-1-1); $and_or[$#and_or_position]='�'; }
			elsif($days_with_and_or[$j] =~ m/^���$/i) { push(@and_or_position,$j-$#and_or_position-1-1); $and_or[$#and_or_position]='���'; }
			}
		
		$phrase=$prev_word.$non_word1;
		
		# preliminary determination of the case
		my $line_before_number=substr($this_line, 0, $pos_begin);
		if($prev_word =~ m/[�-��]/i) { $line_before_number .= $prev_word; }
		my $prel_case=preliminary_case($line_before_number);
		
		if($prel_case ne 'im')
			{
			$case=$prel_case;
			if((int($days[0]) == 2) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; };
			}
		else
			{
			for($prev_word)
				{
				when (m/^(�|��)$/i)				{ $case='dat'; if((int($days[0]) == 2) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; }; } # change ko vtoromu also
				when (m/^(��|��|��)$/i)			{ $case='vin'; 	}
				when (m/^(���������(�|�|�|)|�����)$/i)	{ $case='tv'; 	}
				when (m/^(�|��)$/i)				{ $case='pr';  if((int($days[0]) == 11) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; };}
				default							{ $case='rod'; if((int($days[0]) == 2) && ($prev_word =~ m/^�$/i)) { $phrase =~ s/^$prev_word/��/; }; } # elsif($prev_word =~ m/^(�|��|��|��)$/i)	#processing days (Rod)
				}
			}

		my $counter=0;
		for(my $j=0; $j<$#days+1; $j++)
				{
				$phrase .= num2word($days[$j], 'por', $case, 'n').' ';
				if($j == $and_or_position[$counter]) { $phrase .= $and_or[$counter].' '; $counter++; };
				}
			
		#processing the month
		
		$phrase=$phrase.$months_rod[$i]." ";
		
		#processing the year
		
		if($year_s ne "")
			{
			$phrase .= num2word($year_s, 'por', 'rod', 'm').' ����';
			
			# Treat the point (.) by the shortcuts properly
			if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }
			}
			
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		# $this_line =~ s/\Q$matching/$phrase/;				
		}
		# # # # --- Timing
		# # # $temp_counter++;
		# # # # *** Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"Detect the date like\-2\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line; }

# ***** Detect the date like: (preposition) (0)1 (���\.?|������)2000(�.|��.) and write it in letters

# ----- Detect the date like: (preposition) (���\.?|������)2000(�.|��.) and write it in letters

for(my $i=0; $i<12; $i++)
	{
	
	while ($this_line =~ m/(\w*)(\s*)\b($months_base[$i](�|�|�|�|�|�|��|��|��|�|�|�|��|��|��|��|���|���|��|��)\b|$months_rod_short[$i])\s+(\d{4}|\d{3}|\d{2})(?!\:?\d)(\s*(����\b|����\b|�\b\.?))?/ig)
		{
		my $pos_begin=$-[0];
		my $pos_end=$+[0];
		my $phrase="";
		my $case="";
		my $matching=$&;
		my $prev_word=$1;
		my $non_word1=$2;
		my $month_s=$3;				#as strings
		my $year_s=$5;				#as strings
		
		$phrase = $prev_word.$non_word1;
		
		my $coming_before_s=substr($this_line, 0, $pos_begin).$prev_word;
		
		#processing the month
		
		if($month_s =~ m/^$months_rod_short[$i]$/i)
			{
			if($coming_before_s =~ m/\d\s*[\-\�\�]\s*[�-��]{0,3}\s*$/)
				{
				$phrase .= $month_s;
				}
			else
				{
				# preliminary determination of the case
				my $line_before_number=substr($this_line, 0, $pos_begin);
				if($prev_word =~ m/[�-��]/i) { $line_before_number .= $prev_word; }
				my $prel_case=preliminary_case($line_before_number);
				
				if($prel_case ne 'im')
					{
					$case=$prel_case;
					}
				else
					{
					for($prev_word)
						{
						when (m/^(�|��)$/i)					{ $case = 'dat'; }
						when (m/^(��|��|��)$/i)				{ $case = 'vin'; }
						when (m/^(���������(�|�|�|))$/i)	{ $case = 'tv'; }
						when (m/^(�|��)$/i)					{ $case = 'pr'; }
						when (m/^(�|��|��|�����|�����|�������|����������[��]|�������|����������|��������[���])$/i)				{ $case = 'rod'; }
						default								{ $case = 'vin'; }
						}
					}
			
				$phrase .= $months[$i]{$case};
				}
			}
		else
			{
			$phrase .= $month_s;
			}
		
		# processing the year
		
		$phrase .= ' '.num2word($year_s, 'por', 'rod', 'm').' ����';
		
		# Treat the point (.) by the shortcuts properly
		if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	}

if($this_line !~ m/\d/) { return $this_line; }

# ----- if s 19 po 20 v/vv or g/gg, replace by words

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/\b(��?|��)\s+(\d+)\s+(��|��)\s+(\d+)\s{0,2}((���|���|��\b\.?|��\b\.?|�\b\.?|�\b\.?)[�-��]{0,3})(\W|$)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $number_s=$2;
	my $preposition=$3;
	my $number2_s=$4;
	my $word=$5;
	my $non_word=$7;	
	
	my $temp_word;
	
	if(($number_s =~ m/^(1\d{2}|2)$/)&&($number_s ne '100')&&($prev_word =~ m/^�$/i)) {$prev_word='��'}
	
	if($preposition =~ m/^��$/i)
		{
		$phrase .= $prev_word.' '.num2word($number_s, 'kol', 'rod', 'm').' '.$preposition.' '.num2word($number2_s, 'kol', 'rod', 'm');
		
		$temp_word=$word;
		for($temp_word)
			{
			when (m/�\b\.?$/i)			{ $word=$veka{'pl'}{'rod'}; }
			when (m/�\b\.?$/i)			{ $word=$goda{'pl'}{'rod'}; }
			}
		}
	elsif($preposition =~ m/^��$/i)
		{
		$phrase .= $prev_word.' '.num2word($number_s, 'por', 'rod', 'm').' '.$preposition.' '.num2word($number2_s, 'por', 'vin', 'm');
		
		$temp_word=$word;
		for($temp_word)
			{
			when (m/�\b\.?$/i)			{ $word=$veka{'pl'}{'vin'}; }
			when (m/�\b\.?$/i)			{ $word=$goda{'pl'}{'vin'}; }
			}
		}
		
	$phrase .= ' '.$word.$non_word;
	
	# Treat the point (.) by the shortcuts properly
	if(($temp_word =~ m/\.$/)&&($non_word !~ m/\./)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"if s 19 po 20\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing	

if($this_line !~ m/\d/) { return $this_line;}
	
# ***** if s 19 po 20 v/vv or g/gg, replace by words

# ----- Detect the date like (s) nachala/serediny/polovine... 2004 - (vtoroy) poloviny/... 2006 gg. (/) too.

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\w*)([^\w\-\�\�]+)(\d{1,4})\s*(\-|\�|\�|\/|\b��\b|\b��\b)\s+([�-��]+)(\s+([�-��]+))?(\s+([�-��]+))?\s+(\d{1,4})(\W*)(�\.|��\.?|���[�-��]{0,4}\b|�\.|��\.?|���[�-��]{0,4}\b|)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	
	my $year1_s=$3;								#as strings
	my $connection=$4;
	
	my $special_word_1=$5;
	my $special_word_2=$7;
	my $special_word_3=$9;
	
	my $year2_s=$10;								#as strings
	my $non_word2=$11;
	my $next_word=$12;
	
	my $case;
	
	# --- Processing exceptions
	
	if(($year2_s eq '') && (length($year1_s) == 2)) { next;}	# ???
	if(($next_word =~ m/(�\.|��\.?|���[�-��]{0,4}\b)/i)&&((length($year1_s) =~ m/(1|3)/)||(length($year2_s) =~ m/(1|3)/))) { next; }
	if(($next_word =~ m/^$/)&&((length($year1_s)!=4)||(length($year2_s)!=4))) { next; }
	
	if( (($special_word_1 =~ m/^(�����|�������|����?�)[�-��]{0,3}$/i) && ($special_word_2 =~ m/^\s*((����)?�����|�����|�������|$)[�-��]{0,3}$/i))	||
		(($special_word_1 =~ m/^(����|����|����|����[�]��|�������)[�-��]{0,3}$/i) && ($special_word_2 =~ m/^\s*(�������|�������|�������|�����)[�-��]{0,3}$/i) && ($special_word_3 =~ m/^\s*((����)?�����|�����|�������|$)[�-��]{0,3}$/i)) ||
		(($special_word_1 =~ m/^(�����|�������|����?�)[�-��]{0,3}$/i) && ($special_word_2 =~ m/^(����|����|����|����[�]��|�������)[�-��]{0,3}$/i) && ($special_word_3 =~ m/^\s*(�������|�������|�������|�����)[�-��]{0,3}$/i))
		)
		{
		}
	else
		{
		next;
		}
	
	# *** processing exceptions
	
	$phrase = $phrase.$prev_word.$non_word1;
	
	if($prev_word =~ m/^(�|�|��|��|�����|�����|�������|������|����|���|��|����������[��]|�������|����������|��������[���]|������|�������[�-�]*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*|�������[�-�]*|�������.*|�������.*|����.*|�����.*)$/i) # (Rod)
		{
		$case = 'rod';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
		{
		$case = 'dat';
		}
	elsif($prev_word =~ m/^(��|��|��)$/i) # (Vin)
		{
		$case = 'vin';
		}
	elsif($prev_word =~ m/^(�����)$/i) # (Tv)
		{
		$case = 'tv';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Pr)
		{
		$case = 'pr';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Pr Second)
		{
		$case = 'pr';
		}
	else
		{
		next;
		}
	
	$phrase = $phrase.' '.num2word($year1_s,'por',$case,'m');
	if($connection =~ m/(\b��\b|\b��\b)/i)	{ $phrase .= ' '.$connection; };
	$phrase .= ' '.$special_word_1.' '.$special_word_2.' '.$special_word_3.' '.num2word($year2_s,'por','rod','m');
	
	if($next_word =~ m/(�\.|��\.?|���[�-��]{0,4}\b)/i) { $phrase .= ' ����'}
	elsif($next_word =~ m/(�\.|��\.?|���[�-��]{0,4}\b)/i) { $phrase .= ' ����'}
	elsif($next_word eq '') { }	# May be unnecessary
	
	$phrase =~ s/\ {2,}/ /g;
	
	# Treat the point (.) by the shortcuts properly
	if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E(\s+[IXCLMix])/)) { }	# do nothing (could be ... 20 g. XX veka)
	elsif(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }

	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# print $this_line;
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"Detect the date like\-3\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect the date like (s) nachala/serediny/polovine... 2004 - (vtoroy) poloviny/... 2006 gg. (/) too.

# ----- detect the date like (v) 2004-2006 gg. (/) too. v 2005 g.

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/([�-��]*)(^|[^\w\-\�\�]+)(\d{4}|\d{3}|\d{2})(\s*(\-|\�|\�|\/|�|���)\s*(\d{4}|\d{3}|\d{2}))?(\W*)(�\.|��\.?|���[�-��]{0,4}\b)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	
	my $year1_s=$3;								#as strings
	my $connection=$5;
	my $year2_s=$6;								#as strings
	my $non_word2=$7;
	my $next_word=$8;
	
	# --- Processing exceptions
	
	my $coming_after_s=substr($this_line, $pos_end); # from till the end of the string
	
	if(($year2_s eq '') && (length($year1_s) == 2) && ($coming_after_s !~ m/^\s*(��)?\s*\b�����\s+���/i)) { next;}
	if((length($year1_s)<4)&&($this_line =~ m/\b(�������|�����(?!��)|�����|������)/i))	{ next; } # Vozrast do 100-101 goda
	
	if(($next_word !~ m/^��\.?/i) && (($year1_s =~ m/^\d\d0$/i)||($year2_s =~ m/^\d\d0$/i)) && ($prev_word !~ m/^(�������|����������[��]|�������|����������|��������[���]|������|�������[�-�]*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�������[�-�]*|�������.*|�������.*|����.*|�����.*)/i) && ($this_line !~ m/\b(�\.\s*�\.|���������|[��]����\s+[��]��)/))
		{
		next; # To differentiate between gramms and years
		}
	if((($year1_s =~ m/^\d{2,3}$/i)||($year2_s =~ m/^\d{2,3}$/i)) && ($this_line =~ m/\b((��)?���[^�]|����|������|������(��)?|����(��)?)[�-��]{0,3}\b/i))
		{
		next;
		}

	# *** processing exceptions
	
	$phrase = $phrase.$prev_word.$non_word1;
	
	my $connection_s = '';
	if($connection =~ m/\w/)	{ $connection_s = $connection.' ';}
	
	if(($prev_word =~ m/^(�|�|��|��|�����|�����|�������|������|����|���|��|����������[��]|�������|����������|��������[���]|������|�������[�-�]*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*|�������[�-�]*|�������.*|�������.*|����.*|�����.*)$/i)||($next_word =~ m/^(�����|����)\.?$/i)) # (Rod)
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','rod','m');
		if($year2_s =~ m/\d/) {$phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','rod','m')};
		if(($connection =~ m/\/|(^$)/)||($year2_s !~ m/\d/)) { $phrase = $phrase.' ����';} else { $phrase = $phrase.' �����';};
		}
	elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/^(�����)$/i)) # (Dat)
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','dat','m');
		if($year2_s =~ m/\d/) {$phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','dat','m');};
		if(($connection =~ m/\/|(^$)/)||($year2_s !~ m/\d/)) { $phrase = $phrase.' ����';} else { $phrase = $phrase.' �����';};
		}
	elsif(($prev_word =~ m/^(��|��|��)$/i)||($next_word =~ m/^(����|���)$/i)) # (Vin)
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','vin','m');
		if($year2_s =~ m/\d/) {$phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','vin','m');};
		if(($connection =~ m/\/|(^$)/)||($year2_s !~ m/\d/)) { $phrase = $phrase.' ���';} else { $phrase = $phrase.' ����';};
		}
	elsif(($prev_word =~ m/^(�����)$/i)||($next_word =~ m/^(������|�����)$/i)) # (Tv)
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','tv','m');
		if($year2_s =~ m/\d/) { $phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','tv','m');};
		if(($connection =~ m/\/|(^$)/)||($year2_s !~ m/\d/)) { $phrase = $phrase.' �����';} else { $phrase = $phrase.' ������';};
		}
	elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/^(�����)$/i)) # (Pr)
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','pr','m');
		if($year2_s =~ m/\d/) { $phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','pr','m');};
		if(($connection =~ m/\/|(^$)/)||($year2_s !~ m/\d/)) { $phrase = $phrase.' ����';} else { $phrase = $phrase.' �����';};
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Pr Second)
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','pr','m');
		if($year2_s =~ m/\d/) { $phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','pr','m');};
		if(($connection =~ m/\/|(^$)/)||($year2_s !~ m/\d/)) { $phrase = $phrase.' ����';} else { $phrase = $phrase.' �����';};
		}
	elsif($connection eq '/')
		{
		$phrase = $phrase.' '.num2word($year1_s,'por','rod','m');
		if($year2_s =~ m/\d/) {$phrase = $phrase.' '.num2word($year2_s,'por','rod','m')};
		$phrase = $phrase.' ����';
		}
	elsif(($next_word =~ m/^(�\.|����)$/i) && (length($year1_s) == 4)) # (Pr)
		{
		if((length($year2_s)>0)&&(length($year2_s) != 4)) { next; }
		$phrase = $phrase.' '.num2word($year1_s,'por','rod','m');
		if($year2_s =~ m/\d/) {$phrase = $phrase.' '.$connection_s.num2word($year2_s,'por','rod','m')};
		$phrase = $phrase.' ����';
		}
	else
		{
		next;
		}
	
	# Treat the point (.) by the shortcuts properly
	if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }

	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# print $this_line;
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"Detect the date like\-3\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect the date like (v) 2004-2006 gg.

# ----- Find and replace (1) 2/6 kg, etc, including 1/10-oj kg

my $prev_case='';
my $prev_end_position=0;		# End position of the previous matching's
my $special_preposition=''; 	# if the preposition "po" is met

while($this_line =~ m/((\b[�-��]+|\-|\�|\�|\,)\s+)?((\b\d+)\s+)?(\b(\d+)\s?\/\s?(\d+)(\s*))([\-\�\�]\s*([�-��]{1,3}\b)(\s*))?(\b($shortcuts_pattern)\b|\%|\$|\�)?(\.)?/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase='';
	my $case='';
	my $sin_or_pl;
	
	my $matching=$&;
	my $prev_word=$2;
	my $integer_s=$4;
	my $numerator_s=$6;
	my $denominator_s=$7;
	my $non_word=$8;
	my $word_part=$10;
	my $non_word2=$11;
	my $abbr=$12;
	my $point=$14;

	my $fractional_part_temp=$5;
	my $rest_length=length($this_line)-$pos_end;
	
	# --- Check either everything was caught correctly
	
	if($this_line =~ m/(\b���|\b���|\b��������|\b�����|\b����|\b�����|\b��������|\b�������|\b���|\b�����|\#|\�|N\.|\b��\b\.?|\bno\b\.?|\b�\.|\b��\.|\b���\.|\b��\.|\b�\.|\�)([�-��]{0,3}(?![\w]))?\s{0,2}$fractional_part_temp.{$rest_length,}$/i) { next; } # k paragrafu 1/3 (k paragrafu odin tri)
	
	if(($numerator_s =~ m/^[1,2]?\d{3}$/)&&($denominator_s =~ m/^\d{2}$/)) { next; } # like 1956/57.
	
	# *** Check either everything was caught correctly
	
	# if($prev_word !~ m/^(\-|\�|\�|\,|�|���)$/i) { $prev_case = ''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};};
	
	my $rest_length=length($this_line)-$pos_begin;
	if(($this_line !~ m/^.{$prev_end_position}(\s*(\b[�-��]+\b)?[\,\;\s]*).{$rest_length}$/i)||($prev_case eq ''))
		{
		$prev_case = ''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};
		}
	else
		{
		if($prev_word =~ m/^��$/i) { $special_preposition = '��';}
		}
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin);
	if($prev_word =~ m/[�-��]/i) { $line_before_number .= $prev_word; }
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		for($prev_word)
			{
			when (m/^(�|�|��|��|�����|�����|������|������|�����|�����|�����|�������������|����|���|��|����������[��]|�������|����������|��������[���])$/i)			{ $case = 'rod'; }
			when (m/^(��?)$/i)									{ $case = 'dat';}
			when (m/^(�����)$/i)								{ $case = 'tv'; }
			when (m/^(�|��|�)$/i)								{ $case = 'pr'; }
			when (m/^(��|��|��|�����|��������.{1,2})$/i)		{ $case = 'vin';}
			default												{ $case = 'im'; }
			}
		};
	
	if($case eq 'im')
		{
		if($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)
			{
			$case = $prev_case;
			}
		}
	
	# --- Check the word_part
	
	my $aux_phrase;
	my $gender;
	my $temp_case;
	my $number_interval;
	if(($case eq 'im')&&($prev_case eq ''))
		{
		$flag = 0;
		if($numerator_s =~ m/(^|[^1])1/) # 1/6 - shestaya
			{
			$gender = 'f';
			$temp_case='im';
			$number_interval='1';
			}
		elsif($numerator_s =~ m/(^|[^1])[234]/)
			{
			$gender = 'pl';
			$temp_case='im';
			$number_interval='2_4';
			}
		else
			{
			$gender = 'pl';	# 5/6 - shestyh
			$temp_case='rod';
			$number_interval='0_5_20';
			}
			
		$aux_phrase=num2word($denominator_s, 'por', $temp_case, $gender);
			
		if($aux_phrase =~ m/$word_part\s*$/i) { $case = 'im'; $flag=1; }
		
		# try to figure out the case
		if($flag == 0)
			{
			for(my $j=0; $j<6; $j++) # cases_base
				{
				if(($j == 0)||($j==3)) # im|vin
					{
					if(	(($number_interval eq '1')&&(num2word($denominator_s, 'por', $cases_base[$j], 'f') =~  m/$word_part\s*$/i)) ||
						(($number_interval eq '2_4')&&(num2word($denominator_s, 'por', $cases_base[$j], 'pl') =~  m/$word_part\s*$/i)) ||
						(($number_interval eq '0_5_20')&&(num2word($denominator_s, 'por', 'rod', 'pl') =~  m/$word_part\s*$/i)) )
							{
							$case = $cases_base[$j];
							$j=6;
							}
					}
				else
					{
					if(	(($number_interval eq '1')&&(num2word($denominator_s, 'por', $cases_base[$j], 'f') =~  m/$word_part\s*$/i)) ||
						(($number_interval =~ m/2_4|0_5_20/)&&(num2word($denominator_s, 'por', $cases_base[$j], 'pl') =~  m/$word_part\s*$/i)) )
							{
							$case = $cases_base[$j];
							$j=6;
							}
					}
				
				
				}
			}
		}
	
	# *** Check the word_part
	
	$prev_case = $case;
	
	$phrase .= ' '.$prev_word.' ';
	
	# Integer part
	if($integer_s =~ m/\d/)
		{
		if(($special_preposition =~ m/^(��)$/i) && ($integer_s =~ m/(^1|[^1]1)$/))
			{
			$phrase .= realnum2word_po($integer_s, 'kol' ,$case, 'f').' '.$celyh_adj{'dat'}{'f'}.' ';
			}
		elsif($integer_s =~ m/(^1|[^1]1)$/)
			{
			$phrase .= num2word($integer_s, 'kol' , $case, 'f').' '.$celyh_adj{$case}{'f'}.' ';
			}
		else
			{
			$phrase .= num2word($integer_s, 'kol' , $case, 'f').' ';
			if($case =~ m/(im|vin)/)
				{
				$phrase .= $celyh_adj{'rod'}{'pl'}.' ';
				}
			else
				{
				$phrase .= $celyh_adj{$case}{'pl'}.' ';
				}
			}
		}
	
	# Nominator and denominator
	
	if(($special_preposition =~ m/^(��)$/i) && ($numerator_s =~ m/(^1|[^1]1)$/))
		{
		$phrase .= realnum2word_po($numerator_s, 'kol' ,$case, 'f').' '.num2word($denominator_s, 'por', 'dat', 'f');
		}
	elsif($numerator_s =~ m/(^1|[^1]1)$/)
		{
		$phrase .= num2word($numerator_s, 'kol' , $case, 'f').' '.num2word($denominator_s, 'por', $case, 'f');
		}
	else
		{
		$phrase .= num2word($numerator_s, 'kol' , $case, 'f').' ';
		if($case =~ m/(im|vin)/)
			{
			$phrase .= num2word($denominator_s, 'por', 'rod', 'pl');
			}
		else
			{
			$phrase .= num2word($denominator_s, 'por', $case, 'pl');
			}
		}
	
	if($abbr ne '')
		{
		if($abbr =~ m/^\$$/) { $abbr = '�������'; }
		elsif($abbr =~ m/^\%$/) { $abbr = '��������'; }
		elsif($abbr =~ m/^\�$/) { $abbr = '����'; }
		else
			{ 
				if($shortcuts_rod{$abbr} !~ m/\w/) { $abbr = lc($abbr); }
				$abbr =~ s/^($shortcuts_pattern)$/$shortcuts_rod{$1}/i;
			}
		$phrase .= ' '.$abbr;
		
		# Treat the point (.) by the shortcuts properly
		if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*(\b[�-ߨA-Z]|$)/)) { $phrase .= '.'; }
		}
	else
		{
		$phrase .= $non_word.$non_word2.$point;
		}
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	$prev_end_position=$pos_begin+length($phrase);
	
}

if($this_line !~ m/\d/) { return $this_line; }

# ***** Find and replace (1) 2/6 kg, etc, including 1/10-oj kg

# * delete the spaces between numbers, like 1 000 000 words
$this_line =~ s/(?<=\d)\s(\d{3})\b/$1/g; # Lookbehind assertion

# ***** Find and replace the scores like number:number

while ($this_line =~ m/(\w*)(\s*)(\d+([\.\,]\d+)?)(\s*)\:\5(\d+([\.\,]\d+)?)\b/g)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase;
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $first_num_s=$3;			#as strings
	my $second_num_s=$6;		#as strings
	
	# $first_num_s =~ s/\ //g;
	# $second_num_s =~ s/\ //g;
	
	# if($matching =~ m/���������/){ print $matching."\n"; exit;}
	
	if($prev_word =~ m/^(��(�|�)�|����|������|�����|�������|�����|������)/i)
		{
		$phrase=$prev_word.$non_word1;
		$phrase=$phrase.realnum2word($first_num_s,'kol','im','m');
		if($prev_word =~ m/^������/i) { $phrase .= ' �� '; } else { $phrase .= ' '; }
		$phrase .= realnum2word($second_num_s,'kol','im','m');
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);	
		}
	elsif(($prev_word =~ m/^(�������|�������|�������|����|����|�����|������|����)/i)||(length($first_num_s)>3)||(length($second_num_s)>3))
		{
		$phrase=$prev_word.$non_word1;
		$phrase=$phrase.realnum2word($first_num_s,'kol','im','m')." � ".realnum2word($second_num_s,'kol','dat','m');
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
		
	# if($flag ==1) { print $phrase."\n"; }
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing	
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"Scores\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line; }
	
# ***** Find and replace the scores like number:number

# ----- detect the time data like: (preposition) (2)1:30(:56) and write it in letters

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $prev_case =''; # case on the previous iteration
my $special_preposition=''; # if the preposition "po" is met

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d{1,5})\s?(\:|\.)\s?(\d{2}(?!\.?\d))(\s?\4\s?(\d{2})(?!\.?\d))?(\s*)((a|p)\.?m\.?)?/ig) # It is called negative lookahead assertion (?!assertion)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase;
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $hour_s=$3;						#as strings
	my $min_s=$5;						#as strings
	my $sec_s=$7;						#as strings
	my $next_word=$9;
	
	if((int($min_s)>60)||(int($sec_s)>60)) { next;}
	if(($sec_s eq '')&&(int($hour_s)>24)) { $sec_s = $min_s; $min_s = $hour_s; $hour_s=''; }
	
	$phrase=$prev_word.$non_word1;
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much
	if(($prev_word !~ m/^(�����|������|�$|���$|$)/i) || (($prev_word =~ m/^(�|���|)$/i)&&($coming_before_s !~ m/\b(�����|������)[�-��]{0,3}\s*$/i))) { $prev_case = ''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	my $case='';
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�)$/i)	{ $case = 'dat'; }
		elsif($prev_word =~ m/^(�|�|��|��|�����|�����|������|������|�����|�����|�����|�������|������|����|���|��|����������[��]|�������|����������|��������[���])$/i) { $case = 'rod'; }
		elsif($prev_word =~ m/^(����|�����)$/i) 		{ $case = 'tv'; }
		elsif($prev_word =~ m/^(�|��)$/i) 				{ $case = 'pr'; }
		elsif($prev_word =~ m/^(��|��|�|�����|��)$/i) 	{ $case = 'vin'; }
		else											{ $case = 'im'; }
		}
		
	if($case eq 'im')
		{
		if($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)
			{
			$case = $prev_case;
			}
		}
	
	$prev_case = $case;
	
	# hours
	
	my $sin_or_pl = 'pl';
	if($hour_s =~ m/(^|[^1])1$/) { $sin_or_pl = 'sin'; }
	
	if($hour_s =~ m/\d/)
		{
		if((int($hour_s) == 1)&&(int($sec_s) == 0))
			{
			if($special_preposition eq '��') 	{ $phrase .= $chasy{sin}{dat}; }
			else 								{ $phrase .= $chasy{sin}{$case}; }
			}
		elsif($case =~ m/(im|vin)/)
			{
			if($hour_s =~ m/(^|[^1])1$/)
				{
				if($special_preposition eq '��') 	{ $phrase .= realnum2word_po($hour_s, 'kol', $case, 'm').' '.$chasy{sin}{dat}; }
				else 								{ $phrase .= num2word($hour_s, 'kol', $case, 'm').' '.$chasy{sin}{$case}; }
				}
			elsif($hour_s =~ m/(^|[^1])[2-4]$/)
				{
				$phrase .= num2word($hour_s, 'kol', $case, 'm').' '.$chasy{sin}{rod};
				}
			else
				{
				$phrase .= num2word($hour_s, 'kol', $case, 'm').' '.$chasy{pl}{rod};
				}
			}
		else
			{
			$phrase .= num2word($hour_s, 'kol', $case, 'm').' '.$chasy{$sin_or_pl}{$case};
			}
		}
	
	# minutes
	$sin_or_pl = 'pl';
	if($min_s =~ m/(^|[^1])1$/) { $sin_or_pl = 'sin'; }
	
	if((int($min_s) != 0)||($hour_s !~ m/\d/))
		{
		$phrase .= ' ';
		if($case =~ m/(im|vin)/)
			{
			if($min_s =~ m/(^|[^1])1$/)
				{
				if($special_preposition eq '��') 	{ $phrase .= realnum2word_po($min_s, 'kol', $case, 'f').' '.$minuty{sin}{dat}; }
				else 								{ $phrase .= num2word($min_s, 'kol', $case, 'f').' '.$minuty{sin}{$case}; }
				}
			elsif($min_s =~ m/(^|[^1])[2-4]$/)
				{
				$phrase .= num2word($min_s, 'kol', $case, 'f').' '.$minuty{sin}{rod};
				}
			else
				{
				$phrase .= num2word($min_s, 'kol', $case, 'f').' '.$minuty{pl}{rod};
				}
			}
		else
			{
			$phrase .= num2word($min_s, 'kol', $case, 'f').' '.$minuty{$sin_or_pl}{$case};
			}
		}
	
	
	# seconds
	
	$sin_or_pl = 'pl';
	if($sec_s =~ m/(^|[^1])1$/) { $sin_or_pl = 'sin'; }
	
	if(int($sec_s) != 0)
		{
		$phrase .= ' ';
		if($case =~ m/(im|vin)/)
			{
			if($sec_s =~ m/(^|[^1])1$/)
				{
				if($special_preposition eq '��') 	{ $phrase .= realnum2word_po($sec_s, 'kol', $case, 'f').' '.$sekundy{sin}{dat}; }
				else 								{ $phrase .= num2word($sec_s, 'kol', $case, 'f').' '.$sekundy{sin}{$case}; }
				}
			elsif($sec_s =~ m/(^|[^1])[2-4]$/)
				{
				$phrase .= num2word($sec_s, 'kol', $case, 'f').' '.$sekundy{sin}{rod};
				}
			else
				{
				$phrase .= num2word($sec_s, 'kol', $case, 'f').' '.$sekundy{pl}{rod};
				}
			}
		else
			{
			$phrase .= num2word($sec_s, 'kol', $case, 'f').' '.$sekundy{$sin_or_pl}{$case};
			}
		}
	
	
	if($next_word =~ m/^(a|p)\.m\.$/i) { if($this_line =~ m/\Q$matching\E\s*[�-ߨA-Z]/) { $next_word =~ s/\.//; } else {$next_word =~ s/\.//g; } } 
	elsif($next_word =~ m/^(a|p)\.m$/i) { $next_word =~ s/\.//; }
	
	# $next_word =~ s/^(a|p)m\.?(\.*)$/\1m\2/i;
	### Substitute the a.m. and p.m. by dnem, vecherom, utrom and so on
	# # # if((int($hour_s) eq 12)&&(int($min_s) eq 0))
		# # # {
		# # # $next_word =~ s/^a\.?m\.?(\.*)$/ ����\1/i;
		# # # $next_word =~ s/^p\.?m\.?(\.*)$/ ���\1/i;
		# # # }
	# # # elsif(int($hour_s) eq 12)
		# # # {
		# # # $next_word =~ s/^a\.?m\.?(\.*)$/ ����\1/i;
		# # # $next_word =~ s/^p\.?m\.?(\.*)$/ ���\1/i;
		# # # }
	# # # elsif(int($hour_s)<6)
		# # # {
		# # # $next_word =~ s/^a\.?m\.?(\.*)$/ ����\1/i;
		# # # $next_word =~ s/^p\.?m\.?(\.*)$/ ���\1/i;
		# # # }
	# # # elsif(int($hour_s)<12)
		# # # {
		# # # $next_word =~ s/^a\.?m\.?(\.*)$/ ����\1/i;
		# # # $next_word =~ s/^p\.?m\.?(\.*)$/ ������\1/i;
		# # # }
		
	$phrase = $phrase.' '.$next_word;
		
	# $this_line =~ s/\Q$matching/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
		
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"(2)1:30(:56)" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect the time data like: (preposition) (2)1:30(:56) and write it in letters

# *** delete the spaces inside numbers
# while($this_line =~ s/(\d+)\ (\.|\,\-|\�|\�)(\d+)/\1\2\3/g) {};
# while($this_line =~ s/(\d+)(\.|\,\-|\�|\�)\ (\d+)/\1\2\3/g) {};

# *** deleting the numbers with strange position of points
$this_line =~ s/\b[\d\.\,]+\b([\.\,]\d{0,2}[\.\,]|[\.\,]\d{4,}[\.\,])\b[\d\.\,]+\b/ /g;

# *** delete the delimiter between 1000s
# while($this_line =~ s/(\W+|^)(\d+)(\.|\,)(\d{3})(\W+)/\1\2\4\5/g){};
$this_line =~ s/\b(\d{1,3})(\.|\,)(?=(\d{3})\b)/$1/g;
	
# Delete the numbers like 1)
while($this_line =~ m/\d+\)(\W)/g)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $matching=$&;
	my $non_word = $1;
	my $rest_length=length($this_line)-$pos_begin;
	my $matching2='';
	if($this_line =~ m/(\(|\))[^\(\)]*.{$rest_length}$/) { $matching2 = $1; }
	
	if($matching2 !~ m/\(/)
		{
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$non_word/;
		pos($this_line)=$pos_begin+length($non_word);
		}
	else
		{
		pos($this_line)=$pos_begin+length($non_word);
		next;
		}
	}

if($this_line !~ m/\d/) { return $this_line; }

# ----- Replacing $ or � from beginning to the end $ 1000
while($this_line =~ m/
	(\$|\�)\s*							# currency $1
	(\d+([\,\.]\d+)?)					# number_s $2
	(\s*[\-\�\�]\s*(\d+([\,\.]\d+)?))?	# number2_s $5
	\s*
	((�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\b)?	# tysjach $7
	(\.?)								# point $9
	/xig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $matching=$&;
	my $currency=$1;
	my $number_s=$2;
	my $number2_s=$5;
	my $tysjach=$7;
	my $point=$9;
	
	my $phrase=$number_s;
	if($number2_s =~ m/\d/) { $phrase .= '-'.$number2_s; }
	if($tysjach =~ m/\w/)
		{
		$phrase .= ' '.$tysjach;
		if($point eq '.')
			{
			if($tysjach =~ m/^(�����|�������|��������|��������)/i)
				{
				$phrase .= ' '.$currency.'.'
				}
			elsif($this_line =~ m/^.{$pos_end}(\s+[�-ߨA-Z]|\s*$)/)
				{
				$phrase .= '. '.$currency.'.';
				}
			else
				{
				$phrase .= '. '.$currency;
				}
			}
		else
			{
			$phrase .= ' '.$currency;
			}
		}
	else
		{
		$phrase .= ' '.$currency.' '.$point;
		}
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	}
	
# ***** Replacing $ or � from beginning to the end $ 1000

# ----- Detect 5 iz 36 | ot 15 do 30 word2, s 15 do 30 word2 | s 15 po 20 word

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(([�-��]+)\s+)?(\d+([\.\,]\d+)?)(\s+(��|��|��)\s+)(\d+([\.\,]\d+)?)(?=(\s*([�-��a-z\%\$\�]+))?(\s+([�-��a-z]+))?)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase='';
	my $matching=$&;
	my $prev_word=$2;
	my $number_s=$3;								#as strings
	my $connection=$6;
	my $number2_s=$7;
	my $next_word=$10;
	my $next_word2=$12;
	
	my $case='';
	my $gender='m';
	my $kol_por='kol';
	my $case2='rod'; # the case of the second word
	if(($prev_word =~ m/^�$/i)&&($connection =~ m/^��$/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/)) { $kol_por = 'por'; $case2='vin'; }
	elsif($connection =~ m/^��$/i) { $case2='vin'; }
	if($prev_word =~ m/^(�|�|��|��|�����|�����|������|������|�����|�����|�����|�������|������|����|���|��|����������[��]|�������|����������|��������[���])$/i) # (Rod)
		{
		$case = 'rod';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
		{
		$case = 'dat';
		}
	elsif($prev_word =~ m/^(�����)$/i) # (Tv)
		{
		$case = 'tv';
		}
	elsif($prev_word =~ m/^(�|��|�)$/i) # (Pr)
		{
		$case = 'pr';
		}	
	elsif($prev_word =~ m/^(��|��|��|�����)$/i) # (Vin)
		{
		$case = 'vin';
		}
	else # (Im)
		{
		$case = 'im';
		}
	
	$phrase = $prev_word.' ';	
	
	if($next_word =~ m/^(\b($shortcuts_pattern)\b|\%|\$|\�)$/i)
		{
		if($next_word =~ m/^[\$\%\�]$/) { $gender='m'; }
		else { ($gender,) = get_abbr_props($next_word); };
		}
	elsif($next_word =~ m/^(��?\b|��?\b|���|���)(|�|��)$/i)
		{
		$gender='m';
		}
	elsif(($next_word =~ m/[�-��]/i)&&(($number_s =~ m/^(|\d*[^1\D])[12]$/)||($number2_s =~ m/^(|\d*[^1\D])[12]$/)))
		{
		# Read the properties of the word from mystem program;
		my @morph_properties=get_morph_props($next_word, '-i -w');
		my $flag=0; # Shows that the word is a noun
		my $flag_adj=0;
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/) && ($morph_properties[$i]{case} eq $case2)) {$flag=1; $gender=$morph_properties[$i]{gender}; $i=$#morph_properties; }
			}
		
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{	
			if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties[$i]{adj_form} ne '��')) { $flag_adj=1; $flag=0; $i=$#morph_properties; if($morph_properties[$i]{number} ne 'pl') { $gender=$morph_properties[$i]{gender}; } } # e.g. drugih is a APRO and S both
			}
		
		if(($flag == 0)&&($flag_adj == 1))
			{
			@morph_properties=get_morph_props($next_word2, '-i -w');
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/) && ($morph_properties[$i]{case} eq $case2)) {$flag=1; $gender=$morph_properties[$i]{gender}; $i=$#morph_properties; }
				}
			}
		}
	
	# the first number
	if($prev_word =~ m/^��$/i)
		{
		$phrase .= realnum2word_po($number_s, 'kol', $case, $gender); # case is not needed
		}
	elsif($kol_por eq 'por')
		{
		$phrase .= num2word($number_s, 'por', $case, $gender);
		}
	else
		{
		$phrase .= realnum2word($number_s, 'kol', $case, $gender);
		}
	
	# the second number
	
	$phrase .= ' '.$connection.' ';
	
	# if((isAbbreviation($next_word, 'start_of_line') == 1)||($next_word =~ m/^(��������|��������|�����|�������|��������|��������)[�-��]{0,3}$/i)||(length($next_word) == 1)||((get_abbr_props($next_word) =~ m/\w/)&&($next_word =~ m/^([a-z]*|��|���|���|��|��)$/i)))
	if(($next_word =~ m/^($shortcuts_pattern)$/i)||($next_word =~ m/^(��������|��������|�����|�������|��������|��������)[�-��]{0,3}$/i)||(length($next_word) == 1)||((get_abbr_props($next_word) =~ m/\w/)&&($next_word =~ m/^([a-z]*|��|���|���|��|��)$/i)))
		{
		if($kol_por eq 'por')
			{
			$phrase .= $number2_s.'-'.substr(num2word($number2_s, 'por', $case2, $gender),-3, 3);
			}
		elsif($connection =~ m/^��$/i)
			{
			$phrase .= $number2_s.'-'.substr(realnum2word_po($number2_s, 'kol', $case2, $gender),-3, 3); # case is not needed
			}
		else
			{
			$phrase .= $number2_s.'-'.substr(realnum2word($number2_s, 'kol', $case2, $gender),-3, 3);
			}
		}
	else
		{
		if($kol_por eq 'por')
			{
			$phrase .= num2word($number2_s, 'por', $case2, $gender);
			}
		elsif($connection =~ m/^��$/i)
			{
			$phrase .= realnum2word_po($number2_s, 'kol', $case2, $gender); # case is not needed
			}
		else
			{
			$phrase .= realnum2word($number2_s, 'kol', $case2, $gender);
			}
		}
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase /;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line; }

# ***** Detect 5 iz 36 | ot 15 do 30 word2, s 15 do 30 word2 | s 15 po 20 word

# ----- Detect the chasi, min, sek, milliseconds and gradusy with numbers

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $prev_case =''; 				# case on the previous iteration
my $prev_kol_por='';			# kol_por on the previous iteration
my $special_preposition=''; 	# if the preposition "po" is met
# my $prev_end_position=0;		# End position of the previous matching

while ($this_line =~ m/(\b[�-��]*\b)?([^\w\-\�\�]*)([\-\�\�]\s?)?\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?((((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s+(?=([��][�-��]*\.?\s?)?\b(���|�|���|���|��|�����������|����)([�-��]*\b\.?))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $minus=$3;
	my $number_s=$4;								#as strings
	
	my $word_part1=$8;
	my $and_or=$15;
	my $number2_s=$16;
	my $word_part2=$20;
	my $tysjach=$21;
	my $first_word_part=$22;
	my $second_word_part=$23;					#as strings
	
	my $kol_por='kol'; # Here we may add exceptions;
	
	my @number=split(/[\.\,]/, $number_s);
	if($#number+1 > 2) { next;}
	my @number2=split(/[\.\,]/, $number2_s);
	if($#number2+1 > 2) { next;}
	
	$phrase = $phrase.$prev_word.$non_word1;
	
	if($minus =~ m/[\-\�\�]/) { $phrase .= '����� ';};
	
	my $next_word = $first_word_part.$second_word_part;
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}

	# check, either everything was caught correctly
	my $unit;
	if($second_word_part =~ m/\.$/) { $unit =1;} else {$unit=0;}
	
	if($second_word_part =~ m/[�-��]/i)
		{
		if(($next_word =~ m/^�/i)&&(($next_word !~ m/^���/i)||(length($next_word)>6+$unit))) { next;}
		elsif(($next_word =~ m/^���/i)&&(($next_word !~ m/^�����/i)||(length($next_word)>8+$unit))) { next;}
		elsif(($next_word =~ m/^���/i)&&(($next_word !~ m/^������/i)||(length($next_word)>9+$unit))) { next;}
		elsif(($next_word =~ m/^����/i)&&(($next_word !~ m/^������/i)||(length($next_word)>9+$unit))) { next;}
		elsif($next_word =~ m/^��/i) { next;}
		elsif(($next_word =~ m/^�����������/i)&&(length($next_word)>14+$unit)) { next;}
		}
	
	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	
	# check, either to use the same to the previous case and preposition
	my $word='';
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much
	if(($prev_word !~ m/^(���|�����|������|�����������|������|������|�����|�$|���$|$)/i) || (($prev_word =~ m/^(�|���|)$/i)&&($coming_before_s !~ m/\b(���|�����|������|�����������|������|������|�����)[�-��]{0,3}\s*$/i))) { $prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# ----- determine the interval of a number
	my $number_interval; my $gender;
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}

	# --- processing exceptions
	if(($prev_word =~ m/^�$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && (($tysjach =~ m/[��]\s?$/i) || ($next_word =~ m/(�|�)\.?$/i))) { $case = 'pr'; } # V 2 munitah
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 minutami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions
	
	if($next_word =~ m/^�/i) { $gender='m';}
	elsif($next_word =~ m/^���/i) { $gender='f';}
	elsif($next_word =~ m/^���/i) { $gender='f';}
	elsif($next_word =~ m/^(��\.?$|�����������)/i) { $gender='f';}
	elsif($next_word =~ m/^(����)/i) { $gender='m';};

	if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }	
			}
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;	
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif($special_preposition =~ m/^(��)$/i)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif($special_preposition =~ m/^(��)$/i)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
			
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
			
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}	
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) eq 0) ) ) # for a case if 0,5
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
		if($number2_s =~ m/\d/)
			{
			$sin_or_pl='pl';
			}
		else
			{
			$sin_or_pl='sin';
			}
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};
			
		}
	
	if($next_word =~ m/^(�|���)\.?$/i) {$word = $chasy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) { $word = $minuty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) { $word = $sekundy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) { $word = '�����'.$sekundy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^����\.?$/i) { $word = $gradusy{$sin_or_pl}{$case};}
	else {$word = $next_word;}
	
	$phrase = $phrase.' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach$next_word\E/${1}$phrase/;
	
	pos($this_line)=$pos_begin+length($phrase);
	# if($phrase !~ m/\.$/)	# not necessary any more (the previous_case determination was changed)
		# { pos($this_line) -= length($word); }
		
	# $prev_end_position=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"Detect the chasi\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect the chasi, min, sek, milliseconds and gradusy with numbers

# ----- Detect the (kv)  dm, mm, m, cm with numbers

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $prev_case =''; 				# case on the previous iteration
my $prev_kol_por='';			# kol_por on the previous iteration
my $special_preposition=''; 	# if the preposition "po" is met
# my $prev_end_position=0;		# End position of the previous matching

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?((((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=([��][�-��]*\b\.?\s?)?(��\.?(?![\w])\s*|����\.?(?![\w])\s*|��������[�-��]{0,3}\b\s*|���\.?(?![\w])\s*|����\.?(?![\w])\s*|��������[�-��]{0,3}\b*\s)?(��\b|��\b|��\b|��\b|��\b|�\b|���\b|��\b|\b��������|\b������|\b����|\b��������|\b���������|\b���������|\b���������|\b��������|���|\~dju\~(?!\w))([�-��]{0,3}(?![\w])\.?))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case ='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	my $word_part1=$7;
	my $and_or=$14;
	my $number2_s=$15;
	my $word_part2=$19;
	my $tysjach=$20;
	my $kv_or_kub=$21;
	my $first_word_part=$22;
	my $second_word_part=$23;					#as strings
	
	my $kol_por='kol'; # Here we may add exceptions;
	
	my @number=split(/[\.\,]/, $number_s);
	if($#number+1 > 2) { next;}
	my @number2=split(/[\.\,]/, $number2_s);
	if($#number2+1 > 2) { next;}

	$phrase = $phrase.$prev_word.$non_word1;
	my $next_word = $first_word_part.$second_word_part;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check, either everything was caught correctly
	# my $unit;
	# if($second_word_part =~ m/\.$/) { $unit =1;} else {$unit=0;}
	
	# if($second_word_part =~ m/[�-��]/i)
		# {
		# if($next_word =~ m/^��/i) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		# elsif( ($next_word =~ m/^�/i) && ($next_word !~ m/^����/i) && ($next_word !~ m/^���������/i) && ($next_word !~ m/^���/i)) { next;}
		# elsif( (($next_word =~ m/^����/i)&&(length($next_word)>7+$unit)) || (($next_word =~ m/^���������/i)&&(length($next_word)>12+$unit)) ) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		# elsif(($next_word =~ m/^��������/i)&&(length($next_word)>11+$unit)) { next;}
		# elsif(($next_word =~ m/^������/i)&&(length($next_word)>9+$unit)) { next;}
		# elsif(($next_word =~ m/^��������/i)&&(length($next_word)>11+$unit)) { next;}
		# elsif(($next_word =~ m/^���������/i)&&(length($next_word)>12+$unit)) { next;}
		# elsif(($next_word =~ m/^���/i)&&(length($next_word)>6+$unit)) { next;}
		# }
	
	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	
	my $word='';
	
	# # # # if the number of words between the same previous case is more then 1, delete the previous case and kol_por 
	# # # my $rest_length=length($this_line)-$pos_begin;
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much 
	if(($prev_word !~ m/^(��������|������|����|��������|���������|���������|���������|��������|���|����|�����|�����|������|������|���[�]�|����|�$|���$|$)/i) || (($prev_word =~ m/^(�|���|)$/i)&&($coming_before_s !~ m/\b(��������|������|����|��������|���������|���������|���������|��������|���|����|�����|�����|������|������|���[�]�|����)[�-��]{0,3}\s*$/i))) { $prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};

	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|�)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}
	
	# --- processing exceptions
	if($prev_word =~ m/^�$/)
		{
		if($this_line =~ m/(��������|�����|�������|�����������|������[�]�����)[�-��]{1,4}\s+$matching/i)
			{
			$case = 'vin';
			}
		}
	
	if(($prev_word =~ m/^�$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && ($next_word !~ m/(�|�)\.?$/i) && ((($tysjach !~ m/\w/)&&($next_word !~ m/^(��|��|�|��|��|��|���|���|��)\.?$/i))||(($tysjach =~ m/\w/)&&($tysjach !~ m/[��\.]\s?$/i)))) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 santimetrov"
	
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 kilometrami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; }	# Singular form
	
	# *** processing exceptions
	
	$gender='m';
	
	if($next_word =~ m/^(��\.?$|��������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|������)/i) { $gender='m';}
	elsif($next_word =~ m/^(�\.?$|����)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|��������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(���\.?$|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|��������)/i) { $gender='m';}
	elsif($next_word =~ m/^(���)/i) { $gender='f';};
	
	if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
		
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }	
			}
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}		
		}
			
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif($special_preposition =~ m/^(��)$/i)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif($special_preposition =~ m/^(��)$/i)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
			
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}	
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
		if($number2_s =~ m/\d/)
			{
			$sin_or_pl='pl';	
			}
		else
			{
			$sin_or_pl='sin';
			}
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};
			
		}
	
	if($kv_or_kub =~ m/^(��|���)/i)
		{
		if( (int($last_number[1]) > 0) && ($tysjach !~ m/^�/i) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$case='rod';
			}
		elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
			{
			$case='rod'; $gender='pl';
			}
		elsif($tysjach =~ m/^�/i)
			{
			$case='rod'; $gender='pl';
			}
		elsif(($kol_por eq 'por')||($number_interval eq '1'))
			{
			if($sin_or_pl eq 'pl') { $gender = 'pl'};
			}
		else
			{
			if($case =~ m/im|vin/)
				{
				$case='rod'; $gender='pl';
				}
			else
				{
				$gender='pl';
				}
			}
		
		if($kv_or_kub =~ m/^��/i) { $phrase.=' '.$kv_adj{$case}{$gender};} else { $phrase.=' '.$kub_adj{$case}{$gender};}
		}

	if($next_word =~ m/^��\.?$/i) { $word = '����'.$metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) { $word = $gektary{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\.?$/i) {  $word = $metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) {  $word =  '����'.$metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) { $word =  '�����'.$metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) { $word =  '�����'.$metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) { $word =  '�����'.$metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) { $word =  '����'.$metry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) { $word =  $mili{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^\~dju\~$/i) { $word =  $djujmy{$sin_or_pl}{$case};}
	else {$word = $next_word;}	
	
	$phrase = $phrase.' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$kv_or_kub$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach$kv_or_kub$next_word\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
		
	# $prev_end_position=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

$this_line =~ s/\~dju\~//gi;

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"Detect the (kv)  ��" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect the (kv)  ��, dm, mm, m, cm, ga with numbers

# ----- Detect (ticyach) rubley, kron, dollarov, chelovek, procentov (%)

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?((((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s+(?=([��][�-��]*\.?\s?)?(\b����|\b������|\b����|\b����|\b�������|\b���|\b���|\b���|\b����|\b�|\b����|\b����|\%|\$|\�)([�-��]*\.?))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case ='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	
	my $word_part1=$7;
	my $and_or=$14;
	my $number2_s=$15; #12
	my $word_part2=$19;
	my $tysjach=$20;
	my $first_word_part=$21;
	my $second_word_part=$22;					#as strings
	
	my $kol_por='kol'; # Here we may add exceptions;
	
	my @number=split(/[\.\,]/, $number_s);
	if($#number+1 > 2) { next;}
	my @number2=split(/[\.\,]/, $number2_s);
	if($#number2+1 > 2) { next;}
	
	$phrase = $phrase.$prev_word.$non_word1;
	my $next_word = $first_word_part.$second_word_part;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check, either everything was caught correctly
	
	my $unit;
	if($second_word_part =~ m/\.$/) { $unit =1;} else {$unit=0;}
	
	if($second_word_part =~ m/[�-��]/i)
		{
		if( ($next_word =~ m/^����/i)&&(length($next_word)>7+$unit) ) { next;}
		elsif( ($next_word =~ m/^������/i)&&(length($next_word)>9+$unit) ) { next;}
		elsif($next_word =~ m/^����/i) { next;}
		elsif( ($next_word =~ m/^����/i)&&(length($next_word)>7+$unit) ) { next;}
		elsif( ($next_word =~ m/^�������/i)&&(length($next_word)>10+$unit) ) { next;}
		elsif( ($next_word =~ m/^���/i) && ($next_word !~ m/^�������/i) ) { next;}
		elsif( ($next_word =~ m/^�������/i)&&(length($next_word)>10+$unit) ) { next;}
		elsif( ($next_word =~ m/^�/i) && ($next_word !~ m/^���/i) ) { next;}
		elsif( ($next_word =~ m/^���/i) && ($next_word !~ m/^����/i) ) { next;}
		elsif( ($next_word =~ m/^����/i)&&(length($next_word)>7+$unit) ) { next;}
		elsif( ($next_word =~ m/^���/i) && ($next_word !~ m/^����/i) ) { next;}
		elsif( ($next_word =~ m/^���/i)&&(length($next_word)>9+$unit) ) { next;}
		elsif( ($next_word =~ m/^����/i)&&(length($next_word)>7+$unit) ) { next;}
		elsif($next_word =~ m/^\%/) { next;}
		elsif($next_word =~ m/^\$/) { next;}
		elsif($next_word =~ m/^\�/) { next;}
		}
	
	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	
	my $word='';
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much
	if(($prev_word !~ m/^(����|������|����|����|����|�������|����|�������|����|����[��]�|����|�$|���$|$)/i) || (($prev_word =~ m/^(�|���|)$/i)&&($coming_before_s !~ m/\b(����|������|����|����|����|�������|����|�������|����|����[��]�|����)[�-��]{0,3}\s*$/i))) { $prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}

	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# Number for human cannot be fractional
	if($next_word =~ m/^���/i)
		{
		if($#number+1 == 2) { $number[0] .= $number[1]; delete $number[1]; };
		if($#number2+1 == 2) { $number2[0] .= $number2[1]; delete $number2[1]; };
		};
	
	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|��?|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}
	
	# --- processing exceptions
	if(($prev_word =~ m/^�$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && (($tysjach =~ m/[��]\s?$/i) || ($next_word =~ m/(�|�)\.?$/i))) { $case = 'pr'; } # V 15 tysjachah

	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 rublyami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions
		
	if($next_word =~ m/^(����)/i) { $gender='f';}
	elsif($next_word =~ m/^(\$|������|����)/i) { $gender='m';}
	elsif($next_word =~ m/^(\�|����)/i) { $gender='m';}
	elsif($next_word =~ m/^(����)/i) { $gender='m';}
	elsif($next_word =~ m/^(\%|�������|����|�������)/i) { $gender='m';}
	elsif($next_word =~ m/^(���)/i) { $gender='m';}
	elsif($next_word =~ m/^(�)/i) { $gender='m';}
	elsif($next_word =~ m/^(���)/i) { $gender='f';}
	elsif($next_word =~ m/^(����)/i) { $gender='m';}
	
	if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
		
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/)) # both endings are not empty
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/)) # one of the -omu endings is not empty
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s; } else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;

			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;

	if($number2_s =~ m/\d/)
		{
		if(($case eq 'vin')&&($next_word =~ m/^(���)/i)&&($tysjach !~ m/\w/i))
			{
			if($number[0] =~ m/(^|[^\D1])1$/) { $first_number_case = 'rod'; } else { $first_number_case =  $case; };
			if($number2[0] =~ m/(^|[^\D1])1$/) { $second_number_case = 'rod'; } else { $second_number_case =  $case; };
			if($kol_por eq 'por') { $first_number_case = 'rod'; $second_number_case = 'rod';}
			}
		elsif(($case eq 'im')&&($next_word =~ m/^(��������\b)/i)&&($tysjach !~ m/\w/i))
			{
			if($number[0] =~ m/(^|[^\D1])1$/) { $first_number_case = 'rod'; } else { $first_number_case =  $case; };
			if($number2[0] =~ m/(^|[^\D1])1$/) { $second_number_case = 'rod'; } else { $second_number_case =  $case; };
			if($kol_por eq 'por') { $first_number_case = 'rod'; $second_number_case = 'rod';}
			}
		else
			{
			$first_number_case = $case;
			$second_number_case = $case;
			}
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif($special_preposition =~ m/^(��)$/i)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		if(($case eq 'vin')&&($next_word =~ m/^(���)/i)&&($tysjach !~ m/\w/i))
			{
			if($number[0] =~ m/(^|[^\D1])1$/) { $first_number_case = 'rod';} else { $first_number_case =  $case;};
			if($kol_por eq 'por') { $first_number_case = 'rod';}
			}
		elsif(($case eq 'im')&&($next_word =~ m/^(��������\b)/i)&&($tysjach !~ m/\w/i))
			{
			if($number[0] =~ m/(^|[^\D1])1$/) { $first_number_case = 'rod';} else { $first_number_case =  $case;};
			if($kol_por eq 'por') { $first_number_case = 'rod';}
			}	
		else
			{
			$first_number_case = $case;
			}			
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif($special_preposition =~ m/^(��)$/i)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
			
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
			
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
		if($number2_s =~ m/\d/)
			{
			$sin_or_pl='pl';
			}
		else
			{
			$sin_or_pl='sin';
			}	
			
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			$sin_or_pl='sin';
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			elsif(($case =~ m/(im|vin)/)&&($next_word =~ m/^���/i))
				{
				$case='rod';
				}
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};
			
		};
	
	if($next_word =~ m/^(\$|����\b)/i) { $word = $dollary{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^\�/i) { $word = $evro{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(\%|����\b)/i) { $word = $procenty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) { $word = $chelovek{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(�|���)\.?$/i) { $word = $rubli{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) { $word = $kopejki{$sin_or_pl}{$case};}
	else {$word = $next_word;}

	$phrase = $phrase.' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }
	
	# $this_line =~ s/\Q$matching$tysjach$next_word\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach$next_word\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	# if($phrase !~ m/\.$/)
		# { pos($this_line) -= length($word); }
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"Detect (ticyach) rubley" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# print $this_line."\~\n";
# exit;

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect (ticyach) rubley, kron, dollarov, chelovek

# ----- Detect the tonn, tchentnerov, kg, g, mg

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?((((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=([��][�-��]*\b\.?\s?)?(\b����|\b�������|\b���������|\b�����|\b����������|\b����|\b����|\b��������|\b��������|\b��������|\b�����|\b���������|\b���������|\b����������|\b����������|\b�����|\b���������|\b���������|\b������|\b����������|\b����������|\b����|�\b|�\b|��\b|��\b|�\b|��\b|�\b|��\b|��\b|���\b|���\b|���\b|(?-i:�\b|�\b|A\b)|��\b|��\b|���\b|kA\b|MA\b|mkA\b|��\b|���\b|���\b|����\b|��\b|���\b|���\b|��\b|���\b|���\b|Pa\b|kPa\b|MPa\b|W\b|kW\b|Watt\b|V\b|kV\b|Volt\b|���\b|��\b)([�-��]{0,3}(?![\w])\.?))/ig)
	{ # tonny at the end, because of the tysjachi
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	
	my $word_part1=$7;
	my $and_or=$14;
	my $number2_s=$15; #12
	my $word_part2=$19;
	my $tysjach=$20;
	my $first_word_part=$21;
	my $second_word_part=$22;					#as strings
	
	my $kol_por='kol'; # Here we may create exceptions
	
	my @number=split(/[\.\,]/, $number_s);
	if($#number+1 > 2) { next;}
	my @number2=split(/[\.\,]/, $number2_s);
	if($#number2+1 > 2) { next;}
	
	$phrase = $phrase.$prev_word.$non_word1;
	my $next_word = $first_word_part.$second_word_part;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# --- check either everything was caught correctly
	
	if(($next_word =~ m/^(�|�)$/)&&($this_line =~ m/^\s*\d+\s+(�|�)/)&&(theTopicIs($this_line) !~ m/electricity/)) { next; }	# For example, to exclude: 1 � ���������� ���� 10 ����������. - ����. �����.
	if(($next_word =~ m/^W\.?$/)&&(theTopicIs($this_line) =~ m/geo-location/)) { next; } # For example, ����� ������ � ����������� NW1/4W �� �������
	
	# my $unit;
	# if($second_word_part =~ m/\.$/) { $unit =1;} else {$unit=0;}
	
	# if($second_word_part =~ m/[�-��]/i)
		# {
		# if( ($next_word =~ m/^�/i) && ($next_word !~ m/^����/i) ) { next;}
		# elsif( ($next_word =~ m/^�/i) && ($next_word !~ m/^�������/i) ) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		# elsif( ($next_word =~ m/^�/i) && ($next_word !~ m/^��\.?$/i) && ($next_word !~ m/^�����/i)) { next;}
		# elsif( (($next_word =~ m/^�����/i)&&(length($next_word)>8+$unit)) ) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		# elsif( ($next_word =~ m/^�/i) && ($next_word !~ m/^����/i) ) { next;}
		# elsif($next_word =~ m/^��/i) { next;}
		
		# elsif( ($next_word =~ m/^����/i) && (length($next_word)>7+$unit) ) { next;}
		# elsif( ($next_word =~ m/^�������/i) && (length($next_word)>10+$unit) ) { next;}
		# elsif( ($next_word =~ m/^���������/i) && (length($next_word)>12+$unit) ) { next;}
		# elsif( ($next_word =~ m/^�����/i) && (length($next_word)>8+$unit) ) { next;}
		# elsif( ($next_word =~ m/^����������/i) && (length($next_word)>13+$unit) ) { next;}
		# elsif( ($next_word =~ m/^����/i) && (length($next_word)>7+$unit) ) { next;}
		# elsif( ($next_word =~ m/^���������/i) && (length($next_word)>12+$unit) ) { next;}
		# }
	
	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	if(($next_word =~ m/^�\.?$/i)&&($this_line =~ m/^.{$pos_end}([��][�-��]*\.?\s?)?\b(�\.\s*�\.)/i)) { next; } # for loshadinyh sil and litrov
	if(substr($this_line, $pos_end) =~ m/^\s*�\.\s+(��\s+|)�����\s+���/) { next; } # only for gramms it is necessary
	
	# *** check either everything was caught correctly
	
	my $word='';
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much 
	if($coming_before_s !~ m/\b(����|�������|���������|�����|����������|����|���������)[�-��]{0,3}\s*(\b[�-��a-z\-]+)?\s*[\,\;]?\s*$/i) { $prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};} # the other shortcuts are not in the same class # prev_word like kilogrammy from the previous iteration is not caught
	else
		{
		if($prev_word =~ m/^��$/i) { $special_preposition = '��';}
		}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};

	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}
	
	# --- processing exceptions
	if($prev_word =~ m/^�$/)
		{
		if($this_line =~ m/\b(���|���|���������|�������|����|�������|���|���������|���|������������)[�-��]{0,3}(\s+(��������|��������������))?\s+$matching/i)
			{
			$case = 'vin';
			}
		}
	
	if(($prev_word =~ m/^�$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && ($next_word !~ m/(�|�)\.?$/i) && ((($tysjach !~ m/\w/)&&($next_word !~ m/^(�|�|��|��?|��|�|��|��|���|���|���|�|���|�|��|��|mkA|A|kA|Ma|��|���|���|����|��|���|���|��|���|���|Pa|kPa|MPa|W|kW|Watt|V|kV|Volt|���|��)\.?$/i))||(($tysjach =~ m/\w/)&&($tysjach !~ m/[��\.]\s?$/i)))) { $case = 'vin'; } # V 18 tysjach tonn, e.g.
	
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 tonnami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions
	
	if($next_word =~ m/^(�\.?$|����)/i) { $gender='f';}
	elsif($next_word =~ m/^(�\.?$|�������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��?\.?$|�����)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|����������)/i) { $gender='m';}
	elsif($next_word =~ m/^(�\.?$|����)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���\.?$|���\.?$|���\.?$|W\.?$|kW\.?$|Watt|����|��������|��������|��������)/i) { $gender='m';}
	elsif($next_word =~ m/^(�\.?$|V\.?$|kV\.?$|Volt|�����|���������|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(���\.?$|�\.?$|��\.?$|��\.?$|mkA\.?$|A\.?$|kA\.?$|Ma\.?$|����������|����������|�����|���������|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���\.?$|���\.?$|����\.?$)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|���\.?$|���\.?$|��\.?$|���\.?$|���\.?$|Pa\.?$|kPa\.?$|MPa\.?$|������|����������|����������)/i) { $gender='m';}
	elsif($next_word =~ m/^(���\.?$|���������)/i) { $gender='m';}
	elsif($next_word =~ m/^(��\.?$|����)/i) { $gender='f';}
	
	if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;	
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}	
		}
			
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
			
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if(($special_preposition =~ m/^(��)$/i)&&($case eq 'rod')) { $case = 'dat'; }
		if($number2_s =~ m/\d/)
			{
			$sin_or_pl='pl';
			}
		else
			{
			$sin_or_pl='sin';
			}
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};	
		}
	
	if($next_word =~ m/^�\.?$/i) 						{ $word = $tonny{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\.?$/i) 					{ $word = $centnery{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) 					{ $word = '����'.$grammy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��?\.?$/i) 					{ $word = $grammy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) 					{ $word = '�����'.$grammy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\.?$/i) 					{ $word = $litry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) 					{ $word = '�����'.$litry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��\.?$|W\.?$|Watt)/i) 		{ $word = $vatty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?$|kW\.?$)/i) 			{ $word = '����'.$vatty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?$)/i) 				{ $word = '����'.$vatty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?$)/i) 				{ $word = '����'.$vatty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(�\.?$|V\.?$|Volt)/i) 		{ $word = $volty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(kV\.?$)/i) 					{ $word = '����'.$volty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^((���|mkA)\.?$)/i)			{ $word = '�����'.$ampery{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^((��|mA)\.?$)/) 				{ $word = '�����'.$ampery{$sin_or_pl}{$case};} # case sensitive
	elsif($next_word =~ m/^((�|A)\.?$)/) 				{ $word = $ampery{$sin_or_pl}{$case};}	# case sensitive
	elsif($next_word =~ m/^((��|kA)\.?$)/i)				{ $word = '����'.$ampery{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^((��|MA)\.?$)/)				{ $word = '����'.$ampery{$sin_or_pl}{$case};} # case sensitive
	elsif($next_word =~ m/^(��)\.?$/i) 					{ $word = $omy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���)\.?$/i)					{ $word = '����'.$omy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���)\.?$/)					{ $word = '����'.$omy{$sin_or_pl}{$case};} # case sensitive
	elsif($next_word =~ m/^(���)\.?$/i)					{ $word = '�����'.$omy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(����)\.?$/i)				{ $word = '�����'.$omy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��\.?$|��\.?$|Pa\.?$)/i)	{ $word = $paskali{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?$|���\.?$|kPa\.?$)/i)	{ $word = '����'.$paskali{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?$|���\.?$|MPa\.?$)/i)	{ $word = '����'.$paskali{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?$)/i) 				{ $word=$jekzempljary{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��\.?$|����)/i) 			{ $word=$shtuki{$sin_or_pl}{$case};}
	else {$word = $next_word;}
	
	$phrase .= ' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }
	
	# $this_line =~ s/\Q$matching$tysjach$next_word\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach$next_word\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	# if($phrase !~ m/\.$/)
		# { pos($this_line) -= length($word); }
	
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"Detect the tonn" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect the tonn, tch, kg, g, mg

# ----- Detect other shortcuts with cardinal numbers, more probable

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?((((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=([��][�-��]*\b\.?\s?)?(\b����|\b��������|\b��������|\b��������|\b���������|\b���|\b�������|\b�������|\b�������|\b��������|��\b|���\b|���\b|���\b|hz\b|khz\b|mhz\b|ghz\b|��\b|lm\b|��\.|��\.|��\b|����\b|�����\b|tb\b|tbit\b|tbyte\b|��\b|����\b|�����\b|gb\b|gbit\b|gbyte\b|��\b|����\b|�����\b|mb\b|mbit\b|mbyte\b|��\b|����\b|�����\b|kb\b|kbit\b|kbyte\b|����\b|���\b|byte\b|bit\b|(?-i:��\b|��\b)|��\b|���\b|���\b|��\b|���\b|���\b|�\.\s*�\.|�\.\s*�\.|$months_rod_short_pattern)((?!\w)\.?))/ig)
	{ # tonny at the end, because of the tysjachi
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	
	my $word_part1=$7;
	my $and_or=$14;
	my $number2_s=$15; #12
	my $word_part2=$19;
	my $tysjach=$20;
	my $first_word_part=$21;
	my $second_word_part=$22;					#as strings
	
	my @number=split(/[\.\,]/, $number_s);
	if($#number+1 > 2) { next;}
	my @number2=split(/[\.\,]/, $number2_s);
	if($#number2+1 > 2) { next;}
	
	$phrase = $phrase.$prev_word.$non_word1;
	my $next_word = $first_word_part.$second_word_part;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	my $kol_por='kol'; # Here we may create exceptions
	if($next_word =~ m/^($months_rod_short_pattern)\.*$/i) { $kol_por='por';}
	
	# --- check either everything was caught correctly
	
	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	if((($number_s =~ m/[\.\,]/)||($number2_s =~ m/[\.\,]/)) && ($next_word =~ m/^$months_rod_short_pattern\.?$/i)) { next; } # only for month abbreviations
	
	# *** check either everything was caught correctly
	
	my $word='';
	
	# --- Handle the previous case, etc
	
	my $coming_before_s=substr($this_line, 0, $pos_begin).$prev_word.$non_word1; # from and how much 
	
	if(($coming_before_s =~ m/(��?�|�����|�����)[�-��]{0,3}\s*((\b�\b|\b���\b|\-|\�|\�)\s*)?[\,\;]?\s*$/i)&&($next_word =~ m/^(��|���|���)\.?$/i))
		{
		if($prev_word =~ m/^��$/i) 	{ $special_preposition = '��'; }
		}
	elsif(($coming_before_s =~ m/(����)[�-��]{0,3}\s*((\b�\b|\b���\b|\-|\�|\�)\s*)?[\,\;]?\s*$/i)&&($next_word =~ m/^(���|��|���|���|hz|khz|mhz|ghz)\.?$/i))
		{
		if($prev_word =~ m/^��$/i) 	{ $special_preposition = '��'; }
		}
	elsif(($coming_before_s =~ m/(�����)[�-��]{0,3}\s*((\b�\b|\b���\b|\-|\�|\�)\s*)?[\,\;]?\s*$/i)&&($next_word =~ m/^(��|lm)\.?$/i))
		{
		if($prev_word =~ m/^��$/i) 	{ $special_preposition = '��'; }
		}
	elsif($coming_before_s =~ m/(����|���|\b�\s+�������|\b�\s+������|\b�\s+���|����|�����|�����)[�-��]{0,3}\s*((\b�\b|\b���\b|\-|\�|\�)\s*)?[\,\;]?\s*$/i) 
		{
		if($prev_word =~ m/^��$/i)	{ $special_preposition = '��'; }
		} # the other shortcuts are not in the same class
	elsif($coming_before_s =~ m/($months_base_pattern)[�-��]{0,3}\s*((\b�\b|\b���\b|\-|\�|\�)\s*)?[\,\;]?\s*$/i) 
		{
		if($prev_word =~ m/^��$/i)	{ $special_preposition = '��'; }
		}
	else
		{
		$prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};
		};
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# *** Handle the previous case, etc
	
	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};

	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}
		
	# --- processing exceptions
	if($prev_word =~ m/^�$/)
		{
		if($this_line =~ m/(���|�����|������|������)[�-��]{0,3}\s+$matching/i)
			{
			$case = 'vin';
			}
		}
	
	if(($prev_word =~ m/^�$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && ($tysjach =~ m/\w/)&&($tysjach !~ m/[��\.]\s?$/i)) { $case = 'vin'; } # V 18 tysjach tonn, e.g.
	
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 megabajtami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions

	$gender='m';
	if($next_word =~ m/^(��\.*)$/i) { $gender='f';}					# Important!!!
	elsif($next_word =~ m/^(�\.\s*�\.*)$/i) { $gender='f';}
	elsif($next_word =~ m/^(�\.\s*�\.*)$/i) { $gender='f';}
	elsif($next_word =~ m/^(���\.*)$/i) { $gender='f';}
	
	if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					if(($next_word =~ m/^($months_rod_short_pattern)\.*$/i)&&($i==0)) { next; }

					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;	
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					if(($next_word =~ m/^($months_rod_short_pattern)\.*$/i)&&($i==0)) { next; }
			
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}	
		}

	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
			
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if(($special_preposition =~ m/^(��)$/i)&&($case eq 'rod')) { $case = 'dat'; }
		if($number2_s =~ m/\d/)
			{
			$sin_or_pl='pl';
			}
		else
			{
			$sin_or_pl='sin';
			}
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};	
		}

	if($next_word =~ m/^(��|hz)\.?$/i) 					{ $word = $gercy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���|khz)\.?$/i) 			{ $word = '����'.$gercy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���|mhz)\.?$/i) 			{ $word = '����'.$gercy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���|ghz)\.?$/i) 			{ $word = '����'.$gercy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��|lm)\.?$/i) 				{ $word = $ljumeny{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��)\.*$/i) 					{ $word = $oboroty{$sin_or_pl}{$case};}			# pay attention to asterisk, maybe 2 dots
	elsif($next_word =~ m/^(��)\.*$/i) 					{ $word = $edinicy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��|�����|tb|tbyte|��)\.?$/i){ $word = '�����'.$bajty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��|�����|gb|gbyte)\.?$/i)	{ $word = '����'.$bajty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��|�����|mb|mbyte|��)\.?$/i){ $word = '����'.$bajty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��|�����|kb|kbyte)\.?$/i)	{ $word = '����'.$bajty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(����|byte)\.?$/i)			{ $word = $bajty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(����|tbit)\.?$/i)			{ $word = '�����'.$bity{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(����|gbit)\.?$/i)			{ $word = '����'.$bity{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(����|mbit)\.?$/i)			{ $word = '����'.$bity{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(����|kbit)\.?$/i)			{ $word = '����'.$bity{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���|bit)\.?$/i)				{ $word = $bity{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��)\.?$/i)					{ $word = $dzhoulej{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���)\.?$/i)					{ $word = '����'.$dzhoulej{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���)\.?$/i)					{ $word = '����'.$dzhoulej{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��\.?)$/i) 					{ $word=$dni{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?)$/i) 				{ $word=$nedeli{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(���\.?)$/i) 				{ $word=$mesjacy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^($months_rod_short_pattern)\.?$/i)
														{
														for(my $i=0; $i<12; $i++)
															{
															if($next_word =~ m/^$months_rod_short[$i]\.?$/i)
																{
																$word=$months[$i]{rod}; $i=12;
																# $word=$months[$i]{$case}; $i=12;
																}
															}
														}
	elsif($next_word =~ m/^(�\.\s*�\.*)$/i)				{
														if($sin_or_pl eq 'pl') { $gender = 'pl'; };
														$word = $astronomicheskij_adj{$case}{$gender}.' '.$edinicy{$sin_or_pl}{$case};
														}
	elsif($next_word =~ m/^(�\.\s*�\.*)$/i)				{
														if($sin_or_pl eq 'pl') { $gender = 'pl'; };
														$word = $loshadinyj_adj{$case}{$gender}.' '.$sily{$sin_or_pl}{$case};
														}
	else {$word = $next_word;}
	
	$phrase .= ' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if($next_word =~ m/\.\.$/) { $phrase .= '.'; }
	elsif(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }
	
	# $this_line =~ s/\Q$matching$tysjach$next_word\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach$next_word\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	# if($phrase !~ m/\.$/)
		# { pos($this_line) -= length($word); }
	
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"Detect the tonn" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect other shortcuts with more probable cardinal numbers

# ----- Detect the veka, gody, stranicy, etc - where ordinal number is more probable than cardinal one

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=((�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\b\.?\s?)?(\b��������|\b�����|\b����|\b�����|\b��������|\b�������|\b���|\b���|\b������|\b���������|\b�����|\b�������|\b�����|\b���������|\b���������|\b����?�|\b�����|\b���|\b��������|\b�����|�\b\.?|��\b\.?|���\b\.?|\#|\�|��\b\.?|no\b\.?|�\b\.?|���\b\.?|\�|��\b\.?|��\b\.?|��\b\.?|�\b\.?|�\b\.?|���\b\.?|��\b\.?|�\b\.?|��\b\.?|��\b\.?)([�-��]{0,3}(?![\w])))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';	
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	my $word_part1=$7;
	my $and_or=$13;
	my $number2_s=$14; #12
	my $word_part2=$18;
	my $tysjach=$19;
	my $first_word_part=$21;
	my $second_word_part=$22;					#as strings
	
	my $kol_por='por';
	if(($number_s =~ m/[\.\,]/)||($number2_s =~ m/[\.\,]/)||($tysjach =~ m/[�-��]/i))
		{
		$kol_por='kol';
		}
	# if($prev_word =~ m/^�����$/i) { $kol_por = 'kol'; }
	
	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
		
	$phrase = $phrase.$prev_word.$non_word1;
	my $next_word = $first_word_part.$second_word_part;
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly

	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	
	if($next_word =~ m/[�-��]\.[�-��]/i) { next;} # excludes the case, when one next word consists of a full-stop in the middle
	# Exceptions
	if($next_word =~ m/^�$/i) { next; } # To exclude "1 V ondnoj mile tysjacha ....", "1925 v moih arkticheskih"
	
	my $word='';
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much 
	if(($prev_word !~ m/^(��������|�����|����|�����|��������|�������|���|���|������|���������|�����|�������|�����|���������|���������|����?�|�����|���|��������|�����|�������|����|���|�������|����|�$|���$|$)/i) || (($prev_word =~ m/^(�|���|)$/i)&&($coming_before_s !~ m/\b(��������|�����|����|�����|��������|�������|���|���|������|���������|�����|�������|�����|���������|���������|����?�|�����|���|��������|�����|�������|����|���|�������|����)[�-��]{0,3}\s*$/i))) { $prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}

	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty

	# --- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	# *** determine the interval of a number
	
	if($prev_word =~ m/^�����$/i)
		{ 
		if(($number_interval ne '1')&&($next_word =~ m/^(����|����|���������|������|�����|������|���������|��������|��������|�����������|������|��������|������|����������|�����������|�����|������|����|���������|������)$/i))
			{
			}
		else
			{
			$kol_por = 'kol';
			}
		}
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	my $aux_string=$coming_before_s.$prev_word.$non_word1;
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��|�������.*|�������.*|�������.*|����.*|�����.*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����?�[�-�]*|�����[�-�]*)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($next_word =~ m/^(��?\b\.?$)/i)&&($aux_string =~ m/\b���(|�|�|��|�|�|��|���?|��)\s*$/i)) # (Rod)	$aux_string: If $prev_word eq '', check the end of $coming_before_s
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	my $case_determined=1;
		
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		# ----- Expressions like "Yajca 2 kategorii"
		elsif(($next_word =~ m/^(��������(�|�)\.?|������?\.?|���\b\.?)$/i)&&(length($prev_word)>1)&&($non_word1 =~ m/^\s+$/))
			{
			my @morph_properties;

			@morph_properties=get_morph_props($prev_word, '-i -w');
			
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/)) 
					{
					$case = 'rod';
					$kol_por = 'por';

					if(($next_word =~ m/^(���������\.?|�����\.?)$/i)&&($number2_s eq ''))
						{
						$gender='pl';
						}
					else
						{
						$gender='f';
						}
					
					$case_determined=0;
					$i=$#morph_properties+1;
					}
				}
			
			
			}
		}

	# --- Processing exceptions
	
	if(($prev_word =~ m/^��$/i) && ($next_word =~ m/^(���\.|�����(�|��)\.?|���������(�|��)\.?|���������(��|��)\.?)$/i)) { $case = 'pr'; }
		
	if(($case eq 'im') && ($number_interval ne '2_4') && ($next_word =~ m/^(����|����|���������|������|�����|������|���������|��������|��������|�����������|������|��������|������|����������|�����������|�����|������|����|���������|������)$/i))
		{
		$case = 'rod'; # v nachale 5 glavy
		}
	
	if(($prev_word =~ m/^�$/i) && ($next_word !~ m/(�|�)\.?$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && ((($tysjach !~ m/\w/)&&($next_word !~ m/^(�|��|���|\#|\�|��|no|�|���|\�|��|��|��|�|�|���|��|�|��|��)\.?$/i))||(($tysjach =~ m/\w/)&&($tysjach !~ m/[��\.]\s?$/i)))) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 nomerami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** Processing exceptions
	
	if($gender eq '') { $gender='m';}
	
	if(($word_part1 =~ m/^(��|�?�|�?��)$/i)||($word_part2 =~ m/^(��|�?�|�?��)$/i))	{ $gender='pl'; }	# 20-e glavj
	elsif($next_word =~ m/^(\�$|��������)/i)	 		{ $gender='m';}
	elsif($next_word =~ m/^(�\b\.?$|�����)/i)	 		{ $gender='m';}
	elsif($next_word =~ m/^(��\b\.?$|����)/i)	 		{ $gender='f';}
	elsif($next_word =~ m/^(���\b\.?$|�������)/i) 		{ $gender='f';}
	elsif($next_word =~ m/^(��\b\.?$|�����)/i) 			{ $gender='f';}
	elsif($next_word =~ m/^(��?\b\.?$|���)/i)			{ $gender='m';}
	elsif(($next_word =~ m/^(��\b\.?$|���(�|�|��|��|���|��))/i)&&(($word_part1 =~ m/^�?(�|�|��)$/i)||($word_part2 =~ m/^�?(�|�|��)$/i))) { $gender='pl';}
	elsif(($next_word =~ m/^(�\b\.?$)/i)&&(($word_part1 =~ m/^(�)$/i)||($word_part2 =~ m/^(�)$/i)))	{ $gender='pl';}
	elsif($next_word =~ m/^(��?\b\.?$|���)/i)			{ $gender='m';}
	elsif($next_word =~ m/^(��������)/i)		 		{ $gender='m';}
	elsif($next_word =~ m/^(������)/i)					{ $gender='n';}
	elsif($next_word =~ m/^(���������)/i)		 		{ $gender='n';}
	elsif($next_word =~ m/^(�����)/i)		 			{ $gender='f';}
	elsif($next_word =~ m/^(�������)/i)		 			{ $gender='m';}
	elsif($next_word =~ m/^(�����)/i)			 		{ $gender='m';}
	elsif($next_word =~ m/^(���������)/i)		 		{ $gender='m';}
	elsif($next_word =~ m/^(���������)/i)		 		{ $gender='f';}
	elsif($next_word =~ m/^(����?�)/i)			 		{ $gender='n';}
	elsif($next_word =~ m/^(��\b|\#|\�|no\b|�����)/i)	{ $gender='m';}
	elsif($next_word =~ m/^(�\b|���)/i)		 			{ $gender='m';}
	elsif($next_word =~ m/^(��\b\.?)$/i)				{ $gender='f';}
	elsif($next_word =~ m/^(��\b\.?)$/i)				{ $gender='f';}
	elsif($next_word =~ m/^(��������|�����)[�-��]{0,3}\b/i)		{ $gender='f';}
	elsif($next_word =~ m/^(���)\.?$/i)				{ $gender='f';}
	
	if($gender eq 'pl')	{ }
	elsif(($gender eq 'f')&&(($word_part1 =~ m/^(��|�?�)$/i)||($word_part2 =~ m/^(��|�?�)$/i))) { $gender = 'pl';}	# 20-ye, -m str. (female only)
	elsif($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=1; $i>=0; $i--) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=-1; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }	
			}
			
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
					
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;	
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/ };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1;}
			
			if($flag == 0)
				{
				for(my $i=1; $i>=0; $i--) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=-1; $j=6;
							$flag=1;
							}
						}
					}
				}
			
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	elsif((length($tysjach) == 0)&&($next_word !~ m/^(�\b|��\b|���\b|\#|\�|��\b|no\b|�\b|\�|��\b|��?\b|��?\b|���\b|��\b|�\b|��\b|��\b|���\b)/i))
		{
		if(($case_determined eq 1)||($next_word !~ m/^(��������(�|�)\b|������?\b)/i))
			{
			my @morph_properties;
		
			@morph_properties=get_morph_props($next_word, '-i -w');
			my $array_elem;
			my $flag=0; # Shows that the proper case was found
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{case} eq $case)&&($morph_properties[$i]{gender} eq $gender)&&($morph_properties[$i]{misc} !~ m/(����|���|���)/)) {$flag=1; $array_elem=$i; $i=$#morph_properties+1; }
				}
			
			if($flag == 0) # looks just for the same gender independently from case
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)
					{
					if(($morph_properties[$i]{gender} eq $gender)&&($morph_properties[$i]{case} !~ m/rod/i)) {$flag=1; $case=$morph_properties[$i]{case}; $array_elem=$i; $i=$#morph_properties+1; }
					}
				}
		
			if($morph_properties[$array_elem]{number} =~ m/^pl$/) { $kol_por='kol';}
		
			if(($case =~ m/^im$/)&&($prev_word ne '')&&($number2_s ne '')&&($next_word =~ m/^(�����|��������|�����)\b/i))	# E.g. Hudozhniki 20-21 vekov
				{
				my @morph_properties_prev;
				@morph_properties_prev=get_morph_props($prev_word, '-i -w');
			
				for(my $i=0; $i<$#morph_properties_prev+1; $i++)
					{
					if(($morph_properties_prev[$i]{pos} eq 'S')&&($morph_properties_prev[$i]{misc} !~ m/(����|���|���)/))
						{
						$kol_por = 'por';
						$case = 'rod';
						$gender = 'pl';
						}
					}
			
				}
			}
		}
		
	if(($prev_word =~ m/^�����.*/i)&&($tysjach eq '')&&($next_word =~ m/^(�����|��������)\b/i))
		{
		$kol_por = 'por';
		$case = 'rod';
		$gender = 'm';
		}
	
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);	
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);		}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender);
			};
		}
	
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($gender eq 'pl')
			{
			$sin_or_pl='pl';
			}
		elsif($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
		
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if($number2_s =~ m/\d/)
			{
			if(($special_preposition =~ m/^(��)$/i)&&($case eq 'rod')) { $case = 'dat'; }
			$sin_or_pl='pl';
			}
		else
			{
			if(($special_preposition =~ m/^(��)$/i)&&($case eq 'rod')) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};
			
		}
	
	if($gender eq 'pl') { $sin_or_pl = 'pl'; }
	
	if($next_word =~ m/^\�$/i) 					{ $word = $paragrafy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\b\.?$/i) 			{ $word = $punkty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $glavy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\b\.?$/i) 		{ $word = $stranicy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $statji{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��\b|no\b|\#|\�)/i)	{ $word = $nomera{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\b\.?/i)			{ $word = $toma{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\b\.?$/i) 		{ $word = $kabinety{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $knigi{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\b\.?$/i) 			{ $word = $doma{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $kvartiry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $jetazhi{$sin_or_pl}{$case};}
	# elsif($next_word =~ m/^(��?\b\.?$|���)/i)	{ $word = $veka{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��?\b\.?$)/i)	{ $word = $veka{$sin_or_pl}{$case};}
	elsif(($prev_word =~ m/^�$/i)&&($next_word =~ m/^�\.?$/i))	{ if($sin_or_pl eq 'sin') { $word = '����';} else { $word = '�����';}}
	# elsif($next_word =~ m/^(��?\b\.?$|���)/i)	{ $word = $goda{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��?\b\.?$)/i)	{ $word = $goda{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\b\.?/i)			{ $word = $kategorii{$sin_or_pl}{$case};}
	else {$word = $next_word;}
		
	$phrase .= ' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s+[IXCLMix])/)) { }	# do nothing (could be 20-e gg. XX veka)
	elsif(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }

	# $this_line =~ s/\Q$matching$tysjach$next_word\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach$next_word\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	# pos($this_line)=$pos_begin+length($phrase)-length($word);	# New style
	# if($phrase !~ m/\.$/)
		# { pos($this_line) -= length($word); }
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"Detect the veka" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# print $this_line."\~\n";
# exit;

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect the veka, gody, stranicy, etc - where ordinal number is more probable than cardinal one 

# ----- Detect the tysjach, millionov, trillionov

# # - Timing
# $start=gettimeofday();
# $temp_counter=0; # number of entries into while loop
# # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';
my $prev_end_position=0;		# End position of the previous matching's

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?((((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=([��][�-��]*\b\.?))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
		
	my $word_part1=$7;
	my $and_or=$14;
	my $number2_s=$15; #12
	my $word_part2=$19;
	my $tysjach=$20;
	
	my $kol_por='kol'; # Here we may create exceptions
	
	my @number=split(/[\.\,]/, $number_s);
	if($#number+1 > 2) { next;}
	my @number2=split(/[\.\,]/, $number2_s);
	if($#number2+1 > 2) { next;}
	
	$phrase = $phrase.$prev_word.$non_word1;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly
		
	if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?$/i) { next;}
	
	my $rest_length=length($this_line)-$pos_begin;
	if(($this_line !~ m/^.{$prev_end_position}(\s*(\b[�-��]+\b)?[\,\;\s]*).{$rest_length}$/i)||($prev_case eq '')||($prev_kol_por eq ''))
		{
		$prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};
		}
	else
		{
		if($prev_word =~ m/^��$/i) { $special_preposition = '��';}
		}
		
	# if($prev_word !~ m/^(�$|���$)/i) { $prev_case = '';  if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};

	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}
		
	# --- processing exceptions
	
	if(($prev_word =~ m/^�$/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/)) && ($tysjach !~ m/(�|�)\.?$/i) && ( $tysjach !~ m/^(�|���|���|����|����)\.?$/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	if(($prev_word =~ m/^�$/i)&&($tysjach =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 tysjachami
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($tysjach =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions
	
	if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?$/i) { $gender = 'm';}
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;	
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=2; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}	
		}
			
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	my $sin_or_pl;
	
	if($kol_por eq 'por')
		{
		if($number2_s =~ m/\d/)
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
			else { $sin_or_pl='pl'; };
			}
		else
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		}
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin'; $case='rod';
		}
	elsif($number_interval eq '1')
		{
		if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
		else {$sin_or_pl='sin'; };
		}
	else
		{
		if($case =~ m/(im|vin)/i)
			{
			if($number_interval eq '2_4')
				{
				$sin_or_pl='sin'; $case='rod';
				}
			else
				{
				$sin_or_pl='pl'; $case='rod';
				}
			}
		else
			{
			$sin_or_pl='pl';
			}
		
		}

	my $word='';
	if($tysjach =~ m/^(�|���)\.?$/i) 	{ $word = $thousand{$sin_or_pl}{$case}; }
	elsif($tysjach =~ m/^���\.?$/i) 		{ $word = $million{$sin_or_pl}{$case}; }
	elsif($tysjach =~ m/^����\.?$/i) 	{ $word = $billion{$sin_or_pl}{$case}; }
	elsif($tysjach =~ m/^����\.?$/i) 	{ $word = $trillion{$sin_or_pl}{$case}; }
	else	{ $word = $tysjach; }
	
	$phrase .= ' '.$word;
	
	# Treat the point (.) by the shortcuts properly
	if(($tysjach =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }
	
	# $this_line =~ s/\Q$matching$tysjach\E/$phrase /; # space at the end	
	$this_line =~ s/^(.{$pos_begin})\Q$matching$tysjach\E/${1}$phrase /; # space at the end
	pos($this_line)=$pos_begin+length($phrase);
	# if($phrase !~ m/\.$/)
		# { pos($this_line) -= length($word); }
	
	$prev_end_position=$pos_begin+length($phrase);
	
	# # - Timing
	# $temp_counter++;
	# # * Timing
}

# # - Timing
# if($temp_counter > 0)
	# {
	# $end=gettimeofday();
	# print "\n\>\>Time spent by ".'"Detect the tonn" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# }
# # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect the tysjach, millionov, trillionov

# Detect number raz
# $this_line =~ s/\b�\s*(\d+([\.\,]\d+)?)\s+(����?)\b/'� '.realnum2word($1,'kol','vin','m').' '.$3/ieg; # is solved later
$this_line =~ s/\b(��|��|�����|������|������|����������|�����|������|�����|����)\s+(\d+([\.\,]\d+)?)\s+(����?)\b/$1.' '.realnum2word($2,'kol','rod','m').' '.$4/ieg;
$this_line =~ s/\b(\d+([\.\,]\d+)?)\s+(����?([^�-��]))/realnum2word($1,'kol','im','m').' '.$3/ieg;

# ----- detect the number-word combination, like 15-35-tysjachnyj

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/\b(\d{1,})((\s?)(\-|\�|\�)\3(\d{1,}))?(\-|\�|\�)([�-��]{4,})\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $number_s=$1;								#as strings
	my $number2_s=$5;
	my $word_part=$7;
	
	my $aux_phrase;
	
	if((length($number_s)>3)||(length($number2_s)>3))
		{
		if($word_part !~ m/^(�����|.*����|.*����|.*����[�]�|.*��������)/i)
			{
			next;
			}
		}
	
	$aux_phrase = num2word($number_s,'kol','rod','m');
	$aux_phrase =~ s/����/����/ig;
	$aux_phrase =~ s/���������/���������/ig;
	$aux_phrase =~ s/���/���/ig;
	$aux_phrase =~ s/������?/������/ig;
	$aux_phrase =~ s/\s//g; #delete the spaces
	$aux_phrase =~ s/��$//; #delete the go from odnogo and so on to get odnotysjachnyj, for example
	
	if(($number2_s !~ m/\d/)&&($number_s =~ m/(^|[^1])2$/)&&($word_part =~ m/^(�����|����|����|�������|����|�������|�����|���|����|�����|����|����|�����|������|���|��������|�����|��������|�����|����)/i))
		{
		$aux_phrase =~ s/����$/���/i; # Replace ����- �� ���-
		}
	
	if($number2_s =~ m/\d/)
		{
		$phrase.=$aux_phrase.' ';
		$aux_phrase = '';
		$aux_phrase = num2word($number2_s,'kol','rod','m');
		$aux_phrase =~ s/����/����/ig;
		$aux_phrase =~ s/���������/���������/ig;
		$aux_phrase =~ s/���/���/ig;
		$aux_phrase =~ s/������?/������/ig;
		$aux_phrase =~ s/\s//g;
		$aux_phrase =~ s/��$//;
		
		if(($number2_s =~ m/(^|[^1])2$/)&&($word_part =~ m/^(�����|����|����|�������|����|�������|�����|���|����|�����|����|����|�����|������|���|��������|�����|��������|�����|����)/i))
			{
			$aux_phrase =~ s/����$/���/; # Replace ����- �� ���-
			}
		}
	
	$phrase.=$aux_phrase.$word_part;
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"15-35-tysjachnyj" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect the number-word combination, like 15-35-tysjachnyj

# ----- detect the numbers with shortcuts -omu, e.g. (noun is not obligatory) #new

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';
my $prev_end_position=0;		# End position of the previous matching's

while ($this_line =~ m/
		(\b[�-��]*)? 							# previous word ($1)
		(\W*)									# non_word_1 ($2)
		\b(\d+)									# number_1 ($3)
		((\-|\�|\�)([�-��]{1,3}\b))?			# word_part_1 ($6)
		(
		(((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))	# $and_or ($13)
		(\d+)									# number_2 ($14)
		((\-|\�|\�)([�-��]{1,3}\b))?			# word_part_2 ($17)
		)?
		\b(?=(\s+([�-��]+))?)					# next word ($19)
		/xig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase='';
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	my $word_part1=$6;
	my $and_or=$13;
	my $number2_s=$14;
	my $word_part2=$17;
	my $next_word=$19;
	
	my $kol_por='kol';
	
	$phrase .= $prev_word.$non_word1;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly
	
	if(($word_part1 !~ m/[�-��]/i)&&($word_part2 !~ m/[�-��]/i))
		{
		next;
		}
	
	my $rest_length=length($this_line)-$pos_begin;
	if(($this_line !~ m/^.{$prev_end_position}(\s*(\b[�-��]+\b)?[\,\;\s]*).{$rest_length}$/i)||($prev_case eq '')||($prev_kol_por eq ''))
		{
		$prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};
		}
	else
		{
		if($prev_word =~ m/^��$/i) { $special_preposition = '��';}
		}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# --- Gain the information about the gender
	
	my $gender;
	my $gender_flag=0; # The information about gender is available according to the subject after a numeral
	my $gender_s=''; # Special string consisting all the probable genders
	
	if($next_word =~ m/[�-��]/i)
		{
		# Read the properties of the word from mystem program;
		my @morph_properties=get_morph_props($next_word, '-i -w');
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/))
			{
			$gender_flag=1;
			if($gender_s !~ m/\Q$morph_properties[$i]{gender}\E/) { $gender_s .= '_'.$morph_properties[$i]{gender};} }
			if(($morph_properties[$i]{number} eq 'pl')&&($gender_s !~ m/pl/)) { $gender_s .= '_pl'; }
			}
		}
	
	if($gender_s eq '') { $gender_s = 'pl'; }
	
	# --- New: if the next_word is month, just ignore its morphological information (since it is in "rod", but the number can be in any case)
	
	if($next_word =~ m/^($months_rod_pattern)$/i)
	{
		$gender_flag=0;
	}
	
	# *** New: if the next_word is month, just ignore its morphological information
	
	# *** Gain the information about the gender
	
	# --- determine the interval of a number (only integer)
	
	my $number_interval;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	
	if( (int(substr($last_number_s,-2)) > 4) && (int(substr($last_number_s,-2))<21) ) {$number_interval = '0_5_20';}
	elsif($last_number_s =~ m/[2-4]$/) { $number_interval='2_4';}
	elsif($last_number_s =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	
	# *** determine the interval of a number (only integer)
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	my $flag_prev=0; # the prev_case and prev_kol_por were taken
	if($case eq 'im')
		{
		if($next_word =~ m/^($months_rod_pattern)$/i)	# ^21-go maja...
			{
			$case = 'rod';
			$kol_por = 'por';
			# $flag_prev = 1;
			}
		elsif(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			$flag_prev = 1;
			}
		}

	if(($number_s =~ m/^[12]\d{3}$/) || (($number_s =~ m/^(\d{2})$/)&&($matching !~ m/\-�/i))) # in some cases, set the 'por' by default (this will be changed if no match)
		{
			$kol_por = 'por';
		}
		
	# --- processing exceptions
	
	if($prev_word =~ m/^�$/)
		{
		if($this_line =~ m/(���)[�-��]{0,3}\s+$matching/i)
			{
			$case = 'vin';
			}
		}
	
	if(($prev_word =~ m/^�$/i) && ($next_word !~ m/(�|�)\.?$/i) && ($gender_flag == 1)) { $case = 'vin'; } # V 15 tonn, e.g.

	# *** processing exceptions
	
	my $aux_phrase;
	my $aux_phrase1;
	my $aux_phrase2;
	
	# Try first the probable case and kol_por
	my $flag=0; # proper number case is found
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i))
		{
		for(my $k=0; $k<4; $k++) # genders_base
			{
			if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
			
			# if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k ne 3)) # only for kol and not plurar with po
			if(($prev_word =~ m/^��$/i)&&($kol_por eq 'kol')&&($k != 3)) # only for kol and not plurar with po
				{
				$aux_phrase1 = realnum2word_po($number_s, $kol_por, $case, $genders_base[$k]);
				$aux_phrase2 = realnum2word_po($number2_s, $kol_por, $case, $genders_base[$k]);
				}
			else
				{
				$aux_phrase1 = num2word($number_s, $kol_por, $case, $genders_base[$k]);
				$aux_phrase2 = num2word($number2_s, $kol_por, $case, $genders_base[$k]);
				}
			
			# Check
			if(($aux_phrase1 =~ m/$word_part1\s*$/i)&&($aux_phrase2 =~ m/$word_part2\s*$/i))
				{
				$flag = 1;
				$gender = $genders_base[$k];
				# $kol_por = $kol_por_base[$i];
				# $i=2;
				$k=4;
				}
				
			}
		}
	elsif(($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))
		{
		my $word_part;
		my $number_with_part;
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		for(my $k=0; $k<4; $k++) # genders_base
			{
			if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
			
			# if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k ne 3)) # only for kol and not plurar with po
			if(($prev_word =~ m/^��$/i)&&($kol_por eq 'kol')&&($k != 3)) # only for kol and not plurar with po
				{
				$aux_phrase = realnum2word_po($number_with_part, $kol_por, $case, $genders_base[$k]);
				}
			else
				{
				$aux_phrase = num2word($number_with_part, $kol_por, $case, $genders_base[$k]);
				}
			
			# Check
			if($aux_phrase =~ m/$word_part\s*$/i)
				{
				$flag = 1;
				$gender = $genders_base[$k];
				# $kol_por = $kol_por_base[$i];
				# $i=2;
				$k=4;
				}
			}
		}
		
	# if the kol_por was determined wrong (but not taken from previous case)
	
	if(($flag == 0)&&($flag_prev != 1))
		{
		if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
		
		if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i))
			{
			for(my $k=0; $k<4; $k++) # genders_base
				{
				if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
				
				# if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k ne 3)) # only for kol and not plurar with po
				if(($prev_word =~ m/^��$/i)&&($kol_por eq 'kol')&&($k != 3)) # only for kol and not plurar with po
					{
					$aux_phrase1 = realnum2word_po($number_s, $kol_por, $case, $genders_base[$k]);
					$aux_phrase2 = realnum2word_po($number2_s, $kol_por, $case, $genders_base[$k]);
					}
				else
					{
					$aux_phrase1 = num2word($number_s, $kol_por, $case, $genders_base[$k]);
					$aux_phrase2 = num2word($number2_s, $kol_por, $case, $genders_base[$k]);
					}
				
				# Check
				if(($aux_phrase1 =~ m/$word_part1\s*$/i)&&($aux_phrase2 =~ m/$word_part2\s*$/i))
					{
					$flag = 1;
					$gender = $genders_base[$k];
					# $kol_por = $kol_por_base[$i];
					# $i=2;
					$k=4;
					}
					
				}
			}
		elsif(($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))
			{
			my $word_part;
			my $number_with_part;
			if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
			
			for(my $k=0; $k<4; $k++) # genders_base
				{
				if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
				
				# if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k ne 3)) # only for kol and not plurar with po
				if(($prev_word =~ m/^��$/i)&&($kol_por eq 'kol')&&($k != 3)) # only for kol and not plurar with po
					{
					$aux_phrase = realnum2word_po($number_with_part, $kol_por, $case, $genders_base[$k]);
					}
				else
					{
					$aux_phrase = num2word($number_with_part, $kol_por, $case, $genders_base[$k]);
					}
				
				# Check
				if($aux_phrase =~ m/$word_part\s*$/i)
					{
					$flag = 1;
					$gender = $genders_base[$k];
					# $kol_por = $kol_por_base[$i];
					# $i=2;
					$k=4;
					}
				}
			}
		if($flag == 0)	{ if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; };
		
		}
	
	# If the case or kol_por was determined wrong try all the cases
	
	if($flag == 0)			
		{
		my $stop_flag=0; # necessary to avoid unlimited cycling
		
		if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i))
			{
			for(my $k=0; $k<4; $k++) # genders_base
				{
				if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
				
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k != 3)) # only for kol and not plurar with po
							{
							$aux_phrase1 = realnum2word_po($number_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							$aux_phrase2 = realnum2word_po($number2_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						else
							{
							$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						
						# Check
						if(($aux_phrase1 =~ m/$word_part1\s*$/i)&&($aux_phrase2 =~ m/$word_part2\s*$/i))
							{
							$flag = 1;
							$gender = $genders_base[$k];
							$kol_por = $kol_por_base[$i];
							$case = $cases_base[$j];
							$i=2; $k=4; $j=6;
							}
						}
					}
				if(($gender_flag == 1) && ($k == 3) && ($flag != 1) && ($stop_flag == 0)) { $k=-1; $gender_flag=0; $stop_flag =1; }	# if the gender was determined wrong try all the cases
				}
			}
		elsif(($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))
			{
			my $word_part;
			my $number_with_part;
			if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
			
			for(my $k=0; $k<4; $k++) # genders_base
				{
				if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
				
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k != 3)) # only for kol and not plurar with po
							{
							$aux_phrase = realnum2word_po($number_with_part, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						else
							{
							$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						
						# Check
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$flag = 1;
							$gender = $genders_base[$k];
							$kol_por = $kol_por_base[$i];
							$case = $cases_base[$j];
							$i=2; $k=4; $j=6;
							}
						}
					}
				if(($gender_flag == 1) && ($k == 3) && ($flag != 1) && ($stop_flag == 0)) { $k=-1; $gender_flag=0; $stop_flag =1; }	# if the gender was determined wrong try all the cases
				}
			}
		}
		
	if($flag == 0)
		{
		my $matching_new=$matching;
		if($word_part1 =~ m/[�-��]/i) { $matching_new =~ s/(\-|\�|\�)$word_part1//; }
		if($word_part2 =~ m/[�-��]/i) { $matching_new =~ s/(\-|\�|\�)$word_part2//; }
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$matching_new/;
		pos($this_line)=$pos_begin+length($matching_new);
		next;
		}
		
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin')&&($gender ne 'pl'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.num2word($number_s,'kol',$first_number_case,$gender).$connection.num2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin')&&($gender ne 'pl'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
			
	$prev_end_position=$pos_begin+length($phrase);

	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect the numbers with shortcuts -omu, e.g. (noun is not obligatory) #new

# ----- replace numerals preceded by: paragraph, etc

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*\b)?(\s*)((\b��������|\b�����|\b����|\b�����|\b��������|\b�������|\b���|\b�����|\b���������|\b������?�|\b����|\b������|\b����|\b����?�|\b����|\#|\�|\bN\.|\b��\b\.?|\bno\b\.?|\b�\.|\b��\.|\b���\.|\b��\.|\b�\.|\�|\b���?\.|\b���\.|\b��\.|\b����?\.|\b�\.|\b���\b\.?|\b��\b\.?|\b�\b\.?|\b��\b\.?|\b��\b\.?)[�-��]{0,3})\s{0,10}(\d+)((((\s*)(\-|\�|\�|\/|\,\ |\.)\s*)||(\s+(���|�)\s+))(\d+))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word=$2;
	my $word=$3;
	my $number_s=$5;
	my $connection_sign=$8;
	my $and_or=$12;
	my $number2_s=$13;
	
	my $case;
	my $sin_or_pl;
	
	if($word =~ m/(\b�\.|\b��\.|\b���\.|\b��\.|\b�\.|\�|\#|\�|\bN\.|��\b\.?|no\b\.?|\b���?\.|\b���\.|\b��\.|\b����?\.|\b�\.|\b���\b\.?|\b��\b\.?|\b�\b\.?|\b��\b\.?|\b��\b\.?)$/i)
		{
		# Check the probable case of a number
		
		# preliminary determination of the case
		my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word;
		my $prel_case=preliminary_case($line_before_number);
		
		if($prel_case ne 'im')
			{
			$case=$prel_case;
			# if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
			}
		else
			{
			if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
				{
				$case = 'rod';
				}
			elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
				{
				$case = 'dat';
				}
			elsif($prev_word =~ m/^(�����|���|���)$/i) # (Tv)
				{
				$case = 'tv';
				}
			elsif($prev_word =~ m/^(��|��?|�|��)$/i) # (Pr)
				{
				$case = 'pr';
				}	
			elsif($prev_word =~ m/^(��|�����|��)$/i) # (Vin)
				{
				$case = 'vin';
				}
			else # (Im)
				{
				$case = 'im';
				}
			}
			
		# --- processing exceptions
		
		if(($prev_word =~ m/^��$/i) && ($word =~ m/(\#|\�|\bN\.|��\b\.?|no\b\.?)$/i)) { $case = 'vin'; }
		
		# *** processing exceptions
		
		if(($number2_s =~ m/\d/)&&($connection_sign !~ m/\./)) { $sin_or_pl='pl'; } else { $sin_or_pl='sin';}
		
		my $temp_word=$word;
		for($temp_word)
			{
			when (m/^\�/)				{ $word=$paragrafy{$sin_or_pl}{$case}; }
			when (m/^�\./i)				{ $word=$punkty{$sin_or_pl}{$case}; }
			when (m/^��\./i)			{ $word=$glavy{$sin_or_pl}{$case}; }
			when (m/^���\./i)			{ $word=$stranicy{$sin_or_pl}{$case}; }
			when (m/^��\./i)			{ $word=$statji{$sin_or_pl}{$case}; }
			when (m/^�\./i)				{ $word=$toma{$sin_or_pl}{$case}; }
			when (m/^(\#|\�|N\.|��\b\.?|no\b\.?)$/i)	{ $word=$nomera{$sin_or_pl}{$case}; }
			when (m/^(���?\.)$/i)		{ $word=$illjustracii{$sin_or_pl}{$case}; }
			when (m/^(���\.)$/i)		{ $word=$risunki{$sin_or_pl}{$case}; }
			when (m/^(��\.)$/i)			{ $word=$shemy{$sin_or_pl}{$case}; }
			when (m/^(����?\.)$/i)		{ $word=$tablicy{$sin_or_pl}{$case}; }
			when (m/^(�\.)$/i)			{ $word=$chasti{$sin_or_pl}{$case}; }
			when (m/^���\b\.?$/i)		{ $word = $kabinety{$sin_or_pl}{$case};}
			when (m/^��\b\.?$/i) 		{ $word = $knigi{$sin_or_pl}{$case};}
			when (m/^�\b\.?$/i )		{ $word = $doma{$sin_or_pl}{$case};}
			when (m/^��\b\.?$/i) 		{ $word = $kvartiry{$sin_or_pl}{$case};}
			when (m/^��\b\.?$/i) 		{ $word = $jetazhi{$sin_or_pl}{$case};}
			}
		}
	
	if(($word =~ m/^����(�|�|�|�|��|��|���|��|)$/i)&&((length($number_s)>1)||(length($number2_s)>1))) { next; }
	
	$phrase=$prev_word.$non_word.$word.' '.num2word($number_s, 'kol', 'im', 'm');
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	if($number2_s =~ m/\d/) { $phrase .= $connection.num2word($number2_s, 'kol', 'im', 'm');};
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"replace numerals preceded by" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}
	
# ***** replace numerals preceded by: paragraph, etc

# ----- replace numerals preceded by: godu, vekah, etc

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*)?(\s*)\b((���|���)[�-��]{0,3})\s{0,2}(\d+)((((\s*)(\-|\�|\�|\/)\s*)||(\s+(���|�)\s+))(\d+))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word=$2;
	my $word=$3;
	my $number_s=$5;
	my $and_or=$12;
	my $number2_s=$13;
	
	my $case;
	my $sin_or_pl;
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		# if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��|�������.*|�������.*|�������.*|����.*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			my $stop_flag=0;
			my @morph_properties=get_morph_props($prev_word, '-i -w');
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if($morph_properties[$i]{pos} =~ m/NUM/) { $stop_flag=1; $i=$#morph_properties+1; } # if the word in front of "godu" is a numeral, then it should be skipped by algorithm 
				}
			
			if($stop_flag==1) { next; }
			# print '*'.$this_line."\~\n";
			
			for($word)
				{
				when (m/^(���|���)(�|��)$/i)			{ $case = 'rod'; }
				when (m/^(���|���)(��|���)$/i)			{ $case = 'tv'; }
				when (m/^(���|���)(��)$/i)				{ $case = 'dat'; }
				when (m/^(���|���)(�|��)$/i)			{ $case = 'pr'; }
				when (m/^����$/i)						{ 
														my $rest_length=length($this_line)-$pos_begin;
														if($this_line =~ m/\b�\s+(\b[�-��]+\b\s+)?.{$rest_length}$/i) { $case = 'pr'; } # v novom godu 2007
														elsif($this_line =~ m/\b�\s+(\b[�-��]+\b\s+)?.{$rest_length}$/i) { $case = 'dat'; }
														else { $case = 'im'; }
														}
				default								{ $case = 'im'; }
				}
			}
		}
	
	$phrase=$prev_word.$non_word.$word.' '.num2word($number_s, 'por', $case, 'm');
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	if($number2_s =~ m/\d/) { $phrase .= $connection.num2word($number2_s, 'por', $case, 'm');};
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"replace numerals preceded by" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# print $this_line."\~\n"; exit;

if($this_line !~ m/\d/) { return $this_line;}
	
# ***** replace numerals preceded by: godu, vekah, etc

# ----- detect the numbers followed by special nouns (only plural form) and replace them where possible

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*)(\s*)\b(\d+([\.\,]\d+)?)(((\s*(\-|\�|\�)\s*)|(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?(?=\s+([�-��]+)(\s+([�-��]+))?)/ig)
	{
	# Usage of ?= is a looking ahead, it does not belong to $&, but is checked for correctness (?= is not counted as a brace pair.
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	my $and_or=$10;
	my $number2_s=$11;
	my $next_word=$13;
	my $next_word2=$15;
	
	# Special Exceptions

	my $noun = '';
	my $flag = 0; # The necessary noun is found
	
	if($next_word =~ m/^(����?�|������|����|����|�����$|����?�|���|����|�����|���|������|����?�|�������|������|���|���|����|���������|�����|���|���|������?|������?�|��[�]�|������?�|������?�|�����|����|����|��������?�|�������)(���|��|�|��|�|��|��|��|���|��|��|�|)$/) { $flag = 1; $noun = $next_word;}
	if(($flag == 0)&&($next_word2 =~ m/^(����?�|������|����|����|�����$|����?�|���|����|�����|���|������|����?�|�������|������|���|���|����|���������|�����|���|���|������?|������?�|��[�]�|������?�|������?�|�����|����|����|��������?�|�������)(���|��|�|��|�|��|��|��|���|��|��|�|)$/)) { $flag = 1; $noun = $next_word2;}
	
	if($flag == 0) { next; }	
	
	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
	
	$phrase = $phrase.$prev_word.$non_word1;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection = ' '.$1.' ';} else { $connection =' '; }
	
	my $gender;
	
	if(($next_word2 =~ m/^(����?�|������|����|����|�����$|����?�|����|�����|���|������|����?�|�������|������|���|���|����|���������|�����|���|���|������?|������?�|��[�]�|������?�|������?�|�����|����|����|��������?�|�������)(���|��|�|��|�|��|��|��|���|��|��|�|)$/)&&($flag == 0)) { $flag = 1; $noun = $next_word2;} # chasy and not chas
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if(($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����|���|��|�������|�����������)$/i)&&($noun =~ m/(�|�|�|�|��|��|��?|�?�)$/)) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�)$/i) && ($noun =~ m/(��|��)$/)) # (Dat) # Could be po plus vin "po 1 sanjam"
			{
			$case = 'dat';
			}
		elsif(($prev_word =~ m/^(�����|���)$/i) || ($noun =~ m/(���|���)$/)) # (Tv)
			{
			$case = 'tv';
			}
		elsif(($prev_word =~ m/^(�|��|�)$/i) || ($noun =~ m/(��|��)$/)) # (Pr)
			{
			$case = 'pr';
			}	
		elsif(($prev_word =~ m/^(��|��|�����|���)$/i)&&($noun =~ m/(�|�|�|�|�|�|�|�|��|��|��?|�?�)$/)) # (Vin)
			{
			$case = 'vin';
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($noun =~ m/(�|�|�|�|��|��|��?|�?�|��|�|��)$/)) # (Vin)
			{
			$case = 'vin';
			}
		elsif(($prev_word !~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����|���|��|�������|�����������|�|�����|�|��|�|��|��|�����|��)$/i)&&($noun =~ m/(�|�|�|�|�|�|�|�|��|��|��?|�?�)$/)) # (Im)
			{
			$case = 'im';
			}		
		else
			{
			next;
			}
		}
	
	# --- processing exceptions
	
	if(($prev_word =~ m/^�$/) && ($next_word !~ m/(��$|��$)/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	# *** processing exceptions
	
	$gender = 'm';
	my $aux_addition='';
	
	if(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
		{
		if($number_s =~ m/[\.\,]50*$/)
			{
			$aux_addition = ' � ���������';
			$number_s =~ s/[\.\,]50*$//;
			}
			
		if($number_s =~ m/[\.\,]/)
			{
			$phrase .= realnum2word_po($number_s,'kol',$case,$gender);
			}
		elsif($number_s =~ m/([^\D1]|^0*)1$/)
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$case,$gender);
			$phrase =~ s/\w+\s*$/�����/;
			}
		elsif($number_s =~ m/^0*[2-4]$/)
			{
			if($number_s =~ m/^0*2$/) { $phrase .= '����'; }
			elsif($number_s =~ m/^0*3$/) { $phrase .= '����'; }
			elsif($number_s =~ m/^0*4$/) { $phrase .= '�������'; }
			}
		elsif($number_s =~ m/[^\D1][2-4]$/)
			{
			$phrase = $phrase.num2word($number_s,'kol','dat',$gender);
			}
		else
			{
			$phrase = $phrase.num2word($number_s,'kol','im',$gender);
			}
		
		
		$phrase .= $aux_addition;
		$aux_addition='';
		
		if($number2_s =~ m/\d/)
			{
			$phrase .= $connection;
			
			if($number2_s =~ m/[\.\,]50*$/)
				{
				$aux_addition = ' � ���������';
				$number2_s =~ s/[\.\,]50*$//;
				}
			
			if($number2_s =~ m/[\.\,]/)
				{
				$phrase .= realnum2word_po($number2_s,'kol',$case,$gender);
				}
			elsif($number2_s =~ m/([^\D1]|^0*)1$/)
				{
				$phrase = $phrase.realnum2word_po($number2_s,'kol',$case,$gender);
				$phrase =~ s/\w+\s*$/�����/;
				}
			elsif($number2_s =~ m/^0*[2-4]$/)
				{
				if($number2_s =~ m/^0*2$/) { $phrase .= '����'; }
				elsif($number2_s =~ m/^0*3$/) { $phrase .= '����'; }
				elsif($number2_s =~ m/^0*4$/) { $phrase .= '�������'; }
				}
			elsif($number2_s =~ m/[^\D1][2-4]$/)
				{
				$phrase = $phrase.num2word($number2_s,'kol','dat',$gender);
				}
			else
				{
				$phrase = $phrase.num2word($number2_s,'kol','im',$gender);
				}
			}
		}
	elsif($case =~ m/^(im|vin)$/)
		{
		if($number_s =~ m/[\.\,]50*$/)
			{
			$aux_addition = ' � ���������';
			$number_s =~ s/[\.\,]50*$//;
			}
		
		if($number_s =~ m/[\.\,]/)
			{
			$phrase .= realnum2word($number_s,'kol',$case,$gender);
			}
		elsif($number_s =~ m/([^\D1]|^0*)1$/)
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$case,$gender);
			$phrase =~ s/\w+\s*$/����/;
			}
		elsif($number_s =~ m/^0*[2-4]$/)
			{
			if($number_s =~ m/^0*2$/) { $phrase .= '����'; }
			elsif($number_s =~ m/^0*3$/) { $phrase .= '����'; }
			elsif($number_s =~ m/^0*4$/) { $phrase .= '�������'; }
			}
		elsif($number_s =~ m/[^\D1][2-4]$/)
			{
			next;
			}
		else
			{
			$phrase = $phrase.num2word($number_s,'kol','im',$gender);
			}
		
		$phrase .= $aux_addition;
		$aux_addition='';
		
		if($number2_s =~ m/\d/)
			{
			$phrase.= $connection;
			if($number2_s =~ m/[\.\,]50*$/)
				{
				$aux_addition = ' � ���������';
				$number2_s =~ s/[\.\,]50*$//;
				}
			
			if($number2_s =~ m/[\.\,]/)
				{
				$phrase .= realnum2word($number2_s,'kol',$case,$gender);
				}
			elsif($number2_s =~ m/([^\D1]|^0*)1$/)
				{
				$phrase = $phrase.realnum2word($number2_s,'kol',$case,$gender);
				$phrase =~ s/\w+\s*$/����/;
				}
			elsif($number2_s =~ m/^0*[2-4]$/)
				{
				if($number2_s =~ m/^0*2$/) { $phrase .= '����'; }
				elsif($number2_s =~ m/^0*3$/) { $phrase .= '����'; }
				elsif($number2_s =~ m/^0*4$/) { $phrase .= '�������'; }
				}
			elsif($number2_s =~ m/[^\D1][2-4]$/)
				{
				next;
				}
			else
				{
				$phrase = $phrase.num2word($number2_s,'kol','im',$gender);
				}
			}
		}
	else
		{
		if($number_s =~ m/[\.\,]50*$/)
			{
			$aux_addition = ' � ���������';
			$number_s =~ s/[\.\,]50*$//;
			}
		
		if($number_s =~ m/[\.\,]/)
			{
			$phrase .= realnum2word($number_s,'kol',$case,$gender);
			}
		else
			{
			$phrase = $phrase.num2word($number_s,'kol',$case,'pl');
			}
				
		$phrase .= $aux_addition;
		$aux_addition='';
		
		if($number2_s =~ m/\d/)
			{
			$phrase .= $connection;
			if($number2_s =~ m/[\.\,]50*$/)
				{
				$aux_addition = ' � ���������';
				$number2_s =~ s/[\.\,]50*$//;
				}
			
			if($number2_s =~ m/[\.\,]/)
				{
				$phrase .= realnum2word($number2_s,'kol',$case,$gender);
				}
			else
				{
				$phrase = $phrase.num2word($number2_s,'kol',$case,'pl');
				}
			}
		}
		
	$phrase .= $aux_addition;
	
	# if($this_line =~ m/������/) { print $this_line.'~'.$phrase."\~\n"; exit;}
	
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect the numbers followed by special nouns (only plural form) and replace them where possible

# ----- detect time like "k 2 - 5 utra"

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*)?(\s*)\b(\d{1,2})\b(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))\b(\d{1,2}))?\s+(?=(����|���|���������|������|����)\b)/ig)
	{
	# Usage of ?= is a looking ahead, it does not belong to $&, but is checked for correctness (?= is not counted as a brace pair.
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $anim='';	# odushevlennoe, neodushevlennoe
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	my $and_or=$9;
	my $number2_s=$10;
	my $next_word=$11;
	
	# Special Exceptions
	if($next_word =~ m/^[A-Z�-ߨ]/){ next; } # to exclude "5 John Franklin ..." Not necessary
	
	if((substr($this_line, 0, $pos_begin) =~ m/\d[\,\.]$/)&&($non_word1 eq '')) { next; }	# 2,3 dnya
	
	my $gender='m'; # Chas
	
	# preliminary determination of the case
	
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|��|��|�����|�����|�����|������)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|�)$/i) # (Vin)
			{
			$case = 'vin';
			}
		elsif($prev_word =~ m/^(�����|�����|��)$/i) # Skip
			{
			next;
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	# --- processing exceptions
		
	# *** processing exceptions
	
	$phrase = $phrase.$prev_word.$non_word1;
	
	if($number_s =~ m/^1$/)
		{
		$phrase .= $chasy{sin}{$case}.' ';
		}
	else
		{
		$phrase .= num2word($number_s,'kol',$case,$gender);
		}
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection = ' '.$1.' ';} else { $connection =' '; }
	$phrase .= $connection;
	
	if($number2_s =~ m/^1$/)
		{
		next;
		}
	elsif($number2_s =~ m/\d/)
		{
		$phrase .= num2word($number2_s,'kol',$case,$gender).' ';
		}
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect time like "k 2 - 5 utra"

# Detect number plus/minus number

$this_line =~ s/\b(\d+([\.\,]\d+)?)\s+(����|�����|��������|(���)?������)\s+(\d+([\.\,]\d+)?)\b/realnum2word($1, 'kol', 'im', 'm').' '.$3.' ' .realnum2word($5, 'kol', 'im', 'm')/eig;

# ----- detect the numbers followed by nouns (adjectives also) and replace them where possible

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*)?(\s*)\b(\d+([\.\,]\d+)?)(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?\s+(?=([�-��]+)(\s+([�-��]+))?)/ig)
	{
	# Usage of ?= is a looking ahead, it does not belong to $&, but is checked for correctness (?= is not counted as a brace pair.
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $anim='';	# odushevlennoe, neodushevlennoe
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $number_s=$3;								#as strings
	my $and_or=$10;
	my $number2_s=$11;
	my $next_word=$13;
	my $next_word2=$15;
	
	# Special Exceptions
	if(($next_word =~ m/^[A-Z�-ߨ]/)&&($prev_word eq '')){ next; } # to exclude "5 John Franklin ..."
	
	# if($next_word =~ m/^(����|������|���|����)$/i)
		# {
		# if(($number_s !~ m/^(\d{1,2})[\.\,](\d{2})$/) && (($number2_s !~ m/^(\d{1,2})[\.\,](\d{2})$/)||($number2_s eq '')) && ($number_s !~ m/^(\d{0,2})$/))
			# {
			# }
		# elsif($prev_word !~ m/^(|�����|��|��)$/i)
			# {
			# next;
			# }
		# }
	
	if($next_word =~ m/^(�����(�|��)|�����(�|��)|�����(�|��)|��������������)$/i) { next; } # metrov 20 vysotoj
	my $coming_after_s=substr($this_line, $pos_end); # from till the end of the string
	if($coming_after_s =~ m/^(\b��\s+)?\b�����\s+���/i)	{ next; }
	
	# Read the properties of the word from mystem program;
	my @morph_properties=get_morph_props($next_word, '-i -w');
	my $flag=0; # Shows that the word is a noun
	my $flag_adj=0;
	my $pos='';
	my $flag_noun_word1=0; 	# The first word can be a noun
	my $flag_noun_word2=0; 	# The second word can be a noun
	
	for(my $i=0; $i<$#morph_properties+1; $i++) # check if the 1st word could be a noun
		{
		if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/^(����|���|���)$/)) { $flag=1; $pos='S'; $flag_noun_word1=1; $i=$#morph_properties+1; }
		}
	for(my $i=0; $i<$#morph_properties+1; $i++) # check if the 1st word could be also adjective, if yes try the 2nd word
		{
		if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties[$i]{adj_form} ne '��')) { $flag_adj=1; $pos='A'; $flag = 0; $i=$#morph_properties; } # Toros okolo 20 futov.
		};
		
	my @morph_properties_temp;
	if(($flag == 0)&&($flag_adj == 1)&&($next_word2 =~ m/[�-��]/i)) # if the 1st word could adjective, try the 2nd one to be a noun
		{
		@morph_properties_temp=get_morph_props($next_word2, '-i -w');
		for(my $i=0; $i<$#morph_properties_temp+1; $i++)
			{
			if(($morph_properties_temp[$i]{pos} eq 'S')&&($morph_properties_temp[$i]{gender} ne '')&&($morph_properties_temp[$i]{misc} !~ m/(����|���|���)/)) {$flag=1; $pos='S'; $flag_noun_word2=1; }
			}
		}
	
	if($flag_noun_word2 == 1)
		{
		@morph_properties=@morph_properties_temp;
		}
	elsif(($flag == 0)&&($flag_adj == 1))
		{
		$flag = 1;
		if($flag_noun_word1 == 1) { $pos = 'S'; } else { $pos = 'A'; };
		}		
	
	if($flag == 0) { next;}
	
	# if the pos is A and the case is plural only (e.g. 47000 krasivyh)
	my $number='';
	if($pos eq 'A')
		{
		$number='pl';
		for(my $i=0; $i<$#morph_properties+1; $i++) # check if the case is always plural
			{
			if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties[$i]{adj_form} ne '��')&&($morph_properties[$i]{number} ne 'pl')) { $number=''; $i=$#morph_properties; }
			};
		}
	
	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
	
	$phrase = $phrase.$prev_word.$non_word1;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly

	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	$gender ='mfn'; # Important, that the gender can be whatever, except when it is known
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	
	if(($flag_noun_word1 == 1) && ($next_word =~ m/^���(�|�|�|�|��|�|��|���|��)$/))
		{
		$gender = 'f';
		}
	elsif(($flag_noun_word2 == 1) && ($next_word2 =~ m/^���(�|�|�|�|��|�|��|���|��)$/))
		{
		$gender = 'f';
		}
	
	# --- if the number of the adjective is always plural 
	
	# --- if the number of the adjective is always plural 
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/ )) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# S 25 Chelovekami. Changed. Only plural form
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($next_word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; }	# S 1 Chelovekom. Changed. Singular form
	
	# if(($prev_word =~ m/^�$/i)&&($case_aux = 'tv'))	# !!! Could be added everythere
		# {
		# $case = 'tv';
		# }
	
	# --- processing exceptions
	
	if(($prev_word =~ m/^�$/i) && ($flag_noun_word1 == 1) && ($next_word !~ m/(�$|�$)/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/))) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	elsif(($prev_word =~ m/^�$/i) && ($flag_noun_word2 == 1) && ($next_word2 !~ m/(�$|�$)/i) && (($last_number_s !~ m/[\.\,]/)||($last_number_s =~ m/[\.\,]50*/))) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	if(($prev_word =~ m/^��$/i) && ($flag_noun_word1 == 1) && ($next_word =~ m/(�$|�$)/i)) { $case = 'pr'; } # To exclude the case "na 5 lodkah"
	elsif(($prev_word =~ m/^��$/i) && ($flag_noun_word2 == 1) && ($next_word2 =~ m/(�$|�$)/i)) { $case = 'pr'; } # To exclude the case "na 5 lodkah"
	
	# *** processing exceptions
	
	my $sin_or_pl;
	my $kol_por='kol';
	$flag=0;
	my $array_elem=0;
	
	my $case_of_noun='';	# Special case according to the noun (for some special situations only)
	
	# *** if the number has 000 at the end
	if(($last_number_s =~ m/000$/)&&($last_number_s !~ m/[\.\,]/))
		{
		for(my $p=0; $p<$#morph_properties+1; $p++) 
			{
			if(($morph_properties[$p]{pos} eq $pos) && ((($morph_properties[$p]{case} eq 'rod')&&($case =~ m/(im|vin)/))||(($morph_properties[$p]{case} eq $case)&&($case !~ m/(im|vin)/))) && ($morph_properties[$p]{number} eq 'pl') && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/))
				{
				$flag=1;
				# $case;
				$case_of_noun=$morph_properties[$p]{case};
				$gender=$morph_properties[$p]{gender};
				$sin_or_pl=$morph_properties[$p]{number};
				$array_elem=$p;
				$p=$#morph_properties+1;	
				}
			}	
		
		if($flag==0) # if singular, this could be por
			{		
			for(my $p=0; $p<$#morph_properties+1; $p++) 
				{
				if(($morph_properties[$p]{pos} eq $pos) && (($morph_properties[$p]{case} eq $case) || ($case eq 'im')) && ($morph_properties[$p]{number} eq 'sin') && ($gender =~ m/\Q$morph_properties[$p]{gender}\E/i) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
					{
					$flag=1;
					$case = $morph_properties[$p]{case};
					$case_of_noun=$morph_properties[$p]{case};
					$kol_por='por';
					$gender=$morph_properties[$p]{gender};
					$sin_or_pl='sin';
					$array_elem=$p;
					$p=$#morph_properties+1;
					}
				}
			}
		
		if($flag==0) { next; }
		}
	elsif($last_number_s =~ m/[\.\,]/) # if the last number is fractional
		{
		for(my $p=0; $p<$#morph_properties+1; $p++)
			{
			if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} eq 'rod') && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) &&($morph_properties[$p]{adj_form} ne '��')) # including not a proper case "k 22,3 gruzovikov" (should be "gruzovika")
				{
				$flag=1;
				# $case;
				$case_of_noun=$morph_properties[$p]{case};
				$gender=$morph_properties[$p]{gender};
				$sin_or_pl=$morph_properties[$p]{number};
				$array_elem=$p;
				$p=$#morph_properties+1;
				}
			}
		}
	
	if($flag == 0) # includes also the non-gramatically correct phrases "k 3,35 Amperam" and correct ones "k 3 c polovinoj Amperam"	
		{
		if($case =~ m/(im|vin)/i)
			{
			if($number_interval eq '1')
				{
				for(my $p=0; $p<$#morph_properties+1; $p++)
					{
					if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} =~ m/(im|vin)/i) && (($morph_properties[$p]{number} eq 'sin')||($number2_s =~ m/\d/)) && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
						{
						if(($case eq 'vin')&&($morph_properties[$p]{case} eq 'im')) { next; } #
						elsif(($case eq 'im')&&($morph_properties[$p]{case} eq 'vin')) { $case = 'vin'; } # necessary for the case, like "on uvidel 21 kartinu"
						
						$flag=1;
						# $case;
						$case_of_noun=$morph_properties[$p]{case};
						$gender=$morph_properties[$p]{gender};
						$sin_or_pl=$morph_properties[$p]{number};
						$array_elem=$p;
						$anim=$morph_properties[$p]{anim};
						$p=$#morph_properties+1;
						}
					}
				}
			elsif($number_interval eq '2_4')
				{
				for(my $p=0; $p<$#morph_properties+1; $p++)
					{
					if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} eq 'rod') && (($morph_properties[$p]{number} eq 'sin')||($number2_s =~ m/\d/)) && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) &&($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
						{
						$flag=1;
						# $case;
						$case_of_noun=$morph_properties[$p]{case};
						$gender=$morph_properties[$p]{gender};
						$sin_or_pl=$morph_properties[$p]{number};
						$array_elem=$p;
						$p=$#morph_properties+1;
						}
					}
				}
			elsif($number_interval eq '0_5_20')
				{
				for(my $p=0; $p<$#morph_properties+1; $p++)
					{
					if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} eq 'rod') && ($morph_properties[$p]{number} eq 'pl') && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
						{
						$flag=1;
						# $case;
						$case_of_noun=$morph_properties[$p]{case};
						$gender=$morph_properties[$p]{gender};
						$sin_or_pl=$morph_properties[$p]{number};
						$array_elem=$p;
						$p=$#morph_properties+1;
						}
					}
				}
			}
		else # not im|vin case
			{
			for(my $p=0; $p<$#morph_properties+1; $p++) 
				{
				if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} eq $case) && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
					{
					$flag=1;
					# $case;
					$case_of_noun=$morph_properties[$p]{case};
					$gender=$morph_properties[$p]{gender};
					$sin_or_pl=$morph_properties[$p]{number};
					$array_elem=$p;
					$p=$#morph_properties+1;	
					}
				}
			}
		}
	
	if($flag == 0) # if case is dat and po is prev_word
		{
		if($prev_word =~ m/^(��)$/i)
			{
			for(my $p=0; $p<$#morph_properties+1; $p++)
				{
				if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} eq 'dat') && (($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
					{
					$flag=1;
					if($number_interval eq '1')
						{
						$case = 'vin';
						}
					else
						{
						$case = 'dat';
						$kol_por = 'por';
						}
					# $case;
					$case_of_noun=$morph_properties[$p]{case};
					$gender=$morph_properties[$p]{gender};
					$sin_or_pl=$morph_properties[$p]{number};
					$array_elem=$p;
					$p=$#morph_properties+1;
					}
				}
			}	
		}
	
	if($flag eq 0) # in other cases
		{
		my $flag2=0;

		for(my $p=0; $p<$#morph_properties+1; $p++)
			{
			if(($morph_properties[$p]{pos} eq $pos)&&(($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)||($morph_properties[$p]{number} eq $number)) && ($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
				{
				
				if(($number_s =~ m/^[12]\d{3}$/) && ($number2_s =~ m/^(|[12]\d{3})$/))	# S 1950 Japoncy
					{
					next;
					}
				
				$case=$morph_properties[$p]{case};
				$case_of_noun=$morph_properties[$p]{case};
				$gender=$morph_properties[$p]{gender};
				$sin_or_pl=$morph_properties[$p]{number};
				$p=$#morph_properties+1;
				$flag2=1;
				}
			}
		
		my $rest_length=length($this_line)-$pos_begin;
		if($flag2 == 1)	# check the previous word to be 'empty' or not a 'preposition', 'noun' or 'adjective'
			{
			if($prev_word =~ m/[�-��]/i)
				{
				my $flag3=1; # prev_word is not S,A,PR
				my @morph_properties_temp2;
				@morph_properties_temp2=get_morph_props($prev_word, '-i -w');
				for(my $p=0; $p<$#morph_properties_temp2+1; $p++)
					{
					if((($morph_properties_temp2[$p]{pos} eq 'S')&&($morph_properties_temp2[$p]{gender} ne '')&&($morph_properties_temp2[$p]{misc} !~ m/(����|���|���)/)) || (($morph_properties_temp2[$p]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties_temp2[$p]{adj_form} ne '��')) || ($morph_properties_temp2[$p]{pos} eq 'PR'))
						{
						if(($morph_properties_temp2[$p]{pos} eq 'PR')&&($this_line !~ m/[�-��]\s+.{$rest_length}$/i)) { next; }
						$flag3 = 0;
						$p=$#morph_properties_temp2+1;
						}
					}
				if($flag3 == 1) { $flag = 1;}
				}
			else
				{
				$flag = 1;
				}
			}
		}
	
	if($flag == 0) { next; }
	
	# if the pos is A (no noun next) and the case is plural
	if(($gender eq '')&&($number eq 'pl')) { if(($number_s !~ m/($|[^1])1$/)&&($number2_s !~ m/($|[^1])1$/)) { $gender = 'm'; } else { next; } }		
	
	# change to por if the word is in a singular form
	if(($number_interval ne '1') && ($sin_or_pl eq 'sin') && ($last_number_s !~ m/[\.\,]/))
		{
		if(($number_interval ne '2_4' )||($case !~ m/(im|vin)/i))
#		if(($number_interval ne '2_4' )||($case !~ m/(im|vin)/i)||($prev_word eq ''))
			{
			$kol_por='por';
			}
		elsif(($case_of_noun =~ m/^(im|)$/)&&($flag == 1))
			{
			$kol_por='por';
			}
		}
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if(($case eq 'vin')&&($anim =~ m/^��$/i)&&($prev_word !~ m/^��$/i)&&($gender eq 'm')) # for the case vizhu 21-25 chelovek
			{
			if($number_s =~ m/($|[^1])1$/) 		{ $first_number_case = 'rod'; }
			if($number2_s =~ m/($|[^1])1$/) 	{ $second_number_case = 'rod'; }
			}
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
		
		if(($case eq 'vin')&&($anim =~ m/^��$/i)&&($prev_word !~ m/^��$/i)&&($gender eq 'm')) # for the case vizhu 21 chelovek
			{
			if($number_s =~ m/($|[^1])1$/) 		{ $first_number_case = 'rod'; }
			}
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender);
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender);
			};
		}
	
	$phrase .= ' ';
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** detect the numbers followed by nouns (adjectives also) and replace them where possible

# ----- Detect (noun)?-preposition number(-/and/or number)

while ($this_line =~ m/((\b[�-��]*)\s*)?\b([�-��]+)\s+(\d+([\.\,]\d+)?)(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase='';
	my $matching=$&;
	my $word_full=$1; # with spaces
	my $word=$2;
	my $prev_word=$3;
	my $number_s=$4;								#as strings
	my $and_or=$11;
	my $number2_s=$12;
	
	$phrase = $phrase.$word_full.$prev_word.' ';
	
	# Brought to here from the bottom
	my $line_before_number=substr($this_line, 0, $pos_begin).$word_full.$prev_word;
	my $coming_after_s=substr($this_line, $pos_end); # from till the end of the string
	my $coming_before_s=substr($this_line, 0, $pos_begin);
		
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	if((($number_s =~ m/^[12]\d{3}$/)&&($number2_s =~ m/^([12]\d{3}|)$/))||($line_before_number =~ m/\b(�������|����|���������[�-��]*|�����[�-�]*|����?�[�-�]*|�������[�-�]*|�������[�-�]*|�������[�-�]*|����[�-�]*)\s*(\b�|\b�����)?$/i)||($coming_after_s =~ m/^\s*(\b��\s+)?\b�����\s+���/i)||(($prev_word =~ m/^�����$/i)&&(($coming_after_s =~ m/^\s*\-*\s*�����\b/i)||($coming_before_s =~ m/\b�����\b.*���\W*$/i))))	# If this is a year, like: in 1999.
		{
		my $temp_case='';
		if($line_before_number =~ m/\b(�����[�-�]*|����?�[�-�]*|�������.*|�������.*|�������.*|����.*|����|�\s+��������|�\s+�����������|�\s+��[�]�|��\s+�����|������|������\s+��|������\s+��|�����|��\s+�����������|��\s+��[�]�|������\s+��|����������|���������|����[�]�|�������\s+�|���������\s+��|������������|�����|�|�|��|��|�����|����������|��������[���]|��|�?����)\s*$/i)
			{
			$temp_case = 'rod';
			}
		elsif($line_before_number =~ m/\b(���������|��\s+����\s+�|���������|���������|��\s+�\s+������|��\s+���������\s+�|�������|�������������\s+�|������\s+��|��������|���������|����������|����\s+��|�|�?����)\s*$/i)
			{
			$temp_case = 'dat';
			}
		elsif($line_before_number =~ m/\b(�����|���|�\s+�����\s+�|�\s+������������\s+�|�\s+���������\s+�|�\s+������\s+�|�\s+�������\s+�|������\s+�|��\s+�����\s+�|�������\s+�|�����\s+��|�������\s+�|������\s+�|��\s+���������\s+�|�����\s+�|������\s+��|���������\s+�|��������\s+�|���������\s+�|��������������\s+�|����������\s+�|������������\s+�|���������[�-��]*|�?���)\s*$/i) # (Tv)
			{
			$temp_case = 'tv';
			}
		elsif($line_before_number =~ m/^\b(��\s+������\s+�|�|�|�?���)\s*$/i) # (Pr)
			{
			$temp_case = 'pr';
			}	
		elsif($line_before_number =~ m/^\b(�������|��������|��������\s+��|��������\s+��|��|��|�����|��|���|�?���)\s*$/i) # (Vin)
			{
			$temp_case = 'vin';
			}
		
		if($temp_case ne '')
			{
			$phrase .= num2word($number_s, 'por', $temp_case, 'm');
			
			if($number2_s =~ m/\d/)
				{
				$phrase .= $connection.num2word($number2_s, 'por', $temp_case, 'm');
				}
			
			$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
			pos($this_line)=$pos_begin+length($phrase);
			next;
			}
		}
	
	# if(($number2_s eq '')&&($number_s =~ m/^[12]?\d{3}$/)&&($prev_word =~ m/^(�����[�-�]*|����?�[�-�]*|�������.*|�������.*|�������.*|����.*)$/i))
		# {
		# $phrase .= num2word($number_s, 'por', 'rod', 'm');
		# $this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		# pos($this_line)=$pos_begin+length($phrase);
		# next;
		# }

	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
	
	# check either everything was caught correctly

	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
		
	my $flag_0_5_20=1; # Determine, either all the numbers belong to 0_5_20
	if($number_interval ne '0_5_20')
		{
		$flag_0_5_20=0;
		}
	elsif($number2_s =~ m/\d/)
		{
		if( (int(substr($number[0],-2)) > 4) && (int(substr($number[0],-2))<21) ) {$number_interval = '0_5_20';}
		elsif(($number_s =~ m/^1[\.\,]50*$/)||($number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
		elsif($number[0] =~ m/1$/) { $number_interval = '1';}
		else { $number_interval = '0_5_20'};
		if($number_interval ne '0_5_20')
			{
			$flag_0_5_20=0;
			}	
		}
	
	# preliminary determination of the case
	# my $line_before_number=substr($this_line, 0, $pos_begin).$word_full.$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		if($line_before_number =~ m/\b(����|�\s+�����|�\s+����|�\s+��������|�\s+����|�\s+�����������\s+��|�\s+���������|�\s+��������|�\s+�����������|�\s+�������|�\s+�������\s+��|�\s+���������|�\s+������|�\s+�������|�\s+��������|�\s+�����������|�\s+������|�\s+������|�\s+����������|�\s+����|�\s+����|�\s+������|�\s+������|�\s+�������������|�\s+�������|�\s+�������\s+��|�\s+�����|�\s+��[�]�|�\s+�����|�\s+�����|�\s+�����|������|������\s+��|������|�������\s+��|�����\s+��|�����|������|������|���|���\s+������|���\s+��������|���\s+�����|���\s+�����|��\s+�����|��\s+���|�����|������|�������|������\s+��|������\s+��|�����|����������|��\s+�����������|��\s+��[�]�|������\s+��|����������|��\s+���������|��\s+�������|��\s+����|��\s+�����|���������|����[�]�|�������\s+�|���������\s+��|����������\s+��|������������|��\s+������|��\s+�����|��\s+�������|��\s+������|��\s+�������|��\s+������|��\s+�����|���\s+�����|���\s+������|���\s+������|���\s+����������|���\s+�������|���[�]�|�\s+�����\s+������|�\s+�����|��\s+�������|����|�����|�����\s+����������|���������|��\s+����\s+�|���������|���������|��\s+�\s+������|��\s+�����������\s+�|��\s+���������\s+�|�������|�������������\s+�|������\s+��|��������|���������|��������������|����������|����\s+��|�\s+�����\s+��|�������|��������|��������\s+��|��������\s+��|�\s+�����\s+�|�\s+������������\s+�|�\s+��������\s+�|�\s+�����������\s+�|�\s+����������\s+�|�\s+���������\s+�|�\s+������\s+�|�\s+�������\s+�|������\s+�|��\s+�����\s+�|�������\s+�|�����\s+��|�������\s+�|������\s+�|��\s+���������\s+�|�����\s+�|������\s+��|���������\s+�|��������\s+�|���������\s+�|��������������\s+�|����������\s+�|������������\s+�|��\s+������\s+�)\s*$/i) { next; } # Po Adresu 3
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|��?|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		elsif($prev_word =~ m/^(�?���|�?��|�?��|���|��)$/i) # (Im)
			{
			$case = 'im';
			}
		else # (Im)
			{
			next; #new
			}
		}
		
	# --- processing exceptions
	
	if(($word =~ m/(�$|�$|^����$)/i) && ($prev_word =~ m/^�$/i)) { $case = 'pr'; } # kilometrah v 20.

	if(($prev_word =~ m/^�$/i)&&($word =~ m/(��)\.?$/i)) { $case = 'tv'; }	# Chelovekami s 25. Changed. Only plural form
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions
	
	my $anim='';
	
	if($flag_0_5_20 == 0) # if the interval of a number is not "0_5_20"
		{
		my @morph_properties=get_morph_props($word, '-i -w');
		my $flag=0; # Shows that the word is a noun
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} ne '����') && (($morph_properties[$i]{case} eq $case)||(($morph_properties[$i]{case} eq 'rod')&&($case eq 'vin'))))
				{
				$flag=1;
				$gender=$morph_properties[$i]{gender};
				
				if($morph_properties[$i]{case} =~ m/(rod|vin)/)
					{ $anim=$morph_properties[$i]{anim}; }
				
				$i=$#morph_properties+1;
				}
			}
		if($flag == 0) { $gender='m';}
		}
	else
		{
		$gender='m';
		}
	
	if($word =~ m/^���(�|�|�|�|�|��|��|���|��)$/i) { $gender = 'f'; }
	
	my $kol_por='kol';
	if($word =~ m/^����$/i) { $kol_por = 'por'; }
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if(($case eq 'vin')&&($anim =~ m/^��$/i)&&($prev_word !~ m/^(��)$/i)&&($gender eq 'm')) # for the case chelovek na 21-25
			{
			if($number_s =~ m/($|[^1])1$/) 		{ $first_number_case = 'rod'; }
			if($number2_s =~ m/($|[^1])1$/) 	{ $second_number_case = 'rod'; }
			}
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
		
		if(($case eq 'vin')&&($anim =~ m/^��$/i)&&($prev_word !~ m/^(��)$/i)&&($anim =~ m/^��$/i)&&($gender eq 'm')) # for the case chelovek na 21-25
			{
			if($number_s =~ m/($|[^1])1$/) 		{ $first_number_case = 'rod'; }
			}
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ***** Detect (noun)?-preposition number(-/and/or number)

# ----- Detect (preposition)?-noun number(-/and/or number)

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b([�-��]+)\s+)?(\b[�-��]+)\s+(\d+([\.\,]\d+)?)(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $anim='';	# odushevlennoe, neodushevlennoe
	my $phrase='';
	my $matching=$&;
	my $prev_word_full=$1; # with spaces
	my $prev_word=$2;
	my $word=$3;
	my $number_s=$4;								#as strings
	my $and_or=$11;
	my $number2_s=$12;
	
	$phrase .= $prev_word_full.$word.' ';
	
	# Special Exceptions
	# if($next_word =~ m/^(����|������|���|����|�����(�|��))$/) { next; }
	
	# Read the properties of the word from mystem program;
	my @morph_properties=get_morph_props($word, '-i -w');
	my $flag=0; # Shows that the word is a noun
	for(my $i=0; $i<$#morph_properties+1; $i++)
		{
		if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} ne '����')) { $flag=1; }
		}
		
	if($flag == 0) { next;}
	
	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly

	# ----- determine the interval of a number
	my $number_interval; my $gender;
	
	$gender ='mfn'; # Important, that the gender can be whatever, except when it is known
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	
	if($word =~ m/^���(�|�|�|�|�|��|��|���|��)$/i) { $gender = 'f'; }
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word_full.$word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	# --- processing exceptions
	
	if(($prev_word =~ m/^�$/i) && ($word !~ m/(�$|�$|��$|��$)/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	if(($prev_word =~ m/^�$/i)&&($word =~ m/(��)$/i)) { $case = 'tv'; }	# S Chelovekami 25.
	if(($prev_word =~ m/^�$/i)&&($number_s eq '1')&&($number2_s eq '')&&($word =~ m/(�|[��]�)\.?$/i)) { $case = 'tv'; } # Singular form
	
	# *** processing exceptions
	
	my $sin_or_pl;
	my $kol_por='kol';
	$flag=0;
	my $array_elem=0;
	
	for(my $p=0; $p<$#morph_properties+1; $p++) 
		{
		if(($morph_properties[$p]{case} eq $case) && ($gender =~ m/\Q$morph_properties[$p]{gender}\E/i))
			{
			$flag=1;
			# $case;
			$gender=$morph_properties[$p]{gender};
			$sin_or_pl=$morph_properties[$p]{number};
			$array_elem=$p;
			if($case eq 'vin') { $anim=$morph_properties[$p]{anim}; } # for na chelovek 21-25
			$p=$#morph_properties+1;			
			}
		}
	
	my $flag_im_vin_rod=0;	# Noun is in case 'rod' and preposition is for 'im|vin'
	if($flag == 0)
		{
		if($case =~ m/(im|vin)/i)
			{
			for(my $p=0; $p<$#morph_properties+1; $p++)
				{
				if(($morph_properties[$p]{case} eq 'rod') && ($gender =~ m/\Q$morph_properties[$p]{gender}\E/i))
					{
					# if($this_line =~ m/����� 72 ���� �����/) { print $morph_properties[$p]{case}.'~'.$morph_properties[$p]{gender}.'~'.$gender."\~\n"; exit;}
					$flag=1;
					$flag_im_vin_rod=1;
					# $case;
					$kol_por='kol'; # Na slona 2
					$gender=$morph_properties[$p]{gender};
					$sin_or_pl=$morph_properties[$p]{number};
					$array_elem=$p;
					$p=$#morph_properties+1;
					}
				}
			}	
		}
	
	if($flag eq 0)
		{
		if($prev_word =~ m/^(��)$/i)
			{
			for(my $p=0; $p<$#morph_properties+1; $p++)
				{
				if(($morph_properties[$p]{case} eq 'dat') && ($gender =~ m/\Q$morph_properties[$p]{gender}\E/i))
					{
					$flag=1;
					# $case;
					$gender=$morph_properties[$p]{gender};
					$sin_or_pl=$morph_properties[$p]{number};
					$array_elem=$p;
					$p=$#morph_properties+1;
					}
				}
			}	
		}
		
	if($flag eq 0)
		{
		$case=$morph_properties[0]{case};
		$gender=$morph_properties[0]{gender};
		$sin_or_pl=$morph_properties[0]{number};
		}
		
	if(($number_interval ne '1') && ($sin_or_pl eq 'sin') && ($last_number_s !~ m/[\.\,]/))
		{
		if(($number_interval eq '2_4' )&&($case =~ m/(im|vin)/i)&&($flag == 0))
			{
			}
		elsif($word =~ m/^(�����(�|��)|�����(�|��)|�����(�|��)|������[��]�������|�������|�������|����������|��������[���])$/i)
			{
			$case='im';
			}
		 elsif($flag_im_vin_rod == 1) # Chasa 3
			{
			}
		else
			{
			$kol_por='por';
			}
		}
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if(($anim =~ m/^��$/i)&&($case eq 'vin')&&($prev_word !~ m/^(��)$/i)&&($gender eq 'm')) # for the case na chelovek 21-25
			{
			if($kol_por eq 'kol')
				{
				if($number_s =~ m/($|[^1])1$/) 		{ $first_number_case = 'rod'; }
				if($number2_s =~ m/($|[^1])1$/) 	{ $second_number_case = 'rod'; }
				}
			else
				{
				$first_number_case = 'rod'; 
				$second_number_case = 'rod';
				}
			}
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
		
		if(($anim =~ m/^��$/i)&&($case eq 'vin')&&($prev_word !~ m/^(��)$/i)&&($gender eq 'm')) # for the case na chelovek 21
			{
			if($kol_por eq 'kol')
				{
				if($number_s =~ m/($|[^1])1$/) 		{ $first_number_case = 'rod'; }
				}
			else
				{
				$first_number_case = 'rod'; 
				}
			}
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,$kol_por,$first_number_case,$gender);
			}
		elsif(($prev_word =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,$kol_por,$first_number_case,$gender);
			};
		}
	
	# New: Po Adresu 3.
	
	if(($number_s !~ m/[\.\,]/)&&($number2_s eq '')&&($this_line =~ m/^.{$pos_end}\s*(\:)/))
		{
		$phrase = $prev_word_full.$word.' '.num2word($number_s,'kol','im','m');
		}
	
	# $this_line =~ s/\Q$matching$next_word\E/$phrase/;
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/\d/) { return $this_line;}

# ----- Iz nih number (and number)

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/\b(��|�����)\s+(���)\s+(\d+([\.\,]\d+)?)(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $anim='';
	my $phrase='';
	my $matching=$&;
	my $prev_word1=$1;
	my $prev_word2=$2;
	my $number_s=$3;								#as strings
	my $and_or=$10;
	my $number2_s=$11;
	
	$phrase .= $prev_word1.' '.$prev_word2.' ';
	
	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly

	if(($number_s =~ m/^\d*[12]([\.\,]50*)?$/)||($number2_s =~ m/^\d*[12]([\.\,]50*)?$/))	# We do not know the case
		{
		next;
		}
	
	$phrase .= realnum2word($number_s, 'kol', 'im', 'm').$connection.realnum2word($number2_s, 'kol', 'im', 'm');
	
	# $this_line =~ s/\Q$matching$next_word\E/$phrase/;
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# ***** Iz nih number (and number)

return $this_line;
}

# process roman numerals
sub roman_numeral_process
{

my $this_line=$_[0];

# ----- if s X po XII v/vv or g/gg.

# # # # - Timing
# # # my $start=gettimeofday();
# # # my $end;
# # # my $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/\b��?\s+([MDCLXVI���]+)\s+��\s+([MDCLXVI���]+)\s{0,2}((���|���|��\b\.?|��\b\.?|�\b\.?|�\b\.?)[�-��]{0,3})(\W|$)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $roman_numeral1=$1;
	my $roman_numeral2=$2;
	my $word=$3;
	my $non_word=$5;	
	
	if(($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)||($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/))
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}
	
	my $prev_word;
	my $arab_num1=get_arab_num($roman_numeral1);
	if(($arab_num1 =~ m/^(1\d{2}|2)$/)&&($arab_num1 ne '100')) {$prev_word='�� '} else { $prev_word='� ';}
	
	$phrase .= $prev_word.num2word($arab_num1, 'por', 'rod', 'm').' �� '.num2word(get_arab_num($roman_numeral2), 'por', 'vin', 'm');
	
	my $new_word;
	for($word)
		{
		when (m/�\b\.?$/i)			{ $new_word=$veka{'pl'}{'vin'}; }
		when (m/�\b\.?$/i)			{ $new_word=$goda{'pl'}{'vin'}; }
		default						{ $new_word=$word; }
		}
	
	$phrase .= ' '.$new_word.$non_word;
	
	# Treat the point (.) by the shortcuts properly
	if(($word =~ m/\./)&&($non_word !~ m/\./)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*([�-ߨA-Z]|$)/)) { $phrase .= '.'; }	
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	# $this_line =~ s/\Q$matching\E/$phrase/;
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"if s X po XII" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}

# ***** if s X po XII v/vv or g/gg.

# ----- Detect V iz XXXVI | ot XV do XXX word2, s XV do XXX word2 | s XV po XXX word

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/((\b[�-��]+)\s+)?\b([MDCLXVI���]+)(\s+(��|��|��)\s+)([MDCLXVI���]+)(?=(\s*([�-��a-z]+\b\.?|[\�\#\�]\.?))?(\s+([�-��]+)\b)?)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase='';
	my $matching=$&;
	my $prev_word=$2;
	my $roman_numeral1=$3;								#as strings
	my $connection=$5;
	my $roman_numeral2=$6;
	my $next_word=$8;
	my $next_word2=$10;
	
	if(($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)||($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/))
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}
	
	my $case='';
	my $gender='m';
	my $sin_or_pl='sin';
	my $kol_por='kol';
	my $case2='rod'; # the case of the second word
	if(($prev_word =~ m/^�$/i)&&($connection =~ m/^��$/i)) { $kol_por = 'por'; $case2='vin'; }
	elsif($connection =~ m/^��$/i) { $case2='vin'; }
	
	my $number_s=get_arab_num($roman_numeral1);
	my $number2_s=get_arab_num($roman_numeral2);
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��|�������.*|�������.*|�������.*|����.*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	$phrase = $prev_word.' ';
	
	if($next_word =~ m/^(��?|��?|���(|�|��)|���(|�|��)|�|�|\�|\�|��|no)\.?$/i)
		{
		$gender='m';
		}
	elsif($next_word =~ m/^(��|���|��)\.?$/)
		{
		$gender='f';
		}
	elsif(($next_word =~ m/[�-��]/i)&&(($number_s =~ m/^(|\d*[^1\D])[12]$/)||($number2_s =~ m/^(|\d*[^1\D])[12]$/)))
		{
		# Read the properties of the word from mystem program;
		my @morph_properties=get_morph_props($next_word, '-i -w');
		my $flag=0; # Shows that the word is a noun
		my $flag_adj=0;
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/) && ($morph_properties[$i]{case} eq $case2)) {$flag=1; $gender=$morph_properties[$i]{gender};  $i=$#morph_properties; }
			# if($morph_properties[$i]{pos} eq 'A') { $flag_adj=1; }
			}
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties[$i]{adj_form} ne '��')) { $flag_adj=1; $flag = 0; $i=$#morph_properties; if($morph_properties[$i]{number} ne 'pl') { $gender=$morph_properties[$i]{gender}; } }
			};

		if(($flag == 0)&&($flag_adj == 1))
			{
			@morph_properties=get_morph_props($next_word2, '-i -w');
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/) && ($morph_properties[$i]{case} eq $case2)) {$flag=1; $gender=$morph_properties[$i]{gender}; $i=$#morph_properties; }
				}
			}
		}
	
	# the first number
	if($prev_word =~ m/^��$/i)
		{
		$phrase .= realnum2word_po($number_s, 'kol', $case, $gender); # case is not needed
		}
	elsif($kol_por eq 'por')
		{
		$phrase .= num2word($number_s, 'por', $case, $gender);
		}
	else
		{
		$phrase .= realnum2word($number_s, 'kol', $case, $gender);
		}
	
	# the second number
	
	$phrase .= ' '.$connection.' ';
	
	if($kol_por eq 'por')
		{
		$phrase .= num2word($number2_s, 'por', $case2, $gender);
		}
	elsif($connection =~ m/^��$/i)
		{
		$phrase .= realnum2word_po($number2_s, 'kol', $case2, $gender); # case is not needed
		}
	else
		{
		$phrase .= realnum2word($number2_s, 'kol', $case2, $gender);
		}
	
	if($next_word !~ m/^(��?|��?|�|�|\�|\�|��|no|��|���|��)\.?$/i)
		{
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase /;
		pos($this_line)=$pos_begin+length($phrase)+1;
		}
	else
		{
		my $next_word_new;
		# ----- Detect V iz XXXVI | ot XV do XXX word2, s XV do XXX word2 | s XV po XXX word
		if($connection =~ m/(��|��)/i)
			{
			if($number2_s =~ m/^(|\d*[^1\D])[1]$/)	{ $sin_or_pl = 'sin'; } else { $sin_or_pl = 'pl'; }
			}
		else
			{
			$sin_or_pl = 'pl';
			}
		
		if($next_word =~ m/^��?\.?$/i) 				{ $next_word_new = $veka{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^��?\.?$/i) 			{ $next_word_new = $goda{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^�\.?$/i) 			{ $next_word_new = $punkty{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^�\.?$/i) 			{ $next_word_new = $toma{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^\�\.?$/i) 			{ $next_word_new = $paragrafy{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^(\�|��|no)\.?$/i) 	{ $next_word_new = $nomera{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^��\.?$/i) 			{ $next_word_new = $glavy{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^���\.?$/i) 			{ $next_word_new = $stranicy{$sin_or_pl}{$case2}; }
		elsif($next_word =~ m/^��\.?$/i) 			{ $next_word_new = $statji{$sin_or_pl}{$case2}; }
		
		$phrase .= ' '.$next_word_new;
		
		# Treat the point (.) by the shortcuts properly
		if($this_line =~ m/^(.{$pos_begin})\Q$matching\E(\s*[�-��a-z\�\#\�]+\.\s*([�-ߨA-Z]|$))/) { $phrase .= '.'; }
		
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E(\s*([�-��a-z]+\b\.?|[\�\#\�]\.?))?/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	
	}
	
if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing	

# ***** Detect V iz XXXVI | ot XV do XXX word2, s XV do XXX word2 | s XV po XXX word

# ----- Detect the date like (s) nachala/serediny/polovine... XIX - (vtoroy) poloviny/... XX v. (/) too.

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\w*)([^\w\-\�\�]+)([MDCLXVI���]+)\s*(\-|\�|\�|\/|\b��\b|\b��\b)\s+([�-��]+)(\s+([�-��]+))?(\s+([�-��]+))?\s+([MDCLXVI���]+)(\W*)(�\.|��\.?|���[�-��]{0,4}\b|�\.|��\.?|���[�-��]{0,4}\b)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	
	my $roman_numeral1=$3;								#as strings
	my $connection=$4;
	
	my $special_word_1=$5;
	my $special_word_2=$7;
	my $special_word_3=$9;
	
	my $roman_numeral2=$10;								#as strings
	my $non_word2=$11;
	my $next_word=$12;
	
	my $case;
	
	# --- Processing exceptions
	
	if(($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)||($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/))
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}
		
	my $year1_s=get_arab_num($roman_numeral1);
	my $year2_s=get_arab_num($roman_numeral2);
	
	if(($year2_s eq '') && (length($year1_s) == 2)) { next;}	# ???
	if(($next_word =~ m/(�\.|��\.?|���[�-��]{0,4}\b)/i)&&((length($year1_s) =~ m/(1|3)/)||(length($year2_s) =~ m/(1|3)/))) { next; }
	
	if( (($special_word_1 =~ m/^(�����|�������|����?�)[�-��]{0,3}$/i) && ($special_word_2 =~ m/^\s*((����)?�����|�����|�������|$)[�-��]{0,3}$/i))	||
		(($special_word_1 =~ m/^(����|����|����|����[�]��|�������)[�-��]{0,3}$/i) && ($special_word_2 =~ m/^\s*(�������|�������|�������|�����)[�-��]{0,3}$/i) && ($special_word_3 =~ m/^\s*((����)?�����|�����|�������|$)[�-��]{0,3}$/i)) ||
		(($special_word_1 =~ m/^(�����|�������|����?�)[�-��]{0,3}$/i) && ($special_word_2 =~ m/^(����|����|����|����[�]��|�������)[�-��]{0,3}$/i) && ($special_word_3 =~ m/^\s*(�������|�������|�������|�����)[�-��]{0,3}$/i))
		)
		{
		}
	else
		{
		next;
		}
	
	# *** processing exceptions
	
	$phrase = $phrase.$prev_word.$non_word1;
	
	if($prev_word =~ m/^(�|�|��|��|�����|�����|�������|������|����|���|��|����������[��]|�������|����������|��������[���]|������|�������[�-�]*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*|�������[�-�]*|�������.*|�������.*|����.*|�����.*)$/i) # (Rod)
		{
		$case = 'rod';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
		{
		$case = 'dat';
		}
	elsif($prev_word =~ m/^(��|��|��)$/i) # (Vin)
		{
		$case = 'vin';
		}
	elsif($prev_word =~ m/^(�����)$/i) # (Tv)
		{
		$case = 'tv';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Pr)
		{
		$case = 'pr';
		}
	elsif($prev_word =~ m/^(�|��)$/i) # (Pr Second)
		{
		$case = 'pr';
		}
	else
		{
		next;
		}
	
	$phrase = $phrase.' '.num2word($year1_s,'por',$case,'m');
	if($connection =~ m/(\b��\b|\b��\b)/i)	{ $phrase .= ' '.$connection; };
	$phrase .= ' '.$special_word_1.' '.$special_word_2.' '.$special_word_3.' '.num2word($year2_s,'por','rod','m');
	
	if($next_word =~ m/(�\.|��\.?|���[�-��]{0,4}\b)/i) { $phrase .= ' ����'}
	elsif($next_word =~ m/(�\.|��\.?|���[�-��]{0,4}\b)/i) { $phrase .= ' ����'}
	
	$phrase =~ s/\ {2,}/ /g;
	
	# Treat the point (.) by the shortcuts properly
	if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E(\s+[IXCLMix])/)) { }	# do nothing (could be ... 20 g. XX veka)
	elsif(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }

	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# print $this_line;
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by \"Detect the date like\-3\" process: ".(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}

# ----- Detect the date like (s) nachala/serediny/polovine... XIX - (vtoroy) poloviny/... XX v. (/) too.

# ----- replace roman numerals preceded by: paragraph, etc

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*\b)?(\s*)((\b��������|\b�����|\b����|\b�����|\b��������|\b�������|\b���|\b�����|\b���������|\b������?�|\b����|\b������|\b����|\b����?�|\b����|\#|\�|\b��\b\.?|\bno\b\.?|\b�\.|\b��\.|\b���\.|\b��\.|\b�\.|\�|\b���?\.|\b���\.|\b��\.|\b����?\.|\b�\.|\b���\b\.?|\b��\b\.?|\b�\b\.?|\b��\b\.?|\b��\b\.?)[�-��]{0,3})\s{0,10}\b([MDCLXVI���]+)((((\s*)(\-|\�|\�|\/|\,\ )\s*)||(\s+(���|�)\s+))([MDCLXVI���]+))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word=$2;
	my $word=$3;
	my $roman_numeral1=$5;
	my $and_or=$12;
	my $roman_numeral2=$13;
	
	if($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)
		{
		next;
		}
	elsif(($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)&&($roman_numeral2 ne ''))
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}
		
	my $number_s=get_arab_num($roman_numeral1);
	my $number2_s='';
	if($roman_numeral2 =~ m/\w/) { $number2_s=get_arab_num($roman_numeral2); };
	
	my $case;
	my $sin_or_pl;
	
	if($word =~ m/(\b�\.|\b��\.|\b���\.|\b��\.|\b�\.|\�|\#|\�|��\b\.?|no\b\.?|\b���?\.|\b���\.|\b��\.|\b����?\.|\b�\.|\b���\b\.?|\b��\b\.?|\b�\b\.?|\b��\b\.?|\b��\b\.?)$/i)
		{
		# Check the probable case of a number
		
		# preliminary determination of the case
		my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word;
		my $prel_case=preliminary_case($line_before_number);
		
		if($prel_case ne 'im')
			{
			$case=$prel_case;
			}
		else
			{
			if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
				{
				$case = 'rod';
				}
			elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
				{
				$case = 'dat';
				}
			elsif($prev_word =~ m/^(�����|���|���)$/i) # (Tv)
				{
				$case = 'tv';
				}
			elsif($prev_word =~ m/^(��|�|��|��?)$/i) # (Pr)
				{
				$case = 'pr';
				}	
			elsif($prev_word =~ m/^(��|�����|��)$/i) # (Vin)
				{
				$case = 'vin';
				}
			else # (Im)
				{
				$case = 'im';
				}
			}
			
		# --- processing exceptions
		
		if(($prev_word =~ m/^��$/i) && ($word =~ m/(\#|\�|��\b\.?|no\b\.?)$/i)) { $case = 'vin'; }
		
		# *** processing exceptions
		
		if($number2_s =~ m/\d/) { $sin_or_pl='pl'; } else { $sin_or_pl='sin';}
		
		my $temp_word=$word;
		for($temp_word)
			{
			when (m/^\�/)				{ $word=$paragrafy{$sin_or_pl}{$case}; }
			when (m/^�\./i)				{ $word=$punkty{$sin_or_pl}{$case}; }
			when (m/^��\./i)			{ $word=$glavy{$sin_or_pl}{$case}; }
			when (m/^���\./i)			{ $word=$stranicy{$sin_or_pl}{$case}; }
			when (m/^��\./i)			{ $word=$statji{$sin_or_pl}{$case}; }
			when (m/^�\./i)				{ $word=$toma{$sin_or_pl}{$case}; }
			when (m/^(\#|\�|��\b\.?|no\b\.?)$/i)	{ $word=$nomera{$sin_or_pl}{$case}; }
			when (m/^(���?\.)$/i)		{ $word=$illjustracii{$sin_or_pl}{$case}; }
			when (m/^(���\.)$/i)		{ $word=$risunki{$sin_or_pl}{$case}; }
			when (m/^(��\.)$/i)			{ $word=$shemy{$sin_or_pl}{$case}; }
			when (m/^(����?\.)$/i)		{ $word=$tablicy{$sin_or_pl}{$case}; }
			when (m/^(�\.)$/i)			{ $word=$chasti{$sin_or_pl}{$case}; }
			when (m/^���\b\.?$/i)	 	{ $word = $kabinety{$sin_or_pl}{$case};}
			when (m/^��\b\.?$/i)		{ $word = $knigi{$sin_or_pl}{$case};}
			when (m/^�\b\.?$/i)			{ $word = $doma{$sin_or_pl}{$case};}
			when (m/^��\b\.?$/i)	 	{ $word = $kvartiry{$sin_or_pl}{$case};}
			when (m/^��\b\.?$/i)	 	{ $word = $jetazhi{$sin_or_pl}{$case};}
			}
		}
	
	if(($word =~ m/^����(�|�|�|�|��|��|���|��|)$/i)&&((length($number_s)>1)||(length($number2_s)>1))) { next; }
	
	$phrase=$prev_word.$non_word.$word.' '.num2word($number_s, 'kol', 'im', 'm');
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	if($number2_s =~ m/\d/) { $phrase .= $connection.num2word($number2_s, 'kol', 'im', 'm');};
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"numerals preceded by" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}
	
# ***** replace roman numerals preceded by: paragraph, etc

# ----- Detect the veka, gody, stranicy, etc - where ordinal number is more probable than cardinal one (Roman numerals)

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';

# while ($this_line =~ m/(\b[�-��]*)?(\W*)\b(\d+([\.\,]\d+)?)((\-|\�|\�)([�-��]{1,3}\b))?(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\d+([\.\,]\d+)?))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=((�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\b\.?\s?)?(\b��������|\b�����|\b����|\b�����|\b��������|\b�������|\b���|\b���|\b������|\b���������|\b�����|\b�������|\b�����|\b���������|\b���������|\b����|\b�����|\b���|�\b\.?|��\b\.?|���\b\.?|\�|��\b\.?|��\b\.?|��\b\.?|�\b\.?|�\b\.?|���\b\.?|��\b\.?|�\b\.?|��\b\.?|��\b\.?)([�-��]{0,3}(?![\w])))/ig)

while ($this_line =~ m/(\b[�-��]*)?(\W*)\b([MDCLXVI���]+)((\-|\�|\�)([�-��]{1,3}\b))?(((\s*(\-|\�|\�)\s*)||(\s*(���|�|\,\s)\s*))(\b[MDCLXVI���]+))?((\-|\�|\�)([�-��]{1,3}\b))?\s*(?=((�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\b\.?\s?)?(\b��������|\b�����|\b����|\b�����|\b��������|\b�������|\b���|\b���|\b������|\b���������|\b�����|\b�������|\b�����|\b���������|\b���������|\b����?�|\b�����|\b���|\b��������|\b�����|�\b\.?|��\b\.?|���\b\.?|\#|\�|��\b\.?|no\b\.?|�\b\.?|���\b\.?|��\b\.?|\�|��\b\.?|��\b\.?|��\b\.?|�\b\.?|�\b\.?|���\b\.?|��\b\.?|�\b\.?|��\b\.?|��\b\.?)([�-��]{0,3}(?![\w])))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $roman_numeral1=$3;
	my $word_part1=$6;
	my $and_or=$12;
	my $roman_numeral2=$13;
	my $word_part2=$16;
	my $tysjach=$17;
	my $first_word_part=$19;
	my $second_word_part=$20;					#as strings
	
	if($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)
		{
		next;
		}
	elsif(($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)&&($roman_numeral2 ne ''))
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}

	my $number_s=get_arab_num($roman_numeral1);
	my $number2_s='';
	if($roman_numeral2 =~ m/\w/) { $number2_s=get_arab_num($roman_numeral2); };
	
	my $kol_por='por';
	if(($number_s =~ m/[\.\,]/)||($number2_s =~ m/[\.\,]/)||($tysjach =~ m/[�-��]/i)) # there are only integer numbers, this is left for the algorithm to be simular to arabic numbers
		{
		$kol_por='kol';
		}
	
	my @number=split(/[\.\,]/, $number_s);
	my @number2=split(/[\.\,]/, $number2_s);
	
	$phrase = $phrase.$prev_word.$non_word1;	
	my $next_word = $first_word_part.$second_word_part;
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly
	# if($tysjach !~ m/^($|�\b|���\b|�����|���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { next;}
	
	if($next_word =~ m/[�-��]\.[�-��]/) { next;}
	# Exceptions
	if($next_word =~ m/^�$/) { next; } # To exclude "1 V ondnoj mile tysjacha ....", "1925 v moih arkticheskih"
	
	my $word='';
	
	my $coming_before_s=substr($this_line, 0, $pos_begin); # from and how much 
	if(($prev_word !~ m/^(��������|�����|����|�����|��������|�������|���|���|������|���������|�����|�������|�����|���������|���������|����?�|�����|���|��������|�����|�������|����|���|�������|����|�$|���$|$)/i) || (($prev_word =~ m/^(�|���|)$/i)&&($coming_before_s !~ m/\b(��������|�����|����|�����|��������|�������|���|���|������|���������|�����|�������|�����|���������|���������|����?�|�����|���|��������|�����|�������|����|���|�������|����)[�-��]{0,3}\s*$/i))) { $prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# --- determine the interval of a number
	my $number_interval; my $gender;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	my @last_number=split(/[\.\,]/, $last_number_s);
	
	if( (int(substr($last_number[0],-2)) > 4) && (int(substr($last_number[0],-2))<21) ) {$number_interval = '0_5_20';}
	elsif(($last_number_s =~ m/^1[\.\,]50*$/)||($last_number[0] =~ m/[2-4]$/)) { $number_interval='2_4';}
	elsif($last_number[0] =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	# *** determine the interval of a number

	if($prev_word =~ m/^�����$/i)
		{ 
		if(($number_interval ne '1')&&($next_word =~ m/^(����|����|���������|������|�����|������|���������|��������|��������|�����������|������|��������|������|����������|�����������|�����|������|����|���������|������)$/i))
			{
			}
		else
			{
			$kol_por = 'kol';
			}
		}
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	my $aux_string=$coming_before_s.$prev_word.$non_word1;
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��|�������.*|�������.*|�������.*|����.*|�����.*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($next_word =~ m/^(��?\b\.?$)/i)&&($aux_string =~ m/\b���(|�|�|��|�|�|��|���?|��)\s*$/i)) # (Rod)	$aux_string: If $prev_word eq '', check the end of $coming_before_s
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/)) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	my $case_determined=1;
		
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		# ----- Expressions like "Yajca II kategorii"
		elsif(($next_word =~ m/^(��������(�|�)\.?|������?\.?|���\b\.?|��\b\.?)$/i)&&(length($prev_word)>1)&&($non_word1 =~ m/^\s+$/))
			{
			my @morph_properties;

			@morph_properties=get_morph_props($prev_word, '-i -w');
#			my $array_elem;
#			my $flag=0; # Shows that the proper case was found
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/)) 
					{
#					$flag=1; $array_elem=$i;
					$case = 'rod';
					$kol_por = 'por';

					if(($next_word =~ m/^(���������\.?|�����\.?)$/i)&&($number2_s eq ''))
						{
						$gender='pl';
						}
					else
						{
						$gender='f';
						}
					
					$case_determined=0;
					
					$i=$#morph_properties+1;
					}
				}
			
			
			}
		}
	
	# --- Processing exceptions
	
	if(($prev_word =~ m/^��$/i) && ($next_word =~ m/^(���\.|�����(�|��)\.?|���������(�|��)\.?|���������(��|��)\.?)$/i)) { $case = 'pr'; }
		
	if(($case eq 'im') && ($number_interval ne '2_4') && ($next_word =~ m/^(����|����|���������|������|�����|������|���������|��������|��������|�����������|������|��������|������|����������|�����������|�����|������|����|���������|������)$/i))
		{
		$case = 'rod';
		}

	if(($prev_word =~ m/^�$/i) && ($next_word !~ m/(�|�)\.?$/i) && ((($tysjach !~ m/\w/)&&($next_word !~ m/^(�|��|���|\#|\�|��|no|�|���|��|\�|��|��|��|�|�|���|��|�|��|��)\.?$/i))||(($tysjach =~ m/\w/)&&($tysjach !~ m/[��\.]\s?$/i)))) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	# *** Processing exceptions
	
	if($gender eq '') { $gender='m';}
	
	if(($word_part1 =~ m/^(��|�?�|�?��)$/i)||($word_part2 =~ m/^(��|�?�|�?��)$/i))	{ $gender='pl'; }	# 20-e glavj
	elsif($next_word =~ m/^(\�$|��������)/i)	 		{ $gender='m';}
	elsif($next_word =~ m/^(�\.?$|�����)/i)	 			{ $gender='m';}
	elsif($next_word =~ m/^(��\.?$|����)/i)	 			{ $gender='f';}
	elsif($next_word =~ m/^(���\.?$|�������)/i) 			{ $gender='f';}
	elsif($next_word =~ m/^(��\.?$|�����)/i) 			{ $gender='f';}
	elsif($next_word =~ m/^(��?\b\.?$|���)/i)			{ $gender='m';}
	elsif(($next_word =~ m/^(��\b\.?$|���(�|�|��|��|���|��))/i)&&(($word_part1 =~ m/^�?(�|�|��)$/i)||($word_part2 =~ m/^�?(�|�|��)$/i))) { $gender='pl';}
	elsif(($next_word =~ m/^(�\b\.?$)/i)&&(($word_part1 =~ m/^(�)$/i)||($word_part2 =~ m/^(�)$/i)))	{ $gender='pl';}
	elsif($next_word =~ m/^(��?\b\.?$|���)/i)			{ $gender='m';}
	elsif($next_word =~ m/^(��������)/i)		 		{ $gender='m';}
	elsif($next_word =~ m/^(������)/i)				{ $gender='n';}
	elsif($next_word =~ m/^(���������)/i)		 		{ $gender='n';}
	elsif($next_word =~ m/^(�����)/i)		 		{ $gender='f';}
	elsif($next_word =~ m/^(�������)/i)		 		{ $gender='m';}
	elsif($next_word =~ m/^(�����)/i)			 	{ $gender='m';}
	elsif($next_word =~ m/^(���������)/i)		 		{ $gender='m';}
	elsif($next_word =~ m/^(���������)/i)		 		{ $gender='f';}
	elsif($next_word =~ m/^(����?�)/i)			 	{ $gender='n';}
	elsif($next_word =~ m/^(��\b|\#|\�|no\b|�����)/i)		{ $gender='m';}
	elsif($next_word =~ m/^(�\b|���)/i)		 		{ $gender='m';}
	elsif($next_word =~ m/^(��\b\.?)$/i)				{ $gender='f';}
	elsif($next_word =~ m/^(��\b\.?)$/i)				{ $gender='f';}
	elsif($next_word =~ m/^(��������|�����)[�-��]{0,3}\b/i)		{ $gender='f';}
	elsif($next_word =~ m/^(���|��)\.?$/i)				{ $gender='f';}
	
	if($gender eq 'pl')	{ }
	elsif(($gender eq 'f')&&(($word_part1 =~ m/^(��|�?�)$/i)||($word_part2 =~ m/^(��|�?�)$/i))) { $gender = 'pl';}	# 20-ye, -m str. (female only)
	elsif($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) { $gender = 'f';}
	elsif($tysjach =~ m/^(���\b|����\b|����\b|�������|��������|��������)[�-��]{0,3}\.?\s?$/i) { $gender = 'm';}
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i)&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $numeral1_s=num2word($number_s, $kol_por, $case, $gender);
		my $numeral2_s=num2word($number2_s, $kol_por, $case, $gender);
		
		if(($numeral1_s !~ m/$word_part1\s*$/i)||($numeral2_s !~ m/$word_part2\s*$/i))
			{
			my $aux_phrase1;
			my $aux_phrase2;
			my $flag=0;
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; };
			
			$aux_phrase1=num2word($number_s, $kol_por, $case, $gender);
			$aux_phrase2=num2word($number2_s, $kol_por, $case, $gender);
			
			if(($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) ) { $flag=1; }

			if($flag == 0)
				{
				for(my $i=1; $i>=0; $i--) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $gender);
						$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $gender);
						if( ($aux_phrase1 =~ m/$word_part1\s*$/i) && ($aux_phrase2 =~ m/$word_part2\s*$/i) )
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=-1; $j=6;
							$flag=1;
							}
						}
					}
				}
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }	
			}
			
		}
	elsif((($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))&&($number_s !~ m/[\.\,]/)&&($number2_s !~ m/[\.\,]/))
		{
		my $word_part;
		my $number_with_part;
		
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		my $numeral1_s=num2word($number_with_part, $kol_por, $case, $gender);
		
		if($numeral1_s !~ m/$word_part\s*$/i)
			{
			my $aux_phrase;
			my $flag=0;	
			
			if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/ };
			
			$aux_phrase=num2word($number_with_part, $kol_por, $case, $gender);
			if($aux_phrase =~ m/$word_part\s*$/i) { $flag=1;}

			if($flag == 0)
				{
				for(my $i=1; $i>=0; $i--) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $gender);
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$case = $cases_base[$j];
							$kol_por = $kol_por_base[$i];
							$i=-1; $j=6;
							$flag=1;
							}
						}
					}
				}
			
			if($flag == 0) { if($kol_por !~ s/kol/por/) { $kol_por =~ s/por/kol/; }; }
			}
		}
	elsif((length($tysjach) == 0)&&($next_word !~ m/^(�\b|��\b|���\b|\#|\�|��\b|no\b|�\b|\�|��\b|��?\b|��?\b|���\b|��\b|�\b|��\b|��\b|���\b|��\b)/i))
		{
		if(($case_determined eq 1)||($next_word !~ m/^(��������(�|�)\b|������?\b)/i))
			{
			my @morph_properties;
		
			@morph_properties=get_morph_props($next_word, '-i -w');
			my $array_elem;
			my $flag=0; # Shows that the proper case was found
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{case} eq $case)&&($morph_properties[$i]{gender} eq $gender)&&($morph_properties[$i]{misc} !~ m/(����|���|���)/)) {$flag=1; $array_elem=$i; $i=$#morph_properties+1; }
				}
			
			if($flag == 0) # looks just for the same gender independently from case
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)
					{
					if(($morph_properties[$i]{gender} eq $gender)&&($morph_properties[$i]{case} !~ m/rod/i)) {$flag=1; $case=$morph_properties[$i]{case}; $array_elem=$i; $i=$#morph_properties+1; }
					}
				}
		
			if($morph_properties[$array_elem]{number} =~ m/^pl$/) { $kol_por='kol';}
		
			if(($case =~ m/^im$/)&&($prev_word ne '')&&($number2_s ne '')&&($next_word =~ m/^(�����|��������|�����)\b/i))	# E.g. Hudozhniki XX-XXI vekov
				{
				my @morph_properties_prev;
				@morph_properties_prev=get_morph_props($prev_word, '-i -w');
			
				for(my $i=0; $i<$#morph_properties_prev+1; $i++)
					{
					if(($morph_properties_prev[$i]{pos} eq 'S')&&($morph_properties_prev[$i]{misc} !~ m/(����|���|���)/))
						{
						$kol_por = 'por';
						$case = 'rod';
						$gender = 'pl';
						}
					}
				}
			}
		}
	
	if(($prev_word =~ m/^�����.*/i)&&($tysjach eq '')&&($next_word =~ m/^(�����|��������)\b/i))
		{
		$kol_por = 'por';
		$case = 'rod';
		$gender = 'm';
		}
	
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender).$connection.realnum2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.realnum2word($number_s,'kol',$first_number_case,$gender);
			};
		}
	
	my $sin_or_pl;
	
	if($tysjach =~ m/[�-�]/i)
		{
		if($gender eq 'pl')
			{
			$sin_or_pl='pl';
			}
		elsif($kol_por eq 'por')
			{
			if($number2_s =~ m/\d/)
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='pl'; $case='dat'; }
				else { $sin_or_pl='pl'; };
				}
			else
				{
				if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
				else {$sin_or_pl='sin'; };
				}
			}
		elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
			{
			$sin_or_pl='sin'; $case='rod';
			}
		elsif($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $sin_or_pl='sin'; $case='dat'; }
			else {$sin_or_pl='sin'; };
			}
		else
			{
			if($case =~ m/(im|vin)/i)
				{
				if($number_interval eq '2_4')
					{
					$sin_or_pl='sin'; $case='rod';
					}
				else
					{
					$sin_or_pl='pl'; $case='rod';
					}
				}
			else
				{
				$sin_or_pl='pl';
				}
			
			}
		
		if($tysjach =~ m/^(�\b|���\b|�����)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$thousand{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(���\b|�������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$million{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$billion{$sin_or_pl}{$case}; }
		elsif($tysjach =~ m/^(����\b|��������)[�-��]{0,3}\.?\s?$/i) 	{ $phrase .= ' '.$trillion{$sin_or_pl}{$case}; }
		
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( ($last_number[0] =~ m/000$/) && (($last_number[1] =~ m/^50*$/)||($last_number[1] =~ m/^0*$/)) )
		{
		$sin_or_pl='pl';
		$case='rod';
		}
	elsif( (int($last_number[1]) > 0) && ( ($last_number[1] !~ m/^50*$/) || (int($last_number[0]) == 0) ) )
		{
		$sin_or_pl='sin';
		$case='rod';
		}
	elsif($kol_por eq 'por') # words
		{
		if($number2_s =~ m/\d/)
			{
			if(($special_preposition =~ m/^(��)$/i)&&($case eq 'rod')) { $case = 'dat'; }
			$sin_or_pl='pl';
			}
		else
			{
			if(($special_preposition =~ m/^(��)$/i)&&($case eq 'rod')) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		}
	elsif($kol_por eq 'kol')
		{
		if($number_interval eq '1')
			{
			if($special_preposition =~ m/^(��)$/i) { $case = 'dat'; }
			$sin_or_pl='sin';
			}
		elsif($number_interval eq '2_4')
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='sin';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			}
		else
			{
			if($case =~ m/(im|vin)/)
				{
				$sin_or_pl='pl';
				$case='rod';
				}
			else
				{
				$sin_or_pl='pl';
				}
			};
			
		}
	
	if($gender eq 'pl') { $sin_or_pl = 'pl'; }
	
	if($next_word =~ m/^\�$/i) 					{ $word = $paragrafy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\.?$/i) 			{ $word = $punkty{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) 			{ $word = $glavy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\.?$/i) 			{ $word = $stranicy{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\.?$/i) 			{ $word = $statji{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\b\.?$/i) 		{ $word = $kabinety{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $knigi{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\b\.?$/i) 			{ $word = $doma{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $kvartiry{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?$/i) 			{ $word = $jetazhi{$sin_or_pl}{$case};}
	# elsif($next_word =~ m/^(��?\b\.?$|���)/i)	{ $word = $veka{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��?\b\.?$)/i)	{ $word = $veka{$sin_or_pl}{$case};}
	elsif(($prev_word =~ m/^�$/i)&&($next_word =~ m/^�\.?$/i))	{ if($sin_or_pl eq 'sin') { $word = '����';} else { $word = '�����';}}
	# elsif($next_word =~ m/^(��?\b\.?$|���)/i)	{ $word = $goda{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��?\b\.?$)/i)		{ $word = $goda{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^(��\b|no\b|\#|\�)/i)	{ $word = $nomera{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^�\b\.?/i)			{ $word = $toma{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^���\b\.?/i)			{ $word = $kategorii{$sin_or_pl}{$case};}
	elsif($next_word =~ m/^��\b\.?/i)			{ $word = $gruppy{$sin_or_pl}{$case};}
	else {$word = $next_word;}
		
	$phrase .= ' '.$word;
	
	# # Treat the point (.) by the shortcuts properly
	if(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s+[IXCLMix])/)) { }	# do nothing (could be XX-e gg. XX veka)
	elsif(($next_word =~ m/\.$/)&&($this_line =~ m/^.{$pos_begin}\Q$matching$tysjach$next_word\E(\s*$|\s+[�-ߨA-Z])/)) { $phrase .= '.'; }

	# $this_line =~ s/\Q$matching$tysjach$next_word\E/$phrase/;
	$this_line =~ s/(^.{$pos_begin})\Q$matching$tysjach$next_word\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	# if($phrase !~ m/\.$/)
		# { pos($this_line) -= length($word); }
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"Detect the veka" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# print $this_line."\~\n";
# exit;

if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}

# ***** Detect the veka, gody, stranicy, etc - where ordinal number is more probable than cardinal one (Roman numerals)

# ----- detect the numbers with shortcuts VII-omu, e.g. (noun is not obligatory) - (Roman numerals)

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

my $special_preposition=''; # if the preposition "po" is met
my $prev_case='';
my $prev_kol_por='';
my $prev_end_position=0;		# End position of the previous matching's

while ($this_line =~ m/
		(\b[�-��]*)? 							# previous word ($1)
		(\W*)									# non_word_1 ($2)
		\b([MDCLXVI���]+)						# roman_numeral1 ($3)
		((\-|\�|\�)([�-��]{1,3}\b))?			# word_part_1 ($6)
		(
		(((\s*)(\-|\�|\�)\s*)||(\s+(���|�)\s+))	# $and_or ($13)
		([MDCLXVI���]+)							# roman_numeral2 ($14)
		((\-|\�|\�)([�-��]{1,3}\b))?			# word_part_2 ($17)
		)?
		\b(?=(\s+([�-��]+))?)					# next word ($19)
		/xig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';
	my $phrase='';
	my $matching=$&;
	my $prev_word=$1;
	my $non_word1=$2;
	my $roman_numeral1=$3;								#as strings
	my $word_part1=$6;
	my $and_or=$13;
	my $roman_numeral2=$14;
	my $word_part2=$17;
	my $next_word=$19;

	if($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/) # since the capturing assertion is wider
		{
		next;
		}
	elsif(($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)&&($roman_numeral2 ne '')) # since the capturing assertion is wider
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}
	
	my $number_s=get_arab_num($roman_numeral1);
	my $number2_s='';
	if($roman_numeral2 =~ m/\w/) { $number2_s=get_arab_num($roman_numeral2); };
	
	my $kol_por='por'; # The default is different for roman numerals comparing to arabic ones
	
	$phrase .= $prev_word.$non_word1;
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	# check either everything was caught correctly
	
	if(($word_part1 !~ m/[�-��]/i)&&($word_part2 !~ m/[�-��]/i))
		{
		next;
		}
	
	my $rest_length=length($this_line)-$pos_begin;
	if(($this_line !~ m/^.{$prev_end_position}(\s*(\b[�-��]+\b)?[\,\;\s]*).{$rest_length}$/i)||($prev_case eq '')||($prev_kol_por eq ''))
		{
		$prev_case = ''; $prev_kol_por=''; if($prev_word =~ m/^��$/i) { $special_preposition = '��';} else {$special_preposition='';};
		}
	else
		{
		if($prev_word =~ m/^��$/i) { $special_preposition = '��';}
		}
	
	if($non_word1 =~ m/[\.\?\!\(\)\[\]]/) { $prev_word = ''; $prev_case =''; $special_preposition =''; $prev_kol_por=''; } # Exlude the case like, bolee. 25 tonn
	elsif($non_word1 =~ m/[\,\;]/) { $prev_word = ''; } # Eclude the case, e.g. "no ne bolee, 15 tonn" - the preposition is not bolee, it is empty
	
	# --- Gain the information about the gender
	
	my $gender;
	my $gender_flag=0; # The information about gender is available according to the subject after a numeral
	my $gender_s='pl'; # Special string consisting all the probable genders
	
	if($next_word =~ m/[�-��]/i)
		{
		# Read the properties of the word from mystem program;
		my @morph_properties=get_morph_props($next_word, '-i -w');
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/)) {$gender_flag=1; if($gender_s !~ m/\Q$morph_properties[$i]{gender}\E/) { $gender_s .= '_'.$morph_properties[$i]{gender};} }
			}
		}
	
	# *** Gain the information about the gender
	
	# --- determine the interval of a number (only integer)
	
	my $number_interval;
	
	my $last_number_s;
	if($number2_s =~ m/\d/) { $last_number_s = $number2_s;} else { $last_number_s = $number_s;};
	
	if( (int(substr($last_number_s,-2)) > 4) && (int(substr($last_number_s,-2))<21) ) {$number_interval = '0_5_20';}
	elsif($last_number_s =~ m/[2-4]$/) { $number_interval='2_4';}
	elsif($last_number_s =~ m/1$/) { $number_interval = '1';}
	else { $number_interval = '0_5_20'};
	
	# *** determine the interval of a number (only integer)
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word1;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif(($prev_word =~ m/^(�|��)$/i)||($next_word =~ m/��\.?$/)) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	if($case eq 'im')
		{
		if(($prev_case =~ m/^(im|rod|dat|vin|tv|pr)$/i)&&($prev_kol_por =~ m/^(kol|por)$/i))
			{
			$case = $prev_case;
			$kol_por = $prev_kol_por;
			}
		}

	# --- processing exceptions
	if($prev_word =~ m/^�$/)
		{
		if($this_line =~ m/(���)[�-��]{0,3}\s+$matching/i)
			{
			$case = 'vin';
			}
		}
	
	if(($prev_word =~ m/^�$/i) && ($next_word !~ m/(�|�)\.?$/i) && ($gender_flag == 1)) { $case = 'vin'; } # V 15 tonn, e.g.

	# *** processing exceptions
	
	my $aux_phrase;
	my $aux_phrase1;
	my $aux_phrase2;
	
	# Try first the probable case and kol_por
	my $flag=0; # proper number case is found
	
	if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i))
		{
		for(my $k=0; $k<4; $k++) # genders_base
			{
			if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
			
			# if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k ne 3)) # only for kol and not plurar with po
			if(($prev_word =~ m/^��$/i)&&($kol_por eq 'kol')&&($k != 3)) # only for kol and not plurar with po
				{
				$aux_phrase1 = realnum2word_po($number_s, $kol_por, $case, $genders_base[$k]);
				$aux_phrase2 = realnum2word_po($number2_s, $kol_por, $case, $genders_base[$k]);
				}
			else
				{
				$aux_phrase1 = num2word($number_s, $kol_por, $case, $genders_base[$k]);
				$aux_phrase2 = num2word($number2_s, $kol_por, $case, $genders_base[$k]);
				}
			
			# Check
			if(($aux_phrase1 =~ m/$word_part1\s*$/i)&&($aux_phrase2 =~ m/$word_part2\s*$/i))
				{
				$flag = 1;
				$gender = $genders_base[$k];
				# $kol_por = $kol_por_base[$i];
				# $i=2;
				$k=4;
				}
				
			}
		}
	elsif(($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))
		{
		my $word_part;
		my $number_with_part;
		if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
		
		for(my $k=0; $k<4; $k++) # genders_base
			{
			if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
			
			# if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k ne 3)) # only for kol and not plurar with po
			if(($prev_word =~ m/^��$/i)&&($kol_por eq 'kol')&&($k != 3)) # only for kol and not plurar with po
				{
				$aux_phrase = realnum2word_po($number_with_part, $kol_por, $case, $genders_base[$k]);
				}
			else
				{
				$aux_phrase = num2word($number_with_part, $kol_por, $case, $genders_base[$k]);
				}
			
			# Check
			if($aux_phrase =~ m/$word_part\s*$/i)
				{
				$flag = 1;
				$gender = $genders_base[$k];
				# $kol_por = $kol_por_base[$i];
				# $i=2;
				$k=4;
				}
			}
		}
	
	# If the case or kol_por was determined wrong try all the cases
	
	if($flag == 0)			
		{
		my $stop_flag=0; # necessary to avoid unlimited cycling
		
		if(($word_part1 =~ m/[�-��]/i)&&($word_part2 =~ m/[�-��]/i))
			{
			for(my $k=0; $k<4; $k++) # genders_base
				{
				if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
				
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k != 3)) # only for kol and not plurar with po
							{
							$aux_phrase1 = realnum2word_po($number_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							$aux_phrase2 = realnum2word_po($number2_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						else
							{
							$aux_phrase1 = num2word($number_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							$aux_phrase2 = num2word($number2_s, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						
						# Check
						if(($aux_phrase1 =~ m/$word_part1\s*$/i)&&($aux_phrase2 =~ m/$word_part2\s*$/i))
							{
							$flag = 1;
							$gender = $genders_base[$k];
							$kol_por = $kol_por_base[$i];
							$case = $cases_base[$j];
							$i=2; $k=4; $j=6;
							}
						}
					}
				if(($gender_flag == 1) && ($k == 3) && ($flag != 1) && ($stop_flag == 0)) { $k=-1; $gender_flag=0; $stop_flag =1; }	# if the gender was determined wrong try all the cases
				}
			}
		elsif(($word_part1 =~ m/[�-��]/i)||($word_part2 =~ m/[�-��]/i))
			{
			my $word_part;
			my $number_with_part;
			if($word_part2 =~ m/[�-��]/i) { $word_part = $word_part2; $number_with_part=$number2_s;} else { $word_part = $word_part1; $number_with_part=$number_s;}
			
			for(my $k=0; $k<4; $k++) # genders_base
				{
				if(($gender_s !~ m/\Q$genders_base[$k]\E/) && ($gender_flag == 1)) { next;}
				
				for(my $i=0; $i<2; $i++) # kol_por_base
					{
					for(my $j=0; $j<6; $j++) # cases_base
						{
						if(($prev_word =~ m/^��$/i)&&($i == 0)&&($k != 3)) # only for kol and not plurar with po
							{
							$aux_phrase = realnum2word_po($number_with_part, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						else
							{
							$aux_phrase = num2word($number_with_part, $kol_por_base[$i], $cases_base[$j], $genders_base[$k]);
							}
						
						# Check
						if($aux_phrase =~ m/$word_part\s*$/i)
							{
							$flag = 1;
							$gender = $genders_base[$k];
							$kol_por = $kol_por_base[$i];
							$case = $cases_base[$j];
							$i=2; $k=4; $j=6;
							}
						}
					}
				if(($gender_flag == 1) && ($k == 3) && ($flag != 1) && ($stop_flag == 0)) { $k=-1; $gender_flag=0; $stop_flag =1; }	# if the gender was determined wrong try all the cases
				}
			}
		}
		
	if($flag == 0)
		{
		my $matching_new=$matching;
		if($word_part1 =~ m/[�-��]/i) { $matching_new =~ s/(\-|\�|\�)$word_part1//; }
		if($word_part2 =~ m/[�-��]/i) { $matching_new =~ s/(\-|\�|\�)$word_part2//; }
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$matching_new/;
		pos($this_line)=$pos_begin+length($matching_new);
		next;
		}
		
	$prev_case=$case;
	$prev_kol_por=$kol_por;
	
	my $first_number_case;
	my $second_number_case;
	
	if($number2_s =~ m/\d/)
		{
		$first_number_case = $case; # necessary for versatility/universality
		$second_number_case = $case; # necessary for versatility/universality
		
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender).$connection.num2word($number2_s,'por',$second_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin')&&($gender ne 'pl'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender).$connection.realnum2word_po($number2_s,'kol',$second_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.num2word($number_s,'kol',$first_number_case,$gender).$connection.num2word($number2_s,'kol',$second_number_case,$gender);
			}
		}
	else
		{
		$first_number_case = $case;			
			
		if($kol_por eq 'por')
			{
			$phrase = $phrase.num2word($number_s,'por',$first_number_case,$gender);
			}
		elsif(($special_preposition =~ m/^(��)$/i)&&($case eq 'vin')&&($gender ne 'pl'))
			{
			$phrase = $phrase.realnum2word_po($number_s,'kol',$first_number_case,$gender);
			}
		else
			{
			$phrase = $phrase.num2word($number_s,'kol',$first_number_case,$gender);
			};
		}
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
			
	$prev_end_position=$pos_begin+length($phrase);

	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}

# ***** detect the numbers with shortcuts -omu, e.g. (noun is not obligatory) - (Roman numerals)

# ----- replace roman numerals preceded by: godu, vekah, etc

# # # # - Timing
# # # $start=gettimeofday();
# # # $temp_counter=0; # number of entries into while loop
# # # # * Timing

while ($this_line =~ m/(\b[�-��]*)?(\s*)\b((���|���)[�-��]{0,3})\s{0,2}([MDCLXVI���]+)((((\s*)(\-|\�|\�|\/)\s*)||(\s+(���|�)\s+))([MDCLXVI���]+))?\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase="";
	my $matching=$&;
	my $prev_word=$1;
	my $non_word=$2;
	my $word=$3;
	my $roman_numeral1=$5;
	my $and_or=$12;
	my $roman_numeral2=$13;
	
	if($roman_numeral1 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)
		{
		next;
		}
	elsif(($roman_numeral2 !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)&&($roman_numeral2 ne ''))
		{
		next;
		}
	elsif(($roman_numeral1 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)||($roman_numeral2 =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i))
		{
		next;
		}

	my $number_s=get_arab_num($roman_numeral1);
	my $number2_s='';
	if($roman_numeral2 =~ m/\w/) { $number2_s=get_arab_num($roman_numeral2); };
	
	my $case;
	my $sin_or_pl;
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word.$non_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		# if($prev_word =~ m/^��$/) { $special_preposition = ''; } # the stand-alone preposition 'po' is not included in preliminary case function, only in combinations
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��|�������.*|�������.*|�������.*|����.*|����[�-�]*|���[�-�]*|�����[�-�]*|���[�-�]*|����[�-�]*|�����[�-�]*)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			my $stop_flag=0;
			my @morph_properties=get_morph_props($prev_word, '-i -w');
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if($morph_properties[$i]{pos} =~ m/NUM/) { $stop_flag=1; $i=$#morph_properties+1; } # if the word in front of "godu" is a numeral, then it should be skipped by algorithm 
				}
			
			if($stop_flag==1) { next; }
			# print '*'.$this_line."\~\n";
			
			for($word)
				{
				when (m/^(���|���)(�|��)$/i)			{ $case = 'rod'; }
				when (m/^(���|���)(��|���)$/i)			{ $case = 'tv'; }
				when (m/^(���|���)(��)$/i)				{ $case = 'dat'; }
				when (m/^(���|���)(�|��)$/i)			{ $case = 'pr'; }
				when (m/^����$/i)						{ 
														my $rest_length=length($this_line)-$pos_begin;
														if($this_line =~ m/\b�\s+(\b[�-��]+\b\s+)?.{$rest_length}$/i) { $case = 'pr'; } # v novom godu 2007
														elsif($this_line =~ m/\b�\s+(\b[�-��]+\b\s+)?.{$rest_length}$/i) { $case = 'dat'; }
														else { $case = 'im'; }
														}
				default								{ $case = 'im'; }
				}
			}
		}
	
	$phrase=$prev_word.$non_word.$word.' '.num2word($number_s, 'por', $case, 'm');
	
	my $connection;
	if($and_or =~ m/(���|�)/i) { $connection=' '.$1.' ';} else { $connection =' ';}
	
	if($number2_s =~ m/\d/) { $phrase .= $connection.num2word($number2_s, 'por', $case, 'm');};
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}
	
# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"replace numerals preceded by" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# print $this_line."\~\n"; exit;

if($this_line !~ m/[MDCLXVI���]/i) { return $this_line;}
	
# ***** replace roman numerals preceded by: godu, vekah, etc

# ----- write the roman numerals with personal names like k Aleksandru III

# # # # - Timing
# # # my $start=gettimeofday();
# # # my $temp_counter=0; # number of entries into while loop
# # # my $end;
# # # # * Timing

while ($this_line =~ m/(\b([�-��]+)\s+)?(\b(?-i:[�-ߨ])[�-��]+)\s{1,2}([MDCLXVI���]+)\b/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];	
	my $phrase="";
	my $matching=$&;
	my $prev_word=$2;
	my $name=$3;
	my $roman_numeral=$4;
	
	$phrase = $1;
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if(($prev_word !~ m/[�-��]/i) && (($name =~ m/^(�|�|��|��|�����|�����|������|������|�����|�����|�����|������|�����|����|���|��|����������[��]|�������|����������|��?|�����|��?|��|��|�|��|���|�����|)$/i)||(preliminary_case($name) ne 'im')))
		{
		next;
		}
	
	if($roman_numeral !~  m/\b([MDCLXVImdclxvi��]+|[MDCLXVImdclxvi���]{2,})\b/)
		{
		next;
		}
	elsif($roman_numeral =~ m/\b(CD|CLX|DC|DI|DLL|DM|ID|IM|LCD|MC|MD)\b/i)
		{
		next;
		}
	
	my @morph_properties_prev;
	my $case_prev='';
	my $gender_prev='';
	my $flag_prev=0; # if the previous word has an info about case and gender
	
	if($prev_word =~ m/\w/)
		{
		if(($prev_word !~ m/^(�|�|��|��|�����|�����|�������[�-��]*|������.*|�����|�|��|�|���|��|��|���)$/i)&&($prel_case eq 'im'))
			{
			@morph_properties_prev=get_morph_props($prev_word, '-i -w');
			for(my $i=0; $i<$#morph_properties_prev+1; $i++)
				{
				# if(($morph_properties_prev[$i]{pos} =~ m/^(S|A|APRO)$/)&&($morph_properties_prev[$i]{misc} !~ m/^(����)$/i)&&($morph_properties_prev[$i]{number} !~ m/^(pl)$/))
				if(($morph_properties_prev[$i]{pos} =~ m/^(S|A|APRO)$/)&&($morph_properties_prev[$i]{misc} !~ m/^(����)$/))
					{
					$flag_prev=1;
					$i=$#morph_properties_prev+1;
					}
				}
			}
		}
		
	# if($matching =~ m/�� ��������� IV/) { print $matching."\~\n"; exit;}
	
	# my $last_letter=substr($name, -1, 1);
	my $case;
	my $gender;
	
	if($flag_prev == 1) # if the previous word has an info about case and gender
		{
		$case='';
		}
	else
		{
		if($prel_case ne 'im')
			{
			$case=$prel_case;
			}
		else
			{
			if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��|�������[�-��]*|������.*)$/i) # (Rod)
				{
				$case = 'rod';
				}
			elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
				{
				$case = 'dat';
				}
			elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
				{
				$case = 'tv';
				}
			elsif($prev_word =~ m/^(�|��)$/i) # (Pr)
				{
				$case = 'pr';
				}	
			elsif($prev_word =~ m/^(��|��|�����|��|���|��?)$/i) # (Vin)
				{
				$case = 'vin';
				}
			else # (Im)
				{
				$case = 'im';
				}
			}
		}
	
	my @morph_properties=get_morph_props($name, '-i');
	
	my $flag=0; # Shows that the proper case and gender was found
	
	if($flag_prev == 1) # if the previous word has an info about case and gender
		{
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			for(my $j=0; $j<$#morph_properties_prev+1; $j++)
				{
				if(($morph_properties[$i]{case} eq $morph_properties_prev[$j]{case})&&($morph_properties[$i]{gender} eq $morph_properties_prev[$j]{gender})&&($morph_properties_prev[$j]{misc} !~ m/^(����)$/i)&&($morph_properties[$i]{misc} !~ m/^(����)$/i)&&($morph_properties_prev[$j]{number} !~ m/^(pl)$/)&&($morph_properties[$i]{number} !~ m/^(pl)$/))
					{
					if(($morph_properties_prev[$j]{pos} =~ m/^(A|APRO)$/) || (($morph_properties[$i]{case} !~ m/^(im|vin)$/))) # both words should not have an im/vin case
						{
						$flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $j=$#morph_properties_prev+1; $i=$#morph_properties+1;
						}
					}
				}
			}
		
		if($flag == 0)
			{
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				for(my $j=0; $j<$#morph_properties_prev+1; $j++)
					{
					if(($morph_properties[$i]{case} eq 'rod')&&($morph_properties_prev[$j]{pos} =~ m/^(S)$/)&&($morph_properties_prev[$j]{misc} !~ m/^(����)$/i)&&($morph_properties[$i]{misc} !~ m/^(����)$/i)&&($morph_properties[$i]{number} !~ m/^(pl)$/))
						{
						$flag=1; $case='rod'; $gender=$morph_properties[$i]{gender}; $j=$#morph_properties_prev+1; $i=$#morph_properties+1;
						}
					}
				}
			}
		
		if($flag == 0)
			{
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				for(my $j=0; $j<$#morph_properties_prev+1; $j++)
					{
					if(($morph_properties[$i]{case} eq $morph_properties_prev[$j]{case})&&($morph_properties[$i]{gender} eq $morph_properties_prev[$j]{gender})&&($morph_properties_prev[$j]{misc} !~ m/^(����)$/i)&&($morph_properties[$i]{misc} !~ m/^(����)$/i)&&($morph_properties_prev[$j]{number} !~ m/^(pl)$/)&&($morph_properties[$i]{number} !~ m/^(pl)$/))
						{
						$flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $j=$#morph_properties_prev+1; $i=$#morph_properties+1;
						}
					}
				}
			}
		}
	else
		{
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} =~ m/^(S|A|APRO)$/)&&($morph_properties[$i]{misc} !~ m/^(����)$/i)&&($morph_properties[$i]{number} !~ m/^(pl)$/))
				{
				if($morph_properties[$i]{case} eq $case) {$flag=1; $gender=$morph_properties[$i]{gender}; $i=$#morph_properties+1;}
				}
			}
		
		# Take the first suitable word with any case
		if($flag == 0)
			{
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} =~ m/^(S|A|APRO)$/)&&($morph_properties[$i]{misc} !~ m/^(����)$/i)&&($morph_properties[$i]{number} !~ m/^(pl)$/))
					{
					$flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $i=$#morph_properties+1;
					}
				}
			}
		}
		
	$phrase .= $name.' '.num2word(get_arab_num($roman_numeral), 'por', $case, $gender);
	
	if($flag == 1)
		{
		# $this_line =~ s/\Q$matching\E/$phrase/;
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
		
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
	}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"k Aleksandru III" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing
	
# ***** write the roman numerals with personal names like k Aleksandru III

# If there are still Roman numbers, delete them
# if($this_line =~ m/[MDCLXVImdcxvi]+/) { print "Warning!!! There were unresolved roman numerals still:\n   ".$this_line."\n"; $this_line =~ s/[MDCLXVImdcxvi]+//g};

return $this_line;
}

#processing the numbers in text
sub abbr_process
{
my $this_line=$_[0];

# # # while($this_line =~ m/\b��\./i)
	# # # {
	# # # my $pos_begin=$-[0];
	# # # my $pos_end=$+[0];
	# # # my $case='';
	
	# # # $this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	# # # pos($this_line)=$pos_begin+length($phrase);
	# # # }


$this_line =~ s/\b��\./������ /ig; # it is located here, since sm could mean santimetry

# ----- Process pre-defined abbreviations (depending on the case) - Adjectives!

while($this_line =~ m/(\b([�-��]+)\s+)?\b(�\.\s*�\.|�\/�\b|��\.|����\.|��\b\.?|��\b\.?|��\.|\d+\s*\%|\d+\s*[x�]\b|\d+\'\'|\d+\")(?=\s+([�-��]+)(\s+([�-��]+))?)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';	
	my $phrase="";
	my $matching=$&;
	my $prev_word=$2;
	my $abbr=$3;								#as strings
	my $next_word=$4;
	my $next_word2=$6;
	
	# --- Check either everything was caught correctly
	
	if(($abbr =~ m/^\d+\s*\%/)&&($next_word !~ m/^(�������(�|�|�|��|�|��|���|��)\b|(���������|�������)(|�|�|��|�|�|��|��|���|��)\b)/i)) { next; }
	if(($abbr =~ m/^\d+\s*[x�]\b/i) && (($next_word !~ m/^(�����|���������|���������|����������|(�|��)������|(����|�����|�����)����|(����|�����|�����|)��������)(|�|�|�|�|��|��|���|��|�|�|��|�|�|��|���|��|��|�|��)\b/i)&&($next_word !~ m/^(��������|������������|�����������|���������|����������|(�|��)������|(����|�����|�����)���������)(��|��|��|��|���|��|��|���|��|��|���|��|��|��|��|��|���)\b/i)) ) { next; }
	if(($abbr =~ m/^\d+(\'\'|\")/)&&($next_word !~ m/^((�������|�����|��������|����|������|���������?)(|�|�|��|�|�|��|��|���|��|��|�|�|��|���?|��)\b)/i)) { next; }
	my $length_before = length($this_line)-$pos_begin;
	
	# print substr($this_line,).'~'.$prev_word."\~\n";
	if(($this_line =~ m/\d[\.\,].{$length_before}$/)&&($prev_word eq '')&&($abbr =~ m/^(\d+\s*\%|\d+\s*[x�]\b|\d+\'\'|\d+\")$/i)) { next; }
	
	# *** Check either everything was caught correctly
	
	# Read the properties of the word from mystem program;
	my @morph_properties=get_morph_props($next_word, '-i -w');
	my $flag=0; # Shows that the word is a noun
	my $flag_adj=0;
	my $pos='';
	my $flag_noun_word1=0; # The first word can be a noun
	my $flag_noun_word2=0; # The second word can be a noun
	
	for(my $i=0; $i<$#morph_properties+1; $i++) # check if the 1st word could be noun
		{
		if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/)) {$flag=1; $pos='S'; $flag_noun_word1=1; }
		}
	for(my $i=0; $i<$#morph_properties+1; $i++) # check if the 1st word could be also adjective, if yes try the 2nd word
		{
		if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties[$i]{adj_form} ne '��')) { $flag_adj=1; $flag = 0; $i=$#morph_properties; $pos='A' }
		};
		
	my @morph_properties_temp;
	if(($flag == 0)&&($flag_adj == 1)) # if the 1st word could adjective, try the 2nd one to be a noun
		{
		@morph_properties_temp=get_morph_props($next_word2, '-i -w');
		for(my $i=0; $i<$#morph_properties_temp+1; $i++)
			{
			if(($morph_properties_temp[$i]{pos} eq 'S')&&($morph_properties_temp[$i]{gender} ne '')&&($morph_properties_temp[$i]{misc} !~ m/(����|���|���)/)) {$flag=1; $pos='S'; $flag_noun_word2=1; }
			}
		}
	
	if($flag_noun_word2 == 1)
		{
		@morph_properties=@morph_properties_temp;
		}
	elsif(($flag == 0)&&($flag_adj == 1))
		{
		$flag = 1;
		if($flag_noun_word1 == 1) { $pos = 'S'} else { $pos = 'A'};
		}		
	
	if($flag == 0) { next;}
	
	$phrase .= $prev_word.' ';
	
	my $gender = 'mfn'; # Important, that the gender can be whatever, except when it is known
	
	if(($flag_noun_word1 == 1) && ($next_word =~ m/^���(�|�|�|�|��|�|��|���|��)$/))
		{
		$gender = 'f';
		}
	elsif(($flag_noun_word2 == 1) && ($next_word2 =~ m/^���(�|�|�|�|��|�|��|���|��)$/))
		{
		$gender = 'f';
		}
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	# --- processing exceptions
	
	if(($prev_word =~ m/^�$/i) && ($flag_noun_word1 == 1) && ($next_word !~ m/(�$|�$)/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	elsif(($prev_word =~ m/^�$/i) && ($flag_noun_word2 == 1) && ($next_word2 !~ m/(�$|�$)/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	if(($prev_word =~ m/^��$/i) && ($flag_noun_word1 == 1) && ($next_word =~ m/(�$|�$)/i)) { $case = 'pr'; } # To exclude the case "na lodkah"
	elsif(($prev_word =~ m/^��$/i) && ($flag_noun_word2 == 1) && ($next_word2 =~ m/(�$|�$)/i)) { $case = 'pr'; } # To exclude the case "na odkah"
	
	# *** processing exceptions
	
	my $sin_or_pl;
	$flag=0;
	my $array_elem=0;
	my $anim;

	for(my $p=0; $p<$#morph_properties+1; $p++) 
		{
		if(($morph_properties[$p]{pos} eq $pos) && ($morph_properties[$p]{case} eq $case) && ($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)&&($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
			{
			$flag=1;
			# $case;
			$gender=$morph_properties[$p]{gender};
			$sin_or_pl=$morph_properties[$p]{number};
			$anim=$morph_properties[$p]{anim};
			$array_elem=$p;
			$p=$#morph_properties+1;	
			}
		}
	
	if($flag eq 0) # if the case was determined wrong
		{
		for(my $p=0; $p<$#morph_properties+1; $p++)
			{
			if(($morph_properties[$p]{pos} eq $pos)&&($gender =~ m/\Q$morph_properties[$p]{gender}\E/i)&&($morph_properties[$p]{misc} !~ m/(����|���|���)/) && ($morph_properties[$p]{adj_form} ne '��'))
				{
				$case=$morph_properties[$p]{case};
				$gender=$morph_properties[$p]{gender};
				$sin_or_pl=$morph_properties[$p]{number};
				$anim=$morph_properties[$p]{anim};
				$p=$#morph_properties+1;
				$flag=1;
				}
			}
		}
			
	if($flag == 0) { next; }
	
	if($sin_or_pl eq 'pl') { $gender = 'pl'; }
	
	my $aux_phrase;
	my $number_s;
	
	for($abbr)
		{
		when (m/^[��]\.\s*�\.$/)		{ if($anim eq '��') {$phrase .= '��� '.$nazyvaemyj_adj_od{$case}{$gender};} else {$phrase .= '��� '.$nazyvaemyj_adj{$case}{$gender};} }	
		when (m/^�\/�$/i)				{ if($anim eq '��') {$phrase .= '�������'.$hozjajstvennyj_adj_od{$case}{$gender};} else {$phrase .= '�������'.$hozjajstvennyj_adj{$case}{$gender};} }
		when (m/^(��|����)\.$/i)		{ if($anim eq '��') {$phrase .= $srednij_adj_od{$case}{$gender};} else {$phrase .= $srednij_adj{$case}{$gender};} }
		when (m/^(��)\.?$/i)			{ if($anim eq '��') {$phrase .= $zheleznodorozhnyj_adj_od{$case}{$gender};} else {$phrase .= $zheleznodorozhnyj_adj{$case}{$gender};} }
		when (m/^(��)\.?$/i)			{ $phrase .= $zhidkokristallicheskij_adj{$case}{$gender}; }
		when (m/^(��)\.$/i)				{ if($anim eq '��') {$phrase .= $drugoj_adj_od{$case}{$gender};} else {$phrase .= $drugoj_adj{$case}{$gender};} }
		when (m/^(\d+\s*\%|\d+\s*[x�]|\d+(\'\'|\"))$/i)	
											{
											$abbr =~ m/^(\d+)\s*(\%|[x�]|\'\'|\")/i;
											$number_s=$1;
											# if(int($number_s) == 100) { $aux_phrase = '���'; }
											# elsif((int($number_s) == 2)&&($abbr =~ m/^\d+\s*[x�]\b/i)) { $aux_phrase = '���'; }
											# else { $aux_phrase = num2word($number_s,'kol','rod','m'); };
											
											$aux_phrase = num2word($number_s,'kol','rod','m');
											
											# $aux_phrase =~ s/������?$/������/i;
											$aux_phrase =~ s/����/����/ig;
											$aux_phrase =~ s/���������/���������/ig;
											$aux_phrase =~ s/���/���/ig;
											$aux_phrase =~ s/������?/������/ig;
											$aux_phrase =~ s/\s//g; #delete the spaces
											$aux_phrase =~ s/��$//; #delete the go from odnogo and so on to get odnotysjachnyj, for example
											
											if(($number_s =~ m/(^|[^1])2$/)&&($abbr =~ m/^\d+\s*[x�]\b/i)) { $aux_phrase =~ s/����$/���/i; }
											
											if($abbr =~ m/\%/) { $phrase .= $aux_phrase.$procentnyj_adj{$case}{$gender}; }
											elsif($abbr =~ m/[x�]/i) { $phrase .= $aux_phrase.$kratnyj_adj{$case}{$gender}; }
											elsif($abbr =~ m/\"|\'\'/) { $phrase .= $aux_phrase.$djujmovyj_adj{$case}{$gender}; }
											}
		}
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

$this_line =~ s/\b�\s+��\./� ������ /g; # ������ ���� ��������� ����� � ��������� ����� (������ ������ ��� ���� �������� �����)

# ***** Process pre-defined abbreviations (depending on the case) - Adjectives!

# ----- Process pre-defined abbreviations (depending on the case) - (Adjectives? + Nouns)!

while($this_line =~ m/(\b([�-��]+)\s+)?\b(���\.|�\/�|�\\�\b|�\/�\b|�\\�\b|(�\/�\b|�\\�\b|���\b\.|�������[�-��]{0,3}\b)(\.?)([\s\(\)\-\�\�\d]*\d)?|$months_rod_short_pattern(?!\w))/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $case='';	
	my $phrase="";
	my $matching=$&;
	my $prev_word=$2;
	my $abbr=$3;								#as strings
	my $tel_point=$5;
	my $tel_number=$6;
	
	# --- Check either everything was caught correctly
	
	if(($abbr =~ m/^($months_rod_short_pattern)$/i)&&($abbr !~ m/\.$/i)) { next; } # if without a point
	
	# *** Check either everything was caught correctly
	
	$phrase .= $prev_word.' ';				
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	# --- processing exceptions
	
	if(($abbr =~ m/^���/)&&($tel_number !~ m/\d/)) { next; }
	
	# if(($prev_word =~ m/^�$/i) && ($flag_noun_word1 == 1) && ($next_word !~ m/(�$|�$)/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	# elsif(($prev_word =~ m/^�$/i) && ($flag_noun_word2 == 1) && ($next_word2 !~ m/(�$|�$)/i)) { $case = 'vin'; } # To exclude the case "kerosina pochti v 18 litrov"
	
	# *** processing exceptions
	
	for($abbr)
		{
		when (m/^(�\/�|�\\�)$/)					{ $phrase .= $kinoteatry{sin}{$case}; }
		when (m/^(�\/�|�\\�)$/i)				{ $phrase .= $kinofilmy{sin}{$case}; }
		when (m/^(�\/�|�\\�|���)/i)				{ $phrase .= $telefony{sin}{$case}; }
		when (m/^(���)/i)						{ $phrase .= $koncy{sin}{$case}; }
		when (m/^$months_rod_short_pattern$/i)	{
												for(my $i=0; $i<12; $i++)
													{
													if($abbr =~ m/^$months_rod_short[$i]$/i)
														{
														$phrase .= $months[$i]{$case}; $i=12;
														}
													}
												}
		}
		
	if($abbr =~ m/^(�\/�|�\\�|���)/i)
		{
		my $aux_phrase;
		if($tel_number =~ m/\d/)
			{
			while($tel_number =~ m/(\d)/g)
				{
				$phrase .= ' '.num2word($1, 'kol', 'im', 'm');
				}
			$phrase .=' ';
			}
		else
			{
			$phrase .= $tel_point.$tel_number;
			}
		}
	
	# if($matching =~ m/\.$/) { } # not used yet
	
	# Treat the point (.) by the shortcuts properly
	if(($matching =~ m/\.$/)&&($this_line =~ m/^(.{$pos_begin})\Q$matching\E\s*([�-ߨA-Z]|$)/)) { $phrase .= '.'; }	
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
	# # # # - Timing
	# # # $temp_counter++;
	# # # # * Timing
}

# # # # - Timing
# # # if($temp_counter > 0)
	# # # {
	# # # $end=gettimeofday();
	# # # print "\n\>\>Time spent by ".'"the numbers followed by nouns" process: '.(int(($end-$start)*1000000)/1000000)." sec.\nNumber of entries: ".$temp_counter."\n\n";
	# # # }
# # # # * Timing

# ***** Process pre-defined abbreviations (depending on the case) - (Adjectives? + Nouns)!

# ----- Process v takom-to kr./obl.

while($this_line =~ m/(\b([�-ߨ�-��]+)\s+)?\b([�-ߨ][�-��]+((\-|\�|\�)[�-ߨ][�-��]+)?)\s+(��|���|���|���\.\s*���|���|����)\./g)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase;
	my $matching=$&;
	my $prev_word=$2;
	my $region_name=$3;
	my $abbr=$6;
	
	my $case;
	my $gender;
	
	$phrase = $prev_word.' '.$region_name.' ';
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	# --- processing exceptions
	
	if(($this_line =~ m/�����\s+$matching/i) && ($prev_word eq '�')){ $case = 'tv'; }
	if(($prev_word =~ m/^�$/i)&&($region_name =~ m/(��?|[��]�)$/i)) { $case = 'tv'; }	# S Krasnoyarskim kraem
	
	# *** processing exceptions
	
	for($abbr)
		{
		when (m/^��$/i)							{ $phrase .= $kraja{sin}{$case}; }
		when (m/^���$/i)						{ $phrase .= $oblasti{sin}{$case}; }
		when (m/^���$/i)						{ $phrase .= $okruga{sin}{$case}; }
		when (m/^���\.\s*���$/i)				{ $phrase .= $avtonomnyj_adj{$case}{'m'}.' '.$okruga{sin}{$case}; }
		when (m/^���$/i)						{ $phrase .= $regiony{sin}{$case}; }
		when (m/^����$/i)						{ $phrase .= $respubliki{sin}{$case}; }
		}
		
	# Treat the point (.) by the shortcuts properly
	if($this_line =~ m/^.{$pos_end}(\s*$|\s+[�-ߨA-Z])/) { $phrase .= '.'; }
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
}

# ***** Process v takom-to kr./obl.

# ----- Process v okr/resp./ ... Name

while($this_line =~ m/(\b([�-��]+)\s+)?\b(��|���|���|���\.\s*���|���|����)\.(?=\s+((?-i:[�-ߨ])[�-��]+)\b)/ig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase;
	my $matching=$&;
	my $prev_word=$2;
	my $abbr=$3;
	my $next_word=$4;
	
	my $case;
	my $gender;
	
	$phrase = $prev_word.' ';
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
		
	# --- processing exceptions
	
	if(($this_line =~ m/�����\s+$matching/i) && ($prev_word eq '�')){ $case = 'tv'; }
	if(($prev_word =~ m/^�$/i)&&($next_word =~ m/(��?|[��]�)$/i)) { $case = 'tv'; }	# S Krasnoyarskim kraem
	
	# *** processing exceptions
	
	for($abbr)
		{
		when (m/^��$/i)								{ $phrase .= $kraja{sin}{$case}; }
		when (m/^���$/i)							{ $phrase .= $oblasti{sin}{$case}; }
		when (m/^���$/i)							{ $phrase .= $okruga{sin}{$case}; }
		when (m/^���\.\s*���$/i)					{ $phrase .= $avtonomnyj_adj{$case}{'m'}.' '.$okruga{sin}{$case}; }
		when (m/^���$/i)							{ $phrase .= $regiony{sin}{$case}; }
		when (m/^����$/i)							{ $phrase .= $respubliki{sin}{$case}; }
		}
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	
}

# ***** Process v okr/resp./ ... Name

# ----- Process v pos., ul., prof., doz. kaf., g. etc Name

while($this_line =~ m/\b(([�-ߨ�-��]+)\s+)?\b([��]�|[��]��|[��]�|[��]�|[��]��|[��]�|�|[��]�|[��]���|[��]��|[��]���|[��]��|�|�|�)\.\s*(?=([�-��a-z�-ߨA-Z]+)\.?)/g)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase;
	my $matching=$&;
	my $prev_word=$2;
	my $abbr=$3;
	my $word=$4;
	
	my $case;
	my $gender;
	
	# Check either everything was caught correctly
	
	if(($abbr =~ m/^([��]�|�|�|�|[��]�)$/) && ($word =~ m/^[�-��a-z]/)) { next; }
	
	my $flag=0;
	
	$phrase = $prev_word.' ';
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?|��|���)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|�����|��)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im-vin';
			}
		}
		
	# --- processing exceptions
	
	if(($this_line =~ m/\b(�����|�����[�-��]*|[�-��]*�����[�-��]*)\s+$matching/i) && ($prev_word =~ m/��?/)){ $case = 'tv'; }
	if(($prev_word =~ m/^�$/i)&&($word =~ m/(��?|[��]�)$/i)) { $case = 'tv'; }	# S akad. Gavrilovym
	
	# *** processing exceptions
	
	
	if($abbr =~ m/^��$/i)
		{
		my $sin_or_pl='sin';
		
		my @morph_properties=get_morph_props($word, '-i -w');

		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && ($case =~ m/$morph_properties[$i]{case}/)) {$flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
			}
		
		if($flag == 0)
			{
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/)) { $flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
				}
			}
			
		if($flag == 0)
			{
			my @morph_properties=get_morph_props($word, '-i');
			
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} =~ m/(���)/) && ($case =~ m/$morph_properties[$i]{case}/)) {$flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
				}
			
			if($flag == 0)
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)
					{
					if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} =~ m/(���)/)) { $flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
					}
				}
				
			if(($flag == 0)&&($word =~ m/^[�-ߨA-Z]/)) # If the word is not known as a surname or name but starts with a capital letter
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)
					{
					if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && ($case =~ m/$morph_properties[$i]{case}/)) {$flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
					}
				
				if($flag == 0)
					{
					for(my $i=0; $i<$#morph_properties+1; $i++)
						{
						if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/)) { $flag=1; $case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
						}
					}
				}
			}
		
		if($flag == 0)
			{
			next;
			}
			
		for($abbr)
			{
			when (m/^��$/i)	{ if($gender eq 'm') { $phrase .= $grazhdane{$sin_or_pl}{$case};} else { $phrase .= $grazhdanki{$sin_or_pl}{$case};} }
			}
		
		}

	if($case eq 'im-vin') 
		{
		if($abbr =~ m/^([��]�|[��]���|[��]��|[��]���|[��]��)$/)
			{
			my $sin_or_pl = 'sin';
			my @morph_properties=get_morph_props($word, '-i');
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && (($morph_properties[$i]{misc} =~ m/(���)/) || (($morph_properties[$i]{misc} =~ m/(���)/)&&($abbr =~ m/^[��]�$/)))) { $flag=1; $case=$morph_properties[$i]{case}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
				}
			
			if(($flag == 0)&&($word =~ m/^[�-ߨA-Z]/)) # If the word is not known as a surname or name but starts with a capital letter
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)
					{
					if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/)) { $flag=1; $case=$morph_properties[$i]{case}; $sin_or_pl=$morph_properties[$i]{number}; $i=$#morph_properties; }
					}
				}
						
			if($flag == 1)
				{
				for($abbr)
					{
					when (m/^[��]���$/)	{ $phrase .= $akademiki{$sin_or_pl}{$case}; }
					when (m/^[��]��$/)	{ $phrase .= $docenty{$sin_or_pl}{$case}; }
					when (m/^[��]���$/)	{ $phrase .= $professora{$sin_or_pl}{$case}; }
					when (m/^[��]��$/)	{ $phrase .= $tovarishhi{$sin_or_pl}{$case}; }
					when (m/^[��]�$/)	{ $phrase .= $apostoly{$sin_or_pl}{$case}; }
					}				
				}
			else
				{
				$case = 'im';
				}
			
			}
		else
			{
			$case = 'im';
			}
		}
	
	if($flag == 0)
		{
		for($abbr)
			{
			when (m/^���$/i)		{ $phrase .= $poselki{sin}{$case}; }
			when (m/^��$/i)			{ $phrase .= $stancii{sin}{$case}; }
			when (m/^��$/i)			{ $phrase .= $ulicy{sin}{$case}; }
			when (m/^���$/i)		{ $phrase .= $oblasti{sin}{$case}; }
			when (m/^��$/i)			{ $phrase .= $ozera{sin}{$case}; }
			when (m/^�$/i)			{ $phrase .= $reki{sin}{$case}; }
			when (m/^����$/i)		{ $phrase .= $akademiki{sin}{$case}; }
			when (m/^���$/i)		{ $phrase .= $docenty{sin}{$case}; }
			when (m/^����$/i)		{ $phrase .= $professora{sin}{$case}; }
			when (m/^���$/i)		{ $phrase .= $tovarishhi{sin}{$case}; }
			when (m/^�$/)			{ $phrase .= $derevni{sin}{$case}; }
			when (m/^�$/)			{ $phrase .= $goroda{sin}{$case}; }
			when (m/^�$/)			{ $phrase .= $ostrova{sin}{$case}; }
			when (m/^[��]�$/)		{ $phrase .= $apostoly{sin}{$case}; }
			}
		}
	
	$phrase .= ' ';
	
	for($word)
		{
		when (m/^(���)$/i)		{ $this_line =~ s/^(.{$pos_begin})\Q$matching$word\E\./${1}.$phrase.'������� '/e; pos($this_line)=$pos_begin+length($phrase.'������� '); } # prof. kaf. SA
		when (m/^(���)$/i)		{ $this_line =~ s/^(.{$pos_begin})\Q$matching$word\E\.?/${1}.$phrase.'���������� '/e; pos($this_line)=$pos_begin+length($phrase.'���������� '); }
		when (m/^(����)$/i)		{ $this_line =~ s/^(.{$pos_begin})\Q$matching$word\E\.?/${1}.$phrase.'��������� '/e; pos($this_line)=$pos_begin+length($phrase.'��������� '); }
		default					{ $this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/; pos($this_line)=$pos_begin+length($phrase); }
		}
	}

# ***** Process v pos., ul., prof., doz. kaf., g. etc Name

# ----- Process v nov. Dome
	
while($this_line =~ m/(\b([�-ߨ�-��]+)\s+)?\b([��]��|[��]�|[��]�|[��]�����)\.\s*([�-ߨ�-��]+)\b/g)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase;
	my $matching=$&;
	my $prev_word=$2;
	my $abbr=$3;
	my $word=$4;
	
	my $case;
	my $gender;
	my $anim;
	
	$phrase = $1;
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?|��)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|�����|��|���)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	my $sin_or_pl;
	my $flag=0; # Shows that the word is a noun
	
	# --- processing exceptions
	
	if(($this_line =~ m/\b(�����|�����[�-��]*|[�-��]*���[��]�[�-��]*)\s+$matching/i) && ($prev_word =~ m/��?/)){ $case = 'tv'; }
	if(($abbr =~ m/^��$/i)&&($word =~ m/^����$/i))	{ $flag = 1; $gender = 'm'; $sin_or_pl = 'sin'; $anim = 'od'; $case = 'rod'; }
	if(($abbr =~ m/^��$/i)&&($word =~ m/^����$/i))	{ $flag = 1; $gender = 'f'; $sin_or_pl = 'sin'; $anim = 'od'; $case = 'im'; }
	if(($abbr =~ m/^��$/i)&&($word =~ m/^��������$/i))	{ $flag = 1; $gender = 'f'; $sin_or_pl = 'sin'; $anim = 'od'; $case = 'im'; }
	
	# *** processing exceptions
	
	my @morph_properties=get_morph_props($word, '-i -w');
	my @morph_properties_prev=get_morph_props($prev_word, '-i -w');
	
	if($abbr =~ m/^��$/i)	# No plural form usually, like Sobor Sv. Ioanna
		{
		if($flag == 0)
			{
			if(($morph_properties_prev[0]{pos} eq 'S')||($prev_word =~ m/^(�����|�������|��������|��������|�|���)[�-��]{0,3}\b$/i)) # Like: 20 tysyach novyh mashin
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)	# Like: Sobor Sv. Pavla
					{
					if(($case eq 'im') && ($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && ($morph_properties[$i]{case} eq 'rod') && ($morph_properties[$i]{number} eq 'sin')) {$flag=1; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $anim=$morph_properties[$i]{anim}; $case = 'rod'; $i=$#morph_properties; }
					}
				}
			}

		if($flag == 0)
			{
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && ($morph_properties[$i]{case} eq $case) &&($morph_properties[$i]{number} eq 'sin')) {$flag=1; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $anim=$morph_properties[$i]{anim}; $i=$#morph_properties; }
				}
			}
		}
	else
		{
		if($flag == 0)
			{
			if(($morph_properties_prev[0]{pos} eq 'S')||($prev_word =~ m/^(�����|�������|��������|��������)[�-��]{0,3}\b$/i)) # Like: 20 tysyach novyh mashin
				{
				for(my $i=0; $i<$#morph_properties+1; $i++)	# Like: Sobor Sv. Pavla
					{
					if(($case eq 'im')&&($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && ($morph_properties[$i]{case} eq 'rod')) {$flag=1; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $anim=$morph_properties[$i]{anim}; $case = 'rod'; $i=$#morph_properties; }
					}
				}
			}

		if($flag == 0)
			{
			for(my $i=0; $i<$#morph_properties+1; $i++)
				{
				if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/) && ($morph_properties[$i]{case} eq $case)) {$flag=1; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $anim=$morph_properties[$i]{anim}; $i=$#morph_properties; }
				}
			}
		}
	
	if($flag == 0)
		{
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����)/)) { $flag=1; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $case=$morph_properties[$i]{case}; $anim=$morph_properties[$i]{anim}; $i=$#morph_properties; }
			}
		}
	
	if($flag == 0)
		{
		next;
		}
	
	# print $svjatoj_adj_od{dat}{m}."\n";
	
	if($sin_or_pl eq 'pl') { $gender = 'pl'; } # Special representations of adjectives, pl is inside of the gender
	
	# print $this_line."\~".$starshij_adj_od{$case}{$gender}.'~'.$case.'~'.$gender."\~\n"; exit;
	
	for($abbr)
		{
		when (m/^[��]��$/)							{ if($anim =~ m/^��$/) 	{ $phrase .= $novyj_adj_od{$case}{$gender};} else { $phrase .= $novyj_adj{$case}{$gender};} }
		when (m/^[��]�$/)							{ if($anim =~ m/^��$/)	{ $phrase .= $starshij_adj_od{$case}{$gender}; } else { $phrase .= $starshij_adj{$case}{$gender};} }
		when (m/^[��]�$/)							{ if($anim =~ m/^��$/) 	{ $phrase .= $svjatoj_adj_od{$case}{$gender};} else { $phrase .= $svjatoj_adj{$case}{$gender};} }
		when (m/^[��]�����$/)						{ if($anim =~ m/^��$/) 	{ $phrase .= $informacionnyj_adj_od{$case}{$gender};} else { $phrase .= $informacionnyj_adj{$case}{$gender};} }
		}
	
	$phrase .= ' '.$word;
	
	# $this_line =~ s/\Q$matching\E/$phrase/;
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
	}

# ***** Process v nov. Dome

# ----- na sev.-zap (adj)

while($this_line =~ m/
	(\b([�-��]+)\s+)?						# previous word $2
	\b(���|���|(?-i:�)|��|����)\b\.?		# abbreviation one $3
	(\s*(\-|\�|\�|\,|���|�)\s*)?			# connection $5 (optional)
	(\b(���|���|(?-i:�)|��|����)\b\.?)?		# abbreviation two $7 (optional)
	(?=\s+([�-��]+)\b)						# word after abbreviations $8
	/xig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase='';
	my $matching=$&;
	my $prev_word=$2;
	my $abbr1=$3;
	my $connection=$5;
	my $abbr2=$7;
	my $word=$8;
	
	my $case='';
	my $gender;
	my $sin_or_pl;
	my $word_pos; # part of speech
	my $word_case;
	my $abbr;
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod_';
			}
		if($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat_';
			}
		if($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv_';
			}
		if($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr_';
			}	
		if($prev_word =~ m/^(��|�����|��|���|��)$/i) # (Vin)
			{
			$case = 'vin_';
			}
		else # (Im)
			{
			$case = 'im_';
			}
		}
	
	# --- processing exceptions
	
	if(($this_line =~ m/�����\s+$matching/i) && ($prev_word eq '�')){ $case =~ s/rod/tv/; }
	if(($prev_word =~ m/^�$/i)&&($word =~ m/(��?|[��]�)$/i)) { $case =~ s/rod/tv/; }	# S Sev.-Zap. Frontom
	if(($matching =~ m/\b���\b(?!\s?\.)/i)&&($abbr2 eq '')) { next; }	# Sev na kryshku.
	if(($matching =~ m/\b���\.\s+[�-ߨA-Z]/)&&($abbr2 eq '')) { next; }	# Na stupenki sev.
	
	# *** processing exceptions
	
	my @morph_properties=get_morph_props($word, '-i');

	my $flag=0; # Shows that the word is recognized
	for(my $i=0; $i<$#morph_properties+1; $i++)
		{
		if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/) && ($morph_properties[$i]{case} =~ m/$case/) && ($morph_properties[$i]{adj_form} !~ m/��/) &&($morph_properties[$i]{number} ne 'pl')) { $flag=1; $case = $morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $word_pos='A'; $word_case=$case; $i=$#morph_properties; }
		elsif(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����|���|���)/) && ($morph_properties[$i]{case} =~ m/$case/) && (($morph_properties[$i]{misc} !~ m/���/)||($morph_properties[$i]{number} ne 'pl'))) {$flag=1; $case = $morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $word_pos='S'; $word_case=$case; $i=$#morph_properties; }
		}
	
	if($flag == 0)
		{
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/) && ($morph_properties[$i]{adj_form} !~ m/��/) &&($morph_properties[$i]{number} ne 'pl')) { $flag=1; $word_case=$morph_properties[$i]{case};  $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $word_pos='A'; $i=$#morph_properties; }
			elsif(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����|���|���)/) && (($morph_properties[$i]{misc} !~ m/���/)||($morph_properties[$i]{number} ne 'pl'))) {$flag=1; $word_case=$morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $word_pos='S'; $i=$#morph_properties; }
			}
		}

	if($case =~ m/\_/)
		{
		$case =~ s/\_.*$//;
		}
	
	if($prev_word =~ m/\w/) { $phrase = $prev_word.' '; }
	
	if(($sin_or_pl =~ m/pl/)&&($connection !~ m/^(�|���)$/)) { $gender = 'pl'; }
	
	if($abbr2 =~ m/[�-��]/i)
		{
		if($connection =~ m/^(\-|\�|\�)$/)
			{
			for($abbr1)
				{
				when (m/^�/i)			{ $phrase .= '������-'; }
				when (m/^�/i)			{ $phrase .= '���-'; }
				when (m/^�/i)			{ $phrase .= '�������-'; } # should be next, perhaps
				when (m/^�/i)			{ $phrase .= '��������-'; } # should be next, perhaps
				}
			}
		elsif(($word_pos =~ m/^(S|A)$/)&&($flag == 1))
			{		
			for($abbr1)
				{
				when (m/^�/i)			{ $phrase .= $severnyj_adj{$word_case}{$gender}; }
				when (m/^�/i)			{ $phrase .= $juzhnyj_adj{$word_case}{$gender}; }
				when (m/^�/i)			{ $phrase .= $zapadnyj_adj{$word_case}{$gender}; }
				when (m/^�/i)			{ $phrase .= $vostochnyj_adj{$word_case}{$gender}; }
				}
			if($connection =~ m/^(�|���)$/i)
				{
				$phrase .= ' '.$connection.' ';
				}
			}
		else
			{
			next;
			}
			
		$abbr=$abbr2;
		}
	else
		{
		$abbr=$abbr1;
		}
	
	if(($word_pos =~ m/^(S|A)$/)&&($flag == 1))
		{		
		for($abbr)
			{
			when (m/^�/i)			{ $phrase .= $severnyj_adj{$word_case}{$gender}; }
			when (m/^�/i)			{ $phrase .= $juzhnyj_adj{$word_case}{$gender}; }
			when (m/^�/i)			{ $phrase .= $zapadnyj_adj{$word_case}{$gender}; }
			when (m/^�/i)			{ $phrase .= $vostochnyj_adj{$word_case}{$gender}; }
			}
		}
	else
		{
		next;
		}
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
}

# ***** na sev.-zap (adj)

# ----- na sev.-zap (common)

while($this_line =~ m/
	(\b([�-��]+)\s+)?										# previous word $2
	\b((?-i:[����]{1,2}))\b 								# abbriviation one ($3) - case sensitive
	(\s*(\-|\�|\�|\,|���|�)\s*)?							# connection $5
	(\b((?-i:[����]{1,2}))\b)?								# - and abbriviation two $6 (optional)
	(?=\s+([�-��]+)\b)?										# word after abbreviaions $8 - is not in the matching (optional)
	/xig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];
	my $phrase='';
	my $matching=$&;
	my $prev_word=$2;
	my $abbr1=$3;
	my $connection=$5;
	my $abbr2=$6;
	my $word=$8;
	
	my $case;
	my $gender;
	my $sin_or_pl;
	my $word_pos; # part of speech
	my $word_case;
	my $abbr;
	
	if(($prev_word eq '')&&($abbr1 =~ m/^[��]$/)&&($abbr2 eq '')&&($this_line =~ m/(^|[\W])\s*$matching/)) { next; }
	if(($prev_word =~ m/^[A-Z�-ߨ]{2,}$/)||($word =~ m/^[A-Z�-ߨ]+$/)) { next; }	# V GLAVE 5
	if(($prev_word !~ m/^(�|��|�|��|��)$/i)&&($this_line !~ m/\b(����?�|�����|�����|�����|����������|�����|��������|������|������)/i))	{ next; }
	if($this_line =~ m/\b(����|�����?�|������?�|��[�]�|���\b|������|�������|��������|�������������|�������|�?�����)/i) { next; }
	if(($prev_word !~ m/^(�|��|�|��|��)$/i)&&($abbr1 =~ m/^(�|�)$/)&&($word =~ m/\b(������)/i)) { next; }
	# if(($abbr1 =~ m/^[��]$/)&&($abbr2 eq ''))	{ next; }
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|�����|��|���|��)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	# --- processing exceptions
	
	if(($this_line =~ m/�����\s+$matching/i) && ($prev_word eq '�')){ $case = 'tv'; }
	if($prev_word =~ m/^(��)$/i) { $case = 'vin_pr'; }
	
	my @morph_properties=get_morph_props($word, '-i -w');

	my $flag=0; # Shows that the word is recognized
	for(my $i=0; $i<$#morph_properties+1; $i++)
		{
		# if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/) && ($case =~ m/$morph_properties[$i]{case}/) && ($morph_properties[$i]{adj_form} !~ m/��/) &&($morph_properties[$i]{number} ne 'pl')) { $flag=1; $case = $morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $word_pos='A'; $case=$morph_properties[$i]{case}; $i=$#morph_properties; }
		if(($morph_properties[$i]{pos} eq 'S') && ($morph_properties[$i]{gender} ne '') && ($morph_properties[$i]{misc} !~ m/(����|���|���)/) && ($case =~ m/$morph_properties[$i]{case}/) && ($morph_properties[$i]{number} ne 'pl')) {$flag=1; $case = $morph_properties[$i]{case}; $gender=$morph_properties[$i]{gender}; $sin_or_pl=$morph_properties[$i]{number}; $word_pos='S'; $case=$morph_properties[$i]{case}; $i=$#morph_properties; }
		}
		
	if($case =~ m/\_/)
		{
		$case =~ s/\_.*$//;
		}
	
	if($flag==0)
		{
		# my $flag = 0; # the abbreviation is an adjective 
		$sin_or_pl = 'sin';
		
		if(($connection !~ m/(\,|�|���)/i) || ($abbr2 ne ''))
			{
			for($word)
				{
				when (m/^(�����|������?|������?|�����|�����������|������?|���������|�������)$/i)											{ $case = 'im'; $flag=1; }
				when (m/^(����(�|��)|�����(�|��)|�����(�|��)|�����(�|��)|����������(�|�)|�����(�|��)|��������(�|�)|������(�|))$/i)			{ $case = 'rod'; $flag=1; }
				when (m/^(����(�|��)|�����(�|��)|�����(�|��)|�����(�|��)|����������(�|��)|�����(�|��)|��������(�|��)|������(�|��))$/i)		{ $case = 'dat'; $flag=1; }
				when (m/^(����(��|���)|�����(��|���)|�����(��|���)|�����(��|���)|����������(��|���)|�����(��|���)|��������(��|���)|������(��|���))$/i)	{ $case = 'tv'; $flag=1; }
				when (m/^(����(�|��)|�����(�|��)|�����(�|��)|�����(�|��)|����������(�|��)|�����(�|��)|��������(�|��)|������(��))$/i)		{ $case = 'pr'; $flag=1; }
				}
			
			if($flag == 1)
				{
				if($word =~ m/^(���|�����|�����|�����|�����)/i) 		{ $gender = 'm'; }
				elsif($word =~ m/^(���������|��������)/i)				{ $gender = 'n'; }
				elsif($word =~ m/^(������)/i)							{ $gender = 'f'; }
				}
			
			if($flag == 0)
				{
				for($prev_word)
					{
					when (m/^(�����|������?|������?|�����|�����������|������?|���������|�������)$/i)											{ $case = 'im'; $flag=1; }
					when (m/^(����(�|��)|�����(�|��)|�����(�|��)|�����(�|��)|����������(�|�)|�����(�|��)|��������(�|�)|������(�|))$/i)			{ $case = 'rod'; $flag=1; }
					when (m/^(����(�|��)|�����(�|��)|�����(�|��)|�����(�|��)|����������(�|��)|�����(�|��)|��������(�|��)|������(�|��))$/i)	 	{ $case = 'dat'; $flag=1; }
					when (m/^(����(��|���)|�����(��|���)|�����(��|���)|�����(��|���)|����������(��|���)|�����(��|���)|��������(��|���)|������(��|���))$/i)	{ $case = 'tv'; $flag=1; }
					when (m/^(����(�|��)|�����(�|��)|�����(�|��)|�����(�|��)|����������(�|��)|�����(�|��)|��������(�|��)|������(��))$/i)		{ $case = 'pr'; $flag=1; }
					}
			
				if($flag == 1)
					{
					if($prev_word =~ m/^(���|�����|�����|�����|�����)/i) 		{ $gender = 'm'; }
					elsif($prev_word =~ m/^(���������|��������)/i)				{ $gender = 'n'; }
					elsif($prev_word =~ m/^(������)/i)							{ $gender = 'f'; }
					}
				}
			}
		}

	if($prev_word =~ m/\w/) { $phrase = $prev_word.' '; }
	
	# Combine S-Z into SZ
	if(($abbr1 =~ m/^[����]$/)&&($abbr2 =~ m/^[����]$/)&&($connection =~ m/^(\-|\�|\�)$/))
		{
		$abbr1 .= $abbr2;
		$abbr2 = '';
		$connection = '';
		}
	
	
	
	# Process abbreviation one
	if(length($abbr1) == 2)
		{
		for(substr($abbr1, 0, 1))
			{
			when (m/^�$/i)			{ $phrase .= '������-'; }
			when (m/^�$/i)			{ $phrase .= '���-'; }
			when (m/^�$/i)			{ $phrase .= '�������-'; } # should be next, perhaps
			when (m/^�$/i)			{ $phrase .= '��������-'; } # should be next, perhaps
			}
		$abbr=substr($abbr1, 1, 1);
		}
	else
		{
		$abbr=substr($abbr1, 0, 1);
		}
	
	
	if($flag == 1)
		{
		for($abbr)
			{
			when (m/^�$/)			{ $phrase .= $severnyj_adj{$case}{$gender}; }
			when (m/^�$/)			{ $phrase .= $juzhnyj_adj{$case}{$gender}; }
			when (m/^�$/)			{ $phrase .= $zapadnyj_adj{$case}{$gender}; }
			when (m/^�$/)			{ $phrase .= $vostochnyj_adj{$case}{$gender}; }
			}
		}
	else
		{
		for($abbr)
			{
			when (m/^�$/i)			{ $phrase .= $sever{sin}{$case}; }
			when (m/^�$/i)			{ $phrase .= $zapad{sin}{$case}; }
			when (m/^�$/i)			{ $phrase .= $vostok{sin}{$case}; }
			when (m/^�$/i)			{ $phrase .= $jug{sin}{$case}; }
			}
		}
	
	if(($abbr2 !~ m/[����]/)||($connection =~ m/^(�|���)$/i))
		{
		$phrase .= ' '.$connection;
		}
	
	# The same for a second abbreviation (if it exists and was not shifted to abbreviation one)
	if($abbr2 =~ m/[����]/)
		{
		$phrase .= ' ';
	
		if(length($abbr2) == 2)
			{
			for(substr($abbr2, 0, 1))
				{
				when (m/^�$/i)			{ $phrase .= '������-'; }
				when (m/^�$/i)			{ $phrase .= '���-'; }
				when (m/^�$/i)			{ $phrase .= '�������-'; } # should be next, perhaps
				when (m/^�$/i)			{ $phrase .= '��������-'; } # should be next, perhaps
				}
			$abbr=substr($abbr2, 1, 1);
			}
		else
			{
			$abbr=substr($abbr2, 0, 1);
			}
		
		
		if($flag == 1)
			{
			for($abbr)
				{
				when (m/^�$/)			{ $phrase .= $severnyj_adj{$case}{$gender}; }
				when (m/^�$/)			{ $phrase .= $juzhnyj_adj{$case}{$gender}; }
				when (m/^�$/)			{ $phrase .= $zapadnyj_adj{$case}{$gender}; }
				when (m/^�$/)			{ $phrase .= $vostochnyj_adj{$case}{$gender}; }
				}
			}
		else
			{
			for($abbr)
				{
				when (m/^�$/i)			{ $phrase .= $sever{sin}{$case}; }
				when (m/^�$/i)			{ $phrase .= $zapad{sin}{$case}; }
				when (m/^�$/i)			{ $phrase .= $vostok{sin}{$case}; }
				when (m/^�$/i)			{ $phrase .= $jug{sin}{$case}; }
				}
			}
		}
	
	$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	pos($this_line)=$pos_begin+length($phrase);
}

# ***** na sev.-zap (common)

# ----- Process %-y, $-y, %-yj, etc

while($this_line =~ m/
	(\b([�-��]+)\s+)?										# previous word $2
	([\%\$])												# sign ($3)
	\s*[\-\�\�]\s*											#
	([�-��]{1,3}\b)											# word part $4
	(?=\s*([�-��]+)\b)?										# next word $5 
	/xig)
	{
	my $pos_begin=$-[0];
	my $pos_end=$+[0];	# my $pos_end=pos($this_line); - the same
	my $phrase;
	my $matching=$&;
	my $prev_word=$2;
	my $sign=$3;
	my $word_part=$4;
	my $next_word=$5;
	
	my $case;
	my $flag=0;
	
	$phrase = $prev_word.' ';
	
	# preliminary determination of the case
	my $line_before_number=substr($this_line, 0, $pos_begin).$prev_word;
	
	# --- processing exceptions
	
	if($line_before_number =~ m/\d\s*$/){ next; } # this case is solved by the number processing
	
	# *** processing exceptions
	
	my $prel_case=preliminary_case($line_before_number);
	
	if($prel_case ne 'im')
		{
		$case=$prel_case;
		}
	else
		{
		if($prev_word =~ m/^(�|�|��|��|�����|�����|�����|�����|������|������|����������|��������[���]|��)$/i) # (Rod)
			{
			$case = 'rod';
			}
		elsif($prev_word =~ m/^(�|��)$/i) # (Dat)
			{
			$case = 'dat';
			}
		elsif($prev_word =~ m/^(�����|���)$/i) # (Tv)
			{
			$case = 'tv';
			}
		elsif($prev_word =~ m/^(�|��|��?)$/i) # (Pr)
			{
			$case = 'pr';
			}	
		elsif($prev_word =~ m/^(��|�����|��|���|��)$/i) # (Vin)
			{
			$case = 'vin';
			}
		else # (Im)
			{
			$case = 'im';
			}
		}
	
	# check first the noun in the found case
	if($case ne 'im')
		{
		if($sign =~ m/\%/)
			{
			if($procenty{sin}{$case} =~ m/$word_part$/i)
				{
				$phrase .= $procenty{sin}{$case}; $flag=1;
				}
			elsif($procenty{pl}{$case} =~ m/$word_part$/i)
				{
				$phrase .= $procenty{pl}{$case}; $flag=1;
				}
			}
		elsif($sign =~ m/\$/)
			{
			if($dollary{sin}{$case} =~ m/$word_part$/i)
				{
				$phrase .= $dollary{sin}{$case}; $flag=1;
				}
			elsif($dollary{pl}{$case} =~ m/$word_part$/i)
				{
				$phrase .= $dollary{pl}{$case}; $flag=1;
				}
			}
		}
	
	# check the noun in any case
	if($flag == 0)
		{
		for(my $j=0; $j<6; $j++)
			{
			if($sign =~ m/\%/)
				{
				if($procenty{sin}{$cases_base[$j]} =~ m/$word_part$/i)
					{
					$phrase .= $procenty{sin}{$cases_base[$j]}; $flag=1; $j=6;
					}
				elsif($procenty{pl}{$cases_base[$j]} =~ m/$word_part$/i)
					{
					$phrase .= $procenty{pl}{$cases_base[$j]}; $flag=1; $j=6;
					}
				}
			elsif($sign =~ m/\$/)
				{
				if($dollary{sin}{$cases_base[$j]} =~ m/$word_part$/i)
					{
					$phrase .= $dollary{sin}{$cases_base[$j]}; $flag=1; $j=6;
					}
				elsif($dollary{pl}{$cases_base[$j]} =~ m/$word_part$/i)
					{
					$phrase .= $dollary{pl}{$cases_base[$j]}; $flag=1; $j=6;
					}
				}
			}
		}
	
	# if not, check the adjective first in the found case
	if(($flag == 0)&&($case != 'im'))
		{
		for(my $k=0; $k<4; $k++)
			{
			if($sign =~ m/\%/)
				{
				if($procentnyj_adj{$case}{$genders_base[$k]} =~ m/$word_part$/i)
					{
					$phrase .= $procentnyj_adj{$case}{$genders_base[$k]}; $flag=1;
					}
				}
			elsif($sign =~ m/\$/)
				{
				if($dollarovyj_adj{$case}{$genders_base[$k]} =~ m/$word_part$/i)
					{
					$phrase .= $dollarovyj_adj{$case}{$genders_base[$k]}; $flag=1;
					}
				}
			}
		}
		
	# try the case and gender according to next adjective/noun
	if(($flag == 0)&&($next_word =~ m/\w/))
		{
		my @morph_properties=get_morph_props($next_word, '-i -w');
		for(my $i=0; $i<$#morph_properties+1; $i++)
			{
			if(($morph_properties[$i]{pos} =~ m/^(S|A|APRO)$/)&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{adj_form} ne '��')&&($morph_properties[$i]{number} eq 'sin')&&($morph_properties[$i]{misc} !~ m/(����|���|���)/))
				{
				if(($sign =~ m/\%/)&&($procentnyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}} =~ m/$word_part$/i))
					{
					$phrase = $procentnyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}}; $flag=1; $i=$#morph_properties;
					}
				elsif(($sign =~ m/\$/)&&($dollarovyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}} =~ m/$word_part$/i))
					{
					$phrase = $dollarovyj_adj{$morph_properties[$i]{case}}{$morph_properties[$i]{gender}}; $flag=1; $i=$#morph_properties;
				
					}
				
				}
			}
		}
	
	# try the adjectives of any case and gender
	if($flag == 0)
		{
		for(my $j=0; $j<6; $j++)
			{
			for(my $k=0; $k<4; $k++)
				{
				if(($sign =~ m/\%/)&&($procentnyj_adj{$cases_base[$j]}{$genders_base[$k]} =~ m/$word_part$/))
					{
					$phrase = $procentnyj_adj{$cases_base[$j]}{$genders_base[$k]}; $flag=1;
					}
				elsif(($sign =~ m/\$/)&&($dollarovyj_adj{$cases_base[$j]}{$genders_base[$k]} =~ m/$word_part$/))
					{
					$phrase = $dollarovyj_adj{$cases_base[$j]}{$genders_base[$k]}; $flag=1;
					}
				}
			}
		}
		
	if($flag == 1)
		{
		$this_line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
		pos($this_line)=$pos_begin+length($phrase);
		}
	}

# ***** Process 100%-yj, 15$-yj, %-yj, etc

# # process '=' sign
# while($this_line =~ m/\b([�-��]\b)?\s*\b([�-��]\b)?\s*\b([�-��]\b)?\s*\b([�-��]\b)?\s*\=\s*/ig)
	# {
	# my $pos_begin=$-[0];
	# my $pos_end=$+[0];
	# my $phrase="";
	
	# my $matching=$&;
	# my $prev_word1 = $1;
	# my $prev_word2 = $2;
	# my $word = $3;
	
	# $phrase = $prev_word.' '.$word.' ';
	
	# # Read the properties of the word from mystem program;
	# my @morph_properties=get_morph_props($word, '-i -w');
	# my $gender = '';
	# my $sin_or_pl = '';
	# my $pos = '';
	
	# for(my $i=0; $i<$#morph_properties+1; $i++) # check if the 1st word could be a noun
		# {
		# if(($morph_properties[$i]{pos} eq 'S')&&($morph_properties[$i]{gender} ne '')&&($morph_properties[$i]{number} ne '')&&($morph_properties[$i]{misc} !~ m/^(����|���|���)$/)) 
			# {
			# $gender = $morph_properties[$i]{gender};
			# $sin_or_pl = $morph_properties[$i]{number};
			# $case = $morph_properties[$i]{case};
			# $pos='S';
			# $i=$#morph_properties+1;
			# }
		# }
	
	# if($gender eq '')
		# {
		# for(my $i=0; $i<$#morph_properties+1; $i++) # check if the 1st word could be also adjective, if yes try the 2nd word
			# {
			# if(($morph_properties[$i]{pos} =~ m/^(A|NUM|APRO)$/)&&($morph_properties[$i]{adj_form} ne '��'))
				# {
				# $gender=$morph_properties[$i]{gender};
				# $sin_or_pl = $morph_properties[$i]{number};
				# $case = $morph_properties[$i]{case};
				# $pos='A';
				# $i=$#morph_properties;
				# }
			# };
		# }
	
	
	# if($pos eq 'S')
		# {
		# if(($sin_or_pl eq 'pl')||($case =~ m/(rod|dat|tv|pr|vin)/))
			# {
			# if($case =~ m/rod/) # 5 kilogramm, if 'rod' case comes first
				# {
				# $phrase .= '����� ';
				# }
			# else
				# {
				# $phrase .= '����� ';
				# }
			# }
		# else
			# {
			# if($prev_word =~ m/^$odni_pattern$/i)
				# {
				# if($gender eq 'm') { $phrase .= '����� '; }
				# elsif($gender eq 'f') { $phrase .= '����� '; }
				# else { $phrase .= '����� '; }
				# }
			# else
				# {
				# $phrase .= '����� ';
				# }
			# }
		# }
	# elsif($pos eq 'A')
		# {
		
		# }
	
	# # $line =~ s/\Q$matching\E/$phrase/;
	# $line =~ s/^(.{$pos_begin})\Q$matching\E/${1}$phrase/;
	# pos($line)=$pos_begin+length($phrase);
	
	
	# }

return $this_line;
}

sub num2book
{
# Usage: num2book($numeral_signs_string)
# Input string consists of: numbers, spaces and the following signs "\:", "\,", "\.", "-", "\;"
# For citation to bible texts

my $number_s=$_[0];

$number_s =~ s/^[\D]+//;
$number_s =~ s/[\D]+$//;

my $phrase = '';
my $number_index = 0;
my $new_citation = 1;
my $only_chapters = 0;
my $stihi_flag = 0;	# There was no mentioning of stihi

my @sep_cit = split (/\;/, $number_s);	# Separate citaions, like by. ���� 58:2; 118:5 (there are two citations here, divided by "\:" sign)

for(my $i=0; $i<$#sep_cit+1; $i++)
	{
	if($sep_cit[$i] =~ m/\d\,\ +\d/) { $only_chapters = 1; } else { $only_chapters = 0; }
	
	$number_index = 0;
	
	if($only_chapters == 1)	# If there are only chapters, without poems
		{
		$phrase .= ' ����� ';
		
		while($sep_cit[$i] =~ m/\d+/g)
			{
			my $pos_begin=$-[0];
			my $pos_end=$+[0];
			my $curr_number=$&;
			my $coming_after_s=substr($sep_cit[$i], $pos_end); # from till the end of the string
			my $coming_before_s=substr($sep_cit[$i], 0, $pos_begin); # from and how much
			
			if($coming_before_s =~ m/\ +\-+\ +$/)
				{
				$phrase .= ' �� '.num2word($curr_number, 'por', 'vin', 'f');
				}
			elsif($coming_after_s =~ m/^\ +\-+\ +(\d+)/)
				{
				my $number = $1;
				if($number == 2) { $phrase .= ' �� '; } else { $phrase .= ' � '; }
				$phrase .= num2word($curr_number, 'por', 'rod', 'f');
				}
			else
				{
				$phrase .= ' '.num2word($curr_number, 'por', 'im', 'f');
				}	
			$number_index++;
			}
		}
	else
		{
		$phrase .= " ����� ";
		my $next_stihi_word = 0; # The word before next number should be the "poem"
		
		while($sep_cit[$i] =~ m/\d+/g)
			{
			my $pos_begin=$-[0];
			my $pos_end=$+[0];
			my $curr_number=$&;
			my $coming_after_s=substr($sep_cit[$i], $pos_end); # from till the end of the string
			my $coming_before_s=substr($sep_cit[$i], 0, $pos_begin); # from and how much
			
			if($number_index == 0)
				{
				$phrase .= ' '.num2word($curr_number, 'por', 'im', 'f');
				$next_stihi_word = 1;
				}
			else
				{
				if($coming_before_s =~ m/\ +\-+\ +$/)
					{
					$phrase .= ' �� ����� '.num2word($curr_number, 'por', 'vin', 'f');
					$next_stihi_word = 1;
					}
				else
					{
					if($next_stihi_word == 1)
						{
						if($coming_after_s =~ m/^(\.|\,|\-+)\d/)
							{
							$phrase .= " ����� ";
							}
						else
							{
							$phrase .= " ���� ";
							}
						$next_stihi_word = 0;
						}
					
					if($coming_before_s =~ m/\-$/)
						{
						$phrase .= ' �� '.num2word($curr_number, 'por', 'vin', 'm');
						}
					elsif($coming_after_s =~ m/^\-+(\d+)/)
						{
						my $number = $1;
						if($number == 2) { $phrase .= ' �� '; } else { $phrase .= ' � '; }
						$phrase .= num2word($curr_number, 'por', 'rod', 'm');
						}
					else
						{
						$phrase .= ' '.num2word($curr_number, 'por', 'im', 'm');
						}
					}
				}
			
			$number_index++;
			}
		}
	}

return $phrase;
}

sub get_arab_num
{
#Usage: get_arab_num($roman_numeral_string);
#
#Borrowed and adapted from http://www.cyberforum.ru/cpp-beginners/thread125615.html
	
	my $rom_str_s=$_[0];
	my @rom_str=split(//,$rom_str_s);
	
	my $res = 0;
    for(my $i = 0; $i < $#rom_str+1; $i++)
		{
		for ($rom_str[$i])
			{
			when (m/[Mm�]/) 	{	$res += 1000;	}
			when (m/[Dd]/)		{	$res += 500; 	} 
			when (m/[Cc��]/)	{
									if(($i+1<$#rom_str+1)&&($rom_str[$i+1] =~ m/[DdMm�]/))
										{
										$res -= 100;
										}
									else
										{
										$res += 100;
										};
								}
			when (m/[Ll]/)		{	$res += 50;	} 
			when (m/[Xx��]/)	{
									if(($i+1<$#rom_str+1)&&($rom_str[$i+1] =~ m/[LlCc��]/))
										{
										$res -= 10;
										}
									else
										{
										$res += 10;
										}
								}
			when (m/[Vv]/)		{	$res += 5;} 
			when (m/[Ii]/)		{
									if(($i+1<$#rom_str+1)&&($rom_str[$i+1] =~ m/[VvXx��]/))
										{
										$res -= 1;
										}
									else
										{
										$res += 1;
										}
								}
			} #for
		} #for

    return $res;
}

# ----- Write the real value with (possibly) fractional part in words for a case when the preposition "po"

sub realnum2word_po
{
# Usage: realnum2word('string_number','kol','im/rod/dat/vin/tv/pr','m/f/n/pl'); # case is not really used (only once but probably it is necessary to replace $case by im/vin)

my $number_s=$_[0];
my $num_type=$_[1];
my $case=$_[2];
my $gender=$_[3];
my $phrase='';

$number_s =~ s/^[0\s]*//;
$number_s =~ s/\s*$//;
if($number_s =~ m/[\.\,]/) { $number_s =~ s/[\.\,]?0*$//;}
my @number=split(/[\.\,]/,$number_s);
if($#number+1 >2) { die "Error!!! The number of full points/commas more than 1 in one number (function realnum2word): ".$number_s."\n";}
if($num_type ne 'kol') { die "Error!!! Number type is not equal \"kol\" (function realnum2word_po) :".$num_type."\n";}
#if($gender eq 'pl') { die "Error!!! There should not be \"pl\" parameter (function realnum2word_po) :".$gender."\n";} # to be changed

# like 1,5 ; 2,5 and 3,5 and so on
if($gender ne 'pl') # new
	{
	if($number_s =~ m/^1[\,\.]5$/) { $phrase .= $one_and_half{$gender}{'im'}; return $phrase;}
	if($number_s =~ m/(^\d*(^|[^1]))1[\,\.]5$/) { $phrase .= num2word($1.'0','kol','im',$gender).' '.num2word('1','kol','dat',$gender).' � ���������'; return $phrase;}
	if( ($number_s =~ m/\d+[\,\.]5$/) && (int($number[0]) ne 0)) { $phrase .= num2word($number[0],'kol',$case,$gender).' � ���������'; return $phrase;} # case is used only here, but should be 'im/vin'
	}

my $left_part;
my $right_part;
my $substitute_what;
my $substitute_by;

if($#number+1 eq 2) {$gender = 'f';} # if the number is not integer

if($number[0] =~ m/^1$/) {$phrase .= num2word('1','kol','dat',$gender); }
elsif($number[0] =~ m/(^\d*(^|[^1]))1$/) {$phrase .= num2word($1.'0','kol','im',$gender).' '.num2word('1','kol','dat',$gender); }
elsif($number[0] =~ m/^(\d*(^|[^1]))1((000)+)$/)
	{
	$left_part=$1; $right_part=$3;
	$phrase .= num2word($left_part.'1'.$right_part,'kol','im',$gender);
	if($number[0] =~ m/\d+1000$/) # 1000 is returned by num2word as tysjacha without odna
		{
		$substitute_what='���� '.num2word('1'.$right_part,'kol','im',$gender);
		$substitute_by='����� '.num2word('1'.$right_part,'kol','dat',$gender);
		}
	else
		{
		$substitute_what=num2word('1'.$right_part,'kol','im',$gender);
		$substitute_by=num2word('1'.$right_part,'kol','dat',$gender);
		}
	$phrase =~ s/\Q$substitute_what\E/$substitute_by/eg;
	$case = 'dat';
	}
else{	$phrase .= num2word($number[0],'kol','im',$gender); };

if($#number+1 eq 2)
	{
	
	if($number[0] =~ m/(^\d*(^|[^1]))1$/) { $phrase = $phrase.' '.$celyh_adj{'rod'}{'f'}.' ';}
	else { $phrase = $phrase.' '.$celyh_adj{'rod'}{'pl'}.' ';}
	
	# fractional part
	
	if($number[1] =~ m/^0*1$/) {$phrase .= num2word('1','kol','dat',$gender); }
	elsif($number[1] =~ m/(^\d*(^|[^1]))1$/) {$phrase .= num2word($1.'0','kol','im',$gender).' '.num2word('1','kol','dat',$gender); }
	else{	$phrase .= num2word($number[1],'kol','im',$gender);};

	my $fractional_digits_number = length($number[1]);
	my $fractional_base="1"; for(my $i=0; $i<$fractional_digits_number; $i++) { $fractional_base .='0';}
	
	# like stotysyachnyh
	my $aux_phrase;
	if($number[1] =~ m/(^\d*(^|[^1]))1$/) { $aux_phrase = num2word($fractional_base,'por','dat','f');}
	else { $aux_phrase=num2word($fractional_base,'por','rod','pl');}
	$aux_phrase =~ s/^����//;
	$phrase = $phrase.' '.$aux_phrase;
	}

return $phrase;

}

# ***** Write the real value with (possibly) fractional part in words for a case when the preposition "po"


# ----- Write the real value with (possibly) fractional part in words

sub realnum2word
{
# Usage: realnum2word('string_number','kol','im/rod/dat/vin/tv/pr','m/f/n/pl');

my $number_s=$_[0];
my $num_type=$_[1];
my $case=$_[2];
my $gender=$_[3];
my $phrase='';

if($number_s =~ m/[\.\,]/) { $number_s =~ s/[\.\,]?0*$//;}
my @number=split(/[\.\,]/,$number_s);
if($#number+1 >2) { die "Error!!! The number of full points/commas more than 1 in one number (function realnum2word): ".$number_s."\n";}
if($num_type ne 'kol') { die "Error!!! Number type is not equal \"kol\" (function realnum2word) :".$num_type."\n";}
#if($gender eq 'pl') { die "Error!!! There should not be \"pl\" parameter (function realnum2word) :".$gender."\n";} # to be changed

# like 1,5 ; 2,5 and 3,5 and so on
if($gender ne 'pl') # new
	{
	if($number_s =~ m/^1[\,\.]5$/) { $phrase = $phrase.$one_and_half{$gender}{$case}; return $phrase;}
	if(($number_s =~ m/\d+[\,\.]5$/) && (int($number[0]) ne 0)) { $phrase = $phrase.num2word($number[0],'kol',$case,$gender).' � ���������'; return $phrase;}
	}
	
if($#number+1 eq 1)
	{
	# integer part
	$phrase = $phrase.num2word($number[0],$num_type,$case,$gender);
	}
elsif($#number+1 eq 2)
	{
	$gender='f';
	# integer part
	$phrase = $phrase.num2word($number[0],$num_type,$case,$gender);
	if($number[0] =~ m/(^\d*(^|[^1]))1$/) { $phrase = $phrase.' '.$celyh_adj{$case}{'f'};}
	elsif($case =~ m/(vin|im)/) { $phrase = $phrase.' '.$celyh_adj{'rod'}{'pl'};}
	elsif($number[0] =~ m/000$/) { $phrase = $phrase.' '.$celyh_adj{'rod'}{'pl'};}
	else { $phrase = $phrase.' '.$celyh_adj{$case}{'pl'};}
	
	# fractional part
	$phrase = $phrase.' '.num2word($number[1],$num_type,$case,$gender);
	my $fractional_digits_number = length($number[1]);
	my $fractional_base="1"; for(my $i=0; $i<$fractional_digits_number; $i++) { $fractional_base .='0';}
	
	# like stotysyachnyh
	my $aux_phrase;
	if($number[1] =~ m/(^\d*(^|[^1]))1$/) { $aux_phrase=num2word($fractional_base,'por',$case,'f');}
	elsif($case =~ m/(vin|im)/) { $aux_phrase=num2word($fractional_base,'por','rod','pl');}
	else {$aux_phrase=num2word($fractional_base,'por',$case,'pl');}
	$aux_phrase =~ s/^����//;
	$phrase = $phrase.' '.$aux_phrase;
	}
	
return $phrase;
}

# ***** Write the real value with (possibly) fractional part in words

sub num2word
{
# Usage: num2word('string_number','kol/por','im/rod/dat/vin/tv/pr','m/f/n/pl');

my $number_s=$_[0];
my $num_type=$_[1];
my $case=$_[2];
my $gender=$_[3];
my $phrase='';
my $hundred_s;
my @hundred;
my $rest_number_s;
my @rest_number;
my $num_class_one, my $num_class_from5, my $num_class_till4, my $num_class_gender;

$number_s =~ s/^0+//;

my $num_length=length($number_s);
if($num_length % 3 eq 1) { $number_s = '00'.$number_s; }
elsif($num_length % 3 eq 2) { $number_s = '0'.$number_s; }
$num_length=length($number_s);
my @number=split(//,$number_s);

if(int($number_s) eq 0) {$phrase=$digits{$num_type}{$case}{$gender}[0]; return $phrase;}

for(my $i=$num_length/3; $i>0; $i--)
	{
	if($num_type =~ m/por/i)
		{
		if($i==5) {  $num_class_one=$trillion{sin}{im}; $num_class_from5=$trillion{pl}{rod}; $num_class_till4=$trillion{sin}{rod}; $num_class_gender="m";}
		if($i==4) {  $num_class_one=$billion{sin}{im}; $num_class_from5=$billion{pl}{rod}; $num_class_till4=$billion{sin}{rod}; $num_class_gender="m";}
		if($i==3) {  $num_class_one=$million{sin}{im}; $num_class_from5=$million{pl}{rod}; $num_class_till4=$million{sin}{rod}; $num_class_gender="m";}
		if($i==2) {  $num_class_one=$thousand{sin}{im}; $num_class_from5=$thousand{pl}{rod}; $num_class_till4=$thousand{sin}{rod}; $num_class_gender="f";}
		if($i==1) {  $num_class_one=""; $num_class_from5=""; $num_class_till4=""; $num_class_gender=$gender;}
		}
	elsif($case =~ m/(im)|(vin)/i)
		{
		if($i==5) {  $num_class_one=$trillion{sin}{$case}; $num_class_from5=$trillion{pl}{rod}; $num_class_till4=$trillion{sin}{rod}; $num_class_gender="m";}
		if($i==4) {  $num_class_one=$billion{sin}{$case}; $num_class_from5=$billion{pl}{rod}; $num_class_till4=$billion{sin}{rod}; $num_class_gender="m";}
		if($i==3) {  $num_class_one=$million{sin}{$case}; $num_class_from5=$million{pl}{rod}; $num_class_till4=$million{sin}{rod}; $num_class_gender="m";}
		if($i==2) {  $num_class_one=$thousand{sin}{$case}; $num_class_from5=$thousand{pl}{rod}; $num_class_till4=$thousand{sin}{rod}; $num_class_gender="f";}
		if($i==1) {  $num_class_one=""; $num_class_from5=""; $num_class_till4=""; $num_class_gender=$gender;}
		}
	else
		{
		if($i==5) {  $num_class_one=$trillion{sin}{$case}; $num_class_from5=$trillion{pl}{$case}; $num_class_till4=$trillion{pl}{$case}; $num_class_gender="m";}
		if($i==4) {  $num_class_one=$billion{sin}{$case}; $num_class_from5=$billion{pl}{$case}; $num_class_till4=$billion{pl}{$case}; $num_class_gender="m";}
		if($i==3) {  $num_class_one=$million{sin}{$case}; $num_class_from5=$million{pl}{$case}; $num_class_till4=$million{pl}{$case}; $num_class_gender="m";}
		if($i==2) {  $num_class_one=$thousand{sin}{$case}; $num_class_from5=$thousand{pl}{$case}; $num_class_till4=$thousand{pl}{$case}; $num_class_gender="f";}
		if($i==1) {  $num_class_one=""; $num_class_from5=""; $num_class_till4=""; $num_class_gender=$gender;}
		}

	$hundred_s = substr($number_s,$num_length-$i*3,3); #from which number and how much
	@hundred=split(//,$hundred_s);
	$rest_number_s = substr($number_s,$num_length-$i*3+3); #from which number and till the end of a string
	@rest_number=split(//,$rest_number_s);
	
	if($num_type eq "kol") # cardinal
		{
		
		if(int($hundred_s) ne 0)
			{
			$phrase = $phrase.hundred2word($hundred_s,$num_type,$case,$num_class_gender);
			if($hundred[1] eq 1) {$phrase .= ' '.$num_class_from5.' ';}
			elsif($hundred[2] eq 1) {$phrase .= ' '.$num_class_one.' ';}
			elsif(($hundred[2] >= 5)||($hundred[2] eq 0)) {$phrase .= ' '.$num_class_from5.' ';}
			else {$phrase .= ' '.$num_class_till4.' ';};
			}
		}
	else # serial (ordinal) number
		{
		
		if(int($hundred_s) ne 0)
			{
			if($i==1)
				{
				$phrase = $phrase.hundred2word($hundred_s,$num_type,$case,$num_class_gender);
				}
			elsif(int($rest_number_s) ne 0) # Probably all of the numbers should be connected (information not found)
				{
				$phrase = $phrase.hundred2word($hundred_s,'kol','im',$num_class_gender);
				if($hundred[1] eq 1) {$phrase .= ' '.$num_class_from5.' ';}
				elsif($hundred[2] eq 1) {$phrase .= ' '.$num_class_one.' ';}
				elsif(($hundred[2] >= 5)||($hundred[2] eq 0)) {$phrase .= ' '.$num_class_from5.' ';}
				else {$phrase .= ' '.$num_class_till4.' ';};
				}
			elsif(int($rest_number_s) eq 0)
				{
				if($hundred[0] eq 1) {$phrase = $phrase."���";}
				else { $phrase = $phrase.$hundreds{kol}{rod}{m}[$hundred[0]]; }
				if($hundred_s =~ m/.1./) {$phrase = $phrase.$ten_19{kol}{rod}{m}[$hundred[2]];}
				else
					{
					$phrase = $phrase.$tens{kol}{rod}{m}[$hundred[1]];
					if($hundred[2] eq 1){ $phrase = $phrase."����";}
					elsif($hundred[2] > 1) { $phrase = $phrase.$digits{kol}{rod}{m}[$hundred[2]];}
					}
				if($i==5) { $phrase = $phrase.$one_trillion_adj{$case}{$gender};}
				elsif($i==4) { $phrase = $phrase.$one_billion_adj{$case}{$gender};}
				elsif($i==3) { $phrase = $phrase.$one_million_adj{$case}{$gender};}
				elsif($i==2) { $phrase = $phrase.$one_thousand_adj{$case}{$gender};}
				}
			}
			
		}
	}
	
$phrase =~ s/^\s+//;
$phrase =~ s/\s+$//;
$phrase =~ s/\s{2,}/\ /;

$number_s =~ s/^0+//;
if((length($number_s) == 4) && ($number_s =~ m/^1/)) # Delete "Odna" from "1256"
	{
	$phrase =~ s/^���[�-��]+\s+//; # only with space sign
	}

return $phrase;
}

sub hundred2word
{
my $number_s=$_[0];
my $num_type=$_[1];
my $case=$_[2];
my $gender=$_[3];
my $phrase='';

if(length($number_s) eq 1) { $number_s = '00'.$number_s; }
elsif(length($number_s) eq 2) { $number_s = '0'.$number_s; }
elsif(length($number_s) > 3) { die "Error!!! The number of digits passed into the function \"hundred2word\" is more than 3!\n"; }

my @number=split(//,$number_s);

if($num_type eq "kol")
	{
	
	if(int($number_s) eq 0) { $phrase=$phrase.$digits{$num_type}{$case}{$gender}[0];}
	else
		{
		$phrase=$phrase.$hundreds{$num_type}{$case}{$gender}[$number[0]]." ";
		if($number_s =~ m/.00/) { $phrase =~ s/\s$//;}
		elsif($number[1] eq 1) { $phrase = $phrase.$ten_19{$num_type}{$case}{$gender}[$number[2]];}
		elsif($number[2] eq 0) { $phrase = $phrase.$tens{$num_type}{$case}{$gender}[$number[1]]; }
		else
			{
			if($number[1] ne 0) { $phrase = $phrase.$tens{$num_type}{$case}{$gender}[$number[1]]." "; }
			$phrase=$phrase.$digits{$num_type}{$case}{$gender}[$number[2]];
			}
		}
	
	}
elsif($num_type eq "por")
	{	
	if(int($number_s) eq 0) { $phrase=$phrase.$digits{$num_type}{$case}{$gender}[0];}
	else
		{
		if($number_s =~ m/.00/) { $phrase = $phrase.$hundreds{$num_type}{$case}{$gender}[$number[0]]." ";}
		else {$phrase=$phrase.$hundreds{kol}{im}{m}[$number[0]]." ";}
		
		if($number_s !~ m/.00/)
			{
			if($number[1] eq 1) { $phrase = $phrase.$ten_19{$num_type}{$case}{$gender}[$number[2]];}
			elsif($number[2] eq 0) { $phrase = $phrase.$tens{$num_type}{$case}{$gender}[$number[1]]; }
			else
				{
				if($number[1] ne 0) { $phrase = $phrase.$tens{kol}{im}{m}[$number[1]]." "; }
				$phrase=$phrase.$digits{$num_type}{$case}{$gender}[$number[2]];
				}
			}
		}

	}
else
	{ die "Error!!! The number type passed into the function \"hundred2word\" is not \"kol\" or \"por\"! ".$num_type."\n"; };
	
return $phrase;
	
}

sub get_morph_props
{
# Usage: get_morph_props($string);
#
# Description: takes the string and returns the array of hashes of morphological properties
#
# Return: array of hashes @morph_props

my $string=$_[0];
my $parameters=$_[1];
my $output_s='';

$string =~ s/\.$//;

my @morph_props;

for(my $i=0; $i<$#all_morph_lines+1; $i++)
	{
	if($all_morph_lines[$i] =~ m/^$string\{/i) { $output_s = $all_morph_lines[$i]; $i = $#all_morph_lines+1; }
	}

# start mystem again
if($output_s eq '')
	{
	# Save word into file
	open(write_file, '>encoding(CP1251)', $word_temp_file) or die "Error!!! Can not open file for writing: ".$word_temp_file; 
	print write_file $string;
	close(write_file);

	# Execute the system function Mystem and returns its output
	system($mystem_path.' '.$parameters.' -e cp1251 '.$word_temp_file.' '.$morph_temp_file);

	# Read morphological analysis from a file
	open(read_file, '<:encoding(CP1251)', $morph_temp_file) or die "Error!!! Can not open file for reading: ".$morph_temp_file;
	$output_s = <read_file>;
	close(read_file);
	}
	
$output_s =~ s/^.*?\{//;
$output_s =~ s/\}.*?$//;

if(($output_s =~ m/\?/)&&($parameters =~ m/w/i)) { return @morph_props; } # if the word should be from vocabulary send empty array of hashes

my @output = split(/\|/,$output_s);

# print $output[0]."\n".$output[1]."\n";

# my $counter=0;
for(my $i=0; $i<$#output+1; $i++)
	{
	$output[$i] =~ s/^[�-��]*\??\=//i;
	push @morph_props, { pos => "", tense => "", case => "", number => "", repr => "", adj_form => "", comp => "", pers => "", gender => "", aspect => "", voice => "", anim => "", trans => "", misc => "" };
	while($output[$i] =~ m/[a-z�-��]+/ig)
		{
		my $matching=$&;
		
			 ### ����� ����															### ����� ��������								 		### �����														### ������� ���������		### �����								### ������ �����������
			 # A ��������������														# ����	���������											# ��	������������						     				# ����	������������       		# ������	��������������    	# �����	������� �����
			 # ADV	�������                                                       		# ������	�����������                        		# ��	�������������                          				# ����	�������������         	# �����	�������������          	# ���	�������������� ��������
			 # ADVPRO	������������ �������                          	# ����	���������                                  	                                                                                                                        									        	# ����	����������� ����� ����������
			 # ANUM	���������� ������������                                                                                          		###	������������� � ���������� �������		### ���� �������                ### ��������������           	# ���	��� �����������
			 # APRO	������������ ��������������						### �����	                         	          	    	# �����	������������                       				# 1-�	1-� ����                   # ��	������������              		# �����	���������� �����
			 # COM	����� ��������� (������ ����� ������� ����) 	# ��	������������								 		# ���	���������                           					# 2-�	2-� ����                   # ����	��������������       	# ��	����� ����� �������� � �������� ����
			 # CONJ	����                                                          	# ���	�����������                              		# ����	���������                           					# 3-�	3-� ����                                                               	# ����	��������� �������
			 # INTJ	����������                                                     	# ���	���������                                 		# �����	������������� ����������               									   			### ������������	           		# ���	��������
			 # NUM	������������                                                    # ���	�����������                          	 		# ���	������������� ����������	  				### ���                             	# ��	���������� ������       	# ����	����������
			 # PART	�������                                                        #  ����	������������                               	  		                                                         		# ���	�������                   # ��	������������ ������    	# ����	����������� �����
			 # PR	�������                                                            # ��	����������									  		### ����� ��������������                      		# ���	�������                                                                	# ����	����� ������������� �����
			 # S	���������������                                                   # ����	�������� (������ �����������)     	# ��	�������                                    					# ����	�������                                                                	# ����	����������
			 # SPRO	�����������                                                 # �����	������� (������ ����������)   	# ����	������                                                                                                                                             	# �����	���������� �����
			 # V	������                                                                 	# ����	����������                                 		# ������	��������������                                ### ��� (������) �������                                                                                                                                                                                                                                                                               ### ��� (������) �������                                                 # ���	�������
																																																											# ���	�����������
		                                                                                                                                                                                                                                    # �����	�������������

		for($matching)
			{
			when (m/^(A|ADV|ADVPRO|ANUM|APRO|COM|CONJ|INTJ|NUM|PART|PR|S|SPRO|V)$/) 	{ $morph_props[$i]{pos} = $matching;}
			when (m/^(����|������|����)$/)												{ $morph_props[$i]{tense} = $matching; }
			when (m/^(��|���|���|���|����|��|����|�����|����)$/)						{ $morph_props[$i]{case} = $matching; }
			when (m/^(��|��)$/)															{ $morph_props[$i]{number} = $matching; }
			when (m/^(�����|���|����|�����|���)$/)										{ $morph_props[$i]{repr} = $matching; }
			when (m/^(��|����|������)$/)												{ $morph_props[$i]{adj_form} = $matching; }
			when (m/^(����|����)$/)														{ $morph_props[$i]{comp} = $matching; }
			when (m/^(1\-�|2\-�|3\-�)$/)												{ $morph_props[$i]{pers} = $matching; }
			when (m/^(���|���|����)$/)													{ $morph_props[$i]{gender} = $matching; }
			when (m/^(���|�����)$/)														{ $morph_props[$i]{aspect} = $matching; }
			when (m/^(������|�����)$/)													{ $morph_props[$i]{voice} = $matching; }
			when (m/^(��|����)$/)														{ $morph_props[$i]{anim} = $matching; }
			when (m/^(��|��)$/)															{ $morph_props[$i]{trans} = $matching; }
			when (m/^(�����|���|����|���|�����|��|����|���|����|����|����|����|�����|���)$/) { $morph_props[$i]{misc} = $matching;}
			}
		}
	
	# If the part of speech is undetermined, delete the whole this element
	if($morph_props[$i]{pos} !~ m/[A-Z]/) { pop(@morph_props); next; }
	
	# ----- Artificial changing of the data (comment this if unnecessary)
	
	if($morph_props[$i]{repr} eq '����') { $morph_props[$i]{pos} = 'A';}
	
	my $temp_case=$morph_props[$i]{case};
	for($temp_case)
		{
		when (m/^(��)$/)			{$morph_props[$i]{case}="im";}
		when (m/^(���)$/)			{$morph_props[$i]{case}="rod";}
		when (m/^(���)$/)			{$morph_props[$i]{case}="dat";}
		when (m/^(���)$/)			{$morph_props[$i]{case}="vin";}
		when (m/^(����)$/)			{$morph_props[$i]{case}="tv";}
		when (m/^(��)$/)			{$morph_props[$i]{case}="pr";}
		}
		
	my $temp_gender=$morph_props[$i]{gender};
	for($temp_gender)
		{
		when (m/^(���)$/)			{$morph_props[$i]{gender}="m";}
		when (m/^(���)$/)			{$morph_props[$i]{gender}="f";}
		when (m/^(����)$/)			{$morph_props[$i]{gender}="n";}
		}
	
	if($morph_props[$i]{misc} eq '��') { $morph_props[$i]{gender}="m"; }
	
	my $temp_sin_or_pl=$morph_props[$i]{number};
	for($temp_sin_or_pl)
		{
		when (m/^(��)$/)			{$morph_props[$i]{number}="sin";}
		when (m/^(��)$/)			{$morph_props[$i]{number}="pl";}
		}
		
	# ***** Artificial changing of the data (comment this if unnecessary)
	
	}
	
return @morph_props;
}

sub get_abbr_props
{
my $abbr=$_[0];
my $gender='';
my $kol_por='';

for($abbr)
	{
	when (m/^(���|�����[�-��]{0,3})\.?$/i)				{ $gender = 'f'; $kol_por='kol'; }
	when (m/^(���|�������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol'; }
	when (m/^(����|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol'; }
	when (m/^(����|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol'; }
	
	when (m/^(�|���[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol'; }
	when (m/^(���|�����[�-��]{0,3})\.?$/i)				{ $gender = 'f'; $kol_por='kol'; }
	when (m/^(���|������[�-��]{0,3})\.?$/i)				{ $gender = 'f'; $kol_por='kol';}
	when (m/^(��|�����������[�-��]{0,3})\.?$/i)			{ $gender = 'f'; $kol_por='kol';}
	when (m/^(����|������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	
	when (m/^(��|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(�|����[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���(�|�|�|�|��|�|��|���|��))\.?$/i)		{ $gender = 'f'; $kol_por='kol';}
	
	when (m/^(����[�-��]{0,3})\.?$/i)					{ $gender = 'f'; $kol_por='kol';}
	when (m/^(\$|����|������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(\�|����)\.?$/i)							{ $gender = 'm'; $kol_por='kol';}
	when (m/^(\%|����|�������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|�������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(�|���|����[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|����[�-��]{0,5})\.?$/i)				{ $gender = 'f'; $kol_por='kol';}
	when (m/^(����[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	
	when (m/^(�|����[�-��]{0,3})\.?$/i)					{ $gender = 'f'; $kol_por='kol';}
	when (m/^(�|�������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(�|��|�����[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|����������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(�|����[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|Watt|W|����[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|kW|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|MW|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(�|Volt|V|�����[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|kV|���������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(MV|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(�|A|�����[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|kA|���������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(M�|MA|��������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|mA|����������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|������[�-��]{0,3})\.?$/)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|�������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����|����������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|pa|��|������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|kpa|���|����������[�-��]{0,3})\.?$/i)	{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|mpa|���|����������[�-��]{0,3})\.?$/i)	{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|���������[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|����[�-��]{0,3})\.?$/i)				{ $gender = 'f'; $kol_por='kol';}
	
	when (m/^(��|hz|����[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|khz|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|mhz|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|ghz|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|lm|�����[�-��]{0,3})\.?$/i)			{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|������[�-��]{0,3})\.?$/i)				{ $gender = 'f'; $kol_por='kol';}
	when (m/^(��|��|�����|tb|tbyte|���������[�-��]{0,3})\.?$/i)	{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|�����|gb|gbyte|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|��|�����|mb|mbyte|��������[�-��]{0,3})\.?$/i)	{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|�����|kb|kbyte|��������[�-��]{0,3})\.?$/i)		{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����|byte|����[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����|tbit|��������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����|gbit|�������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����|mbit|�������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(����|kbit|�������[�-��]{0,3})\.?$/i)				{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|bit|���[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|�����[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|���������[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|���������[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	when (m/^(��|�[�]�[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	when (m/^(���|�����[�-��]{0,3})\.?$/i)						{ $gender = 'f'; $kol_por='kol';}
	when (m/^(���|�����[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	
	# when (m/^(�|�����[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	# when (m/^(��|����[�-��]{0,3})\.?$/i)						{ $gender = 'f'; $kol_por='kol';}
	# when (m/^(���|�������[�-��]{0,3})\.?$/i)					{ $gender = 'f'; $kol_por='kol';}
	# when (m/^(�|���[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	# when (m/^(��|����[��][�-��]{0,3})\.?$/i)					{ $gender = 'f'; $kol_por='kol';}
	# when (m/^(��?|���[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	# when (m/^(��?|���[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	# when (m/^(���|�������[�-��]{0,3})\.?$/i)					{ $gender = 'm'; $kol_por='kol';}
	# when (m/^(��|����[�-��]{0,3})\.?$/i)						{ $gender = 'f'; $kol_por='kol';}
	# when (m/^(��|�������[�-��]{0,3})\.?$/i)					{ $gender = 'f'; $kol_por='kol';}
	# when (m/^(��|����[�-��]{0,3})\.?$/i)						{ $gender = 'm'; $kol_por='kol';}
	
	when (m/^(��������|�����|��������|���|���|�������|�����|���������|�����|���)[�-��]{0,3}\.?$/i) 	{ $gender = 'm'; $kol_por='por'; }
	when (m/^(����|����[��]|�������|�����|���������)[�-��]{0,3}\.?$/i)								{ $gender = 'f'; $kol_por='por';}
	when (m/^(����?�|������|���������)[�-��]{0,3}\.?$/i)											{ $gender = 'n'; $kol_por='por';}
	
	when (m/^(�)\.?$/i)						{ $gender = 'm'; $kol_por='por';}
	when (m/^(��)\.?$/i)					{ $gender = 'f'; $kol_por='por';}
	when (m/^(���)\.?$/i)					{ $gender = 'f'; $kol_por='por';}
	when (m/^(\�)$/)						{ $gender = 'm'; $kol_por='por';}
	when (m/^(��)\.?$/i)					{ $gender = 'f'; $kol_por='por';}
	when (m/^(��?)\.?$/i)					{ $gender = 'm'; $kol_por='por';}
	when (m/^(��?)\.?$/i)					{ $gender = 'm'; $kol_por='por';}
	}

return ($gender, $kol_por);
}

sub preliminary_case
{
# Usage: preliminary_case($string);
#
# Description: takes the string and returns the most probable case (the end of the string is important and being compared)
#
# Return: case

my $string=$_[0];
my $case='';

my $special_flag=0; # for example, k primerno 20 slonikam
if($string =~ s/(��������������|��������)\s*$//i) { $special_flag = 1; };

if($string =~ m/\b(���|���|��|��|��-��|��-���|��|�|�����|���\s+������|���\s+�������������|����|�\s+�����|�\s+����|�\s+����|�\s+��������|�\s+����|�\s+����|�\s+�����������\s+��|�\s+����|�\s+���������|�\s+��������|�\s+��������|�\s+�����������|�\s+�������|�\s+�������\s+��|�\s+���������|�\s+������|�\s+�������|�\s+��������|�\s+�����������|�\s+������|�\s+������|�\s+����������|�\s+����|�\s+�����|�\s+����|�\s+������|�\s+������|�\s+�������������|�\s+�������\s+��|�\s+�������|�\s+�������\s+��|�\s+�����|�\s+��[�]�|�\s+�������|�\s+��������|�\s+�����|�\s+����|�\s+�����|�\s+�����|�\s+�����|������|������\s+��|������|�������\s+��|�����\s+��|�����|������|������|���|���\s+������|���\s+��������|���\s+�����|���\s+�����|��\s+�����|��\s+���|�����|������|�������|������\s+��|������\s+��|�����|����������|��\s+�����������|��\s+��[�]�|������\s+��|����������|�����|����|����|��\s+���������|��\s+�������|��\s+����|��\s+�����|�������|��������|���������|��������|����[�]�|�������\s+�|��\s+������|��\s+������\s+��|���������\s+��|����������\s+��|�����|�����|������������|��\s+������|��\s+�����|��\s+����|��\s+�������|��\s+������|��\s+�������|��\s+������|��\s+�����|������|���\s+�����|���\s+������|���\s+���������|�����|���\s+�������|���\s+���������|������|������|�����[�]�|�������|�����|�������|���������|�����������|������|���\s+������|���\s+����������|���\s+�������|������|���[�]�|����|�\s+�������|�\s+�����\s+������|�\s+�����|�����|�����|�����|�����|��\s+�������|�����|����|�����|�����\s+����������)(\s+(�?����|�?���|����|���))?\s*$/i) { $case = 'rod'; } # ����� 2 �����?, ���?, �, ����������: �����,
# ������|������|�����|�����|���������� - should be added where necessary (not included at the web-site)
# ? ����� - not ���� �����
elsif($string =~ m/\b(�|��|���������|�\s+��������|�\s+����������|�\s+�����������������|�������|�����|�����\s+��|��\s+����\s+�|���������|���������|��\s+�\s+������|��\s+�����������\s+�|��\s+���������\s+�|�������|�������������\s+�|������\s+��|��������|���������|��������������|����������|����\s+��|�?����)(\s+(�?����|�?���|����|���))?\s*$/i) { $case = 'dat'; } # ��, ����������: �,
# ����.{1,3}|�����|�����.{3,4} - should be added where necessary (not included at the web-site)
elsif($string =~ m/\b(���|�����|�\s+�����\s+��|�������|�����\s+��|��������|��������\s+��|��������\s+��|������|������|������|������|�?��)(\s+(�?���|�?����|�?��|�?��|���|��))?\s*$/i) { $case = 'vin'; } # �, ��, ��, �, ��, ��, ���
elsif($string =~ m/\b(���|�����|�\s+�����\s+�|�\s+������������\s+�|�\s+��������\s+�|�\s+�����������\s+�|�\s+����������\s+�|�\s+���������\s+�|�\s+������\s+�|�\s+�������\s+�|������\s+�|��\s+�����\s+�|�������\s+�|�����\s+��|������|�������\s+�|������\s+�|�������|��\s+���������\s+�|�����\s+�|������\s+��|���������\s+�|��������\s+�|���������\s+�|��������������\s+�|����������\s+�|������������\s+�|�����|����)(\s+(�?���|�?���|�����|����))?\s*$/i) { $case = 'tv'; } # �, ��, ���, �����, ���, ����������: �����
elsif($string =~ m/\b(���|��\s+������\s+�|����)(\s+(�?���|�?���|����|���))?\s*$/i) { $case = 'pr'; } # �, ��, �, ��
else { $case = 'im'; }

# second check
if($case eq 'im')
{
if($string =~ m/\b(�|���|�����) (�?����|�?���|����|���)\s*$/i) { $case = 'rod'; }
elsif($string =~ m/\b(��) (�?����|�?���|����|���)\s*$/i) { $case = 'dat'; }
elsif($string =~ m/\b(�|��|��|��?|��|���) (�?���|�?����|�?��|�?��|���|��)\s*$/i) { $case = 'vin'; }
elsif($string =~ m/\b(�|��|���|�����|���) (�?���|�?���|�����|����)\s*$/i) { $case = 'tv'; }
elsif($string =~ m/\b(�|��|��?|��) (�?���|�?���|����|���)\s*$/i) { $case = 'pr'; }
}

# third check

if(($case eq 'im')&&($special_flag == 1))
{
if($string =~ m/\b(�|�����|�����|�����|������|������|����������|��������[���])\s*$/i) { $case = 'rod'; }
elsif($string =~ m/\b(�|��|�����|����[���]|�����[��]?��?��)\s*$/i) {	$case = 'dat'; }
elsif($string =~ m/\b(�����|���)\s$/i) { $case = 'tv'; }
elsif($string =~ m/\b(�|��|�)\s*$/i) { $case = 'pr';	}	
elsif($string =~ m/\b(��|��|��|���)\s*$/i) { $case = 'vin'; }
}

# fourth check

if($case eq 'im')
	{
	if($string =~ m/\b(�������|������(��|��|��)|�������[��]��|�������[��]��?|������(���|�)(��|��|��|��)|����?�(|�|�|��|�))\s*$/i) { $case = 'rod'; }
	elsif($string =~ m/\b(�����|����[���]|�����[��]?��?��|������(��|�[���]��)|����(��|��|��|��)|������������|�����������[���]|�����������(��|��|��|��)|����������(�[��]�|��[���]?)|�������������(��|��|��|��))\s*$/i) { $case = 'dat'; }
	}

# could be multiple cases: �, ���, �����, ��, �, ��, ��, �, ��, ���

return $case;
}

sub isAbbreviation
{
# Usage: isAbbreviation($string, 'start_of_line/end_of_line');
#
# Description: takes the string and returns "1", if this string starts or ends with an abbreviaion; otherwise 0 is returned
#
# Return: array of hashes @morph_props

my $string=$_[0];
my $position=$_[1];

# $string = '�\/�';

if($position eq 'end_of_line')
	{
	if($string =~ m/(\b|\d)($abbr_letters_merged)$/i) { return 1; } else { return 0; };
	}
elsif($position eq 'start_of_line')
	{
	if($string =~ m/^\s*($abbr_letters_merged)\b/i) { return 1; } else { return 0; };
	}
else
	{
	die "Error!!! The parameter of the function \"isAbbreviation\" (".$position.") is not equal to start_of_line\/end_of_line\n";
	}
}

sub theTopicIs
{
my $string=$_[0];
my $topic='';

if($string =~ m/\b(������|���������|���[�]�)[�-��]{0,3}\b/i) { $topic .= 'geo-location_'};
if($string =~ m/\b(��������|���|���������|������������|��������)[�-��]{0,3}\b/i) { $topic .= 'electricity_'};

return $topic;
}
